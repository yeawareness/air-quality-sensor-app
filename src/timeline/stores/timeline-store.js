/**
 * Copyright (c) 2017-present, Virida YEAwareness, Org. All rights reserved.
 *
 *------------------------------------------------------------------------
 *
 * @module TimelineStore
 * @description - Virida client-native app timeline store.
 *
 * @author Tuan Le (tuan.t.lei@gmail.com)
 *
 *------------------------------------------------------------------------
 * @flow
 */
'use strict'; // eslint-disable-line

import { Hf } from 'hyperflow';

import EVENT from '../events/timeline-event';

const TimelineStore = Hf.Store.augment({
    state: {
        status: {
            value: {
                active: false,
                idle: false
            }
        }
    },
    setup: function setup (done) {
        const store = this;
        store.incoming(EVENT.DO.TIMELINE_RESET).handle(() => {
            store.reconfig({
                status: {
                    active: false,
                    idle: false
                }
            }, {
                suppressMutationEvent: true
            });
        });
        store.incoming(EVENT.DO.MUTATE_TIMELINE_STATUS).handle((status) => {
            if (store.reduce({
                status
            }, {
                suppressMutationEvent: true
            })) {
                if (store.status.active && !store.status.idle) {
                    store.outgoing(EVENT.AS.TIMELINE_ACTIVATED).emit();
                } else {
                    store.outgoing(EVENT.AS.TIMELINE_DEACTIVATED).emit();
                }
            }
        });
        done();
    },
    teardown: function teardown (done) {
        done();
    }
});
export default TimelineStore;
