/**
 * Copyright (c) 2017-present, Virida YEAwareness, Org. All rights reserved.
 *
 *------------------------------------------------------------------------
 *
 * @description - Virida client native app timeline event ids.
 *
 * @author Tuan Le (tuan.t.lei@gmail.com)
 *
 *------------------------------------------------------------------------
 * @flow
 */
'use strict'; // eslint-disable-line

import { Hf } from 'hyperflow';

export default Hf.Event.create({
    onEvents: [
    ],
    asEvents: [
        `timeline-activated`,
        `timeline-deactivated`
    ],
    doEvents: [
        `timeline-reset`,
        `timeline-activation`,
        `mutate-timeline-status`
    ],
    broadcastEvents: [
        `timeline-alert`,
        `run-mode`,
        `background-timer-refreshing`
    ],
    requestEvents: [
    ]
});
