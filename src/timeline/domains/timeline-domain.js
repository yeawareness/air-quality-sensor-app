/**
 * Copyright (c) 2017-present, Virida YEAwareness, Org. All rights reserved.
 *
 *------------------------------------------------------------------------
 *
 * @module TimelineDomain
 * @description - Virida client-native app timeline domain.
 *
 * @author Tuan Le (tuan.t.lei@gmail.com)
 *
 *------------------------------------------------------------------------
 * @flow
 */
'use strict'; // eslint-disable-line

import { Hf } from 'hyperflow';

import TimelineStore from '../stores/timeline-store';

import TimelineViewInterface from '../interfaces/timeline-view-interface';

import EVENT from '../events/timeline-event';

const TimelineDomain = Hf.Domain.augment({
    $init: function $init () {
        const domain = this;
        domain.register({
            store: TimelineStore({
                name: `timeline-store`
            }),
            intf: TimelineViewInterface({
                name: `timeline-view`
            })
        });
    },
    setup: function setup (done) {
        const domain = this;
        domain.incoming(EVENT.DO.TIMELINE_RESET).repeat();
        domain.incoming(EVENT.BROADCAST.RUN_MODE).handle((runMode) => {
            return {
                idle: runMode === `background-running`
            };
        }).relay(EVENT.DO.MUTATE_TIMELINE_STATUS);
        domain.incoming(EVENT.DO.TIMELINE_ACTIVATION).handle((active) => {
            return {
                active
            };
        }).relay(EVENT.DO.MUTATE_TIMELINE_STATUS);
        done();
    },
    teardown: function teardown (done) {
        done();
    }
});
export default TimelineDomain;
