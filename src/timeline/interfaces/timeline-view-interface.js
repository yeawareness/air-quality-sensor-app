/**
 * Copyright (c) 2017-present, Virida YEAwareness, Org. All rights reserved.
 *
 *------------------------------------------------------------------------
 *
 * @module TimelineViewInterface
 * @description - Virida client-native app timeline view interface.
 *
 * @author Tuan Le (tuan.t.lei@gmail.com)
 *
 *------------------------------------------------------------------------
 * @flow
 */
'use strict'; // eslint-disable-line

import { Hf } from 'hyperflow';

import { Ht } from 'hypertoxin';

import moment from 'moment';

import React from 'react';

import ReactNative from 'react-native'; // eslint-disable-line

import { Agenda } from 'react-native-calendars';

import CONSTANT from '../../common/constant';

const {
    View
} = ReactNative;

const {
    BodyView
} = Ht.View;

const TimelineViewInterface = Hf.Interface.augment({
    composites: [
        Hf.React.ComponentComposite
    ],
    setup: function setup (done) {
        done();
    },
    teardown: function teardown (done) {
        done();
    },
    render: function render () {
        return (
            <BodyView style = {{
                flexShrink: 1,
                height: CONSTANT.GENERAL.DEVICE_HEIGHT - 48
            }}>
                <Agenda
                    // agenda container style
                    style = {{
                        width: CONSTANT.GENERAL.DEVICE_WIDTH,
                        backgroundColor: `transparent`
                    }}
                    // the list of items that have to be displayed in agenda. If you want to render item as empty date
                    // the value of date key kas to be an empty array []. If there exists no value for date key it is
                    // considered that the date in question is not yet loaded
                    items = {{
                        '2017-11-01': [{
                            text: `item 1 - any js object`
                        }],
                        '2017-10-23': [{
                            text: `item 2 - any js object`
                        }],
                        '2017-09-24': [],
                        '2017-10-25': [{
                            text: `item 3 - any js object`
                        }, {
                            text: `any js object`
                        }]
                    }}
                    // callback that gets called when items for a certain month should be loaded (month became visible)
                    loadItemsForMonth = {(month) => {
                        console.log(month);
                    }}
                    // callback that gets called on day press
                    onDayPress = {(day) => {
                        console.log(day);
                    }}
                    // callback that gets called when day changes while scrolling agenda list
                    onDayChange = {(day) => {
                        console.log(day);
                    }}
                    // initially selected day
                    selected = { moment().format(`YYYY-MM-DD`) }
                    // Minimum date that can be selected, dates before minDate will be grayed out. Default = undefined
                    minDate = { moment().subtract(50, `months`).format(`YYYY-MM-DD`) }
                    // Maximum date that can be selected, dates after maxDate will be grayed out. Default = undefined
                    maxDate = { moment().format(`YYYY-MM-DD`) }
                    // Max amount of months allowed to scroll to the past. Default = 50
                    pastScrollRange = { 50 }
                    // Max amount of months allowed to scroll to the future. Default = 50
                    futureScrollRange = { 1 }
                    // specify how each item should be rendered in agenda
                    renderItem = {(item, firstItemInDay) => {
                        return (
                            <View/>
                        );
                    }}
                    // specify how each date should be rendered. day can be undefined if the item is not first in that day.
                    renderDay = {(day, item) => {
                        return (
                            <View/>
                        );
                    }}
                    // specify how empty date content with no items should be rendered
                    renderEmptyDate = {() => {
                        return (
                            <View/>
                        );
                    }}
                    // specify your item comparison function for increased performance
                    rowHasChanged = {(r1, r2) => r1.text !== r2.text}
                    // By default, agenda dates are marked if they have at least one item, but you can override this if needed
                    // markedDates = {{
                    //     '2017-11-01': {
                    //         selected: true,
                    //         marked: true
                    //     },
                    //     '2017-10-17': [{
                    //         endingDay: true,
                    //         color: `green`,
                    //         textColor: `red`
                    //     }],
                    //     '2017-10-18': {
                    //         disabled: true
                    //     }
                    // }}
                    // markingType = 'interactive'
                    // agenda theme
                    theme = {{
                        agendaDayTextColor: Ht.Theme.palette.yellow,
                        agendaDayNumColor: Ht.Theme.palette.green,
                        agendaTodayColor: Ht.Theme.palette.red,
                        agendaKnobColor: Ht.Theme.palette.blue
                    }}
                />
            </BodyView>
        );
    }
});
export default TimelineViewInterface;
