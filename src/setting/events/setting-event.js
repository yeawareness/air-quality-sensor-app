/**
 * Copyright (c) 2017-present, Virida YEAwareness, Org. All rights reserved.
 *
 *------------------------------------------------------------------------
 *
 * @description - Virida client native app setting event ids.
 *
 * @author Tuan Le (tuan.t.lei@gmail.com)
 *
 *------------------------------------------------------------------------
 * @flow
 */
'use strict'; // eslint-disable-line

import { Hf } from 'hyperflow';

export default Hf.Event.create({
    onEvents: [
        `aqs-rediscovery`,
        `toggle-unhealthy-aq-alert-notification`,
        `toggle-daily-aq-alert-notification`,
        `toggle-show-intro`,
        `apply-aqs-info-changes`,
        `cancel-aqs-info-changes`,
        `apply-aqs-setting-changes`,
        `cancel-aqs-setting-changes`,
        `change-aqs-alias`,
        `change-aqs-led-brightness-level`,
        `change-aqs-sample-frequency`,
        `change-aqs-sample-count`
    ],
    asEvents: [
        `setting-activated`,
        `setting-deactivated`,
        `notification-mutated`,
        `show-intro-mutated`,
        `aqs-info-mutated`,
        `aqs-general-setting-mutated`,
        `aqs-calibration-setting-mutated`
    ],
    doEvents: [
        `setting-reset`,
        `setting-activation`,
        `rediscover-aqs`,
        `restore-aqs-info`,
        `restore-aqs-general-setting`,
        `restore-aqs-calibration-setting`,
        `mutate-setting-status`,
        `mutate-notification`,
        `mutate-show-intro`,
        `mutate-aqs-info`,
        `mutate-aqs-general-setting`,
        `mutate-aqs-calibration-setting`
    ],
    broadcastEvents: [
        `setting-alert`,
        `run-mode`,
        `background-timer-refreshing`,
        `setting-written`,
        `aqs-info-written`,
        `aqs-general-setting-written`,
        `aqs-calibration-setting-written`
    ],
    requestEvents: [
        `read-setting`,
        `write-setting`,
        `read-aqs-info`,
        `write-aqs-info`,
        `read-aqs-general-setting`,
        `write-aqs-general-setting`,
        `read-aqs-calibration-setting`,
        `write-aqs-calibration-setting`
    ]
});
