/**
 * Copyright (c) 2017-present, Virida YEAwareness, Org. All rights reserved.
 *
 *------------------------------------------------------------------------
 *
 * @module AQSSettingViewInterface
 * @description - Virida client-native app aqs setting view interface.
 *
 * @author Tuan Le (tuan.t.lei@gmail.com)
 *
 *------------------------------------------------------------------------
 * @flow
 */
'use strict'; // eslint-disable-line

import { Hf } from 'hyperflow';

import { Ht } from 'hypertoxin';

import React from 'react';

import ReactNative from 'react-native'; // eslint-disable-line

import CONSTANT from '../../common/constant';

import EVENT from '../events/setting-event';

const {
    SegmentedControlIOS,
    Slider
} = ReactNative;

const {
    ScreenView,
    HeaderView,
    BodyView,
    LayoutView
} = Ht.View;

const {
    FlatButton,
    RaisedButton
} = Ht.Button;

const {
    SubtitleText,
    InfoText
} = Ht.Text;

const {
    IconImage
} = Ht.Image;

const AQSSettingViewInterface = Hf.Interface.augment({
    composites: [
        Hf.React.ComponentComposite,
        Ht.ViewComposite.HeaderViewSlideAndFadeAnimation
    ],
    state: {
        changed: {
            value: false
        },
        ledBrightnessLvl: {
            value: 20
        },
        aqSampleCount: {
            value: 4
        },
        mInterval: {
            value: 9
        }
    },
    setup: function setup (done) {
        done();
    },
    teardown: function teardown (done) {
        done();
    },
    onPressGoBackButton: function onPressGoBackButton () {
        const component = this;
        const {
            changed,
            navigation
        } = component.props;
        if (changed) {
            component.outgoing(EVENT.ON.CANCEL_AQS_SETTING_CHANGES).emit();
        }
        component.animateHeaderViewExit({
            duration: 300,
            onAnimationEnd: navigation.goBack
        });
    },
    onSelectAQSLEDBrightnessLevelSlider: function onSelectAQSLEDBrightnessLevelSlider (ledBrightnessLvl) {
        const component = this;
        component.outgoing(EVENT.ON.CHANGE_AQS_LED_BRIGHTNESS_LEVEL).emit(() => ledBrightnessLvl);
    },
    onSelectAQSSampleCountSegmentControl: function onSelectAQSSampleCountSegmentControl (index) {
        const component = this;
        component.outgoing(EVENT.ON.CHANGE_AQS_SAMPLE_COUNT).emit(() => {
            switch (index) {
            case `4`:
                return 4;
            case `8`:
                return 8;
            case `16`:
                return 16;
            default:
                return 4;
            }
        });
    },
    onSelectAQSSampleFreqSegmentControl: function onSelectAQSSampleFreqSegmentControl (index) {
        const component = this;
        const {
            aqSampleCount
        } = component.props;
        component.outgoing(EVENT.ON.CHANGE_AQS_SAMPLE_FREQUENCY).emit(() => {
            switch (index) {
            case `Minimal`:
                return aqSampleCount + 44;
            case `Moderate`:
                return aqSampleCount + 34;
            case `High`:
                return aqSampleCount + 24;
            default:
                return aqSampleCount + 34;
            }
        });
    },
    onPressApplyChangesButton: function onPressApplyChangesButton () {
        const component = this;
        // const [
        //     aqSampleCountSegmentedControl,
        //     sampleFreqSegmentedControl,
        //     ledBrightnessLevelSlider
        // ] = component.lookupComponentRefs(
        // `sample-count-segmented-control`,
        // `sample-freq-segmented-control`,
        // `led-brightness-level-slider`);
        const ledBrightnessLvl = 0;
        const aqSampleCount = 0;
        const mInterval = 0;

        component.outgoing(EVENT.ON.APPLY_AQS_SETTING_CHANGES).emit(() => {
            return {
                ledBrightnessLvl,
                aqSampleCount,
                mInterval
            };
        });
    },
    render: function render () {
        const component = this;
        const {
            changed,
            ledBrightnessLvl,
            aqSampleCount,
            mInterval
        } = component.props;
        return (
            <ScreenView style = {{
                backgroundColor: Ht.Theme.palette.silver
            }}>
                <HeaderView
                    cId = 'h0'
                    ref = { component.assignComponentRef(`animated-header`) }
                    label = 'Air Quality Sensor Settings'
                >
                    <FlatButton
                        room = 'action-left'
                        overlay = 'transparent'
                        onPress = { component.onPressGoBackButton }
                    >
                        <IconImage
                            room = 'content-center'
                            source = 'back'
                            size = 'large'
                        />
                    </FlatButton>
                </HeaderView>
                <BodyView>
                    <LayoutView
                        style = {{
                            ...Ht.Theme.general.dropShadow.extraShallow,
                            marginVertical: 6,
                            padding: 6
                        }}
                        overlay = 'opaque'
                        orientation = 'horizontal'
                        alignment = 'stretch'
                        selfAlignment = 'stretch'
                    >
                        <LayoutView
                            orientation = 'vertical'
                            alignment = 'stretch'
                            selfAlignment = 'stretch'
                        >
                            <SubtitleText> Air Quality Sample Count </SubtitleText>
                        </LayoutView>
                        <SegmentedControlIOS
                            ref = { component.assignComponentRef(`sample-count-segmented-control`) }
                            values = {[ `4`, `8`, `16` ]}
                            selectedIndex = {(() => {
                                switch (aqSampleCount.toString()) {
                                case `4`:
                                    return 0;
                                case `8`:
                                    return 1;
                                case `16`:
                                    return 2;
                                default:
                                    return 0;
                                }
                            })()}
                            tintColor = { Ht.Theme.general.color.light.primary }
                            onValueChange = { component.onSelectAQSSampleCountSegmentControl }
                        />
                        <InfoText
                            alignment = 'left'
                            indentation = { 3 }
                        > Number of sensor samplings per sample average </InfoText>
                    </LayoutView>
                    <LayoutView
                        style = {{
                            ...Ht.Theme.general.dropShadow.extraShallow,
                            marginVertical: 6,
                            padding: 6
                        }}
                        overlay = 'opaque'
                        orientation = 'horizontal'
                        alignment = 'stretch'
                        selfAlignment = 'stretch'
                    >
                        <LayoutView
                            orientation = 'vertical'
                            alignment = 'stretch'
                            selfAlignment = 'stretch'
                        >
                            <SubtitleText> Air Quality Sampling Frequency </SubtitleText>
                        </LayoutView>
                        <SegmentedControlIOS
                            ref = { component.assignComponentRef(`sample-freq-segmented-control`) }
                            values = {[ `Minimal`, `Moderate`, `High` ]}
                            selectedIndex = {(() => {
                                switch ((mInterval - aqSampleCount).toString()) {
                                case `44`:
                                    return 0;
                                case `34`:
                                    return 1;
                                case `24`:
                                    return 2;
                                default:
                                    return 0;
                                }
                            })()}
                            tintColor = { Ht.Theme.general.color.light.primary }
                            onValueChange = { component.onSelectAQSSampleFreqSegmentControl }
                        />
                        <InfoText
                            alignment = 'left'
                            indentation = { 3 }
                        > Frequency of sensor samplings per minute </InfoText>
                    </LayoutView>
                    <LayoutView
                        style = {{
                            ...Ht.Theme.general.dropShadow.extraShallow,
                            marginVertical: 6,
                            padding: 6
                        }}
                        overlay = 'opaque'
                        orientation = 'horizontal'
                        alignment = 'stretch'
                        selfAlignment = 'stretch'
                    >
                        <LayoutView
                            orientation = 'vertical'
                            alignment = 'stretch'
                            selfAlignment = 'stretch'
                        >
                            <SubtitleText> LED Brightness Level </SubtitleText>
                        </LayoutView>
                        <LayoutView
                            orientation = 'vertical'
                            alignment = 'center'
                        >
                            <SubtitleText size = 'small' > Off </SubtitleText>
                            <Slider
                                ref = { component.assignComponentRef(`led-brightness-level-slider`) }
                                style = {{
                                    width: CONSTANT.GENERAL.DEVICE_WIDTH - 85
                                }}
                                minimumTrackTintColor = { Ht.Theme.general.color.light.primary }
                                minimumValue = { 0 }
                                maximumValue = { 50 }
                                value = { ledBrightnessLvl }
                                step = { 10 }
                                onSlidingComplete = { component.onSelectAQSLEDBrightnessLevelSlider }
                            />
                            <IconImage
                                source = 'brightness'
                                size = 'small'
                            />
                        </LayoutView>
                        <InfoText
                            alignment = 'left'
                            indentation = { 3 }
                        > Adjusting air quality sesnor LED brightness level </InfoText>
                    </LayoutView>
                    <LayoutView
                        style = {{
                            ...Ht.Theme.general.dropShadow.extraShallow,
                            marginVertical: 6,
                            padding: 6
                        }}
                        overlay = 'opaque'
                        orientation = 'horizontal'
                        alignment = 'stretch'
                        selfAlignment = 'stretch'
                    >
                        <RaisedButton
                            label = 'APPLY CHANGES'
                            disabled = { !changed }
                            onPress = { component.onPressApplyChangesButton }
                        />
                    </LayoutView>
                </BodyView>
            </ScreenView>
        );
    }
});
export default AQSSettingViewInterface;
