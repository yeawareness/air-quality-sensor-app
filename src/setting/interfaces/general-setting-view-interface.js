/**
 * Copyright (c) 2017-present, Virida YEAwareness, Org. All rights reserved.
 *
 *------------------------------------------------------------------------
 *
 * @module GeneralSettingViewInterface
 * @description - Virida client-native app general setting view interface.
 *
 * @author Tuan Le (tuan.t.lei@gmail.com)
 *
 *------------------------------------------------------------------------
 * @flow
 */
'use strict'; // eslint-disable-line

import { Hf } from 'hyperflow';

import { Ht } from 'hypertoxin';

import React from 'react';

import ReactNative from 'react-native'; // eslint-disable-line

import Switch from '../../common/components/switch-component';

import EVENT from '../events/setting-event';

// const {
//     SegmentedControlIOS
// } = ReactNative;

const {
    ScreenView,
    HeaderView,
    BodyView
    // LayoutView
} = Ht.View;

const {
    FlatButton
} = Ht.Button;

// const {
//     SubtitleText,
//     InfoText
// } = Ht.Text;

const {
    IconImage
} = Ht.Image;

const GeneralSettingViewInterface = Hf.Interface.augment({
    composites: [
        Hf.React.ComponentComposite,
        Ht.ViewComposite.HeaderViewSlideAndFadeAnimation
    ],
    state: {
        showIntro: {
            value: true
        },
        mapTheme: {
            value: `flat`,
            oneOf: [ `lite`, `flat`, `retro` ]
        },
        notification: {
            value: {
                unhealthyAQAlert: true,
                dailyAQAlert: true
            }
        }
    },
    setup: function setup (done) {
        done();
    },
    teardown: function teardown (done) {
        done();
    },
    onPressGoBackButton: function onPressGoBackButton () {
        const component = this;
        const {
            navigation,
            showIntro,
            // mapTheme,
            notification
        } = component.props;
        const [
            dailyAQAlertSwitch,
            unhealthyAQAlertSwitch,
            showIntroSwitch
        ] = component.lookupComponentRefs(
            `daily-air-quality-alert-switch`,
            `unhealthy-air-quality-alert-switch`,
            `show-intro-switch`
        );
        if (notification.dailyAQAlert !== dailyAQAlertSwitch.value() && dailyAQAlertSwitch !== null) {
            component.outgoing(EVENT.ON.TOGGLE_DAILY_AQ_ALERT_NOTIFICATION).emit();
        }
        if (notification.unhealthyAQAlert !== unhealthyAQAlertSwitch.value() && unhealthyAQAlertSwitch !== null) {
            component.outgoing(EVENT.ON.TOGGLE_UNHEALTHY_AQ_ALERT_NOTIFICATION).emit();
        }
        if (showIntro !== showIntroSwitch.value() && showIntroSwitch !== null) {
            component.outgoing(EVENT.ON.TOGGLE_SHOW_INTRO).emit();
        }
        component.animateHeaderViewExit({
            duration: 300,
            onAnimationEnd: navigation.goBack
        });
    },
    render: function render () {
        const component = this;
        const {
            showIntro,
            // mapTheme,
            notification
        } = component.props;
        return (
            <ScreenView style = {{
                backgroundColor: Ht.Theme.palette.silver
            }}>
                <HeaderView
                    cId = 'h0'
                    ref = { component.assignComponentRef(`animated-header`) }
                    label = 'General Settings'
                >
                    <FlatButton
                        room = 'action-left'
                        overlay = 'transparent'
                        onPress = { component.onPressGoBackButton }
                    >
                        <IconImage
                            room = 'content-center'
                            source = 'back'
                            size = 'large'
                        />
                    </FlatButton>
                </HeaderView>
                <BodyView>
                    <Switch
                        ref = { component.assignComponentRef(`daily-air-quality-alert-switch`) }
                        initialValue = { notification.dailyAQAlert }
                        title = 'Daily Air Quality Level Alert'
                        info = 'Occasionally notify air quality level in morning'
                    />
                    <Switch
                        ref = { component.assignComponentRef(`unhealthy-air-quality-alert-switch`) }
                        initialValue = { notification.unhealthyAQAlert }
                        title = 'Unhealthy Air Quality Level Alert'
                        info = 'Notify unhealthy air quality level when AQI > 100 while in motion or AQI > 150 while stationary'
                    />
                    <Switch
                        ref = { component.assignComponentRef(`show-intro-switch`) }
                        initialValue = { showIntro }
                        title = 'Show Intro'
                        info = 'Always show the introduction slides during startup'
                    />
                    {/* <LayoutView
                        style = {{
                            ...Ht.Theme.general.dropShadow.extraShallow,
                            marginVertical: 6,
                            padding: 6
                        }}
                        overlay = 'opaque'
                        orientation = 'horizontal'
                        alignment = 'stretch'
                        selfAlignment = 'stretch'
                    >
                        <LayoutView
                            orientation = 'vertical'
                            alignment = 'stretch'
                            selfAlignment = 'stretch'
                        >
                            <SubtitleText> Map Themes </SubtitleText>
                        </LayoutView>
                        <SegmentedControlIOS
                            ref = { component.assignComponentRef(`map-themes-segmented-control`) }
                            values = {[ `Lite`, `Flat`, `Retro` ]}
                            tintColor = { Ht.Theme.general.color.light.primary }
                        />
                        <InfoText
                            alignment = 'left'
                            indentation = { 3 }
                        > Available map themes </InfoText>
                    </LayoutView> */}
                </BodyView>
            </ScreenView>
        );
    }
});
export default GeneralSettingViewInterface;
