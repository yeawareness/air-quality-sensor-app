/**
 * Copyright (c) 2017-present, Virida YEAwareness, Org. All rights reserved.
 *
 *------------------------------------------------------------------------
 *
 * @module SettingViewInterface
 * @description - Virida client-native app setting view interface.
 *
 * @author Tuan Le (tuan.t.lei@gmail.com)
 *
 *------------------------------------------------------------------------
 * @flow
 */
'use strict'; // eslint-disable-line

import { Hf } from 'hyperflow';

import { Ht } from 'hypertoxin';

import React from 'react';

import ReactNative from 'react-native'; // eslint-disable-line

import { createStackNavigator } from 'react-navigation';

import AQSSettingViewInterface from './aqs-setting-view-interface';

import AQSCalibrationViewInterface from './aqs-calibration-view-interface';

import PM25SensorCalibrationViewInterface from './pm25-sensor-calibration-view-interface';

import VOCSensorCalibrationViewInterface from './voc-sensor-calibration-view-interface';

import AQSInfoViewInterface from './aqs-info-view-interface';

import GeneralSettingViewInterface from './general-setting-view-interface';

import PolicyViewInterface from './policy-view-interface';

import AboutViewInterface from './about-view-interface';

import MovingWave from '../../common/components/moving-wave-component';

// import CONSTANT from '../../common/constant';

import EVENT from '../events/setting-event';

const {
    ScreenView,
    BodyView,
    LayoutView
} = Ht.View;

const {
    RaisedButton
} = Ht.Button;

const {
    WallpaperImage
} = Ht.Image;

const SettingStackNavigator = createStackNavigator({
    setting: {
        screen: (props) => {
            const {
                // screenProps,
                navigation
            } = props;
            // const {
            //     component
            // } = screenProps;
            // const {
            //     aqs
            // } = component.state;
            // const aqsSkipped = Hf.isEmpty(aqs.info.mId);
            return (
                <ScreenView style = {{
                    backgroundColor: Ht.Theme.palette.yellow
                }}>
                    <WallpaperImage source = { Ht.Theme.imageSource.backgroundGradient }>
                        <BodyView>
                            <MovingWave
                                waves = {[{
                                    color: Ht.Theme.palette.cyan,
                                    opacity: 0.7,
                                    lineThickness: 3,
                                    amplitude: 40,
                                    phase: 45,
                                    verticalOffset: 5
                                }, {
                                    color: Ht.Theme.palette.teal,
                                    opacity: 0.8,
                                    lineThickness: 1,
                                    amplitude: 60,
                                    phase: 90,
                                    verticalOffset: 15
                                }, {
                                    color: Ht.Theme.palette.deepBlue,
                                    opacity: 0.5,
                                    lineThickness: 9,
                                    amplitude: 80,
                                    phase: 120,
                                    verticalOffset: 20
                                }]}
                            />
                            <LayoutView
                                style = {{
                                    marginHorizontal: 6
                                    // marginTop: CONSTANT.GENERAL.DEVICE_HEIGHT * 0.18
                                }}
                                orientation = 'horizontal'
                                alignment = 'stretch'
                                selfAlignment = 'stretch'
                            >
                                {/* <RaisedButton
                                    label = 'AIR QUALITY SENSOR INFO'
                                    disabled = { aqsSkipped }
                                    onPress = {() => navigation.navigate({
                                        key: `aqsInfo`,
                                        routeName: `aqsInfo`
                                    })}
                                />
                                <RaisedButton
                                    label = 'AIR QUALITY SENSOR SETTINGS'
                                    disabled = { aqsSkipped }
                                    onPress = {() => navigation.navigate({
                                        key: `aqsSetting`,
                                        routeName: `aqsSetting`
                                    })}
                                /> */}
                                <RaisedButton
                                    label = 'GENERAL SETTINGS'
                                    onPress = {() => navigation.navigate({
                                        key: `generalSetting`,
                                        routeName: `generalSetting`
                                    })}
                                />
                                {/* <RaisedButton
                                    label = 'REDISCOVER AIR QUALITY SENSOR'
                                    onPress = { component.onPressRediscoverAQSButton }
                                /> */}
                                <RaisedButton
                                    label = 'ABOUT'
                                    onPress = {() => navigation.navigate({
                                        key: `about`,
                                        routeName: `about`
                                    })}
                                />
                            </LayoutView>
                        </BodyView>
                    </WallpaperImage>
                </ScreenView>
            );
        },
        navigationOptions: {
            header: {
                visible: false
            }
        }
    },
    aqsSetting: {
        screen: (props) => {
            const {
                component
            } = props.screenProps;
            const [
                AQSSettingView
            ] = component.getComponentComposites(`aqs-setting-view`);
            const {
                aqs,
                aqsGeneralSettingChanged
            } = component.state;
            return (
                <AQSSettingView
                    changed = { aqsGeneralSettingChanged }
                    { ...aqs.setting.general }
                    { ...props }
                />
            );
        },
        navigationOptions: {
            header: {
                visible: false
            }
        }
    },
    aqsInfo: {
        screen: (props) => {
            const {
                component
            } = props.screenProps;
            const [
                AQSInfoView
            ] = component.getComponentComposites(`aqs-info-view`);
            const {
                aqs
            } = component.state;
            return (
                <AQSInfoView
                    { ...aqs.info }
                    cDatestamp = '2016-03-12 13:00:00' // = { aqs.info.cDatestamp }
                    pm25CalibratedLowLvlMv = { aqs.setting.calibration.pm25.lowLvlMv }
                    vocCalibratedRo = { aqs.setting.calibration.voc.ro }
                    { ...props }
                />
            );
        },
        navigationOptions: {
            header: {
                visible: false
            }
        }
    },
    aqsCalibration: {
        screen: (props) => {
            const {
                component
            } = props.screenProps;
            const [
                AQSCalibrationView
            ] = component.getComponentComposites(`aqs-calibration-view`);
            // const {
            //     aqs
            // } = component.state;
            return (
                <AQSCalibrationView
                    // cDatestamp = { aqs.info.cDatestamp }
                    cDatestamp = '2016-03-12 13:00:00'
                    { ...props }
                />
            );
        },
        navigationOptions: {
            header: {
                visible: false
            }
        }
    },
    pm25SensorCalibration: {
        screen: (props) => {
            const {
                component
            } = props.screenProps;
            const [
                PM25SensorCalibrationView
            ] = component.getComponentComposites(`pm25-sensor-calibration-view`);
            const {
                aqs
            } = component.state;
            return (
                <PM25SensorCalibrationView
                    { ...aqs.setting.calibration.pm25 }
                    { ...props }
                />
            );
        },
        navigationOptions: {
            header: {
                visible: false
            }
        }
    },
    vocSensorCalibration: {
        screen: (props) => {
            const {
                component
            } = props.screenProps;
            const [
                VOCSensorCalibrationView
            ] = component.getComponentComposites(`voc-sensor-calibration-view`);
            const {
                aqs
            } = component.state;
            return (
                <VOCSensorCalibrationView
                    { ...aqs.setting.calibration.voc }
                    { ...props }
                />
            );
        },
        navigationOptions: {
            header: {
                visible: false
            }
        }
    },
    generalSetting: {
        screen: (props) => {
            const {
                component
            } = props.screenProps;
            const [
                GeneralSettingView
            ] = component.getComponentComposites(`general-setting-view`);
            const {
                showIntro,
                mapTheme,
                notification
            } = component.state;
            return (
                <GeneralSettingView
                    showIntro = { showIntro }
                    mapTheme = { mapTheme }
                    notification = { notification }
                    { ...props }
                />
            );
        },
        navigationOptions: {
            header: {
                visible: false
            }
        }
    },
    policy: {
        screen: (props) => {
            const {
                component
            } = props.screenProps;
            const [
                PolicyView
            ] = component.getComponentComposites(`policy-view`);

            return (
                <PolicyView { ...props } />
            );
        },
        navigationOptions: {
            header: {
                visible: false
            }
        }
    },
    about: {
        screen: (props) => {
            const {
                component
            } = props.screenProps;
            const [
                AboutView
            ] = component.getComponentComposites(`about-view`);

            return (
                <AboutView { ...props } />
            );
        },
        navigationOptions: {
            header: {
                visible: false
            }
        }
    }
}, {
    initialRouteName: `setting`,
    mode: `card`,
    headerMode: `none`,
    lazy: true,
    transitionConfig: () => ({
        transitionSpec: {
            duration: 300
        }
    }),
    cardStyle: {
        backgroundColor: `transparent`
    }
});
const SettingViewInterface = Hf.Interface.augment({
    composites: [
        Hf.React.ComponentComposite
    ],
    $init: function $init () {
        const intf = this;

        intf.composedOf(
            AQSSettingViewInterface({
                name: `aqs-setting-view`
            }),
            AQSCalibrationViewInterface({
                name: `aqs-calibration-view`
            }),
            PM25SensorCalibrationViewInterface({
                name: `pm25-sensor-calibration-view`
            }),
            VOCSensorCalibrationViewInterface({
                name: `voc-sensor-calibration-view`
            }),
            AQSInfoViewInterface({
                name: `aqs-info-view`
            }),
            GeneralSettingViewInterface({
                name: `general-setting-view`
            }),
            PolicyViewInterface({
                name: `policy-view`
            }),
            AboutViewInterface({
                name: `about-view`
            })
        );
    },
    setup: function setup (done) {
        const intf = this;
        intf.incoming(
            EVENT.ON.TOGGLE_UNHEALTHY_AQ_ALERT_NOTIFICATION,
            EVENT.ON.TOGGLE_DAILY_AQ_ALERT_NOTIFICATION,
            EVENT.ON.TOGGLE_SHOW_INTRO,
            EVENT.ON.CHANGE_AQS_LED_BRIGHTNESS_LEVEL,
            EVENT.ON.CHANGE_AQS_SAMPLE_COUNT,
            EVENT.ON.CHANGE_AQS_SAMPLE_FREQUENCY,
            EVENT.ON.APPLY_AQS_SETTING_CHANGES,
            EVENT.ON.CANCEL_AQS_SETTING_CHANGES
        ).repeat();
        done();
    },
    teardown: function teardown (done) {
        done();
    },
    onPressRediscoverAQSButton: function onPressRediscoverAQSButton () {
        const component = this;
        component.outgoing(EVENT.ON.AQS_REDISCOVERY).emit();
    },
    render: function render () {
        const component = this;
        const {
            navigation
        } = component.props;
        return (
            <SettingStackNavigator
                screenProps = {{
                    component
                }}
                onNavigationStateChange = {(prevState, currentState) => {
                    if (currentState.index === 0) {
                        navigation.setParams({
                            tabBarVisible: true
                        });
                    } else {
                        navigation.setParams({
                            tabBarVisible: false
                        });
                    }
                    return null;
                }}
            />
        );
    }
});
export default SettingViewInterface;
