/**
 * Copyright (c) 2017-present, Virida YEAwareness, Org. All rights reserved.
 *
 *------------------------------------------------------------------------
 *
 * @module AboutViewInterface
 * @description - Virida client-native app about view interface.
 *
 * @author Tuan Le (tuan.t.lei@gmail.com)
 *
 *------------------------------------------------------------------------
 * @flow
 */
'use strict'; // eslint-disable-line

import { Hf } from 'hyperflow';

import { Ht } from 'hypertoxin';

import React from 'react';

import ReactNative from 'react-native'; // eslint-disable-line

import FBLikeWebView from '../../common/components/fb-like-webview-component';

import CONSTANT from '../../common/constant';

const {
    Linking,
    Image,
    StatusBar,
    TouchableOpacity
} = ReactNative;

const {
    ScreenView,
    HeaderView,
    BodyView,
    LayoutView,
    ItemView
} = Ht.View;

const {
    FlatButton,
    RaisedButton
} = Ht.Button;

const {
    IconImage
} = Ht.Image;

const {
    SubtitleText,
    InfoText
} = Ht.Text;

const {
    Divider
} = Ht.Misc;

const AboutViewInterface = Hf.Interface.augment({
    composites: [
        Hf.React.ComponentComposite,
        Ht.ViewComposite.HeaderViewSlideAndFadeAnimation
    ],
    setup: function setup (done) {
        done();
    },
    teardown: function teardown (done) {
        done();
    },
    onPressGoBackButton: function onPressGoBackButton () {
        const component = this;
        const {
            navigation
        } = component.props;

        component.animateHeaderViewExit({
            duration: 300,
            onAnimationEnd: navigation.goBack
        });
    },
    onPressGoToAboutProjectViridaButton: function onPressGoToAboutProjectViridaButton () {
        Linking.openURL(CONSTANT.URL.YEA_PROJECT_VIRIDA).catch(() => {
            Hf.log(`warn1`, `AboutViewInterface - Unable to open Project Virida website at ${CONSTANT.URL.YEA_PROJECT_VIRIDA}.`);
        });
    },
    onPressGoToAboutYEAButton: function onPressGoToAboutYEAButton () {
        Linking.openURL(CONSTANT.URL.YEA_ABOUT).catch(() => {
            Hf.log(`warn1`, `AboutViewInterface - Unable to open Y.E.A website at ${CONSTANT.URL.YEA_ABOUT}.`);
        });
    },
    onPressGoToAboutAirNowButton: function onPressGoToAboutAirNowButton () {
        Linking.openURL(CONSTANT.URL.AIR_NOW_ABOUT).catch(() => {
            Hf.log(`warn1`, `AboutViewInterface - Unable to open AirNow website at ${CONSTANT.URL.AIR_NOW_ABOUT}.`);
        });
    },
    onPressGoToAboutAQICNButton: function onPressGoToAboutAQICNButton () {
        Linking.openURL(CONSTANT.URL.AQICN_ABOUT).catch(() => {
            Hf.log(`warn1`, `AboutViewInterface - Unable to open AQICN website at ${CONSTANT.URL.AQICN_ABOUT}.`);
        });
    },
    onPressGoToYEAFBButton: function onPressGoToYEAFBButton () {
        Linking.canOpenURL(CONSTANT.URL.YEA_FB1).then((supported) => {
            if (supported) {
                return Linking.openURL(CONSTANT.URL.YEA_FB1);
            } else {
                return Linking.openURL(CONSTANT.URL.YEA_FB2);
            }
        }).catch(() => {
            Hf.log(`warn1`, `AboutViewInterface - Unable to open Y.E.A app or website at ${CONSTANT.URL.YEA_ABOUT}.`);
        });
    },
    onPressGoToAboutYEAInstagramButton: function onPressGoToAboutYEAInstagramButton () {
        Linking.openURL(CONSTANT.URL.YEA_INSTAGRAM).catch(() => {
            Hf.log(`warn1`, `AboutViewInterface - Unable to open Y.E.A Instagram app or website at ${CONSTANT.URL.YEA_INSTAGRAM}.`);
        });
    },
    onPressDonateToYEAButton: function onPressDonateToYEAButton () {
        Linking.openURL(CONSTANT.URL.YEA_DONATION).catch(() => {
            Hf.log(`warn1`, `AboutViewInterface - Unable to open Y.E.A donation at ${CONSTANT.URL.YEA_DONATION}.`);
        });
    },
    // onPressGoToYEAContactButton: function onPressGoToYEAContactButton () {
    //     Linking.openURL(CONSTANT.URL.YEA_CONTACT).catch(() => {
    //         Hf.log(`warn1`, `AboutViewInterface - Unable to open Y.E.A contact link at ${CONSTANT.URL.YEA_CONTACT}.`);
    //     });
    // },
    onPressGoToPolicyButton: function onPressGoToPolicyButton () {
        const component = this;
        const {
            navigation
        } = component.props;

        navigation.navigate({
            key: `policy`,
            routeName: `policy`
        });
    },
    render: function render () {
        const component = this;
        return (
            <ScreenView style = {{
                backgroundColor: Ht.Theme.palette.silver
            }}>
                <StatusBar
                    barStyle = 'dark-content'
                    networkActivityIndicatorVisible = { false }
                />
                <HeaderView
                    cId = 'h0'
                    ref = { component.assignComponentRef(`animated-header`) }
                    label = 'About'
                >
                    <FlatButton
                        room = 'action-left'
                        overlay = 'transparent'
                        onPress = { component.onPressGoBackButton }
                    >
                        <IconImage
                            room = 'content-center'
                            source = 'back'
                            size = 'large'
                        />
                    </FlatButton>
                </HeaderView>
                <BodyView
                    style = {{
                        marginBottom: 72
                    }}
                    scrollable = { true }
                >
                    <LayoutView
                        style = {{
                            ...Ht.Theme.general.dropShadow.extraShallow,
                            marginVertical: 6,
                            padding: 6
                        }}
                        overlay = 'opaque'
                        orientation = 'horizontal'
                        alignment = 'stretch'
                        selfAlignment = 'stretch'
                    >
                        <ItemView
                            style = {{
                                width: CONSTANT.GENERAL.DEVICE_WIDTH
                            }}
                            onPress = { component.onPressGoToAboutProjectViridaButton }
                        >
                            <SubtitleText room = 'content-left' > About Project Virida </SubtitleText>
                            <FlatButton
                                room = 'action-right'
                                overlay = 'transparent'
                                onPress = { component.onPressGoToAboutProjectViridaButton }
                            >
                                <IconImage
                                    room = 'content-center'
                                    source = 'forward'
                                    size = 'large'
                                />
                            </FlatButton>
                        </ItemView>
                        <ItemView
                            style = {{
                                width: CONSTANT.GENERAL.DEVICE_WIDTH
                            }}
                            onPress = { component.onPressGoToAboutYEAButton }
                        >
                            <LayoutView
                                room = 'content-left'
                                orientation = 'vertical'
                                alignment = 'center'
                                selfAlignment = 'start'
                            >
                                <SubtitleText> About Y.E.A </SubtitleText>
                                <Image
                                    style = {{
                                        width: 24,
                                        height: 24,
                                        marginLeft: 6
                                    }}
                                    resizeMode = 'contain'
                                    source = { Ht.Theme.imageSource.yeaLogo }
                                />
                            </LayoutView>
                            <FlatButton
                                room = 'action-right'
                                overlay = 'transparent'
                                onPress = { component.onPressGoToAboutYEAButton }
                            >
                                <IconImage
                                    room = 'content-center'
                                    source = 'forward'
                                    size = 'large'
                                />
                            </FlatButton>
                        </ItemView>
                        <Divider/>
                        <LayoutView
                            orientation = 'horizontal'
                            alignment = 'center'
                            selfAlignment = 'center'
                        >
                            <SubtitleText size = 'small' > Please Like Us On Facebook </SubtitleText>
                            <FBLikeWebView/>
                            <SubtitleText size = 'small' > Or Check Us Out On Facebook Or Instagram </SubtitleText>
                            <LayoutView
                                room = 'content-left'
                                orientation = 'vertical'
                                alignment = 'center'
                                selfAlignment = 'center'
                            >
                                <TouchableOpacity onPress = { component.onPressGoToYEAFBButton }>
                                    <Image
                                        style = {{
                                            width: 36,
                                            height: 36,
                                            margin: 6
                                        }}
                                        resizeMode = 'contain'
                                        source = { Ht.Theme.imageSource.fbLogo }
                                    />
                                </TouchableOpacity>
                                <TouchableOpacity onPress = { component.onPressGoToAboutYEAInstagramButton }>
                                    <Image
                                        style = {{
                                            width: 36,
                                            height: 36,
                                            margin: 6
                                        }}
                                        resizeMode = 'contain'
                                        source = { Ht.Theme.imageSource.instagramLogo }
                                    />
                                </TouchableOpacity>
                            </LayoutView>
                        </LayoutView>
                    </LayoutView>
                    <LayoutView
                        style = {{
                            ...Ht.Theme.general.dropShadow.extraShallow,
                            marginVertical: 6,
                            padding: 6
                        }}
                        overlay = 'opaque'
                        orientation = 'horizontal'
                        alignment = 'stretch'
                        selfAlignment = 'stretch'
                    >
                        <RaisedButton
                            label = 'DONATE TO Y.E.A'
                            color = { Ht.Theme.palette.pink }
                            onPress = { component.onPressDonateToYEAButton }
                        />
                    </LayoutView>
                    <LayoutView
                        style = {{
                            ...Ht.Theme.general.dropShadow.extraShallow,
                            marginVertical: 6,
                            padding: 6
                        }}
                        overlay = 'opaque'
                        orientation = 'horizontal'
                        alignment = 'stretch'
                        selfAlignment = 'stretch'
                    >
                        <ItemView
                            style = {{
                                width: CONSTANT.GENERAL.DEVICE_WIDTH
                            }}
                            onPress = { component.onPressGoToAboutAirNowButton }
                        >
                            <SubtitleText room = 'content-left' > About AirNow </SubtitleText>
                            <FlatButton
                                room = 'action-right'
                                overlay = 'transparent'
                                onPress = { component.onPressGoToAboutAirNowButton }
                            >
                                <IconImage
                                    room = 'content-center'
                                    source = 'forward'
                                    size = 'large'
                                />
                            </FlatButton>
                        </ItemView>
                        <ItemView
                            style = {{
                                width: CONSTANT.GENERAL.DEVICE_WIDTH
                            }}
                            onPress = { component.onPressGoToAboutAQICNButton }
                        >
                            <SubtitleText room = 'content-left' > About AQICN </SubtitleText>
                            <FlatButton
                                room = 'action-right'
                                overlay = 'transparent'
                                onPress = { component.onPressGoToAboutAQICNButton }
                            >
                                <IconImage
                                    room = 'content-center'
                                    source = 'forward'
                                    size = 'large'
                                />
                            </FlatButton>
                        </ItemView>
                    </LayoutView>
                    <LayoutView
                        style = {{
                            ...Ht.Theme.general.dropShadow.extraShallow,
                            marginVertical: 6,
                            padding: 6
                        }}
                        overlay = 'opaque'
                        orientation = 'horizontal'
                        alignment = 'stretch'
                        selfAlignment = 'stretch'
                    >
                        <ItemView
                            style = {{
                                width: CONSTANT.GENERAL.DEVICE_WIDTH
                            }}
                            onPress = { component.onPressGoToPolicyButton }
                        >
                            <SubtitleText room = 'content-left' > Privacy Policy </SubtitleText>
                            <FlatButton
                                room = 'action-right'
                                overlay = 'transparent'
                                onPress = { component.onPressGoToPolicyButton }
                            >
                                <IconImage
                                    room = 'action-right'
                                    room = 'content-center'
                                    source = 'forward'
                                    size = 'large'
                                />
                            </FlatButton>
                        </ItemView>
                    </LayoutView>
                    <LayoutView
                        style = {{
                            ...Ht.Theme.general.dropShadow.extraShallow,
                            marginVertical: 6,
                            padding: 6
                        }}
                        overlay = 'opaque'
                        orientation = 'horizontal'
                        alignment = 'stretch'
                        selfAlignment = 'stretch'
                    >
                        <LayoutView
                            orientation = 'vertical'
                            alignment = 'center'
                            selfAlignment = 'stretch'
                        >
                            <SubtitleText> Version </SubtitleText>
                            <InfoText>{ `v${CONSTANT.VERSION}.${CONSTANT.CODE_PUSH_BUILD}` }</InfoText>
                        </LayoutView>
                    </LayoutView>
                </BodyView>
            </ScreenView>
        );
    }
});
export default AboutViewInterface;
