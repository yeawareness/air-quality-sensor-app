/**
 * Copyright (c) 2017-present, Virida YEAwareness, Org. All rights reserved.
 *
 *------------------------------------------------------------------------
 *
 * @module AQSInfoViewInterface
 * @description - Virida client-native app aqs info view interface.
 *
 * @author Tuan Le (tuan.t.lei@gmail.com)
 *
 *------------------------------------------------------------------------
 * @flow
 */
'use strict'; // eslint-disable-line

import { Hf } from 'hyperflow';

import { Ht } from 'hypertoxin';

import moment from 'moment';

import React from 'react';

import ReactNative from 'react-native'; // eslint-disable-line

import CONSTANT from '../../common/constant';

const {
    ScreenView,
    HeaderView,
    BodyView,
    LayoutView
} = Ht.View;

const {
    TextField
} = Ht.Field;

const {
    FlatButton,
    RaisedButton
} = Ht.Button;

const {
    IconImage
} = Ht.Image;

const {
    SubtitleText,
    InfoText
} = Ht.Text;

const AQSInfoViewInterface = Hf.Interface.augment({
    composites: [
        Hf.React.ComponentComposite,
        Ht.ViewComposite.HeaderViewSlideAndFadeAnimation
    ],
    state: {
        mId: {
            value: ``
        },
        alias: {
            value: `Virida Sensor`,
            required: true
        },
        rev: {
            value: ``
        },
        skew: {
            value: ``
        },
        firmwareVersion: {
            value: ``
        },
        cDatestamp: {
            value: ``
        },
        pm25CalibratedLowLvlMv: {
            value: 0
        },
        vocCalibratedRo: {
            value: 0
        }
    },
    setup: function setup (done) {
        done();
    },
    teardown: function teardown (done) {
        done();
    },
    onPressGoBackButton: function onPressGoBackButton () {
        const component = this;
        const {
            navigation
        } = component.props;

        component.animateHeaderViewExit({
            duration: 300,
            onAnimationEnd: navigation.goBack
        });
    },
    render: function render () {
        const component = this;
        const {
            navigation,
            // mId,
            alias,
            rev,
            skew,
            firmwareVersion,
            cDatestamp,
            pm25CalibratedLowLvlMv,
            vocCalibratedRo
        } = component.props;
        const lastCalibrationElapsedDayCount = moment().diff(moment(cDatestamp), `days`);
        const calibrationRequired = lastCalibrationElapsedDayCount > CONSTANT.AQS.CALIBRATION_REQUIRED_ELAPSED_DAY_COUNT;

        return (
            <ScreenView style = {{
                backgroundColor: Ht.Theme.palette.silver
            }}>
                <HeaderView
                    cId = 'h0'
                    ref = { component.assignComponentRef(`animated-header`) }
                    label = 'Info'
                >
                    <FlatButton
                        room = 'action-left'
                        overlay = 'transparent'
                        onPress = { component.onPressGoBackButton }
                    >
                        <IconImage
                            room = 'content-center'
                            source = 'back'
                            size = 'large'
                        />
                    </FlatButton>
                </HeaderView>
                <BodyView>
                    <LayoutView
                        style = {{
                            ...Ht.Theme.general.dropShadow.extraShallow,
                            marginVertical: 6,
                            padding: 6
                        }}
                        overlay = 'opaque'
                        orientation = 'horizontal'
                        alignment = 'stretch'
                        selfAlignment = 'stretch'
                    >
                        <TextField
                            overlay = 'translucent'
                            label = 'Air Quality Sensor Alias'
                            initialValue = { alias }
                            charLimit = { 7 }
                            onValidate = {(value) => {
                                const validated = !Hf.isEmpty(value);
                                return {
                                    validated,
                                    status: validated ? `` : `Air quality sensor alias cannot be empty`
                                };
                            }}
                        >
                            <IconImage
                                room = 'content-left'
                                source = 'sensor'
                                color = 'secondary'
                            />
                            <FlatButton
                                room = 'action-right'
                                overlay = 'transparent'
                            >
                                <IconImage
                                    room = 'content-center'
                                    source = 'close'
                                    size = 'small'
                                />
                            </FlatButton>
                        </TextField>
                    </LayoutView>
                    <LayoutView
                        style = {{
                            ...Ht.Theme.general.dropShadow.extraShallow,
                            marginVertical: 6,
                            padding: 6
                        }}
                        overlay = 'opaque'
                        orientation = 'horizontal'
                        alignment = 'stretch'
                        selfAlignment = 'stretch'
                    >
                        <LayoutView
                            orientation = 'vertical'
                            alignment = 'center'
                            selfAlignment = 'stretch'
                        >
                            <SubtitleText> Air Quality Sensor Info </SubtitleText>
                        </LayoutView>
                        <LayoutView
                            orientation = 'vertical'
                            alignment = 'center'
                            selfAlignment = 'stretch'
                        >
                            <InfoText
                                alignment = 'left'
                                indentation = { 3 }
                            > Firmware </InfoText>
                            <InfoText>{ firmwareVersion }</InfoText>
                        </LayoutView>
                        <LayoutView
                            orientation = 'vertical'
                            alignment = 'center'
                            selfAlignment = 'stretch'
                        >
                            <InfoText
                                alignment = 'left'
                                indentation = { 3 }
                            > Model </InfoText>
                            <InfoText>{ `${rev}-${skew}` }</InfoText>
                        </LayoutView>
                        <LayoutView
                            orientation = 'vertical'
                            alignment = 'center'
                            selfAlignment = 'stretch'
                        >
                            <InfoText
                                alignment = 'left'
                                indentation = { 3 }
                            > Last Calibration Date </InfoText>
                            <InfoText >{ moment().format(`MMM Do YY`) }</InfoText>
                        </LayoutView>
                        <LayoutView
                            orientation = 'vertical'
                            alignment = 'center'
                            selfAlignment = 'stretch'
                        >
                            <InfoText
                                alignment = 'left'
                                indentation = { 3 }
                                color = { calibrationRequired ? Ht.Theme.palette.red : Ht.Theme.palette.green }
                            > Calibration Required </InfoText>
                            <InfoText
                                color = { calibrationRequired ? Ht.Theme.palette.red : Ht.Theme.palette.green }
                            >{ calibrationRequired ? `Required` : `Not Required` }</InfoText>
                        </LayoutView>
                    </LayoutView>
                    <LayoutView
                        style = {{
                            ...Ht.Theme.general.dropShadow.extraShallow,
                            marginVertical: 6,
                            padding: 6
                        }}
                        overlay = 'opaque'
                        orientation = 'horizontal'
                        alignment = 'stretch'
                        selfAlignment = 'stretch'
                    >
                        <LayoutView
                            orientation = 'vertical'
                            alignment = 'center'
                            selfAlignment = 'stretch'
                        >
                            <SubtitleText> PM25 Sensor Calibrated Parameters </SubtitleText>
                        </LayoutView>
                        <LayoutView
                            orientation = 'vertical'
                            alignment = 'center'
                            selfAlignment = 'stretch'
                        >
                            <InfoText
                                alignment = 'left'
                                indentation = { 3 }
                            > PM25 No Dust Votage Level </InfoText>
                            <InfoText >{ `${pm25CalibratedLowLvlMv} mV` }</InfoText>
                        </LayoutView>
                    </LayoutView>
                    <LayoutView
                        style = {{
                            ...Ht.Theme.general.dropShadow.extraShallow,
                            marginVertical: 6,
                            padding: 6
                        }}
                        overlay = 'opaque'
                        orientation = 'horizontal'
                        alignment = 'stretch'
                        selfAlignment = 'stretch'
                    >
                        <LayoutView
                            orientation = 'vertical'
                            alignment = 'center'
                            selfAlignment = 'stretch'
                        >
                            <SubtitleText> VOC Sensor Calibrated Parameters </SubtitleText>
                        </LayoutView>
                        <LayoutView
                            orientation = 'vertical'
                            alignment = 'center'
                            selfAlignment = 'stretch'
                        >
                            <InfoText
                                alignment = 'left'
                                indentation = { 3 }
                            > VOC Ro </InfoText>
                            <InfoText>{ `${vocCalibratedRo} Ohm` }</InfoText>
                        </LayoutView>
                    </LayoutView>
                    <LayoutView
                        style = {{
                            ...Ht.Theme.general.dropShadow.extraShallow,
                            marginVertical: 6,
                            padding: 6
                        }}
                        overlay = 'opaque'
                        orientation = 'horizontal'
                        alignment = 'stretch'
                        selfAlignment = 'stretch'
                    >
                        <RaisedButton
                            label = 'AIR QUALITY SENSOR CALIBRATIONS'
                            disabled = { !calibrationRequired }
                            onPress = {() => navigation.navigate({
                                key: `aqsCalibration`,
                                routeName: `aqsCalibration`
                            })}
                        />
                    </LayoutView>
                </BodyView>
            </ScreenView>
        );
    }
});

export default AQSInfoViewInterface;
