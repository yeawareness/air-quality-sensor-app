/**
 * Copyright (c) 2017-present, Virida YEAwareness, Org. All rights reserved.
 *
 *------------------------------------------------------------------------
 *
 * @module PM25SensorCalibrationViewInterface
 * @description - Virida client-native app pm25 calibration view interface.
 *
 * @author Tuan Le (tuan.t.lei@gmail.com)
 *
 *------------------------------------------------------------------------
 * @flow
 */
'use strict'; // eslint-disable-line

import { Hf } from 'hyperflow';

import { Ht } from 'hypertoxin';

import React from 'react';

import ReactNative from 'react-native'; // eslint-disable-line

import AppIntroSlider from 'react-native-app-intro-slider';

import CONSTANT from '../../common/constant';

const {
    StatusBar,
    View
} = ReactNative;

const {
    BodyView,
    LayoutView
} = Ht.View;

const {
    RaisedButton
} = Ht.Button;

const {
    HeadlineText,
    TitleText,
    SubtitleText,
    InfoText
} = Ht.Text;

const {
    WallpaperImage
} = Ht.Image;

const pm25CalibrationSlides = [
    {
        key: `0`,
        headline1: ``,
        headline2: ``,
        info1: ``,
        info2: ``,
        image: null,
        backgroundColor: Ht.Theme.palette.blue
    }, {
        key: `1`,
        headline1: ``,
        headline2: ``,
        info1: ``,
        info2: ``,
        image: null,
        backgroundColor: Ht.Theme.palette.orange
    }, {
        key: `3`,
        headline1: ``,
        headline2: ``,
        info1: ``,
        info2: ``,
        image: null,
        backgroundColor: Ht.Theme.palette.red
    }, {
        key: `4`,
        headline1: ``,
        headline2: ``,
        info1: ``,
        info2: ``,
        image: null,
        backgroundColor: Ht.Theme.palette.green
    }
];

const PM25SensorCalibrationViewInterface = Hf.Interface.augment({
    composites: [
        Hf.React.ComponentComposite
    ],
    state: {
        lowLvlMv: {
            value: 0
        },
        lowLvlTargetThreshold: {
            value: 0
        }
    },
    setup: function setup (done) {
        done();
    },
    teardown: function teardown (done) {
        done();
    },
    onPressStartCalibrationButton: function onPressStartCalibrationButton () {
        const component = this;
    },
    onPressCancelButton: function onPressCancelButton () {
        const component = this;
        const {
            navigation
        } = component.props;
        navigation.goBack();
    },
    renderCalibrationSlide: function renderCalibrationSlide (slide) {
        const component = this;
        return (
            <View style = {{
                flexGrow: 1,
                flexDirection: `column`,
                justifyContent: `center`,
                alignItems: `center`,
                backgroundColor: slide.backgroundColor
            }}>
                <StatusBar
                    barStyle = 'dark-content'
                    networkActivityIndicatorVisible = { false }
                />
                <WallpaperImage
                    style = {{
                        justifyContent: `center`,
                        alignItems: `center`
                    }}
                    source = { Ht.Theme.imageSource.backgroundGradient }
                >
                    <BodyView>
                        <LayoutView
                            style = {{
                                marginHorizontal: 6
                            }}
                            orientation = 'horizontal'
                            alignment = 'stretch'
                            selfAlignment = 'stretch'
                        >
                            <HeadlineText shade = 'dark' >{ slide.headline1 }</HeadlineText>
                            <HeadlineText
                                shade = 'dark'
                                size = 'small'
                            >{ slide.headline2 }</HeadlineText>
                            <LayoutView
                                orientation = 'horizontal'
                                alignment = 'start'
                                selfAlignment = 'stretch'
                            >
                                <InfoText
                                    shade = 'dark'
                                    size = 'large'
                                >{ slide.info1 }</InfoText>
                            </LayoutView>
                        </LayoutView>
                        <LayoutView
                            style = {{
                                marginHorizontal: 6
                            }}
                            orientation = 'horizontal'
                            alignment = 'stretch'
                            selfAlignment = 'stretch'
                        >
                            <LayoutView
                                orientation = 'horizontal'
                                alignment = 'start'
                                selfAlignment = 'stretch'
                            >
                                <InfoText
                                    shade = 'dark'
                                    size = 'large'
                                >{ slide.info2 }</InfoText>
                            </LayoutView>
                            {
                                slide.key === `4` ? <RaisedButton
                                    label = 'START CALIBRATION'
                                    color = { Ht.Theme.palette.green }
                                    onPress = { component.onPressStartCalibrationButton }
                                /> : null
                            }
                        </LayoutView>
                    </BodyView>
                </WallpaperImage>
            </View>
        );
    },
    render: function render () {
        const component = this;
        return (
            <AppIntroSlider
                showSkipButton = { true }
                showPrevButton = { true }
                nextLabel = 'NEXT'
                prevLabel = 'PREV'
                doneLabel = 'CANCEL'
                skipLabel = 'CANCEL'
                slides = { pm25CalibrationSlides }
                renderItem = { component.renderCalibrationSlide }
                onSkip = { component.onPressCancelButton }
                onDone = { component.onPressCancelButton }
            />
        );
    }
});
export default PM25SensorCalibrationViewInterface;
