/**
 * Copyright (c) 2017-present, Virida YEAwareness, Org. All rights reserved.
 *
 *------------------------------------------------------------------------
 *
 * @module PolicyViewInterface
 * @description - Virida client-native app policy view interface.
 *
 * @author Tuan Le (tuan.t.lei@gmail.com)
 *
 *------------------------------------------------------------------------
 * @flow
 */
'use strict'; // eslint-disable-line

import { Hf } from 'hyperflow';

import { Ht } from 'hypertoxin';

import React from 'react';

import ReactNative from 'react-native'; // eslint-disable-line

import CONSTANT from '../../common/constant';

const {
    Linking
} = ReactNative;

const {
    ScreenView,
    HeaderView,
    BodyView,
    LayoutView
} = Ht.View;

const {
    FlatButton
} = Ht.Button;

const {
    IconImage
} = Ht.Image;

const {
    TitleText,
    InfoText
} = Ht.Text;

const {
    Divider
} = Ht.Misc;

const PolicyViewInterface = Hf.Interface.augment({
    composites: [
        Hf.React.ComponentComposite,
        Ht.ViewComposite.HeaderViewSlideAndFadeAnimation
    ],
    setup: function setup (done) {
        done();
    },
    teardown: function teardown (done) {
        done();
    },
    onPressGoBackButton: function onPressGoBackButton () {
        const component = this;
        const {
            navigation
        } = component.props;

        component.animateHeaderViewExit({
            duration: 300,
            onAnimationEnd: navigation.goBack
        });
    },
    onPressGoToYEAContactButton: function onPressGoToYEAContactButton () {
        Linking.openURL(CONSTANT.URL.YEA_CONTACT).catch(() => {
            Hf.log(`warn1`, `PolicyViewInterface - Unable to open Y.E.A contact link at ${CONSTANT.URL.YEA_CONTACT}.`);
        });
    },
    render: function render () {
        const component = this;

        return (
            <ScreenView>
                <HeaderView
                    cId = 'h0'
                    ref = { component.assignComponentRef(`animated-header`) }
                    label = 'Privacy Policy'
                >
                    <FlatButton
                        room = 'action-left'
                        overlay = 'transparent'
                        onPress = { component.onPressGoBackButton }
                    >
                        <IconImage
                            room = 'content-center'
                            source = 'back'
                            size = 'large'
                        />
                    </FlatButton>
                </HeaderView>
                <BodyView
                    style = {{
                        marginBottom: 72
                    }}
                    scrollable = { true }
                >
                    <LayoutView
                        style = {{
                            marginHorizontal: 6
                        }}
                        orientation = 'horizontal'
                        alignment = 'stretch'
                        selfAlignment = 'stretch'
                    >
                        <TitleText size = 'large' > Privacy Policy </TitleText>
                        <InfoText
                            alignment = 'left'
                            indentation = { 3 }
                        >   Y.E.A built the Viria app as a Free app. This SERVICE is provided by Y.E.A at no cost and is intended for use as is.
                        This page is used to inform website visitors regarding our policies with the collection, use, and disclosure of Personal Information if anyone decided to use our Service.
                        If you choose to use our Service, then you agree to the collection and use of information in relation to this policy. The Personal Information that we collect is used for providing and improving the Service.
                        We will not use or share your information with anyone except as described in this Privacy Policy.
                        The terms used in this Privacy Policy have the same meanings as in our Terms and Conditions, which is accessible at Virida unless otherwise defined in this Privacy Policy. </InfoText>
                    </LayoutView>
                    <Divider/>
                    <LayoutView
                        style = {{
                            marginHorizontal: 9
                        }}
                        orientation = 'horizontal'
                        alignment = 'stretch'
                        selfAlignment = 'stretch'
                    >
                        <TitleText> Determining and Sharing Location </TitleText>
                        <InfoText
                            alignment = 'left'
                            indentation = { 3 }
                        >   Some features and functionality in Virida app may require that you provide your location.
                        Virida app may use your location data to make request to airnow.gov and aqicn.org and retrieve air quality data in your region for display. </InfoText>
                    </LayoutView>
                    <Divider/>
                    <LayoutView
                        style = {{
                            marginHorizontal: 6
                        }}
                        orientation = 'horizontal'
                        alignment = 'stretch'
                        selfAlignment = 'stretch'
                    >
                        <TitleText> Changes to This Privacy Policy </TitleText>
                        <InfoText
                            alignment = 'left'
                            indentation = { 3 }
                        >   We may update our Privacy Policy from time to time. Thus, you are advised to review this page periodically for any changes.
                        We will notify you of any changes by posting the new Privacy Policy on this page. These changes are effective immediately after they are posted on this page. </InfoText>
                    </LayoutView>
                    <Divider/>
                    <LayoutView
                        style = {{
                            marginHorizontal: 6
                        }}
                        orientation = 'horizontal'
                        alignment = 'stretch'
                        selfAlignment = 'stretch'
                    >
                        <TitleText> Contact Us </TitleText>
                        <InfoText
                            alignment = 'left'
                            indentation = { 3 }
                        >   If you have any questions or suggestions about our Privacy Policy, do not hesitate to contact us. </InfoText>
                        <FlatButton
                            overlay = 'transparent'
                            label = 'CONTACT Y.E.A'
                            onPress = { component.onPressGoToYEAContactButton }
                        />
                    </LayoutView>
                </BodyView>
            </ScreenView>
        );
    }
});
export default PolicyViewInterface;
