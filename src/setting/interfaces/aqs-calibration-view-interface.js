/**
 * Copyright (c) 2017-present, Virida YEAwareness, Org. All rights reserved.
 *
 *------------------------------------------------------------------------
 *
 * @module AQSCalibrationViewInterface
 * @description - Virida client-native app aqs calibration view interface.
 *
 * @author Tuan Le (tuan.t.lei@gmail.com)
 *
 *------------------------------------------------------------------------
 * @flow
 */
'use strict'; // eslint-disable-line

import { Hf } from 'hyperflow';

import { Ht } from 'hypertoxin';

import moment from 'moment';

import React from 'react';

import ReactNative from 'react-native'; // eslint-disable-line

// import CONSTANT from '../../common/constant';

const {
    ScreenView,
    HeaderView,
    BodyView,
    LayoutView
} = Ht.View;

const {
    FlatButton,
    RaisedButton
} = Ht.Button;

const {
    IconImage
} = Ht.Image;

const {
    SubtitleText,
    InfoText
} = Ht.Text;

const AQSCalibrationViewInterface = Hf.Interface.augment({
    composites: [
        Hf.React.ComponentComposite,
        Ht.ViewComposite.HeaderViewSlideAndFadeAnimation
    ],
    state: {
        cDatestamp: {
            value: ``
        }
    },
    setup: function setup (done) {
        done();
    },
    teardown: function teardown (done) {
        done();
    },
    onPressGoBackButton: function onPressGoBackButton () {
        const component = this;
        const {
            navigation
        } = component.props;

        component.animateHeaderViewExit({
            duration: 300,
            onAnimationEnd: navigation.goBack
        });
    },
    onPressStartPM25SensorCalibrationButton: function onPressStartPM25SensorCalibrationButton () {
        const component = this;
        const {
            navigation
        } = component.props;

        navigation.navigate({
            key: `pm25SensorCalibration`,
            routeName: `pm25SensorCalibration`
        });
    },
    onPressStartVOCSensorCalibrationButton: function onPressStartVOCSensorCalibrationButton () {
        const component = this;
        const {
            navigation
        } = component.props;

        navigation.navigate({
            key: `vocSensorCalibration`,
            routeName: `vocSensorCalibration`
        });
    },
    render: function render () {
        const component = this;
        const {
            cDatestamp
        } = component.props;
        const lastCalibrationElapsedDayCount = moment().diff(moment(cDatestamp), `days`);

        return (
            <ScreenView style = {{
                backgroundColor: Ht.Theme.palette.silver
            }}>
                <HeaderView
                    cId = 'h0'
                    ref = { component.assignComponentRef(`animated-header`) }
                    label = 'Air Quality Sensor Calibration'
                >
                    <FlatButton
                        room = 'action-left'
                        overlay = 'transparent'
                        onPress = { component.onPressGoBackButton }
                    >
                        <IconImage
                            room = 'content-center'
                            source = 'back'
                            size = 'large'
                        />
                    </FlatButton>
                </HeaderView>
                <BodyView>
                    <LayoutView
                        style = {{
                            ...Ht.Theme.general.dropShadow.extraShallow,
                            marginVertical: 6,
                            padding: 6
                        }}
                        overlay = 'opaque'
                        orientation = 'horizontal'
                        alignment = 'stretch'
                        selfAlignment = 'stretch'
                    >
                        <LayoutView
                            orientation = 'vertical'
                            alignment = 'center'
                            selfAlignment = 'stretch'
                        >
                            <SubtitleText> Was Calibrated </SubtitleText>
                            <InfoText>{ `${lastCalibrationElapsedDayCount} Days Ago` }</InfoText>
                        </LayoutView>
                    </LayoutView>
                    <LayoutView
                        style = {{
                            ...Ht.Theme.general.dropShadow.extraShallow,
                            marginVertical: 6,
                            padding: 6
                        }}
                        overlay = 'opaque'
                        orientation = 'horizontal'
                        alignment = 'stretch'
                        selfAlignment = 'stretch'
                    >
                        <RaisedButton
                            label = 'START PM25 SENSOR CALIBRATION'
                            onPress = { component.onPressStartPM25SensorCalibrationButton }
                        />
                        <RaisedButton
                            label = 'START VOC SENSOR CALIBRATION'
                            onPress = { component.onPressStartVOCSensorCalibrationButton }
                        />
                    </LayoutView>
                </BodyView>
            </ScreenView>
        );
    }
});
export default AQSCalibrationViewInterface;
