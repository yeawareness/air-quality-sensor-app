/**
 * Copyright (c) 2017-present, Virida YEAwareness, Org. All rights reserved.
 *
 *------------------------------------------------------------------------
 *
 * @module SettingDomain
 * @description - Virida client-native app setting domain.
 *
 * @author Tuan Le (tuan.t.lei@gmail.com)
 *
 *------------------------------------------------------------------------
 * @flow
 */
'use strict'; // eslint-disable-line

import { Hf } from 'hyperflow';

import SettingStore from '../stores/setting-store';

import SettingViewInterface from '../interfaces/setting-view-interface';

import StorageService from '../../common/services/storage-service';

import EVENT from '../events/setting-event';

const SettingDomain = Hf.Domain.augment({
    $init: function $init () {
        const domain = this;
        domain.register({
            store: SettingStore({
                name: `setting-store`
            }),
            intf: SettingViewInterface({
                name: `setting-view`
            }),
            services: [
                StorageService({
                    name: `storage-service`
                })
            ]
        });
    },
    setup: function setup (done) {
        const domain = this;
        domain.incoming(EVENT.DO.SETTING_RESET).repeat();
        domain.incoming(EVENT.BROADCAST.RUN_MODE).handle((runMode) => {
            return {
                idle: runMode === `background-running`
            };
        }).relay(EVENT.DO.MUTATE_SETTING_STATUS);
        domain.incoming(EVENT.DO.SETTING_ACTIVATION).handle((active) => {
            return {
                active
            };
        }).relay(EVENT.DO.MUTATE_SETTING_STATUS);
        domain.incoming(EVENT.AS.SETTING_ACTIVATED).forward(
            EVENT.REQUEST.READ_SETTING,
            EVENT.REQUEST.READ_AQS_INFO,
            EVENT.REQUEST.READ_AQS_GENERAL_SETTING,
            EVENT.REQUEST.READ_AQS_CALIBRATION_SETTING
        );
        domain.incoming(EVENT.RESPONSE.TO.READ_SETTING.OK).handle((setting) => {
            domain.outgoing(EVENT.DO.MUTATE_SHOW_INTRO).emit(() => () => {
                return {
                    showIntro: setting.showIntro
                };
            });
            domain.outgoing(EVENT.DO.MUTATE_NOTIFICATION).emit(() => () => {
                return {
                    notification: {
                        unhealthyAQAlert: setting.notification.unhealthyAQAlert,
                        dailyAQAlert: setting.notification.dailyAQAlert
                    }
                };
            });
        });
        domain.incoming(EVENT.RESPONSE.TO.READ_AQS_INFO.OK).forward(EVENT.DO.MUTATE_AQS_INFO);
        domain.incoming(EVENT.RESPONSE.TO.READ_AQS_GENERAL_SETTING.OK).handle((aqsGeneralSetting) => {
            return {
                ...aqsGeneralSetting,
                editable: {
                    ledBrightnessLvl: aqsGeneralSetting.ledBrightnessLvl,
                    aqSampleCount: aqsGeneralSetting.aqSampleCount,
                    mInterval: aqsGeneralSetting.mInterval
                }
            };
        }).relay(EVENT.DO.MUTATE_AQS_GENERAL_SETTING);
        domain.incoming(EVENT.RESPONSE.TO.READ_AQS_CALIBRATION_SETTING.OK).handle((aqsCalibrationSetting) => {
            return {
                ...aqsCalibrationSetting,
                editable: {
                    pm25: {
                        lowLvlTargetThreshold: aqsCalibrationSetting.pm25.lowLvlTargetThreshold
                    },
                    voc: {
                        refCOPPM: aqsCalibrationSetting.voc.refCOPPM,
                        refCOPPMTargetWindow: aqsCalibrationSetting.voc.refCOPPMTargetWindow
                    }
                }
            };
        }).relay(EVENT.DO.MUTATE_AQS_CALIBRATION_SETTING);
        domain.incoming(EVENT.ON.TOGGLE_DAILY_AQ_ALERT_NOTIFICATION).handle(() => ({
            notification
        }) => {
            return {
                notification: {
                    dailyAQAlert: !notification.dailyAQAlert
                }
            };
        }).relay(EVENT.DO.MUTATE_NOTIFICATION);
        domain.incoming(EVENT.ON.TOGGLE_UNHEALTHY_AQ_ALERT_NOTIFICATION).handle(() => ({
            notification
        }) => {
            return {
                notification: {
                    unhealthyAQAlert: !notification.unhealthyAQAlert
                }
            };
        }).relay(EVENT.DO.MUTATE_NOTIFICATION);
        domain.incoming(EVENT.ON.TOGGLE_SHOW_INTRO).handle(() => ({
            showIntro
        }) => {
            return {
                showIntro: !showIntro
            };
        }).relay(EVENT.DO.MUTATE_SHOW_INTRO);
        domain.incoming(EVENT.AS.NOTIFICATION_MUTATED).handle((notification) => {
            return {
                notification
            };
        }).relay(EVENT.REQUEST.WRITE_SETTING);
        domain.incoming(EVENT.AS.SHOW_INTRO_MUTATED).handle((showIntro) => {
            return {
                showIntro
            };
        }).relay(EVENT.REQUEST.WRITE_SETTING);

        domain.incoming(EVENT.RESPONSE.TO.WRITE_SETTING.OK).forward(EVENT.BROADCAST.SETTING_WRITTEN);
        domain.incoming(EVENT.RESPONSE.TO.WRITE_AQS_INFO.OK).forward(EVENT.BROADCAST.AQS_INFO_WRITTEN);
        domain.incoming(EVENT.RESPONSE.TO.WRITE_AQS_GENERAL_SETTING.OK).forward(EVENT.BROADCAST.AQS_GENERAL_SETTING_WRITTEN);
        domain.incoming(EVENT.RESPONSE.TO.WRITE_AQS_CALIBRATION_SETTING.OK).forward(EVENT.BROADCAST.AQS_CALIBRATION_SETTING_WRITTEN);

        domain.incoming(EVENT.ON.AQS_REDISCOVERY).forward(EVENT.DO.REDISCOVER_AQS);
        domain.outgoing(EVENT.ON.CHANGE_AQS_ALIAS).emit((alias) => {
            return {
                editable: {
                    alias
                }
            };
        });
        domain.incoming(EVENT.ON.CHANGE_AQS_LED_BRIGHTNESS_LEVEL).handle((ledBrightnessLvl) => {
            return {
                editable: {
                    ledBrightnessLvl
                }
            };
        }).relay(EVENT.DO.MUTATE_AQS_GENERAL_SETTING);
        domain.incoming(EVENT.ON.CHANGE_AQS_SAMPLE_COUNT).handle((aqSampleCount) => {
            return {
                editable: {
                    aqSampleCount
                }
            };
        }).relay(EVENT.DO.MUTATE_AQS_GENERAL_SETTING);
        domain.incoming(EVENT.ON.CHANGE_AQS_SAMPLE_FREQUENCY).handle((mInterval) => {
            return {
                editable: {
                    mInterval
                }
            };
        }).relay(EVENT.DO.MUTATE_AQS_GENERAL_SETTING);
        domain.incoming(EVENT.ON.CANCEL_AQS_INFO_CHANGES).forward(EVENT.DO.RESTORE_AQS_INFO);
        domain.incoming(EVENT.ON.CANCEL_AQS_SETTING_CHANGES).forward(
            EVENT.DO.RESTORE_AQS_GENERAL_SETTING,
            EVENT.DO.RESTORE_AQS_CALIBRATION_SETTING
        );
        domain.incoming(EVENT.ON.APPLY_AQS_INFO_CHANGES).handle(() => {
        });
        domain.incoming(EVENT.ON.APPLY_AQS_SETTING_CHANGES).handle(() => {
        });
        domain.incoming(
            EVENT.RESPONSE.TO.READ_SETTING.NOT_FOUND,
            EVENT.RESPONSE.TO.READ_SETTING.ERROR,
            EVENT.RESPONSE.TO.READ_AQS_INFO.NOT_FOUND,
            EVENT.RESPONSE.TO.READ_AQS_INFO.ERROR,
            EVENT.RESPONSE.TO.READ_AQS_GENERAL_SETTING.NOT_FOUND,
            EVENT.RESPONSE.TO.READ_AQS_GENERAL_SETTING.ERROR,
            EVENT.RESPONSE.TO.READ_AQS_CALIBRATION_SETTING.NOT_FOUND,
            EVENT.RESPONSE.TO.READ_AQS_CALIBRATION_SETTING.ERROR
        ).handle(() => {
            return {
                visible: true,
                title: `App Storage`,
                message: `Unable To Read Data From Storage.`
            };
        }).relay(EVENT.BROADCAST.SETTING_ALERT);
        domain.incoming(
            EVENT.RESPONSE.TO.WRITE_SETTING.ERROR,
            EVENT.RESPONSE.TO.WRITE_AQS_INFO.ERROR,
            EVENT.RESPONSE.TO.WRITE_AQS_GENERAL_SETTING.ERROR,
            EVENT.RESPONSE.TO.WRITE_AQS_CALIBRATION_SETTING.ERROR
        ).handle(() => {
            return {
                visible: true,
                title: `App Storage`,
                message: `Unable To Write Data To Storage.`
            };
        }).relay(EVENT.BROADCAST.SETTING_ALERT);

        done();
    },
    teardown: function teardown (done) {
        done();
    }
});
export default SettingDomain;
