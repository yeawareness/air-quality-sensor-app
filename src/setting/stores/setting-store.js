/**
 * Copyright (c) 2017-present, Virida YEAwareness, Org. All rights reserved.
 *
 *------------------------------------------------------------------------
 *
 * @module SettingStore
 * @description - Virida client-native app setting store.
 *
 * @author Tuan Le (tuan.t.lei@gmail.com)
 *
 *------------------------------------------------------------------------
 * @flow
 */
'use strict'; // eslint-disable-line

import { Hf } from 'hyperflow';

import EVENT from '../events/setting-event';

const SettingStore = Hf.Store.augment({
    state: {
        status: {
            value: {
                active: false,
                idle: false
            }
        },
        showIntro: {
            value: true
        },
        mapTheme: {
            value: `flat`,
            oneOf: [ `lite`, `flat`, `retro` ]
        },
        notification: {
            value: {
                unhealthyAQAlert: true,
                dailyAQAlert: true
            }
        },
        aqs: {
            value: {
                info: {
                    docked: false,
                    mId: ``,
                    alias: `Virida Sensor`,
                    rev: ``,
                    skew: ``,
                    firmwareVersion: ``,
                    cDatestamp: ``,
                    editable: {
                        alias: `Virida Sensor`
                    }
                },
                setting: {
                    general: {
                        outdoor: false,
                        ledBrightnessLvl: 20,
                        aqSampleCount: 4,
                        mInterval: 28,
                        editable: {
                            ledBrightnessLvl: 20,
                            aqSampleCount: 4,
                            mInterval: 28
                        }
                    },
                    calibration: {
                        pm25: {
                            lowLvlMv: 600,
                            lowLvlTargetThreshold: 0.5
                        },
                        voc: {
                            ro: 400000,
                            refCOPPM: 0.2,
                            refCOPPMTargetWindow: 0.5
                        },
                        editable: {
                            pm25: {
                                lowLvlTargetThreshold: 0.5
                            },
                            voc: {
                                refCOPPM: 0.2,
                                refCOPPMTargetWindow: 0.5
                            }
                        }
                    }
                }
            }
        },
        aqsInfoChanged: {
            computable: {
                contexts: [
                    `aqs`
                ],
                compute () {
                    return this.aqs.info.alias !== this.aqs.info.editable.alias;
                }
            }
        },
        aqsGeneralSettingChanged: {
            computable: {
                contexts: [
                    `aqs`
                ],
                compute () {
                    return this.aqs.setting.general.ledBrightnessLvl !== this.aqs.setting.general.editable.ledBrightnessLvl ||
                           this.aqs.setting.general.aqSampleCount !== this.aqs.setting.general.editable.aqSampleCount ||
                           this.aqs.setting.general.mInterval !== this.aqs.setting.general.editable.mInterval;
                }
            }
        },
        aqsCalibrationSettingChanged: {
            computable: {
                contexts: [
                    `aqs`
                ],
                compute () {
                    return this.aqs.setting.calibration.pm25.lowLvlTargetThreshold !== this.aqs.setting.calibration.editable.pm25.lowLvlTargetThreshold ||
                           this.aqs.setting.calibration.voc.refCOPPM !== this.aqs.setting.calibration.editable.voc.refCOPPM ||
                           this.aqs.setting.calibration.voc.refCOPPMTargetWindow !== this.aqs.setting.calibration.editable.voc.refCOPPMTargetWindow;
                }
            }
        }
    },
    setup: function setup (done) {
        const store = this;
        store.incoming(EVENT.DO.SETTING_RESET).handle(() => {
            store.reconfig({
                status: {
                    active: false,
                    idle: false
                },
                showIntro: true,
                mapTheme: `flat`,
                notification: {
                    dailyAQAlert: true,
                    unhealthyAQAlert: true
                },
                aqs: {
                    info: {
                        docked: false,
                        mId: ``,
                        alias: `Virida Sensor`,
                        rev: ``,
                        skew: ``,
                        firmwareVersion: ``,
                        cDatestamp: ``,
                        editable: {
                            alias: `Virida Sensor`
                        }
                    },
                    setting: {
                        general: {
                            outdoor: false,
                            ledBrightnessLvl: 20,
                            aqSampleCount: 4,
                            mInterval: 28,
                            editable: {
                                ledBrightnessLvl: 20,
                                aqSampleCount: 4,
                                mInterval: 28
                            }
                        },
                        calibration: {
                            pm25: {
                                lowLvlMv: 600,
                                lowLvlTargetThreshold: 0.5
                            },
                            voc: {
                                ro: 400000,
                                refCOPPM: 0.2,
                                refCOPPMTargetWindow: 0.5
                            },
                            editable: {
                                pm25: {
                                    lowLvlTargetThreshold: 0.5
                                },
                                voc: {
                                    refCOPPM: 0.2,
                                    refCOPPMTargetWindow: 0.5
                                }
                            }
                        }
                    }
                }
            }, {
                suppressMutationEvent: true
            });
        });
        store.incoming(EVENT.DO.MUTATE_SETTING_STATUS).handle((status) => {
            if (store.reduce({
                status
            }, {
                suppressMutationEvent: true
            })) {
                if (store.status.active && !store.status.idle) {
                    store.outgoing(EVENT.AS.SETTING_ACTIVATED).emit();
                } else {
                    store.outgoing(EVENT.AS.SETTING_DEACTIVATED).emit();
                }
            }
        });
        store.incoming(EVENT.DO.MUTATE_SHOW_INTRO).handle((mutateShowIntro) => {
            if (store.reduce(mutateShowIntro, {
                suppressMutationEvent: !store.status.active || store.status.idle
            })) {
                store.outgoing(EVENT.AS.SHOW_INTRO_MUTATED).emit(() => store.showIntro);
            }
        });
        store.incoming(EVENT.DO.MUTATE_NOTIFICATION).handle((mutateNotification) => {
            if (store.reduce(mutateNotification, {
                suppressMutationEvent: !store.status.active || store.status.idle
            })) {
                store.outgoing(EVENT.AS.NOTIFICATION_MUTATED).emit(() => store.notification);
            }
        });
        store.incoming(EVENT.DO.RESTORE_AQS_INFO).handle(() => {
            store.reduce({
                aqs: {
                    info: {
                        editable: {
                            alias: store.aqs.info.alias
                        }
                    }
                }
            }, {
                suppressMutationEvent: !store.status.active || store.status.idle
            });
        });
        store.incoming(EVENT.DO.MUTATE_AQS_INFO).handle((aqsInfo) => {
            if (store.reduce({
                aqs: {
                    info: aqsInfo
                }
            }, {
                suppressMutationEvent: !store.status.active || store.status.idle
            })) {
                store.outgoing(EVENT.AS.AQS_INFO_MUTATED).emit(() => aqsInfo);
            }
        });
        store.incoming(EVENT.DO.RESTORE_AQS_GENERAL_SETTING).handle(() => {
            store.reduce({
                aqs: {
                    setting: {
                        general: {
                            editable: {
                                ledBrightnessLvl: store.aqs.setting.general.ledBrightnessLvl,
                                aqSampleCount: store.aqs.setting.general.aqSampleCount,
                                mInterval: store.aqs.setting.general.mInterval
                            }
                        }
                    }
                }
            }, {
                suppressMutationEvent: !store.status.active || store.status.idle
            });
        });
        store.incoming(EVENT.DO.MUTATE_AQS_GENERAL_SETTING).handle((aqsGeneralSetting) => {
            if (store.reduce({
                aqs: {
                    setting: {
                        general: aqsGeneralSetting
                    }
                }
            }, {
                suppressMutationEvent: !store.status.active || store.status.idle
            })) {
                store.outgoing(EVENT.AS.AQS_GENERAL_SETTING_MUTATED).emit(() => aqsGeneralSetting);
            }
        });
        store.incoming(EVENT.DO.RESTORE_AQS_CALIBRATION_SETTING).handle(() => {
            store.reduce({
                aqs: {
                    setting: {
                        calibration: {
                            editable: {
                                pm25: {
                                    lowLvlTargetThreshold: store.aqs.setting.calibration.pm25.lowLvlTargetThreshold
                                },
                                voc: {
                                    refCOPPM: store.aqs.setting.calibration.voc.refCOPPM,
                                    refCOPPMTargetWindow: store.aqs.setting.calibration.voc.refCOPPMTargetWindow
                                }
                            }
                        }
                    }
                }
            }, {
                suppressMutationEvent: !store.status.active || store.status.idle
            });
        });
        store.incoming(EVENT.DO.MUTATE_AQS_CALIBRATION_SETTING).handle((aqsCalibrationSetting) => {
            if (store.reduce({
                aqs: {
                    setting: {
                        calibration: aqsCalibrationSetting
                    }
                }
            }, {
                suppressMutationEvent: !store.status.active || store.status.idle
            })) {
                store.outgoing(EVENT.AS.AQS_CALIBRATION_SETTING_MUTATED).emit(() => aqsCalibrationSetting);
            }
        });
        done();
    },
    teardown: function teardown (done) {
        done();
    }
});
export default SettingStore;
