/**
 * Copyright (c) 2017-present, Virida YEAwareness, Org. All rights reserved.
 *
 *------------------------------------------------------------------------
 *
 * @description - Boki client native app map event ids.
 *
 * @author Tuan Le (tuan.t.lei@gmail.com)
 *
 *------------------------------------------------------------------------
 * @flow
 */
'use strict'; // eslint-disable-line

import { Hf } from 'hyperflow';

const mapEventMap = {
    onEvents: [
        `changing-to-new-region`,
        `search-new-region`,
        `go-to-home-region`,
        `refresh-aqr-site-data`,
        `select-aqr-site`,
        `deselect-aqr-site`,
        `show-aq-actionable-tip-modal`,
        `close-aq-actionable-tip-modal`,
        `show-aq-whatis-modal`,
        `close-aq-whatis-modal`
    ],
    asEvents: [
        `map-activated`,
        `map-deactivated`,
        `region-mutated`
    ],
    doEvents: [
        `map-reset`,
        `map-activation`,
        `mutate-map-status`,
        `mutate-region`,
        `mutate-region-with-suppression`,
        `mutate-aqr-info`,
        `mutate-aqr-sites`,
        `mutate-selected-aqr-site`,
        `mutate-aq-actionable-tip`,
        `mutate-aq-whatis`
    ],
    broadcastEvents: [
        `run-mode`,
        `periodic-refresh`,

        `map-alert`
    ],
    requestEvents: [
        `aqr-info`,
        `aqicn-aqr-site-data`
    ]
};

export default Hf.Event.create(mapEventMap);
