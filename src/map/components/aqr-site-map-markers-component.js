/**
 * Copyright (c) 2017-present, Virida YEAwareness, Org. All rights reserved.
 *
 *------------------------------------------------------------------------
 *
 * @module AQRSiteMapMarkersComponent
 * @description - Virida client-native app air quality region site map markers wrapper component.
 *
 * @author Tuan Le (tuan.t.lei@gmail.com)
 *
 *------------------------------------------------------------------------
 * @flow
 */
'use strict'; // eslint-disable-line

import { Hf } from 'hyperflow';

import { Ht } from 'hypertoxin';

import React from 'react';

import ReactNative from 'react-native'; // eslint-disable-line

import PropTypes from 'prop-types';

import MapView from 'react-native-maps';

const {
    Component
} = React;

const {
    ImageBackground
} = ReactNative;

const {
    Marker
} = MapView;

const {
    TitleText
} = Ht.Text;

class TrackedMarker extends Component {
    static propTypes = {
        groupTracking: PropTypes.bool,
        aqrSite: PropTypes.shape({
            info: PropTypes.shape({
                code: PropTypes.string,
                name: PropTypes.string,
                latitude: PropTypes.number,
                longitude: PropTypes.number
            }),
            aqSample: PropTypes.shape({
                aqParam: PropTypes.string,
                aqi: PropTypes.number,
                aqUnit: PropTypes.string,
                aqConcentration: PropTypes.number,
                aqAlertIndex: PropTypes.number,
                aqAlertMessage: PropTypes.string
            })
        }),
        onPress: PropTypes.func
    }
    static defaultProps = {
        groupTracking: false,
        aqrSite: {
            info: {
                code: ``,
                name: ``,
                latitude: 0,
                longitude: 0
            },
            aqSample: {
                aqParam: ``,
                aqi: 0,
                aqUnit: ``,
                aqConcentration: 0,
                aqAlertIndex: 0,
                aqAlertMessage: ``
            }
        },
        onPress: () => null
    }
    constructor (props) {
        super(props);
        this.state = {
            tracking: props.groupTracking
        };
    }
    componentWillReceiveProps (nextProps) {
        const component = this;
        if (nextProps.groupTracking) {
            component.setState(() => {
                return {
                    tracking: true
                };
            });
        }
    }
    componentDidUpdate () {
        const component = this;
        const {
            tracking
        } = component.state;
        if (tracking) {
            component.setState(() => {
                return {
                    tracking: false
                };
            });
        }
    }
    shouldComponentUpdate (nextProps, nextState) {
        const component = this;
        const {
            tracking
        } = component.state;
        return tracking || nextState.tracking;
    }
    render () {
        const component = this;
        const {
            aqrSite,
            onPress
        } = component.props;
        const {
            tracking
        } = component.state;
        return (
            <Marker
                identifier = { aqrSite.info.code }
                flat = { true }
                tracksViewChanges = { tracking }
                stopPropagation = { true }
                coordinate = {{
                    latitude: aqrSite.info.latitude,
                    longitude: aqrSite.info.longitude
                }}
                onPress = {() => {
                    onPress(aqrSite);
                }}
            >
                {
                    !tracking ? <ImageBackground
                        style = {{
                            ...Ht.Theme.general.dropShadow.shallow,
                            flexDirection: `column`,
                            justifyContent: `center`,
                            alignItems: `center`,
                            width: 48,
                            height: 36,
                            paddingBottom: 6
                        }}
                        resizeMode = 'stretch'
                        source = { Ht.Theme.imageSource.markers[aqrSite.aqSample.aqAlertIndex] }
                    >
                        <TitleText
                            color = { Ht.Theme.palette.white }
                            size = 'small'
                        >{ aqrSite.aqSample.aqi }</TitleText>
                    </ImageBackground> : null
                }
            </Marker>
        );
    }
}

export default class AQRSiteMapMarkersComponent extends Component {
    static propTypes = {
        updating: PropTypes.bool,
        aqrSites: PropTypes.array,
        onPress: PropTypes.func
    }
    static defaultProps = {
        updating: false,
        aqrSites: [],
        onPress: () => null
    }
    constructor (props) {
        super(props);
        this.state = {
            groupTracking: props.updating
        };
    }
    componentWillReceiveProps (nextProps) {
        const component = this;
        if (!Hf.isEmpty(nextProps.aqrSites) && nextProps.updating) {
            component.setState(() => {
                return {
                    groupTracking: true
                };
            });
        }
    }
    componentDidUpdate () {
        const component = this;
        const {
            groupTracking
        } = component.state;
        if (groupTracking) {
            component.setState(() => {
                return {
                    groupTracking: false
                };
            });
        }
    }
    shouldComponentUpdate (nextProps, nextState) {
        const component = this;
        const {
            groupTracking
        } = component.state;
        return groupTracking || nextState.groupTracking;
    }
    render () {
        const component = this;
        const {
            aqrSites,
            onPress
        } = component.props;
        const {
            groupTracking
        } = component.state;
        Hf.log(`info1`, `Rendering ${aqrSites.length} map markers.`);
        return aqrSites.map((aqrSite, index) => {
            if (aqrSite.aqSample.aqParam === `pm25`) {
                return (
                    <TrackedMarker
                        key = { `${index}` }
                        groupTracking = { groupTracking }
                        aqrSite = { aqrSite }
                        onPress = { onPress }
                    />
                );
            } else {
                return null;
            }
        });
    }
}
