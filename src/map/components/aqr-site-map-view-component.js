/**
 * Copyright (c) 2017-present, Virida YEAwareness, Org. All rights reserved.
 *
 *------------------------------------------------------------------------
 *
 * @module AQRSiteMapViewComponent
 * @description - Virida client-native app air quality region site map view wrapper component.
 *
 * @author Tuan Le (tuan.t.lei@gmail.com)
 *
 *------------------------------------------------------------------------
 * @flow
 */
'use strict'; // eslint-disable-line

import { Hf } from 'hyperflow';

import { Ht } from 'hypertoxin';

import React from 'react';

import ReactNative from 'react-native'; // eslint-disable-line

import PropTypes from 'prop-types';

import SuperCluster from 'supercluster';

import MapView, { PROVIDER_GOOGLE } from 'react-native-maps';

import AQRSiteMapMarkers from './aqr-site-map-markers-component';

import MapUtilsComposite from '../composites/map-utils-composite';

import AQAlertComposite from '../../common/composites/aq-alert-composite';

import CONSTANT from '../../common/constant';

const {
    Component
} = React;

const {
    getRegionBBox
} = MapUtilsComposite.getTemplate();

const {
    getAQIAlert
} = AQAlertComposite.getTemplate();

const aqrSiteCluster = SuperCluster({
    minZoom: CONSTANT.MAP.MIN_ZOOM_LEVEL,
    maxZoom: CONSTANT.MAP.MAX_ZOOM_LEVEL,
    radius: CONSTANT.MAP.MARKER_CLUSTERING.RADIUS_PIXEL,
    extent: CONSTANT.MAP.MARKER_CLUSTERING.EXTENT,
    nodeSize: CONSTANT.MAP.MARKER_CLUSTERING.NODE_SIZE,
    initial: () => {
        return {
            info: {
                code: ``,
                name: ``,
                clusterCount: 0,
                latitude: 0,
                longitude: 0
            },
            aqSample: {
                aqParam: ``,
                aqUnit: ``,
                aqi: 0,
                aqiSum: 0,
                aqConcentration: 0,
                aqConcentrationSum: 0
            }
        };
    },
    reduce: (aqrSiteAccumulated, aqrSite) => {
        aqrSiteAccumulated.info.clusterCount++;
        if (aqrSiteAccumulated.info.clusterCount > 1) {
            if (aqrSiteAccumulated.info.code.indexOf(aqrSite.info.code) === -1) {
                aqrSiteAccumulated.info.code = `${aqrSite.info.code}-${aqrSiteAccumulated.info.code}`;
                aqrSiteAccumulated.info.name = `${aqrSite.info.name},\n${aqrSiteAccumulated.info.name}`;
            }
            aqrSiteAccumulated.aqSample.aqiSum += aqrSite.aqSample.aqi;
            aqrSiteAccumulated.aqSample.aqConcentrationSum += aqrSite.aqSample.aqConcentration;
            aqrSiteAccumulated.aqSample.aqi = parseInt(Math.round(aqrSiteAccumulated.aqSample.aqiSum / aqrSiteAccumulated.info.clusterCount), 10);
            aqrSiteAccumulated.aqSample.aqConcentration = aqrSiteAccumulated.aqSample.aqConcentrationSum / aqrSiteAccumulated.info.clusterCount;
        } else {
            aqrSiteAccumulated.info.code = aqrSite.info.code;
            aqrSiteAccumulated.info.name = aqrSite.info.name;
            aqrSiteAccumulated.aqSample.aqi = aqrSite.aqSample.aqi;
            aqrSiteAccumulated.aqSample.aqiSum = aqrSite.aqSample.aqi;
            aqrSiteAccumulated.aqSample.aqConcentration = aqrSite.aqSample.aqConcentration;
            aqrSiteAccumulated.aqSample.aqConcentrationSum = aqrSite.aqSample.aqConcentration;
        }
        aqrSiteAccumulated.aqSample.aqParam = aqrSite.aqSample.aqParam;
        aqrSiteAccumulated.aqSample.aqUnit = aqrSite.aqSample.aqUnit;
    }
});

export default class AQRSiteMapViewComponent extends Component {
    static propTypes = {
        locked: PropTypes.bool,
        initialRegion: PropTypes.shape({
            latitude: PropTypes.number,
            longitude: PropTypes.number,
            latitudeDelta: PropTypes.number,
            longitudeDelta: PropTypes.number
        }),
        aqrSites: PropTypes.array,
        onPress: PropTypes.func,
        onPressAQRSiteMarker: PropTypes.func,
        onRegionUpdated: PropTypes.func
    }
    static defaultProps = {
        locked: false,
        initialRegion: {
            latitude: 0,
            longitude: 0,
            latitudeDelta: 0,
            longitudeDelta: 0
        },
        aqrSites: [],
        onPress: () => null,
        onPressAQRSiteMarker: () => null,
        onRegionUpdated: () => null
    }
    constructor (props) {
        super(props);
        this.mapView = null;
        this.aqrSiteClusterIndex = null;
        this.state = {
            ready: false,
            updating: false,
            initialRegionChangeCompleted: false,
            selectedAQRSite: null,
            clusteredAQRSites: []
        };
    }
    getAQRSiteClusteredMapMarkers = (region) => {
        const component = this;
        if (CONSTANT.MAP.MARKER_CLUSTERING.ENABLED && component.aqrSiteClusterIndex !== null) {
            const clusterRegionBBox = getRegionBBox(region, CONSTANT.MAP.MARKER_CLUSTERING.REGION_BBOX_DELTA_PADDING, true);
            return component.aqrSiteClusterIndex.getClusters([
                clusterRegionBBox.southwest.longitude,
                clusterRegionBBox.southwest.latitude,
                clusterRegionBBox.northeast.longitude,
                clusterRegionBBox.northeast.latitude
            ], clusterRegionBBox.zoomLvl).map((aqrClusteredSite) => {
                const [
                    aqrSiteLongitude,
                    aqrSiteLatitude
                ] = aqrClusteredSite.geometry.coordinates;
                const aqrSite = aqrClusteredSite.properties;
                const aqAlert = getAQIAlert(aqrSite.aqSample.aqi);
                return {
                    info: {
                        ...aqrSite.info,
                        latitude: aqrSiteLatitude,
                        longitude: aqrSiteLongitude
                    },
                    aqSample: {
                        ...aqrSite.aqSample,
                        aqAlertIndex: aqAlert.index,
                        aqAlertMessage: aqAlert.message
                    }
                };
            });
        } else {
            return [];
        }
    }
    onMapReady = () => {
        const component = this;
        const {
            ready
        } = component.state;
        if (!ready) {
            component.setState(() => {
                return {
                    ready: true
                };
            }, () => {
                Hf.log(`info1`, `Map is ready.`);
            });
        }
    }
    onRegionChangeComplete = (newRegion) => {
        const component = this;
        const {
            locked,
            onRegionUpdated
        } = component.props;
        const {
            ready,
            updating,
            initialRegionChangeCompleted
        } = component.state;
        if (ready && !locked && !updating) {
            if (initialRegionChangeCompleted) {
                if (CONSTANT.MAP.MARKER_CLUSTERING.ENABLED && component.aqrSiteClusterIndex !== null) {
                    component.setState(() => {
                        return {
                            updating: true,
                            clusteredAQRSites: component.getAQRSiteClusteredMapMarkers(newRegion)
                        };
                    }, () => {
                        onRegionUpdated(newRegion);
                        Hf.log(`info1`, `Map markers clustering.`);
                    });
                } else {
                    component.setState(() => {
                        return {
                            updating: false
                        };
                    }, () => {
                        onRegionUpdated(newRegion);
                    });
                }
                Hf.log(`info1`, `Map finished changing to new region.`);
            } else {
                component.setState(() => {
                    return {
                        updating: false,
                        initialRegionChangeCompleted: true
                    };
                });
            }
        }
    }
    onPress = () => {
        const component = this;
        const {
            onPress
        } = component.props;
        onPress();
    }
    onAQRSiteMapMarkerPress = (aqrSite) => {
        const component = this;
        const {
            onPressAQRSiteMarker
        } = component.props;
        component.setState(() => {
            return {
                selectedAQRSite: aqrSite
            };
        }, () => {
            onPressAQRSiteMarker(aqrSite);
        });
    }
    componentWillMount () {
        const component = this;
        const {
            initialRegion,
            aqrSites
        } = component.props;
        if (CONSTANT.MAP.MARKER_CLUSTERING.ENABLED) {
            component.aqrSiteClusterIndex = aqrSiteCluster.load(aqrSites.map((aqrSite) => {
                return {
                    type: `Feature`,
                    geometry: {
                        type: `Point`,
                        coordinates: [
                            aqrSite.info.longitude,
                            aqrSite.info.latitude
                        ]
                    },
                    properties: {
                        info: {
                            ...aqrSite.info,
                            clusterCount: 0
                        },
                        aqSample: {
                            ...aqrSite.aqSample,
                            aqiSum: 0,
                            aqConcentrationSum: 0
                        }
                    }
                };
            }));
            component.setState(() => {
                return {
                    updating: true,
                    clusteredAQRSites: component.getAQRSiteClusteredMapMarkers(initialRegion)
                };
            }, () => {
                Hf.log(`info1`, `Map markers clustering.`);
            });
        } else {
            component.setState(() => {
                return {
                    updating: true
                };
            });
        }
    }
    componentWillReceiveProps (nextProps) {
        const component = this;
        if (CONSTANT.MAP.MARKER_CLUSTERING.ENABLED) {
            component.aqrSiteClusterIndex = aqrSiteCluster.load(nextProps.aqrSites.map((aqrSite) => {
                return {
                    type: `Feature`,
                    geometry: {
                        type: `Point`,
                        coordinates: [
                            aqrSite.info.longitude,
                            aqrSite.info.latitude
                        ]
                    },
                    properties: {
                        info: {
                            ...aqrSite.info,
                            clusterCount: 0
                        },
                        aqSample: {
                            ...aqrSite.aqSample,
                            aqiSum: 0,
                            aqConcentrationSum: 0
                        }
                    }
                };
            }));
            component.setState(() => {
                return {
                    updating: true,
                    clusteredAQRSites: component.getAQRSiteClusteredMapMarkers(nextProps.initialRegion)
                };
            }, () => {
                component.mapView.animateToRegion(nextProps.initialRegion, 300);
                Hf.log(`info1`, `Map is updating and map markers clustering.`);
            });
        } else {
            component.setState(() => {
                return {
                    updating: true
                };
            }, () => {
                component.mapView.animateToRegion(nextProps.initialRegion, 300);
                Hf.log(`info1`, `Map is updating.`);
            });
        }
    }
    shouldComponentUpdate (nextProps, nextState) {
        const component = this;
        const {
            updating
        } = component.state;
        return updating || nextState.updating;
    }
    componentWillUpdate () {
        const component = this;
        const {
            selectedAQRSite
        } = component.state;
        if (Hf.isObject(selectedAQRSite)) {
            component.mapView.animateToCoordinate({
                latitude: selectedAQRSite.info.latitude,
                longitude: selectedAQRSite.info.longitude
            }, 300);
        }
    }
    componentDidUpdate () {
        const component = this;
        const {
            updating
        } = component.state;
        if (updating) {
            component.setState(() => {
                return {
                    updating: false,
                    selectedAQRSite: null
                };
            }, () => {
                Hf.log(`info1`, `Map updated.`);
            });
        } else {
            component.setState(() => {
                return {
                    selectedAQRSite: null
                };
            });
        }
    }
    componentWillUnmount () {
        const component = this;
        component.mapView = null;
        component.aqrSiteClusterIndex = null;
    }
    render () {
        const component = this;
        const {
            locked,
            initialRegion,
            aqrSites
        } = component.props;
        const {
            ready,
            updating,
            clusteredAQRSites
        } = component.state;
        if (CONSTANT.MAP.USE_GOOGLE_MAP) {
            return (
                <MapView
                    ref = {(mapView) => {
                        component.mapView = mapView;
                    }}
                    style = {{
                        position: `absolute`,
                        top: 0,
                        left: 0,
                        right: 0,
                        bottom: -28,
                        height: CONSTANT.GENERAL.DEVICE_HEIGHT + 28
                    }}
                    mapPadding = {{
                        top: 6,
                        right: 6,
                        bottom: 6,
                        left: 6
                    }}
                    customMapStyle = { Ht.Theme.general.googleMap.lite }
                    showsUserLocation = { false }
                    showsMyLocationButton = { false }
                    followsUserLocation = { false }
                    cacheEnabled = { false }
                    showsIndoors = { false }
                    toolbarEnabled = { false }
                    pitchEnabled = { false }
                    rotateEnabled = { false }
                    scrollEnabled = { !ready || !locked }
                    zoomEnabled = { !ready || !locked }
                    minZoomLevel = { CONSTANT.MAP.MIN_ZOOM_LEVEL }
                    maxZoomLevel = { CONSTANT.MAP.MAX_ZOOM_LEVEL }
                    provider = { PROVIDER_GOOGLE }
                    mapType = 'standard'
                    initialRegion = { initialRegion }
                    onPress = { component.onPress }
                    onMapReady = { component.onMapReady }
                    onRegionChange = { component.onRegionChange }
                    onRegionChangeComplete = { component.onRegionChangeComplete }
                >
                    {
                        <AQRSiteMapMarkers
                            updating = { !ready || updating }
                            aqrSites = { CONSTANT.MAP.MARKER_CLUSTERING.ENABLED ? clusteredAQRSites : aqrSites }
                            onPress = { component.onAQRSiteMapMarkerPress }
                        />
                    }
                </MapView>
            );
        } else {
            return (
                <MapView
                    ref = {(mapView) => {
                        component.mapView = mapView;
                    }}
                    style = {{
                        position: `absolute`,
                        top: 0,
                        left: 0,
                        right: 0,
                        bottom: -28,
                        height: CONSTANT.GENERAL.DEVICE_HEIGHT + 28
                    }}
                    mapPadding = {{
                        top: 6,
                        right: 6,
                        bottom: 6,
                        left: 6
                    }}
                    customMapStyle = { Ht.Theme.general.googleMap.lite }
                    showsUserLocation = { false }
                    showsMyLocationButton = { false }
                    followsUserLocation = { false }
                    cacheEnabled = { false }
                    showsIndoors = { false }
                    toolbarEnabled = { false }
                    pitchEnabled = { false }
                    rotateEnabled = { false }
                    scrollEnabled = { !ready || !locked }
                    zoomEnabled = { !ready || !locked }
                    minZoomLevel = { CONSTANT.MAP.MIN_ZOOM_LEVEL }
                    maxZoomLevel = { CONSTANT.MAP.MAX_ZOOM_LEVEL }
                    provider = { null }
                    mapType = 'standard'
                    initialRegion = { initialRegion }
                    region = { initialRegion }
                    onPress = { component.onPress }
                    onMapReady = { component.onMapReady }
                    onRegionChange = { component.onRegionChange }
                    onRegionChangeComplete = { component.onRegionChangeComplete }
                >
                    {
                        <AQRSiteMapMarkers
                            updating = { !ready || updating }
                            aqrSites = { CONSTANT.MAP.MARKER_CLUSTERING.ENABLED ? clusteredAQRSites : aqrSites }
                            onPress = { component.onAQRSiteMapMarkerPress }
                        />
                    }
                </MapView>
            );
        }
    }
}
