/**
 * Copyright (c) 2017-present, Virida YEAwareness, Org. All rights reserved.
 *
 *------------------------------------------------------------------------
 *
 * @module MapUtilsComposite
 * @description - Virida client-native map utils composite.
 *
 * @author Tuan Le (tuan.t.lei@gmail.com)
 *
 *------------------------------------------------------------------------
 * @flow
 */
'use strict'; // eslint-disable-line

import { Hf } from 'hyperflow';

export default Hf.Composite({
    template: {
        isInsideRegionBBox: function isInsideRegionBBox (regionBBox, coordinate) {
            let longRange = false;
            if (regionBBox.northeast.longitude < regionBBox.southwest.longitude) {
                longRange = coordinate.longitude >= regionBBox.southwest.longitude || coordinate.longitude <= regionBBox.northeast.longitude;
            } else {
                longRange = coordinate.longitude >= regionBBox.southwest.longitude && coordinate.longitude <= regionBBox.northeast.longitude;
            }
            return coordinate.latitude >= regionBBox.southwest.latitude && coordinate.latitude <= regionBBox.northeast.latitude && longRange;
        },
        getRegionZoomLevel: function getRegionZoomLevel (regionLongitudeDelta) {
            return Math.round(Math.log(360 / regionLongitudeDelta) / Math.LN2);
        },
        getRegionBBox: function getRegionBBox (region, deltaPadding = 0, returnIncludesDeltas = false) {
            const latitudeDelta = region.latitudeDelta + deltaPadding;
            const longitudeDelta = region.longitudeDelta < 0 ? region.longitudeDelta + deltaPadding + 360 : region.longitudeDelta + deltaPadding;
            if (returnIncludesDeltas) {
                return {
                    northeast: {
                        latitude: region.latitude + latitudeDelta + deltaPadding,
                        longitude: region.longitude + longitudeDelta + deltaPadding
                    },
                    southwest: {
                        latitude: region.latitude - latitudeDelta,
                        longitude: region.longitude - longitudeDelta
                    },
                    zoomLvl: Math.round(Math.log(360 / longitudeDelta) / Math.LN2),
                    latitudeDelta,
                    longitudeDelta
                };
            } else {
                return {
                    northeast: {
                        latitude: region.latitude + latitudeDelta,
                        longitude: region.longitude + longitudeDelta
                    },
                    southwest: {
                        latitude: region.latitude - latitudeDelta,
                        longitude: region.longitude - longitudeDelta
                    }
                };
            }
        }
    }
});
