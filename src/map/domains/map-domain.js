/**
 * Copyright (c) 2017-present, Virida YEAwareness, Org. All rights reserved.
 *
 *------------------------------------------------------------------------
 *
 * @module MapDomain
 * @description - Virida client-native app setting domain.
 *
 * @author Tuan Le (tuan.t.lei@gmail.com)
 *
 *------------------------------------------------------------------------
 * @flow
 */
'use strict'; // eslint-disable-line

import { Hf } from 'hyperflow';

import moment from 'moment';

import ReactNative from 'react-native'; // eslint-disable-line

import BackgroundGeolocation from 'react-native-background-geolocation';

import MapStore from '../stores/map-store';

import MapViewInterface from '../interfaces/map-view-interface';

import AQRSitesAPIService from '../../common/services/aqr-sites-api-service';

import MapUtilsComposite from '../composites/map-utils-composite';

import CONSTANT from '../../common/constant';

import EVENT from '../events/map-event';

const {
    NetInfo
} = ReactNative;

const MapDomain = Hf.Domain.augment({
    composites: [
        MapUtilsComposite
    ],
    $init: function $init () {
        const domain = this;
        domain.register({
            store: MapStore({
                name: `map-store`
            }),
            intf: MapViewInterface({
                name: `map-view`
            }),
            services: [
                AQRSitesAPIService({
                    name: `aqr-sites-api`
                })
            ]
        });
    },
    setup: function setup (done) {
        const domain = this;
        domain.incoming(EVENT.DO.MAP_RESET).repeat();
        domain.incoming(EVENT.BROADCAST.RUN_MODE).handle((runMode) => {
            return {
                idle: runMode === `background-running`
            };
        }).relay(EVENT.DO.MUTATE_MAP_STATUS);
        domain.incoming(EVENT.DO.MAP_ACTIVATION).handle((active) => {
            return {
                active
            };
        }).relay(EVENT.DO.MUTATE_MAP_STATUS);
        domain.incoming(EVENT.AS.MAP_ACTIVATED).handle((aqrInfo) => {
            if (!aqrInfo.status.initialized) {
                BackgroundGeolocation.getCurrentPosition({
                    samples: 1,
                    persist: false
                }).then((location) => {
                    const {
                        coords: coordinate
                    } = location;
                    const initialRegion = {
                        latitude: coordinate.latitude,
                        longitude: coordinate.longitude,
                        latitudeDelta: CONSTANT.MAP.LATITUDE_DELTA,
                        longitudeDelta: CONSTANT.MAP.LONGITUDE_DELTA
                    };
                    domain.outgoing(EVENT.DO.MUTATE_REGION).emit(() => {
                        return {
                            ...initialRegion,
                            bbox: domain.getRegionBBox(initialRegion)
                        };
                    });
                }).catch(error => {
                    domain.outgoing(EVENT.BROADCAST.MAP_ALERT).emit(() => {
                        return {
                            visible: true,
                            title: `Air Quality Site Map Alert`,
                            message: `Unable to get device's geolocation. ${error.message}`
                        };
                    });
                    Hf.log(`warn1`, `MapDomain - Unable to get device's geolocation. ${error.message}`);
                });
            }
        });
        domain.incoming(EVENT.ON.DESELECT_AQR_SITE).handle(() => {
            return {
                info: {
                    code: ``,
                    name: ``,
                    latitude: 0,
                    longitude: 0,
                    clusterCount: 0
                },
                aqSample: {
                    aqParam: ``,
                    aqi: 0,
                    aqUnit: ``,
                    aqAlertMessage: ``,
                    aqAlertIndex: 0,
                    aqConcentration: 0,
                    aqiSum: 0,
                    aqConcentrationSum: 0
                }
            };
        }).relay(EVENT.DO.MUTATE_SELECTED_AQR_SITE);
        domain.incoming(EVENT.ON.SELECT_AQR_SITE).forward(EVENT.DO.MUTATE_SELECTED_AQR_SITE);
        domain.incoming(EVENT.BROADCAST.PERIODIC_REFRESH).handle(() => {
            NetInfo.isConnected.fetch().then((online) => {
                if (online) {
                    domain.outgoing(EVENT.REQUEST.AQR_INFO).emit();
                }
            });
        });
        domain.incoming(EVENT.RESPONSE.WITH.AQR_INFO).handle((aqrInfo) => {
            if (!aqrInfo.status.loading && aqrInfo.status.initialized) {
                domain.outgoing(EVENT.DO.MUTATE_AQR_INFO).emit(() => {
                    return {
                        status: {
                            loading: true
                        }
                    };
                });
                domain.outgoing(EVENT.REQUEST.AQICN_AQR_SITE_DATA).emit(() => {
                    return {
                        aqParams: [ `pm25` ],
                        timestamp: moment().format(),
                        regionBBox: aqrInfo.regionBBox
                    };
                });
                Hf.log(`info1`, `Periodic map regional air quality site data refreshing.`);
            }
        });
        domain.incoming(EVENT.ON.REFRESH_AQR_SITE_DATA).handle((aqrInfo) => {
            if (!aqrInfo.status.loading) {
                domain.outgoing(EVENT.DO.MUTATE_AQR_INFO).emit(() => {
                    return {
                        status: {
                            loading: true
                        }
                    };
                });
                domain.outgoing(EVENT.REQUEST.AQICN_AQR_SITE_DATA).emit(() => {
                    return {
                        aqParams: [ `pm25` ],
                        timestamp: moment().format(),
                        regionBBox: aqrInfo.regionBBox
                    };
                });
                Hf.log(`info1`, `Manual map regional air quality site data refreshing.`);
            }
        });
        domain.incoming(EVENT.ON.CHANGING_TO_NEW_REGION).handle((newRegion) => {
            return {
                ...newRegion,
                bbox: domain.getRegionBBox(newRegion)
            };
        }).relay(EVENT.DO.MUTATE_REGION_WITH_SUPPRESSION);
        domain.incoming(EVENT.ON.SEARCH_NEW_REGION).handle((newRegion) => {
            return {
                ...newRegion,
                bbox: domain.getRegionBBox(newRegion)
            };
        }).relay(EVENT.DO.MUTATE_REGION);
        domain.incoming(EVENT.ON.GO_TO_HOME_REGION).handle(() => {
            BackgroundGeolocation.getCurrentPosition({
                samples: 1,
                persist: false
            }).then((location) => {
                const {
                    coords: coordinate
                } = location;
                const homeRegion = {
                    latitude: coordinate.latitude,
                    longitude: coordinate.longitude,
                    latitudeDelta: CONSTANT.MAP.LATITUDE_DELTA,
                    longitudeDelta: CONSTANT.MAP.LONGITUDE_DELTA
                };
                domain.outgoing(EVENT.DO.MUTATE_REGION).emit(() => {
                    return {
                        ...homeRegion,
                        bbox: domain.getRegionBBox(homeRegion)
                    };
                });
            }).catch(error => {
                domain.outgoing(EVENT.BROADCAST.MAP_ALERT).emit(() => {
                    return {
                        visible: true,
                        title: `Air Quality Site Map Alert`,
                        message: `Unable to get device's geolocation. ${error.message}`
                    };
                });
                Hf.log(`warn1`, `MapDomain - Unable to get device's geolocation. ${error.message}`);
            });
        });
        domain.incoming(EVENT.AS.REGION_MUTATED).handle(({
            region,
            aqrInfo
        }) => {
            if (!aqrInfo.status.initialized) {
                domain.outgoing(EVENT.DO.MUTATE_AQR_INFO).emit(() => {
                    return {
                        status: {
                            initialized: true
                        }
                    };
                });
            }
            if (!(domain.isInsideRegionBBox(aqrInfo.regionBBox, region.bbox.northeast) &&
                  domain.isInsideRegionBBox(aqrInfo.regionBBox, region.bbox.southwest))) {
                const regionBBox = domain.getRegionBBox(region, CONSTANT.AQR.REGION_BBOX_DELTA_PADDING);
                domain.outgoing(EVENT.DO.MUTATE_AQR_INFO).emit(() => {
                    return {
                        regionBBox,
                        status: {
                            loading: true
                        }
                    };
                });
                domain.outgoing(EVENT.REQUEST.AQICN_AQR_SITE_DATA).emit(() => {
                    return {
                        aqParams: [ `pm25` ],
                        timestamp: moment().format(),
                        regionBBox
                    };
                });
                Hf.log(`info1`, `Updating to new map regional air quality sites.`);
            }
        });
        domain.incoming(EVENT.RESPONSE.TO.AQICN_AQR_SITE_DATA.OK).handle(({
            aqrInfo,
            aqrSites
        }) => {
            if (Hf.isNonEmptyArray(aqrSites)) {
                domain.outgoing(EVENT.DO.MUTATE_AQR_SITES).emit(() => aqrSites);
                domain.outgoing(EVENT.DO.MUTATE_AQR_INFO).emit(() => {
                    return {
                        ...aqrInfo,
                        status: {
                            loading: false,
                            availability: {
                                siteData: true
                            }
                        }
                    };
                });
                Hf.log(`info1`, `Received ${aqrSites.length} regional air quality site data from AQICN.`);
            } else {
                domain.outgoing(EVENT.DO.MUTATE_AQR_INFO).emit(() => {
                    return {
                        ...aqrInfo,
                        status: {
                            loading: false,
                            availability: {
                                siteData: false
                            }
                        }
                    };
                });
                Hf.log(`info1`, `Received no regional air quality site data from AQICN.`);
            }
        });
        domain.incoming(
            EVENT.RESPONSE.TO.AQICN_AQR_SITE_DATA.NOT_FOUND,
            EVENT.RESPONSE.TO.AQICN_AQR_SITE_DATA.TIMED_OUT,
            EVENT.RESPONSE.TO.AQICN_AQR_SITE_DATA.ERROR
        ).handle(() => {
            domain.outgoing(EVENT.DO.MUTATE_AQR_INFO).emit(() => {
                return {
                    status: {
                        loading: false,
                        availability: {
                            siteData: false
                        }
                    }
                };
            });
            domain.outgoing(EVENT.BROADCAST.MAP_ALERT).emit(() => {
                return {
                    visible: true,
                    title: `Air Quality Site Map Alert`,
                    message: `Unable to Retrieve Regional Air Quality Site Data From AQICN.`
                };
            });
            Hf.log(`warn1`, `MapDomain - Unable to receive regional air quality site data from AQICN.`);
        });
        domain.incoming(EVENT.ON.SHOW_AQ_ACTIONABLE_TIP_MODAL).forward(EVENT.DO.MUTATE_AQ_ACTIONABLE_TIP);
        domain.incoming(EVENT.ON.SHOW_AQ_WHATIS_MODAL).forward(EVENT.DO.MUTATE_AQ_WHATIS);
        domain.incoming(EVENT.ON.CLOSE_AQ_ACTIONABLE_TIP_MODAL).handle(() => {
            return {
                visible: false,
                aqSample: {
                    aqParam: ``,
                    aqAlertMessage: ``,
                    aqAlertIndex: 0,
                    aqi: 0
                }
            };
        }).relay(EVENT.DO.MUTATE_AQ_ACTIONABLE_TIP);
        domain.incoming(EVENT.ON.CLOSE_AQ_WHATIS_MODAL).handle(() => {
            return {
                visible: false,
                aqSample: {
                    aqParam: ``,
                    aqAlertIndex: 0,
                    aqi: 0,
                    aqConcentration: 0,
                    aqUnit: ``
                }
            };
        }).relay(EVENT.DO.MUTATE_AQ_WHATIS);
        done();
    },
    teardown: function teardown (done) {
        done();
    }
});
export default MapDomain;
