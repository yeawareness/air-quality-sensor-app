/**
 * Copyright (c) 2017-present, Virida YEAwareness, Org. All rights reserved.
 *
 *------------------------------------------------------------------------
 *
 * @module MapViewInterface
 * @description - Virida client-native app map view interface.
 *
 * @author Tuan Le (tuan.t.lei@gmail.com)
 *
 *------------------------------------------------------------------------
 * @flow
 */
'use strict'; // eslint-disable-line

import { Hf } from 'hyperflow';

import { Ht } from 'hypertoxin';

import moment from 'moment';

import React from 'react';

import ReactNative from 'react-native'; // eslint-disable-line

import GooglePlaces from 'react-native-google-places';

import AQRSiteMapView from '../components/aqr-site-map-view-component';

import AQActionableTipModal from '../../common/components/aq-actionable-tip-modal-component';

import AQWhatisModal from '../../common/components/aq-whatis-modal-component';

import CONSTANT from '../../common/constant';

import EVENT from '../events/map-event';

const {
    ActivityIndicator,
    TouchableOpacity
} = ReactNative;

const {
    BodyView,
    LayoutView,
    ItemView
} = Ht.View;

const {
    SearchField
} = Ht.Field;

const {
    FlatButton,
    RaisedButton
} = Ht.Button;

const {
    IconImage
} = Ht.Image;

const {
    HeadlineText,
    TitleText,
    SubtitleText,
    InfoText,
    CaptionText
} = Ht.Text;

const MapViewInterface = Hf.Interface.augment({
    composites: [
        Hf.React.ComponentComposite
    ],
    setup: function setup (done) {
        const intf = this;
        intf.postUpdateStage((component) => {
            const {
                aqr
            } = component.state;
            if (!aqr.info.status.loading && aqr.info.status.initialized) {
                const [ aqrSiteCalloutView ] = component.lookupComponentRefs(`aqr-site-callout-view`);
                if (aqrSiteCalloutView !== null) {
                    if (!Hf.isEmpty(aqr.selectedSite.info.code)) {
                        aqrSiteCalloutView.animate({
                            to: {
                                opacity: 1,
                                translateY: 0
                            },
                            easing: `ease-in`,
                            loopCount: 1,
                            duration: 300
                        });
                    } else {
                        aqrSiteCalloutView.animate({
                            to: {
                                opacity: 0,
                                translateY: CONSTANT.GENERAL.DEVICE_HEIGHT * 0.275
                            },
                            easing: `ease-out`,
                            loopCount: 1,
                            duration: 300
                        });
                    }
                }
            }
        });
        done();
    },
    teardown: function teardown (done) {
        done();
    },
    onPressGoToHomeRegionButton: function onPressGoToHomeRegionButton () {
        const component = this;
        component.outgoing(
            EVENT.ON.GO_TO_HOME_REGION,
            EVENT.ON.DESELECT_AQR_SITE
        ).emit();
    },
    onMapPress: function onMapPress () {
        const component = this;
        component.outgoing(EVENT.ON.DESELECT_AQR_SITE).emit();
    },
    onPressAQRSiteMarker: function onPressAQRSiteMarker (aqrSite) {
        const component = this;
        component.outgoing(EVENT.ON.SELECT_AQR_SITE).emit(() => aqrSite);
    },
    onRefreshAQRSiteData: function onRefreshAQRSiteData () {
        const component = this;
        const {
            aqr
        } = component.state;
        component.outgoing(EVENT.ON.DESELECT_AQR_SITE).emit();
        component.outgoing(EVENT.ON.REFRESH_AQR_SITE_DATA).emit(() => aqr.info);
    },
    onCloseAQActionalbleTipModal: function onCloseAQActionalbleTipModal () {
        const component = this;
        component.outgoing(EVENT.ON.CLOSE_AQ_ACTIONABLE_TIP_MODAL).emit();
    },
    onCloseAQWhatisModal: function onCloseAQWhatisModal () {
        const component = this;
        component.outgoing(EVENT.ON.CLOSE_AQ_WHATIS_MODAL).emit();
    },
    onRegionUpdated: function onRegionUpdated (newRegion) {
        const component = this;
        component.outgoing(EVENT.ON.CHANGING_TO_NEW_REGION).emit(() => newRegion);
    },
    onSearchCity: function onSearchCity (city) {
        const component = this;
        const {
            region
        } = component.state;
        component.outgoing(EVENT.ON.DESELECT_AQR_SITE).emit();
        if (!Hf.isEmpty(city)) {
            GooglePlaces.getAutocompletePredictions(city, {
                type: `cities`
            }).then((results) => {
                if (!Hf.isEmpty(results)) {
                    const {
                        placeID: cityId
                    } = results[0];
                    GooglePlaces.lookUpPlaceByID(cityId).then((result) => {
                        const coordinate = result;
                        component.outgoing(EVENT.ON.SEARCH_NEW_REGION).emit(() => {
                            return {
                                latitude: coordinate.latitude,
                                longitude: coordinate.longitude,
                                latitudeDelta: region.latitudeDelta,
                                longitudeDelta: region.longitudeDelta
                            };
                        });
                        Hf.log(`info1`, `Changing to new searched region.`);
                    }).catch((error) => {
                        Hf.log(`warn1`, `MapViewInterface - Unable to get city id lookup. ${error.message}`);
                    });
                }
            }).catch((error) => {
                Hf.log(`warn1`, `MapViewInterface - Unable to get city autocompletions. ${error.message}`);
            });
        }
    },
    onGetCityAutocompletions: async function onGetCityAutocompletions (city) {
        let results = [];
        if (!Hf.isEmpty(city)) {
            try {
                results = await GooglePlaces.getAutocompletePredictions(city, {
                    type: `cities`
                });
            } catch (error) {
                Hf.log(`warn1`, `MapViewInterface - Unable to get city autocompletions. ${error.message}`);
            }
            if (Hf.isNonEmptyArray(results)) {
                return results.slice(0, 4).map((result) => result.fullText.replace(/,([^\s])/g, `, $1`));
            } else {
                return results;
            }
        } else {
            return results;
        }
    },
    renderCitySuggestionEntry: function renderCitySuggestionEntry (entry, onPressSelectAndSubmit) {
        let [
            city,
            stateOrProvince,
            country
        ] = Hf.stringToArray(entry.value, `,`);
        city = Hf.isNonEmptyString(city) ? city.replace(/\s/g, ``) : ``;
        stateOrProvince = Hf.isNonEmptyString(stateOrProvince) ? stateOrProvince.replace(/\s/g, ``) : ``;
        country = Hf.isNonEmptyString(country) ? country.replace(/\s/g, ``) : ``;
        if (!Hf.isEmpty(stateOrProvince) && Hf.isEmpty(country)) {
            country = stateOrProvince;
            stateOrProvince = ``;
        } else {
            stateOrProvince = `${stateOrProvince}, `;
        }
        return (
            <ItemView
                style = {{
                    width: CONSTANT.GENERAL.DEVICE_WIDTH
                }}
                shade = 'light'
                onPress = { onPressSelectAndSubmit }
            >
                <LayoutView
                    room = 'content-left'
                    orientation = 'vertical'
                    alignment = 'center'
                    selfAlignment = 'start'
                >
                    <IconImage
                        style = {{
                            marginLeft: 6,
                            marginRight: 9
                        }}
                        color = { Ht.Theme.general.color.light.primary }
                        source = { entry.historyType ? `history` : `search` }
                    />
                    {
                        <SubtitleText>{ entry.historyType ? city : `${city} ${stateOrProvince}${country}` }</SubtitleText>
                    }
                </LayoutView>
            </ItemView>
        );
    },
    renderSeachField: function renderSeachField () {
        const component = this;
        const {
            aqr
        } = component.state;
        return aqr.info.status.initialized ? (
            <LayoutView
                style = {{
                    marginTop: 36
                }}
                orientation = 'horizontal'
                selfAlignment = 'start'
            >
                <SearchField
                    ref = { component.assignComponentRef(`search-field`) }
                    overlay = 'opaque'
                    hint = 'Search City'
                    autoFocus = { false }
                    initiallyHidden = { false }
                    initiallyCollapsed = { true }
                    suggestive = { true }
                    onGetAutocompletions = { component.onGetCityAutocompletions }
                    onSearch = { component.onSearchCity }
                    renderSuggestionEntry = { component.renderCitySuggestionEntry }
                >
                    <FlatButton
                        room = 'action-left'
                        action = 'expand'
                        overlay = 'transparent'
                    >
                        <IconImage
                            room = 'content-center'
                            source = 'search'
                        />
                    </FlatButton>
                    <FlatButton
                        room = 'action-left'
                        action = 'collapse'
                        overlay = 'transparent'
                    >
                        <IconImage
                            room = 'content-center'
                            source = 'back'
                        />
                    </FlatButton>
                    <FlatButton
                        room = 'action-right'
                        action = 'clear'
                        overlay = 'transparent'
                    >
                        <IconImage
                            room = 'content-center'
                            source = 'close'
                        />
                    </FlatButton>
                </SearchField>
            </LayoutView>
        ) : null;
    },
    renderGoHomeRegionButton: function renderGoHomeRegionButton () {
        const component = this;
        const {
            aqr
        } = component.state;
        return aqr.info.status.initialized ? (
            <LayoutView
                style = {{
                    ...Ht.Theme.general.dropShadow.shallow,
                    width: 52,
                    height: 52,
                    borderRadius: 26,
                    marginTop: 12,
                    marginHorizontal: 6
                }}
                orientation = 'horizontal'
                alignment = 'center'
                selfAlignment = 'start'
                overlay = 'opaque'
            >
                <FlatButton
                    overlay = 'transparent'
                    onPress = { component.onPressGoToHomeRegionButton }
                >
                    <IconImage
                        room = 'content-center'
                        source = 'current-location'
                    />
                </FlatButton>
            </LayoutView>
        ) : null;
    },
    renderAQRSiteMap: function renderAQRSiteMap () {
        const component = this;
        const {
            region,
            aqr
        } = component.state;
        return (
            <AQRSiteMapView
                // locked = { aqr.info.status.loading || !aqr.info.status.initialized || !aqr.info.status.availability.siteData }
                locked = { aqr.info.status.loading || !aqr.info.status.initialized }
                aqrSites = { aqr.sites }
                initialRegion = {{
                    latitude: region.latitude,
                    longitude: region.longitude,
                    latitudeDelta: region.latitudeDelta,
                    longitudeDelta: region.longitudeDelta
                }}
                onPress = { component.onMapPress }
                onPressAQRSiteMarker = { component.onPressAQRSiteMarker }
                onRegionUpdated = { component.onRegionUpdated }
            />
        );
    },
    renderAQRSiteCallout: function renderAQRSiteCallout () {
        const component = this;
        const {
            aqr
        } = component.state;
        let lastUpdatedMessage = ``;
        const lastUpdatedDurration = moment().diff(moment(aqr.info.timestamp), `minutes`);
        if (lastUpdatedDurration <= 1) {
            lastUpdatedMessage = `Updated Just Now`;
        } else if (lastUpdatedDurration > 1 && lastUpdatedDurration <= 60) {
            lastUpdatedMessage = `Updated ${lastUpdatedDurration} Minutes Ago`;
        } else if (lastUpdatedDurration > 60 && lastUpdatedDurration <= 120) {
            lastUpdatedMessage = `Updated An Hour Ago`;
        } else if (lastUpdatedDurration > 120) {
            lastUpdatedMessage = `Updated ${Math.round(lastUpdatedDurration / 60)} Hours Ago`;
        }
        return aqr.info.status.initialized ? (
            <LayoutView
                ref = { component.assignComponentRef(`aqr-site-callout-view`) }
                style = {{
                    ...Ht.Theme.general.dropShadow.shallow,
                    position: `absolute`,
                    top: CONSTANT.GENERAL.DEVICE_HEIGHT * 0.675,
                    width: CONSTANT.GENERAL.DEVICE_WIDTH,
                    height: CONSTANT.GENERAL.DEVICE_HEIGHT * 0.275,
                    opacity: 0
                }}
                orientation = 'horizontal'
                alignment = 'stretch'
                selfAlignment = 'stretch'
                overlay = 'opaque'
            >
                {
                    !Hf.isEmpty(aqr.selectedSite.info.code) ? <LayoutView
                        style = {{
                            marginHorizontal: 6
                        }}
                        orientation = 'horizontal'
                        alignment = 'center'
                    >
                        <TouchableOpacity onPress = {() => {
                            component.outgoing(EVENT.ON.SHOW_AQ_ACTIONABLE_TIP_MODAL).emit(() => {
                                return {
                                    visible: true,
                                    aqSample: {
                                        aqParam: aqr.selectedSite.aqSample.aqParam,
                                        aqAlertMessage: aqr.selectedSite.aqSample.aqAlertMessage,
                                        aqAlertIndex: aqr.selectedSite.aqSample.aqAlertIndex,
                                        aqi: aqr.selectedSite.aqSample.aqi
                                    }
                                };
                            });
                        }}>
                            <HeadlineText color = { Ht.Theme.general.aqAlertColors[aqr.selectedSite.aqSample.aqAlertIndex] }
                            >{ `Air Quality is ${aqr.selectedSite.aqSample.aqAlertMessage}` }</HeadlineText>
                        </TouchableOpacity>
                        <LayoutView
                            style = {{
                                marginBottom: 3
                            }}
                            orientation = 'vertical'
                            alignment = 'center'
                        >
                            <LayoutView
                                style = {{
                                    height: 52
                                }}
                                orientation = 'horizontal'
                                alignment = 'start'
                            >
                                <TitleText size = 'small' > AQI   </TitleText>
                                <TitleText size = 'small' > PM25  </TitleText>
                            </LayoutView>
                            <LayoutView
                                style = {{
                                    height: 52
                                }}
                                orientation = 'horizontal'
                                alignment = 'start'
                            >
                                <TitleText
                                    size = 'small'
                                    color = { Ht.Theme.general.aqAlertColors[aqr.selectedSite.aqSample.aqAlertIndex] }
                                >{ aqr.selectedSite.aqSample.aqi }</TitleText>
                                <LayoutView
                                    orientation = 'vertical'
                                    alignment = 'center'
                                    selfAlignment = 'start'
                                >
                                    <TitleText
                                        size = 'small'
                                        color = { Ht.Theme.general.aqAlertColors[aqr.selectedSite.aqSample.aqAlertIndex] }
                                    >{ aqr.selectedSite.aqSample.aqConcentration }</TitleText>
                                    <InfoText
                                        size = 'small'
                                        indentation = { 3 }
                                    >{ aqr.selectedSite.aqSample.aqUnit }</InfoText>
                                    <FlatButton
                                        overlay = 'transparent'
                                        onPress = {() => {
                                            component.outgoing(EVENT.ON.SHOW_AQ_WHATIS_MODAL).emit(() => {
                                                return {
                                                    visible: true,
                                                    aqSample: {
                                                        aqParam: aqr.selectedSite.aqSample.aqParam,
                                                        aqAlertIndex: aqr.selectedSite.aqSample.aqAlertIndex,
                                                        aqi: aqr.selectedSite.aqSample.aqi,
                                                        aqConcentration: aqr.selectedSite.aqSample.aqConcentration,
                                                        aqUnit: aqr.selectedSite.aqSample.aqUnit
                                                    }
                                                };
                                            });
                                        }}
                                    >
                                        <IconImage
                                            room = 'content-center'
                                            source = 'info'
                                        />
                                    </FlatButton>
                                </LayoutView>
                            </LayoutView>
                        </LayoutView>
                        <CaptionText>{ lastUpdatedMessage }</CaptionText>
                    </LayoutView> : null
                }
            </LayoutView>
        ) : null;
    },
    renderManualAQRSitesRefresh: function renderManualAQRSitesRefresh () {
        const component = this;
        const {
            aqr
        } = component.state;
        return !aqr.info.status.availability.siteData ? (
            <LayoutView
                style = {{
                    ...Ht.Theme.general.dropShadow.shallow,
                    position: `absolute`,
                    top: CONSTANT.GENERAL.DEVICE_HEIGHT * 0.675,
                    width: CONSTANT.GENERAL.DEVICE_WIDTH,
                    height: CONSTANT.GENERAL.DEVICE_HEIGHT * 0.275,
                    padding: 15
                }}
                overlay = 'opaque'
                orientation = 'horizontal'
                alignment = 'stretch'
                selfAlignment = 'center'
            >
                <HeadlineText size = 'small' > Regional Air Quality </HeadlineText>
                <HeadlineText size = 'small' > Monitor Site Is Unavailable </HeadlineText>
                <RaisedButton
                    label = 'REFRESH'
                    onPress = { component.onRefreshAQRSiteData }
                />
            </LayoutView>
        ) : null;
    },
    renderLoadingIndicator: function renderLoadingIndicator () {
        return (
            <LayoutView
                style = {{
                    ...Ht.Theme.general.dropShadow.shallow,
                    position: `absolute`,
                    top: CONSTANT.GENERAL.DEVICE_HEIGHT * 0.18,
                    padding: 15
                }}
                overlay = 'opaque'
                orientation = 'horizontal'
                alignment = 'center'
                selfAlignment = 'center'
            >
                <ActivityIndicator size = 'large' />
                <HeadlineText> Please Wait... </HeadlineText>
            </LayoutView>
        );
    },
    render: function render () {
        const component = this;
        const {
            aqr,
            aqActionableTip,
            aqWhatis
        } = component.state;
        return (
            <BodyView>
                {
                    component.renderAQRSiteMap()
                }
                {
                    aqr.info.status.loading ? null : component.renderSeachField()
                }
                {
                    aqr.info.status.loading ? null : component.renderGoHomeRegionButton()
                }
                {
                    component.renderAQRSiteCallout()
                }
                {
                    aqr.info.status.loading ? null : component.renderManualAQRSitesRefresh()
                }
                {
                    !aqr.info.status.loading ? null : component.renderLoadingIndicator()
                }
                <AQActionableTipModal
                    { ...aqActionableTip }
                    onClose = { component.onCloseAQActionalbleTipModal }
                />
                <AQWhatisModal
                    { ...aqWhatis }
                    onClose = { component.onCloseAQWhatisModal }
                />
            </BodyView>
        );
    }
});
export default MapViewInterface;
