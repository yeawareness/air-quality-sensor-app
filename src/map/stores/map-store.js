/**
 * Copyright (c) 2017-present, Virida YEAwareness, Org. All rights reserved.
 *
 *------------------------------------------------------------------------
 *
 * @module MapStore
 * @description - Virida client-native app map store.
 *
 * @author Tuan Le (tuan.t.lei@gmail.com)
 *
 *------------------------------------------------------------------------
 * @flow
 */
'use strict'; // eslint-disable-line

import { Hf } from 'hyperflow';

import EVENT from '../events/map-event';

const MapStore = Hf.Store.augment({
    state: {
        status: {
            value: {
                active: false,
                idle: false
            }
        },
        region: {
            value: {
                latitude: 0,
                longitude: 0,
                latitudeDelta: 0,
                longitudeDelta: 0,
                bbox: {
                    northeast: {
                        latitude: 0,
                        longitude: 0
                    },
                    southwest: {
                        latitude: 0,
                        longitude: 0
                    }
                }
            }
        },
        aqr: {
            value: {
                info: {
                    timestamp: ``,
                    regionBBox: {
                        northeast: {
                            latitude: 0,
                            longitude: 0
                        },
                        southwest: {
                            latitude: 0,
                            longitude: 0
                        }
                    },
                    status: {
                        initialized: false,
                        loading: false,
                        availability: {
                            siteData: true
                        }
                    }
                },
                selectedSite: {
                    info: {
                        code: ``,
                        name: ``,
                        latitude: 0,
                        longitude: 0,
                        clusterCount: 0
                    },
                    aqSample: {
                        aqParam: ``,
                        aqi: 0,
                        aqUnit: ``,
                        aqAlertMessage: ``,
                        aqAlertIndex: 0,
                        aqConcentration: 0,
                        aqiSum: 0,
                        aqConcentrationSum: 0
                    }
                },
                sites: []
            }
        },
        aqActionableTip: {
            value: {
                visible: false,
                aqSample: {
                    aqParam: ``,
                    aqAlertMessage: ``,
                    aqAlertIndex: 0,
                    aqi: 0
                }
            }
        },
        aqWhatis: {
            value: {
                visible: false,
                aqSample: {
                    aqParam: ``,
                    aqAlertIndex: 0,
                    aqi: 0,
                    aqConcentration: 0,
                    aqUnit: ``
                }
            }
        }
    },
    setup: function setup (done) {
        const store = this;
        store.incoming(EVENT.DO.MAP_RESET).handle(() => {
            store.reconfig({
                status: {
                    active: false,
                    idle: false
                },
                region: {
                    latitude: 0,
                    longitude: 0,
                    latitudeDelta: 0,
                    longitudeDelta: 0,
                    bbox: {
                        northeast: {
                            latitude: 0,
                            longitude: 0
                        },
                        southwest: {
                            latitude: 0,
                            longitude: 0
                        }
                    }
                },
                aqr: {
                    info: {
                        timestamp: ``,
                        regionBBox: {
                            northeast: {
                                latitude: 0,
                                longitude: 0
                            },
                            southwest: {
                                latitude: 0,
                                longitude: 0
                            }
                        },
                        status: {
                            initialized: false,
                            loading: false,
                            availability: {
                                siteData: true
                            }
                        }
                    },
                    selectedSite: {
                        info: {
                            code: ``,
                            name: ``,
                            latitude: 0,
                            longitude: 0,
                            clusterCount: 0
                        },
                        aqSample: {
                            aqParam: ``,
                            aqi: 0,
                            aqUnit: ``,
                            aqAlertMessage: ``,
                            aqAlertIndex: 0,
                            aqConcentration: 0,
                            aqiSum: 0,
                            aqConcentrationSum: 0
                        }
                    },
                    sites: []
                },
                aqActionableTip: {
                    visible: false,
                    aqSample: {
                        aqParam: ``,
                        aqAlertMessage: ``,
                        aqAlertIndex: 0,
                        aqi: 0
                    }
                },
                aqWhatis: {
                    visible: false,
                    aqSample: {
                        aqParam: ``,
                        aqAlertIndex: 0,
                        aqi: 0,
                        aqConcentration: 0,
                        aqUnit: ``
                    }
                }
            }, {
                suppressMutationEvent: true
            });
        });
        store.incoming(EVENT.DO.MUTATE_MAP_STATUS).handle((status) => {
            if (store.reduce({
                status
            }, {
                suppressMutationEvent: true
            })) {
                if (store.status.active && !store.status.idle) {
                    store.outgoing(EVENT.AS.MAP_ACTIVATED).emit(() => store.aqr.info);
                } else {
                    store.outgoing(EVENT.AS.MAP_DEACTIVATED).emit();
                }
            }
        });
        store.incoming(EVENT.REQUEST.AQR_INFO).handle(() => store.aqr.info).relay(EVENT.RESPONSE.WITH.AQR_INFO);
        store.incoming(EVENT.DO.MUTATE_REGION).handle((region) => {
            if (store.reduce({
                region
            }, {
                suppressMutationEvent: !store.status.active || store.status.idle
            })) {
                store.outgoing(EVENT.AS.REGION_MUTATED).emit(() => {
                    return {
                        region: store.region,
                        aqrInfo: store.aqr.info
                    };
                });
            }
        });
        store.incoming(EVENT.DO.MUTATE_REGION_WITH_SUPPRESSION).handle((region) => {
            if (store.reduce({
                region
            }, {
                suppressMutationEvent: true
            })) {
                store.outgoing(EVENT.AS.REGION_MUTATED).emit(() => {
                    return {
                        region: store.region,
                        aqrInfo: store.aqr.info
                    };
                });
            }
        });
        store.incoming(EVENT.DO.MUTATE_AQR_INFO).handle((aqrInfo) => {
            store.reduce({
                aqr: {
                    info: aqrInfo
                }
            }, {
                suppressMutationEvent: false // !store.status.active || store.status.idle
            });
        });
        store.incoming(EVENT.DO.MUTATE_SELECTED_AQR_SITE).handle((selectedAQRSite) => {
            store.reduce({
                aqr: {
                    selectedSite: selectedAQRSite
                }
            }, {
                suppressMutationEvent: !store.status.active || store.status.idle
            });
        });
        store.incoming(EVENT.DO.MUTATE_AQR_SITES).handle((aqrSites) => {
            store.reconfig({
                aqr: {
                    sites: aqrSites
                }
            }, {
                suppressMutationEvent: !store.status.active || store.status.idle
            });
        });
        store.incoming(EVENT.DO.MUTATE_AQ_ACTIONABLE_TIP).handle((aqActionableTip) => {
            store.reduce({
                aqActionableTip
            }, {
                suppressMutationEvent: !store.status.active || store.status.idle
            });
        });
        store.incoming(EVENT.DO.MUTATE_AQ_WHATIS).handle((aqWhatis) => {
            store.reduce({
                aqWhatis
            }, {
                suppressMutationEvent: !store.status.active || store.status.idle
            });
        });
        done();
    },
    teardown: function teardown (done) {
        done();
    }
});
export default MapStore;
