/**
 * Copyright (c) 2017-present, Viria, Inc. All rights reserved.
 *
 *------------------------------------------------------------------------
 *
 * @description - Viria client native app air quality sensor and regional monitor event ids.
 *
 * @author Tuan Le (tuan.t.lei@gmail.com)
 *
 *------------------------------------------------------------------------
 * @flow
 */
'use strict'; // eslint-disable-line

import { Hf } from 'hyperflow';

export default Hf.Event.create({
    onEvents: [
        `aqs-rediscovery`,
        `refresh-aqr-data`,
        `refresh-aqr-forecast-data`,
        `show-aq-actionable-tip-modal`,
        `close-aq-actionable-tip-modal`,
        `show-aq-whatis-modal`,
        `close-aq-whatis-modal`
    ],
    asEvents: [
        `monitor-activated`,
        `monitor-deactivated`,
        `region-mutated`
    ],
    doEvents: [
        `rediscover-aqs`,
        `monitor-reset`,
        `monitor-activation`,
        `mutate-monitor-status`,
        `mutate-region`,
        `mutate-aqr-info`,
        `mutate-aqr-samples`,
        `mutate-aqr-forecasts`,
        `mutate-aq-actionable-tip`,
        `mutate-aq-whatis`,
        `mutate-notification-setting`
    ],
    broadcastEvents: [
        `run-mode`,
        `periodic-refresh`,

        `setting-written`,
        `aqs-info-written`,
        `aqs-general-setting-written`,
        `aqs-calibration-setting-written`,

        `monitor-alert`,
        `daily-aq-alert`,
        `unhealthy-aq-alert`
    ],
    requestEvents: [
        `aqs-and-aqr-info`,

        `aqicn-aqr-feed-data`,
        `air-now-aqr-forecast-data`
    ]
});
