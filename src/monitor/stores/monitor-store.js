/**
 * Copyright (c) 2017-present, Virida YEAwareness, Org. All rights reserved.
 *
 *------------------------------------------------------------------------
 *
 * @module MonitorStore
 * @description - Virida client-native app air quality sensor and regional dust concentration PM25 and other pollutants monitor store.
 *
 * @author Tuan Le (tuan.t.lei@gmail.com)
 *
 *------------------------------------------------------------------------
 * @flow
 */
'use strict'; // eslint-disable-line

import { Hf } from 'hyperflow';

import EVENT from '../events/monitor-event';

const MonitorStore = Hf.Store.augment({
    state: {
        status: {
            value: {
                active: false,
                idle: false
            }
        },
        region: {
            value: {
                timestamp: ``,
                radius: 0,
                latitude: 0,
                longitude: 0
            }
        },
        aqs: {
            value: {
                info: {
                    alias: `Virida Sensor`,
                    mId: ``,
                    timestamp: ``,
                    status: {
                        initialized: false,
                        outdoor: false,
                        docked: false,
                        monitoring: false,
                        sampling: false,
                        availability: {
                            aqSampleData: true
                        }
                    }
                },
                condition: {
                    battLvl: {
                        unit: ``,
                        value: 0
                    },
                    temp: {
                        unit: ``,
                        value: -1
                    },
                    humidity: {
                        unit: ``,
                        value: -1
                    }
                },
                statistics: [],
                aqSamples: [],
                aqRecords: []
            }
        },
        aqr: {
            value: {
                info: {
                    timestamp: ``,
                    reportingRegionName: ``,
                    status: {
                        initialized: false,
                        loading: false,
                        availability: {
                            aqSampleData: true,
                            aqForecastData: true
                        }
                    }
                },
                aqSamples: [],
                aqForecasts: []
            }
        },
        aqActionableTip: {
            value: {
                visible: false,
                aqSample: {
                    aqParam: ``,
                    aqAlertMessage: ``,
                    aqAlertIndex: 0,
                    aqi: 0
                }
            }
        },
        aqWhatis: {
            value: {
                visible: false,
                aqSample: {
                    aqParam: ``,
                    aqAlertIndex: 0,
                    aqi: 0,
                    aqConcentration: 0,
                    aqUnit: ``
                }
            }
        }
    },
    setup: function setup (done) {
        const store = this;
        store.incoming(EVENT.DO.MONITOR_RESET).handle(() => {
            store.reconfig({
                status: {
                    active: false,
                    idle: false
                },
                region: {
                    radius: 0,
                    latitude: 0,
                    longitude: 0
                },
                aqs: {
                    info: {
                        alias: `Virida Sensor`,
                        mId: ``,
                        timestamp: ``,
                        status: {
                            initialized: false,
                            outdoor: false,
                            docked: false,
                            monitoring: false,
                            sampling: false,
                            availability: {
                                aqSampleData: true
                            }
                        }
                    },
                    condition: {
                        battLvl: {
                            unit: ``,
                            value: 0
                        },
                        temp: {
                            unit: ``,
                            value: -1
                        },
                        humidity: {
                            unit: ``,
                            value: -1
                        }
                    },
                    statistics: [],
                    aqSamples: [],
                    aqRecords: []
                },
                aqr: {
                    info: {
                        timestamp: ``,
                        reportingRegionName: ``,
                        status: {
                            initialized: false,
                            loading: false,
                            availability: {
                                aqSampleData: true,
                                aqForecastData: true
                            }
                        }
                    },
                    aqSamples: [],
                    aqForecasts: []
                },
                aqActionableTip: {
                    visible: false,
                    aqSample: {
                        aqParam: ``,
                        aqAlertMessage: ``,
                        aqAlertIndex: 0,
                        aqi: 0
                    }
                },
                aqWhatis: {
                    visible: false,
                    aqSample: {
                        aqParam: ``,
                        aqAlertIndex: 0,
                        aqi: 0,
                        aqConcentration: 0,
                        aqUnit: ``
                    }
                }
            }, {
                suppressMutationEvent: true
            });
        });
        store.incoming(EVENT.DO.MUTATE_MONITOR_STATUS).handle((status) => {
            if (store.reduce({
                status
            }, {
                suppressMutationEvent: true
            })) {
                if (store.status.active && !store.status.idle) {
                    store.outgoing(EVENT.AS.MONITOR_ACTIVATED).emit(() => {
                        return {
                            aqsInfo: store.aqs.info,
                            aqrInfo: store.aqr.info
                        };
                    });
                } else {
                    store.outgoing(EVENT.AS.MONITOR_DEACTIVATED).emit();
                }
            }
        });
        store.incoming(EVENT.DO.MUTATE_REGION).handle((region) => {
            if (store.reduce({
                region
            }, {
                suppressMutationEvent: true
            })) {
                store.outgoing(EVENT.AS.REGION_MUTATED).emit(() => {
                    return {
                        // status: store.status,
                        region: store.region,
                        aqsInfo: store.aqs.info,
                        aqrInfo: store.aqr.info
                    };
                });
            }
        });
        store.incoming(EVENT.REQUEST.AQS_AND_AQR_INFO).handle(() => {
            return {
                aqsInfo: store.aqs.info,
                aqrInfo: store.aqr.info
            };
        }).relay(EVENT.RESPONSE.WITH.AQS_AND_AQR_INFO);
        store.incoming(EVENT.DO.MUTATE_AQR_INFO).handle((aqrInfo) => {
            store.reduce({
                aqr: {
                    info: aqrInfo
                }
            }, {
                suppressMutationEvent: false // !store.status.active || store.status.idle
            });
        });
        store.incoming(EVENT.DO.MUTATE_AQR_SAMPLES).handle((aqrSamples) => {
            store.reconfig({
                aqr: {
                    aqSamples: aqrSamples
                }
            }, {
                suppressMutationEvent: !store.status.active || store.status.idle
            });
        });
        store.incoming(EVENT.DO.MUTATE_AQR_FORECASTS).handle((aqrForecasts) => {
            store.reconfig({
                aqr: {
                    aqForecasts: aqrForecasts
                }
            }, {
                suppressMutationEvent: !store.status.active || store.status.idle
            });
        });
        store.incoming(EVENT.DO.MUTATE_AQ_ACTIONABLE_TIP).handle((aqActionableTip) => {
            store.reduce({
                aqActionableTip
            }, {
                suppressMutationEvent: !store.status.active || store.status.idle
            });
        });
        store.incoming(EVENT.DO.MUTATE_AQ_WHATIS).handle((aqWhatis) => {
            store.reduce({
                aqWhatis
            }, {
                suppressMutationEvent: !store.status.active || store.status.idle
            });
        });
        done();
    },
    teardown: function teardown (done) {
        done();
    }
});
export default MonitorStore;
