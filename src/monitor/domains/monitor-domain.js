/**
 * Copyright (c) 2017-present, Virida YEAwareness, Org. All rights reserved.
 *
 *------------------------------------------------------------------------
 *
 * @module MonitorDomain
 * @description - Virida client-native app air quality sensor and regional dust concentration PM25 and other pollutants monitor domain.
 *
 * @author Tuan Le (tuan.t.lei@gmail.com)
 *
 *------------------------------------------------------------------------
 * @flow
 */
'use strict'; // eslint-disable-line

import { Hf } from 'hyperflow';

import moment from 'moment';

import ReactNative from 'react-native'; // eslint-disable-line

import BackgroundGeolocation from 'react-native-background-geolocation';

import MonitorStore from '../stores/monitor-store';

import MonitorViewInterface from '../interfaces/monitor-view-interface';

import AQRFeedAPIService from '../../common/services/aqr-feed-api-service';

import AQRForecastAPIService from '../../common/services/aqr-forecast-api-service';

import AQRNotificationAPIService from '../../common/services/aqr-notification-api-service';

import CONSTANT from '../../common/constant';

import EVENT from '../events/monitor-event';

const {
    NetInfo
} = ReactNative;

const MonitorDomain = Hf.Domain.augment({
    $init: function $init () {
        const domain = this;
        domain.register({
            store: MonitorStore({
                name: `monitor-store`
            }),
            intf: MonitorViewInterface({
                name: `monitor-view`
            }),
            services: [
                AQRFeedAPIService({
                    name: `aqr-feed-api`
                }),
                AQRForecastAPIService({
                    name: `aqr-forecast-api`
                }),
                AQRNotificationAPIService({
                    name: `aqr-notification-api`
                })
            ]
        });
    },
    setup: function setup (done) {
        const domain = this;

        domain.incoming(EVENT.DO.MONITOR_RESET).repeat();
        domain.incoming(EVENT.BROADCAST.RUN_MODE).handle((runMode) => {
            return {
                idle: runMode === `background-running`
            };
        }).relay(EVENT.DO.MUTATE_MONITOR_STATUS);
        domain.incoming(EVENT.DO.MONITOR_ACTIVATION).handle((active) => {
            return {
                active
            };
        }).relay(EVENT.DO.MUTATE_MONITOR_STATUS);
        domain.incoming(EVENT.AS.MONITOR_ACTIVATED).handle(({
            aqsInfo,
            aqrInfo
        }) => {
            if (!aqrInfo.status.initialized) {
                BackgroundGeolocation.getCurrentPosition({
                    samples: 1,
                    persist: false
                }).then((location) => {
                    const timestamp = moment().format();
                    const {
                        coords: coordinate
                    } = location;
                    domain.outgoing(EVENT.DO.MUTATE_REGION).emit(() => {
                        return {
                            timestamp,
                            latitude: coordinate.latitude,
                            longitude: coordinate.longitude,
                            radius: CONSTANT.AQR.REGION_SEARCH_RADIUS_MILE
                        };
                    });
                }).catch(error => {
                    domain.outgoing(EVENT.BROADCAST.MONITOR_ALERT).emit(() => {
                        return {
                            visible: true,
                            title: `Air Quality Regional Monitor Alert`,
                            message: `Unable to get device's geolocation. ${error.message}`
                        };
                    });
                    Hf.log(`warn1`, `MonitorDomain - Unable to get device's geolocation. ${error.message}`);
                });
            }
        });
        domain.incoming(EVENT.BROADCAST.SETTING_WRITTEN).handle(({
            notification
        }) => {
            if (Hf.isObject(notification)) {
                domain.outgoing(EVENT.DO.MUTATE_NOTIFICATION_SETTING).emit(() => notification);
            }
        });
        domain.incoming(EVENT.ON.AQS_REDISCOVERY).forward(EVENT.DO.REDISCOVER_AQS);
        domain.incoming(EVENT.BROADCAST.PERIODIC_REFRESH).handle(() => {
            NetInfo.isConnected.fetch().then((online) => {
                if (online) {
                    domain.outgoing(EVENT.REQUEST.AQS_AND_AQR_INFO).emit();
                }
            });
        });
        domain.incoming(EVENT.RESPONSE.WITH.AQS_AND_AQR_INFO).handle(({
            aqsInfo,
            aqrInfo
        }) => {
            if (!aqrInfo.status.loading && aqrInfo.status.initialized) {
                BackgroundGeolocation.getCurrentPosition({
                    samples: 1,
                    persist: false
                }).then((location) => {
                    const {
                        coords: coordinate
                    } = location;
                    domain.outgoing(EVENT.DO.MUTATE_REGION).emit(() => {
                        return {
                            timestamp: moment().format(),
                            latitude: coordinate.latitude,
                            longitude: coordinate.longitude,
                            radius: CONSTANT.AQR.REGION_SEARCH_RADIUS_MILE
                        };
                    });
                    Hf.log(`info1`, `Periodic monitor regional air quality feed & forecast data refreshing.`);
                }).catch(error => {
                    domain.outgoing(EVENT.BROADCAST.MONITOR_ALERT).emit(() => {
                        return {
                            visible: true,
                            title: `Air Quality Regional Monitor Alert`,
                            message: `Unable to get device's geolocation. ${error.message}`
                        };
                    });
                    Hf.log(`warn1`, `MonitorDomain - Unable to get device's geolocation. ${error.message}`);
                });
            }
        });
        domain.incoming(
            EVENT.ON.REFRESH_AQR_DATA,
            EVENT.ON.REFRESH_AQR_FORECAST_DATA
        ).handle((aqrInfo) => {
            if (!aqrInfo.status.loading) {
                BackgroundGeolocation.getCurrentPosition({
                    samples: 1,
                    persist: false
                }).then((location) => {
                    const {
                        coords: coordinate
                    } = location;
                    domain.outgoing(EVENT.DO.MUTATE_REGION).emit(() => {
                        return {
                            timestamp: moment().format(),
                            latitude: coordinate.latitude,
                            longitude: coordinate.longitude,
                            radius: CONSTANT.AQR.REGION_SEARCH_RADIUS_MILE
                        };
                    });
                    Hf.log(`info1`, `Manual monitor regional air quality feed & forecast data refreshing.`);
                }).catch(error => {
                    domain.outgoing(EVENT.BROADCAST.MONITOR_ALERT).emit(() => {
                        return {
                            visible: true,
                            title: `Air Quality Regional Monitor Alert`,
                            message: `Unable to get device's geolocation. ${error.message}`
                        };
                    });
                    Hf.log(`warn1`, `MonitorDomain - Unable to get device's geolocation. ${error.message}`);
                });
            }
        });
        domain.incoming(EVENT.AS.REGION_MUTATED).handle(({
            region,
            aqsInfo,
            aqrInfo
        }) => {
            if (!aqrInfo.status.initialized) {
                domain.outgoing(EVENT.DO.MUTATE_AQR_INFO).emit(() => {
                    return {
                        status: {
                            initialized: true
                        }
                    };
                });
            }
            if (!aqrInfo.status.loading) {
                domain.outgoing(EVENT.DO.MUTATE_AQR_INFO).emit(() => {
                    return {
                        status: {
                            loading: true
                        }
                    };
                });
                domain.outgoing(EVENT.REQUEST.AQICN_AQR_FEED_DATA).emit(() => {
                    return {
                        ...region,
                        aqParams: [ `pm25`, `o3`, `no2`, `so2`, `co` ]
                    };
                });
                domain.outgoing(EVENT.REQUEST.AIR_NOW_AQR_FORECAST_DATA).emit(() => {
                    return {
                        ...region,
                        aqParams: [ `pm25` ]
                    };
                });
            }
        });
        domain.incoming(EVENT.RESPONSE.TO.AQICN_AQR_FEED_DATA.OK).handle(({
            aqrInfo,
            aqrSamples
        }) => {
            if (Hf.isNonEmptyArray(aqrSamples)) {
                domain.outgoing(EVENT.DO.MUTATE_AQR_SAMPLES).emit(() => aqrSamples);
                domain.outgoing(EVENT.DO.MUTATE_AQR_INFO).emit(() => {
                    return {
                        ...aqrInfo,
                        status: {
                            loading: false,
                            availability: {
                                aqSampleData: true
                            }
                        }
                    };
                });
                Hf.log(`info1`, `Received AQICN regional air quality feed data.`);
            } else {
                domain.outgoing(EVENT.DO.MUTATE_AQR_INFO).emit(() => {
                    return {
                        ...aqrInfo,
                        status: {
                            loading: false,
                            availability: {
                                aqSampleData: false
                            }
                        }
                    };
                });
            }
        });
        domain.incoming(EVENT.RESPONSE.TO.AIR_NOW_AQR_FORECAST_DATA.OK).handle(({
            aqrInfo,
            aqrForecasts
        }) => {
            if (Hf.isNonEmptyArray(aqrForecasts)) {
                domain.outgoing(EVENT.DO.MUTATE_AQR_FORECASTS).emit(() => aqrForecasts);
                domain.outgoing(EVENT.DO.MUTATE_AQR_INFO).emit(() => {
                    return {
                        ...aqrInfo,
                        status: {
                            loading: false,
                            availability: {
                                aqForecastData: aqrForecasts.some((aqForecast) => aqForecast.aqi > 0)
                            }
                        }
                    };
                });
                Hf.log(`info1`, `Received AirNow regional air quality forecast data.`);
            } else {
                domain.outgoing(EVENT.DO.MUTATE_AQR_INFO).emit(() => {
                    return {
                        ...aqrInfo,
                        status: {
                            loading: false,
                            availability: {
                                aqForecastData: false
                            }
                        }
                    };
                });
            }
        });
        domain.incoming(
            EVENT.RESPONSE.TO.AQICN_AQR_FEED_DATA.NOT_FOUND,
            EVENT.RESPONSE.TO.AQICN_AQR_FEED_DATA.TIMED_OUT,
            EVENT.RESPONSE.TO.AQICN_AQR_FEED_DATA.ERROR
        ).handle(() => {
            domain.outgoing(EVENT.DO.MUTATE_AQR_INFO).emit(() => {
                return {
                    status: {
                        loading: false,
                        availability: {
                            aqSampleData: false
                        }
                    }
                };
            });
            domain.outgoing(EVENT.BROADCAST.MONITOR_ALERT).emit(() => {
                return {
                    visible: true,
                    title: `Regional Air Quality Monitor Alert`,
                    message: `Unable to Retrieve Regional Air Quality Feed Data From AQICN.`
                };
            });
            Hf.log(`warn1`, `Unable to received regional air quality feed data from AQICN.`);
        });
        domain.incoming(
            EVENT.RESPONSE.TO.AIR_NOW_AQR_FORECAST_DATA.NOT_FOUND,
            EVENT.RESPONSE.TO.AIR_NOW_AQR_FORECAST_DATA.TIMED_OUT,
            EVENT.RESPONSE.TO.AIR_NOW_AQR_FORECAST_DATA.ERROR
        ).handle(() => {
            domain.outgoing(EVENT.DO.MUTATE_AQR_INFO).emit(() => {
                return {
                    status: {
                        loading: false,
                        availability: {
                            aqForecastData: false
                        }
                    }
                };
            });
            domain.outgoing(EVENT.BROADCAST.MONITOR_ALERT).emit(() => {
                return {
                    visible: true,
                    title: `Regional Air Quality Monitor Alert`,
                    message: `Unable to Retrieve Regional Air Quality Forecast Data From AirNow.`
                };
            });
            Hf.log(`warn1`, `Unable to received regional air quality forecast data from AirNow.`);
        });
        domain.incoming(EVENT.ON.SHOW_AQ_ACTIONABLE_TIP_MODAL).forward(EVENT.DO.MUTATE_AQ_ACTIONABLE_TIP);
        domain.incoming(EVENT.ON.SHOW_AQ_WHATIS_MODAL).forward(EVENT.DO.MUTATE_AQ_WHATIS);
        domain.incoming(EVENT.ON.CLOSE_AQ_ACTIONABLE_TIP_MODAL).handle(() => {
            return {
                visible: false,
                aqSample: {
                    aqParam: ``,
                    aqAlertMessage: ``,
                    aqAlertIndex: 0,
                    aqi: 0
                }
            };
        }).relay(EVENT.DO.MUTATE_AQ_ACTIONABLE_TIP);
        domain.incoming(EVENT.ON.CLOSE_AQ_WHATIS_MODAL).handle(() => {
            return {
                visible: false,
                aqSample: {
                    aqParam: ``,
                    aqAlertIndex: 0,
                    aqi: 0,
                    aqConcentration: 0,
                    aqUnit: ``
                }
            };
        }).relay(EVENT.DO.MUTATE_AQ_WHATIS);

        done();
    },
    teardown: function teardown (done) {
        done();
    }
});
export default MonitorDomain;
