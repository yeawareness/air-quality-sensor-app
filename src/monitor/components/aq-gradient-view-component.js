/**
 * Copyright (c) 2017-present, Virida YEAwareness, Org. All rights reserved.
 *
 *------------------------------------------------------------------------
 *
 * @module AQGradientViewComponent
 * @description - Virida client-native app aq gradient view component.
 *
 * @author Tuan Le (tuan.t.lei@gmail.com)
 *
 *------------------------------------------------------------------------
 * @flow
 */
'use strict'; // eslint-disable-line

import { Ht } from 'hypertoxin';

import React from 'react';

import ReactNative from 'react-native'; // eslint-disable-line

import PropTypes from 'prop-types';

import Color from 'color';

import CONSTANT from '../../common/constant';

const {
    Component
} = React;

const {
    Animated
} = ReactNative;

const {
    WallpaperImage
} = Ht.Image;

export default class AQGradientViewComponent extends Component {
    static propTypes = {
        aqAlertIndex: PropTypes.number
    }
    static defaultProps = {
        aqAlertIndex: 0
    }
    constructor (props) {
        const {
            aqAlertIndex
        } = props;
        super(props);
        this.state = {
            animatedValue: new Animated.Value(0),
            prevAQAlertColor: Ht.Theme.general.aqAlertColors[0],
            nextAQAlertColor: Ht.Theme.general.aqAlertColors[aqAlertIndex]
        };
    }
    componentWillMount () {
        const component = this;
        const {
            aqAlertIndex
        } = component.props;
        component.setState((prevState) => {
            return {
                prevAQAlertColor: prevState.nextAQAlertColor,
                nextAQAlertColor: Ht.Theme.general.aqAlertColors[aqAlertIndex]
            };
        });
    }
    componentDidMount () {
        const component = this;
        const {
            animatedValue,
            prevAQAlertColor,
            nextAQAlertColor
        } = component.state;
        if (prevAQAlertColor !== nextAQAlertColor) {
            Animated.timing(animatedValue, {
                toValue: 1,
                duration: 5000
            }).start(() => {
                animatedValue.resetAnimation();
            });
        }
    }
    componentDidUpdate () {
        const component = this;
        const {
            animatedValue,
            prevAQAlertColor,
            nextAQAlertColor
        } = component.state;
        if (prevAQAlertColor !== nextAQAlertColor) {
            Animated.timing(animatedValue, {
                toValue: 1,
                duration: 5000
            }).start(() => {
                animatedValue.resetAnimation();
            });
        }
    }
    componentWillReceiveProps (nextProps) {
        const component = this;
        const {
            aqAlertIndex
        } = nextProps;

        component.setState((prevState) => {
            return {
                prevAQAlertColor: prevState.nextAQAlertColor,
                nextAQAlertColor: Ht.Theme.general.aqAlertColors[aqAlertIndex]
            };
        });
    }
    componentWillUnmount () {
        const component = this;
        component.state.animatedValue.removeAllListeners();
    }
    render () {
        const component = this;
        const {
            children
        } = component.props;
        const {
            animatedValue,
            prevAQAlertColor,
            nextAQAlertColor
        } = component.state;
        const interpolatedColor = animatedValue.interpolate({
            inputRange: [ 0, 1 ],
            outputRange: [
                Color(prevAQAlertColor).rgb().string(),
                Color(nextAQAlertColor).rgb().string()
            ]
        });

        return (
            <Animated.View style = {{
                backgroundColor: interpolatedColor
            }}>
                <WallpaperImage
                    source = { Ht.Theme.imageSource.logoBackgroundGradient }
                    style = {{
                        height: (7 * CONSTANT.GENERAL.DEVICE_HEIGHT) / 11
                    }}
                >
                    {
                        children
                    }
                </WallpaperImage>
            </Animated.View>
        );
    }
}
