/**
 * Copyright (c) 2017-present, Virida YEAwareness, Org. All rights reserved.
 *
 *------------------------------------------------------------------------
 *
 * @module MonitorViewInterface
 * @description - Virida client-native app air qualitysensor and regional dust concentration PM25 and other pollutants monitor view interface.
 *
 * @author Tuan Le (tuan.t.lei@gmail.com)
 *
 *------------------------------------------------------------------------
 * @flow
 */
'use strict'; // eslint-disable-line

import { Hf } from 'hyperflow';

import { Ht } from 'hypertoxin';

import moment from 'moment';

import React from 'react';

import ReactNative from 'react-native'; // eslint-disable-line

import { createMaterialTopTabNavigator } from 'react-navigation';

import { StackedBarChart, XAxis, YAxis } from 'react-native-svg-charts';

import { Text as SvgText } from 'react-native-svg';

import AQGradientView from '../components/aq-gradient-view-component';

import AQActionableTipModal from '../../common/components/aq-actionable-tip-modal-component';

import AQWhatisModal from '../../common/components/aq-whatis-modal-component';

import CONSTANT from '../../common/constant';

import EVENT from '../events/monitor-event';

const {
    ActivityIndicator,
    Image,
    ImageBackground,
    FlatList,
    Linking,
    Text,
    TouchableOpacity
} = ReactNative;

const {
    ScreenView,
    BodyView,
    LayoutView,
    ItemView
} = Ht.View;

const {
    FlatButton,
    RaisedButton
} = Ht.Button;

const {
    HeadlineText,
    TitleText,
    InfoText,
    CaptionText
} = Ht.Text;

const {
    IconImage
} = Ht.Image;

const MonitorTabNavigator = createMaterialTopTabNavigator({
    outdoorPollutants: {
        screen: (props) => {
            const {
                screenProps
            } = props;
            const {
                component,
                aqSamples
            } = screenProps;
            return (
                <LayoutView
                    style = {{
                        marginHorizontal: 6
                    }}
                    orientation = 'horizontal'
                    alignment = 'center'
                    selfAlignment = 'stretch'
                >
                    <FlatList
                        scrollEnabled = { false }
                        data = { aqSamples.filter((aqSample) => aqSample.source === `outdoor`).map((aqSample, index) => {
                            return {
                                key: `${index}`,
                                ...aqSample
                            };
                        }) }
                        renderItem = {(listData) => {
                            const aqSample = listData.item;
                            return (
                                <ItemView
                                    style = {{
                                        width: CONSTANT.GENERAL.DEVICE_WIDTH,
                                        height: 20
                                    }}
                                    onPress = {() => {
                                        component.outgoing(EVENT.ON.SHOW_AQ_WHATIS_MODAL).emit(() => {
                                            return {
                                                visible: true,
                                                aqSample: {
                                                    aqParam: aqSample.aqParam,
                                                    aqAlertIndex: aqSample.aqAlertIndex,
                                                    aqi: aqSample.aqi,
                                                    aqConcentration: aqSample.aqConcentration,
                                                    aqUnit: aqSample.aqUnit
                                                }
                                            };
                                        });
                                    }}
                                >
                                    <LayoutView
                                        style = {{
                                            marginLeft: 24
                                        }}
                                        room = 'content-left'
                                        orientation = 'vertical'
                                        alignment = 'start'
                                        selfAlignment = 'start'
                                    >
                                        <TitleText size = 'small' >{ `${aqSample.aqParam.toUpperCase()}   \t` }</TitleText>
                                        <TitleText
                                            size = 'small'
                                            color = { Ht.Theme.general.aqAlertColors[aqSample.aqAlertIndex] }
                                        >{ aqSample.aqConcentration }</TitleText>
                                        <InfoText indentation = { 6 } >{ aqSample.aqUnit }</InfoText>
                                    </LayoutView>
                                    <FlatButton
                                        room = 'action-right'
                                        overlay = 'transparent'
                                        onPress = {() => {
                                            component.outgoing(EVENT.ON.SHOW_AQ_WHATIS_MODAL).emit(() => {
                                                return {
                                                    visible: true,
                                                    aqSample: {
                                                        aqParam: aqSample.aqParam,
                                                        aqAlertIndex: aqSample.aqAlertIndex,
                                                        aqi: aqSample.aqi,
                                                        aqConcentration: aqSample.aqConcentration,
                                                        aqUnit: aqSample.aqUnit
                                                    }
                                                };
                                            });
                                        }}
                                    >
                                        <IconImage
                                            room = 'content-center'
                                            source = 'info'
                                            size = 'small'
                                        />
                                    </FlatButton>
                                </ItemView>
                            );
                        }}
                    />
                </LayoutView>
            );
        },
        navigationOptions: () => {
            return {
                tabBarLabel: `OUTDOOR POLLUTANTS`
            };
        }
    },
    forecast: {
        screen: (props) => {
            const {
                screenProps
            } = props;
            const {
                component
            } = screenProps;
            const {
                aqr
            } = component.state;
            if (!aqr.info.status.loading) {
                const dataPts = aqr.aqForecasts.map((aqForecast) => {
                    let good = 0;
                    let moderate = 0;
                    let unhealthy = 0;
                    let harmful = 0;
                    let deadly = 0;
                    if (aqForecast.aqi >= 0 && aqForecast.aqi <= 50) {
                        good = aqForecast.aqi;
                    } else if (aqForecast.aqi > 50 && aqForecast.aqi <= 100) {
                        good = 50;
                        moderate = aqForecast.aqi - good;
                    } else if (aqForecast.aqi > 100 && aqForecast.aqi <= 200) {
                        good = 50;
                        moderate = 50;
                        unhealthy = aqForecast.aqi - (good + moderate);
                    } else if (aqForecast.aqi > 200 && aqForecast.aqi <= 300) {
                        good = 50;
                        moderate = 50;
                        unhealthy = 100;
                        harmful = aqForecast.aqi - (good + moderate + unhealthy);
                    } else if (aqForecast.aqi > 300) {
                        good = 50;
                        moderate = 50;
                        unhealthy = 100;
                        harmful = 100;
                        deadly = aqForecast.aqi - (good + moderate + unhealthy + harmful);
                    }
                    return {
                        xLabel: aqForecast.day,
                        aqi: aqForecast.aqi,
                        aqAlertIndex: aqForecast.aqAlertIndex,
                        good,
                        moderate,
                        unhealthy,
                        harmful,
                        deadly
                    };
                });
                if (aqr.info.status.initialized && aqr.info.status.availability.aqForecastData) {
                    const StackedBarLabels = ({
                        x,
                        y,
                        data: _dataPts
                    }) => (
                        _dataPts.filter((dataPt) => dataPt.aqi > 0).map((dataPt, index) => {
                            return (
                                <SvgText
                                    key = { index }
                                    x = { x(index) + 14 }
                                    y = { y(dataPt.aqi) - 5 }
                                    fontSize = { 12 }
                                    fontWeight = 'bold'
                                    fill = { Ht.Theme.general.aqAlertColors[dataPt.aqAlertIndex] }
                                    alignmentBaseline = 'middle'
                                >{ dataPt.aqi }</SvgText>
                            );
                        })
                    );
                    return (
                        <BodyView>
                            <LayoutView
                                style = {{
                                    marginHorizontal: 6
                                }}
                                orientation = 'horizontal'
                                alignment = 'center'
                                selfAlignment = 'stretch'
                            >
                                <LayoutView
                                    style = {{
                                        marginLeft: 15
                                    }}
                                    orientation = 'vertical'
                                    alignment = 'center'
                                    selfAlignment = 'stretch'
                                >
                                    {/* <YAxis
                                        style = {{
                                            height: (10 * CONSTANT.GENERAL.DEVICE_HEIGHT) / 49
                                        }}
                                        contentInset = {{
                                            top: 10,
                                            bottom: 20
                                        }}
                                        svg = {{
                                            ...Ht.Theme.font.thinSmallest,
                                            fill: Ht.Theme.general.color.text.light
                                        }}
                                        data={ StackedBarChart.extractDataPoints(dataPts, [
                                            `good`,
                                            `moderate`,
                                            `unhealthy`,
                                            `harmful`,
                                            `deadly`
                                        ]) }
                                    /> */}
                                    <LayoutView
                                        orientation = 'horizontal'
                                        alignment = 'center'
                                        selfAlignment = 'stretch'
                                    >
                                        <StackedBarChart
                                            style = {{
                                                ...Ht.Theme.general.dropShadow.extraShallow,
                                                width: CONSTANT.GENERAL.DEVICE_WIDTH - 25,
                                                height: (10 * CONSTANT.GENERAL.DEVICE_HEIGHT) / 49
                                            }}
                                            contentInset = {{
                                                top: 15,
                                                bottom: 10,
                                                left: 10,
                                                right: 10
                                            }}
                                            spacingInner = { 0.35 }
                                            spacingOuter = { 0.25 }
                                            keys = {[
                                                `good`,
                                                `moderate`,
                                                `unhealthy`,
                                                `harmful`,
                                                `deadly`
                                            ]}
                                            colors = { Ht.Theme.general.aqAlertColors }
                                            data = { dataPts }
                                            showGrid = { false }
                                        >
                                            <StackedBarLabels/>
                                        </StackedBarChart>
                                        <XAxis
                                            style = {{
                                                width: CONSTANT.GENERAL.DEVICE_WIDTH - 95
                                            }}
                                            contentInset = {{
                                                left: 15,
                                                right: 10
                                            }}
                                            svg = {{
                                                ...Ht.Theme.font.thinSmallest,
                                                fill: Ht.Theme.general.color.text.light
                                            }}
                                            data = { dataPts }
                                            formatLabel = { (index) => dataPts[index].xLabel }
                                        />
                                    </LayoutView>
                                </LayoutView>
                            </LayoutView>
                        </BodyView>
                    );
                } else if (aqr.info.status.initialized && !aqr.info.status.availability.aqForecastData) {
                    return (
                        <BodyView>
                            <LayoutView
                                style = {{
                                    marginHorizontal: 6
                                }}
                                orientation = 'horizontal'
                                alignment = 'stretch'
                                selfAlignment = 'stretch'
                            >
                                <HeadlineText size = 'small' > Regional Air Quality </HeadlineText>
                                <HeadlineText size = 'small' > Forecast Is Unavailable </HeadlineText>
                                <RaisedButton
                                    label = 'REFRESH'
                                    onPress = { component.onRefreshAQRForecastData }
                                />
                            </LayoutView>
                        </BodyView>
                    );
                } else {
                    return null;
                }
            } else {
                return null;
            }
        },
        navigationOptions: () => {
            return {
                tabBarLabel: `FORECAST`
            };
        }
    }
    // sensorFeed: {
    //     screen: (props) => {
    //         const {
    //             screenProps
    //         } = props;
    //         const {
    //             component
    //         } = screenProps;
    //         const {
    //             aqs
    //         } = component.state;
    //         const aqsSkipped = Hf.isEmpty(aqs.info.mId);
    //         return aqsSkipped ? (
    //             <LayoutView
    //                 style = {{
    //                     marginHorizontal: 6
    //                 }}
    //                 orientation = 'horizontal'
    //                 alignment = 'stretch'
    //                 selfAlignment = 'stretch'
    //             >
    //                 <HeadlineText size = 'small' > Air Quality Sensor </HeadlineText>
    //                 <HeadlineText size = 'small' > Is Unavailable </HeadlineText>
    //                 <RaisedButton
    //                     label = 'REDISCOVER AIR QUALITY SENSOR'
    //                     onPress = { component.onPressRediscoverAQSButton }
    //                 />
    //             </LayoutView>
    //         ) : null;
    //     },
    //     navigationOptions: () => {
    //         return {
    //             tabBarLabel: `SENSOR FEED`
    //         };
    //     }
    // }
}, {
    swipeEnabled: true,
    animationEnabled: true,
    lazy: true,
    removeClippedSubviews: true,
    initialRouteName: `outdoorPollutants`,
    transitionConfig: () => ({
        transitionSpec: {
            duration: 300
        }
    }),
    tabBarOptions: {
        scrollEnabled: true,
        showIcon: false,
        activeTintColor: Ht.Theme.general.color.light.secondary,
        inactiveTintColor: Ht.Theme.general.color.light.primary,
        activeBackgroundColor: `transparent`,
        inactiveBackgroundColor: `transparent`,
        labelStyle: {
            ...Ht.Theme.font.boldSmaller
        },
        tabStyle: {
            justifyContent: `center`,
            alignItems: `center`,
            width: CONSTANT.GENERAL.DEVICE_WIDTH / 2, // CONSTANT.GENERAL.DEVICE_WIDTH / 3,
            borderBottomWidth: 2,
            borderColor: `transparent`,
            backgroundColor: `transparent`
        },
        indicatorStyle: {
            height: 2,
            backgroundColor: Ht.Theme.palette.orange
        },
        style: {
            width: CONSTANT.GENERAL.DEVICE_WIDTH,
            backgroundColor: `transparent`
        }
    }
});

const MonitorViewInterface = Hf.Interface.augment({
    composites: [
        Hf.React.ComponentComposite
    ],
    setup: function setup (done) {
        done();
    },
    teardown: function teardown (done) {
        done();
    },
    onRefreshAQRData: function onRefreshAQRData () {
        const component = this;
        const {
            aqr
        } = component.state;
        component.outgoing(EVENT.ON.REFRESH_AQR_DATA).emit(() => aqr.info);
    },
    onRefreshAQRForecastData: function onRefreshAQRForecastData () {
        const component = this;
        const {
            aqr
        } = component.state;
        component.outgoing(EVENT.ON.REFRESH_AQR_FORECAST_DATA).emit(() => aqr.info);
    },
    onPressRediscoverAQSButton: function onPressRediscoverAQSButton () {
        const component = this;
        component.outgoing(EVENT.ON.AQS_REDISCOVERY).emit();
    },
    onCloseAQActionalbleTipModal: function onCloseAQActionalbleTipModal () {
        const component = this;
        component.outgoing(EVENT.ON.CLOSE_AQ_ACTIONABLE_TIP_MODAL).emit();
    },
    onCloseAQWhatisModal: function onCloseAQWhatisModal () {
        const component = this;
        component.outgoing(EVENT.ON.CLOSE_AQ_WHATIS_MODAL).emit();
    },
    onPressGoToYEAFBButton: function onPressGoToYEAFBButton () {
        Linking.canOpenURL(CONSTANT.URL.YEA_FB1).then((supported) => {
            if (supported) {
                return Linking.openURL(CONSTANT.URL.YEA_FB1);
            } else {
                return Linking.openURL(CONSTANT.URL.YEA_FB2);
            }
        }).catch(() => {
            Hf.log(`warn1`, `AboutViewInterface - Unable to open Y.E.A app or website at ${CONSTANT.URL.YEA_ABOUT}.`);
        });
    },
    onPressGoToAboutYEAInstagramButton: function onPressGoToAboutYEAInstagramButton () {
        Linking.openURL(CONSTANT.URL.YEA_INSTAGRAM).catch(() => {
            Hf.log(`warn1`, `AboutViewInterface - Unable to open Y.E.A Instagram app or website at ${CONSTANT.URL.YEA_INSTAGRAM}.`);
        });
    },
    onPressDonateToYEAButton: function onPressDonateToYEAButton () {
        Linking.openURL(CONSTANT.URL.YEA_DONATION).catch(() => {
            Hf.log(`warn1`, `AboutViewInterface - Unable to open Y.E.A donation at ${CONSTANT.URL.YEA_DONATION}.`);
        });
    },
    renderAQMonitor: function renderAQMonitor (aqSummary) {
        const component = this;
        const {
            aqr
        } = component.state;
        const aqiScaleIndex = aqSummary.aqi > 440 ? 440 * CONSTANT.GENERAL.AQI_SCALE_INDEX : aqSummary.aqi * CONSTANT.GENERAL.AQI_SCALE_INDEX;
        const lastUpdatedDurration = moment().diff(moment(aqr.info.timestamp), `minutes`);
        let faceIndicator;
        let lastUpdatedMessage = ``;
        if (aqSummary.aqAlertIndex === 0) {
            faceIndicator = Ht.Theme.imageSource.happyFace;
        } else if (aqSummary.aqAlertIndex === 1) {
            faceIndicator = Ht.Theme.imageSource.okFace;
        } else {
            faceIndicator = Ht.Theme.imageSource.sadFace;
        }
        if (lastUpdatedDurration <= 1) {
            lastUpdatedMessage = `Updated Just Now`;
        } else if (lastUpdatedDurration > 1 && lastUpdatedDurration <= 60) {
            lastUpdatedMessage = `Updated ${lastUpdatedDurration} Minutes Ago`;
        } else if (lastUpdatedDurration > 60 && lastUpdatedDurration <= 120) {
            lastUpdatedMessage = `Updated An Hour Ago`;
        } else if (lastUpdatedDurration > 120) {
            lastUpdatedMessage = `Updated ${Math.round(lastUpdatedDurration / 60)} Hours Ago`;
        }

        if (aqr.info.status.initialized && aqr.info.status.availability.aqSampleData) {
            return (
                <LayoutView
                    style = {{
                        marginTop: 36,
                        marginHorizontal: 6
                    }}
                    orientation = 'horizontal'
                    alignment = 'stretch'
                    selfAlignment = 'stretch'
                >
                    <LayoutView
                        orientation = 'horizontal'
                        alignment = 'center'
                        selfAlignment = 'stretch'
                    >
                        <LayoutView
                            orientation = 'horizontal'
                            alignment = 'end'
                            selfAlignment = 'stretch'
                        >
                            <TitleText size = 'small'>{ aqr.info.reportingRegionName }</TitleText>
                            <CaptionText indentation = { 9 }>{ lastUpdatedMessage }</CaptionText>
                        </LayoutView>
                        <LayoutView
                            style = {{
                                marginVertical: 9
                            }}
                            orientation = 'horizontal'
                            alignment = 'center'
                            selfAlignment = 'stretch'
                        >
                            <TouchableOpacity onPress = {() => {
                                component.outgoing(EVENT.ON.SHOW_AQ_ACTIONABLE_TIP_MODAL).emit(() => {
                                    return {
                                        visible: true,
                                        aqSample: {
                                            aqParam: aqSummary.aqParam,
                                            aqAlertMessage: aqSummary.aqAlertMessage,
                                            aqAlertIndex: aqSummary.aqAlertIndex,
                                            aqi: aqSummary.aqi
                                        }
                                    };
                                });
                            }}>
                                <LayoutView
                                    orientation = 'vertical'
                                    alignment = 'center'
                                >
                                    <Text
                                        style = {{
                                            ...Ht.Theme.general.dropShadow.extraShallow,
                                            flexWrap: `wrap`,
                                            textAlign: `center`,
                                            textDecorationLine: `none`,
                                            fontSize: 50,
                                            fontWeight: `800`,
                                            color: Ht.Theme.general.aqAlertColors[aqSummary.aqAlertIndex],
                                            backgroundColor: `transparent`
                                        }}
                                        ellipsizeMode = 'tail'
                                        numberOfLines = { 1 }
                                    >{ aqSummary.aqAlertMessage }</Text>
                                    <Image
                                        style = {{
                                            ...Ht.Theme.general.dropShadow.extraShallow,
                                            height: 48,
                                            width: 40,
                                            marginLeft: 15,
                                            tintColor: Ht.Theme.general.aqAlertColors[aqSummary.aqAlertIndex]
                                        }}
                                        resizeMode = 'contain'
                                        source = { faceIndicator }
                                    />
                                </LayoutView>
                            </TouchableOpacity>
                        </LayoutView>
                    </LayoutView>
                    <LayoutView
                        style = {{
                            ...Ht.Theme.general.dropShadow.extraShallow,
                            borderRadius: 16,
                            padding: 3
                        }}
                        orientation = 'horizontal'
                        alignment = 'center'
                        selfAlignment = 'stretch'
                        overlay = 'opaque'
                    >
                        <LayoutView
                            style = {{
                                marginLeft: `${aqiScaleIndex}%`
                            }}
                            orientation = 'horizontal'
                            alignment = 'stretch'
                            selfAlignment = 'stretch'
                        >
                            <TouchableOpacity onPress = {() => {
                                component.outgoing(EVENT.ON.SHOW_AQ_ACTIONABLE_TIP_MODAL).emit(() => {
                                    return {
                                        visible: true,
                                        aqSample: {
                                            aqParam: aqSummary.aqParam,
                                            aqAlertMessage: aqSummary.aqAlertMessage,
                                            aqAlertIndex: aqSummary.aqAlertIndex,
                                            aqi: aqSummary.aqi
                                        }
                                    };
                                });
                            }}>
                                <ImageBackground
                                    style = {{
                                        ...Ht.Theme.general.dropShadow.extraShallow,
                                        flexDirection: `column`,
                                        justifyContent: `center`,
                                        alignItems: `center`,
                                        width: 58,
                                        height: 44,
                                        paddingBottom: 6
                                    }}
                                    resizeMode = 'stretch'
                                    source = { Ht.Theme.imageSource.markers[aqSummary.aqAlertIndex] }
                                >
                                    <TitleText color = { Ht.Theme.palette.white }>{ aqSummary.aqi }</TitleText>
                                </ImageBackground>
                            </TouchableOpacity>
                        </LayoutView>
                        <LayoutView
                            style = {{
                                width: CONSTANT.GENERAL.DEVICE_WIDTH - 75,
                                marginLeft: -9,
                                marginBottom: 3
                            }}
                            orientation = 'vertical'
                            alignment = 'stretch'
                        >
                            <CaptionText color = { Ht.Theme.general.aqAlertColors[0] }
                            > Good </CaptionText>
                            <CaptionText color = { Ht.Theme.general.aqAlertColors[1] }
                            > Moderate </CaptionText>
                            <CaptionText color = { Ht.Theme.general.aqAlertColors[2] }
                            > Unhealthy </CaptionText>
                            <CaptionText color = { Ht.Theme.general.aqAlertColors[3] }
                            > Harmful </CaptionText>
                            <CaptionText color = { Ht.Theme.general.aqAlertColors[4] }
                            > Deadly </CaptionText>
                        </LayoutView>
                        <Image
                            style = {{
                                height: 10,
                                width: CONSTANT.GENERAL.DEVICE_WIDTH - 70
                            }}
                            resizeMode = 'stretch'
                            source = { Ht.Theme.imageSource.aqGradientScale }
                        />
                        <LayoutView
                            style = {{
                                width: CONSTANT.GENERAL.DEVICE_WIDTH - 75,
                                marginLeft: -6
                            }}
                            orientation = 'vertical'
                            alignment = 'stretch'
                        >
                            <InfoText
                                size = 'small'
                                color = { Ht.Theme.general.aqAlertColors[0] }
                            > 0     </InfoText>
                            <InfoText
                                size = 'small'
                                color = { Ht.Theme.general.aqAlertColors[1] }
                            > 100 </InfoText>
                            <InfoText
                                size = 'small'
                                color = { Ht.Theme.general.aqAlertColors[2] }
                            > 200 </InfoText>
                            <InfoText
                                size = 'small'
                                color = { Ht.Theme.general.aqAlertColors[3] }
                            > 300 </InfoText>
                            <InfoText
                                size = 'small'
                                color = { Ht.Theme.general.aqAlertColors[4] }
                            > 400 </InfoText>
                        </LayoutView>
                    </LayoutView>
                    <LayoutView
                        style = {{
                            marginVertical: 6,
                            marginLeft: 21
                        }}
                        orientation = 'vertical'
                        alignment = 'center'
                        selfAlignment = 'start'
                    >
                        <TouchableOpacity onPress = { component.onPressDonateToYEAButton }>
                            <CaptionText
                                size = 'large'
                                color = { Ht.Theme.palette.pink }
                                indentation = { 6 }
                            > Donate to Y.E.A </CaptionText>
                        </TouchableOpacity>
                        <TouchableOpacity onPress = { component.onPressGoToYEAFBButton }>
                            <Image
                                style = {{
                                    width: 24,
                                    height: 24,
                                    margin: 6
                                }}
                                resizeMode = 'contain'
                                source = { Ht.Theme.imageSource.fbLogo }
                            />
                        </TouchableOpacity>
                        <TouchableOpacity onPress = { component.onPressGoToAboutYEAInstagramButton }>
                            <Image
                                style = {{
                                    width: 24,
                                    height: 24,
                                    margin: 6
                                }}
                                resizeMode = 'contain'
                                source = { Ht.Theme.imageSource.instagramLogo }
                            />
                        </TouchableOpacity>
                    </LayoutView>
                    <InfoText
                        size = 'large'
                        indentation = { 3 }
                    >{ `${CONSTANT.AQ_ACTIONABLE.PM25_TIPS[aqSummary.aqAlertIndex].actionableMessage}` }</InfoText>
                </LayoutView>
            );
        } else if (aqr.info.status.initialized && !aqr.info.status.availability.aqSampleData) {
            return (
                <LayoutView
                    style = {{
                        marginTop: 36,
                        marginHorizontal: 6
                    }}
                    orientation = 'horizontal'
                    alignment = 'stretch'
                    selfAlignment = 'stretch'
                >
                    <HeadlineText size = 'small' > Regional Air Quality </HeadlineText>
                    <HeadlineText size = 'small' > Summary Is Unavailable </HeadlineText>
                    <RaisedButton
                        label = 'REFRESH'
                        onPress = { component.onRefreshAQRData }
                    />
                </LayoutView>
            );
        } else {
            return null;
        }
    },
    renderLoadingIndicator: function renderLoadingIndicator () {
        return (
            <LayoutView
                style = {{
                    ...Ht.Theme.general.dropShadow.shallow,
                    position: `absolute`,
                    top: CONSTANT.GENERAL.DEVICE_HEIGHT * 0.18,
                    padding: 15
                }}
                overlay = 'opaque'
                orientation = 'horizontal'
                alignment = 'center'
                selfAlignment = 'center'
            >
                <ActivityIndicator size = 'large' />
                <HeadlineText> Please Wait... </HeadlineText>
            </LayoutView>
        );
    },
    render: function render () {
        const component = this;
        const {
            aqs,
            aqr,
            aqActionableTip,
            aqWhatis
        } = component.state;
        const aqSamples = aqs.aqSamples.map((aqSample) => {
            return {
                ...aqSample,
                source: `sensor`
            };
        }).concat(aqr.aqSamples.map((aqSample) => {
            return {
                ...aqSample,
                source: `outdoor`
            };
        }));
        const aqSummary = aqSamples.reduce((_aqSummary, aqSample) => {
            if (_aqSummary.aqi <= aqSample. aqi) {
                return {
                    aqi: aqSample.aqi,
                    aqParam: aqSample.aqParam,
                    aqAlertIndex: aqSample.aqAlertIndex,
                    aqAlertMessage: aqSample.aqAlertMessage
                };
            } else {
                return _aqSummary;
            }
        }, {
            aqi: 0,
            aqParam: ``,
            aqAlertIndex: 0,
            aqAlertMessage: ``
        });
        return (
            <ScreenView>
                <AQGradientView aqAlertIndex = { aqSummary.aqAlertIndex }>
                    <BodyView>
                        {
                            aqr.info.status.loading ? null : component.renderAQMonitor(aqSummary)
                        }
                        {
                            !aqr.info.status.loading ? null : component.renderLoadingIndicator()
                        }
                    </BodyView>
                </AQGradientView>
                {
                    aqr.info.status.loading ? null : <MonitorTabNavigator
                        screenProps = {{
                            component,
                            aqSamples
                        }}
                    />
                }
                <AQActionableTipModal
                    { ...aqActionableTip }
                    onClose = { component.onCloseAQActionalbleTipModal }
                />
                <AQWhatisModal
                    { ...aqWhatis }
                    onClose = { component.onCloseAQWhatisModal }
                />
            </ScreenView>
        );
    }
});
export default MonitorViewInterface;
