/**
  * Copyright (c) 2017-present, Virida YEAwareness, Org. All rights reserved.
  *
  *------------------------------------------------------------------------
  *
  * @description -  Virida client-native app ui color theme.
  *
  * @author Tuan Le (tuan.t.lei@gmail.com)
  *
  *------------------------------------------------------------------------
  * @flow
  */
'use strict'; // eslint-disable-line

export default {
    opacity: `aa`,
    aqAlerts: [
        `#8cd75f`,
        `#e3d44a`,
        `#eea426`,
        `#e71b1b`,
        `#ba0c94`,
        `#690721`
    ],
    light: {
        default: `#22333b`,
        accent: `#53F2F7`,
        primary: `#38D1C3`,
        secondary: `#0C60A3`,
        disabled: `#b6cdd4`
    },
    dark: {
        default: `#e7f4f6`,
        accent: `#53F2F7`,
        primary: `#38D1C3`,
        secondary: `#0C60A3`,
        disabled: `#b6cdd4`
    },
    text: {
        dark: `#e7f4f6`,
        light: `#319DB4`
    }
};
