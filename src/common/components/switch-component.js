/**
 * Copyright (c) 2017-present, Virida YEAwareness, Org. All rights reserved.
 *
 *------------------------------------------------------------------------
 *
 * @module SwitchComponent
 * @description - Virida client-native app switch wrapper component.
 *
 * @author Tuan Le (tuan.t.lei@gmail.com)
 *
 *------------------------------------------------------------------------
 * @flow
 */
'use strict'; // eslint-disable-line

import { Ht } from 'hypertoxin';

import React from 'react';

import ReactNative from 'react-native'; // eslint-disable-line

import PropTypes from 'prop-types';

const {
    Component
} = React;

const {
    Switch
} = ReactNative;

const {
    LayoutView
} = Ht.View;

const {
    SubtitleText,
    InfoText
} = Ht.Text;

export default class SwitchComponent extends Component {
    static propTypes = {
        initialValue: PropTypes.bool,
        title: PropTypes.string,
        info: PropTypes.string,
        onToggle: PropTypes.func
    }
    static defaultProps = {
        initialValue: false,
        title: ``,
        info: ``,
        onToggle: () => null
    }
    constructor (props) {
        super(props);
        this.state = {
            value: props.initialValue
        };
    }
    value = () => {
        const component = this;
        const {
            value
        } = component.state;
        return value;
    };
    onValueChange = () => {
        const component = this;
        const {
            onToggle
        } = component.props;
        component.setState((prevState) => {
            return {
                value: !prevState.value
            };
        }, () => {
            onToggle();
        });
    }
    componentWillReceiveProps (nextProps) {
        const component = this;
        const {
            initialValue
        } = nextProps;
        component.setState(() => {
            return {
                value: initialValue
            };
        });
    }
    render () {
        const component = this;
        const {
            title,
            info
        } = component.props;
        const {
            value
        } = component.state;
        return (
            <LayoutView
                style = {{
                    ...Ht.Theme.general.dropShadow.extraShallow,
                    marginVertical: 6,
                    padding: 6
                }}
                overlay = 'opaque'
                orientation = 'horizontal'
                alignment = 'stretch'
                selfAlignment = 'stretch'
            >
                <LayoutView
                    orientation = 'vertical'
                    alignment = 'stretch'
                    selfAlignment = 'stretch'
                >
                    <SubtitleText>{ title }</SubtitleText>
                    <Switch
                        value = { value }
                        tintColor = { Ht.Theme.palette.lightGrey }
                        onTintColor = { Ht.Theme.general.color.light.primary }
                        onValueChange = { component.onValueChange }
                    />
                </LayoutView>
                <InfoText
                    alignment = 'left'
                    indentation = { 3 }
                >{ info }</InfoText>
            </LayoutView>
        );
    }
}
