/**
 * Copyright (c) 2017-present, Virida YEAwareness, Org. All rights reserved.
 *
 *------------------------------------------------------------------------
 *
 * @module FBLikeWebViewComponent
 * @description - Virida client-native app facebook like webview component.
 *
 * @author Tuan Le (tuan.t.lei@gmail.com)
 *
 *------------------------------------------------------------------------
 * @flow
 */
'use strict'; // eslint-disable-line

import { Hf } from 'hyperflow';

import { Ht } from 'hypertoxin';

import React from 'react';

import ReactNative from 'react-native'; // eslint-disable-line

import CONSTANT from '../../common/constant';

const {
    Component
} = React;

const {
    Linking,
    NetInfo,
    View,
    WebView
} = ReactNative;

const {
    LayoutView
} = Ht.View;

const {
    TitleText
} = Ht.Text;

export default class FBLikeWebViewComponent extends Component {
    constructor (props) {
        super(props);
        this.webView = null;
        this.state = {
            disabled: false,
            shouldShowWebView: false
        };
    }
    onShouldStartLoadWithRequest = (navigator) => {
        const component = this;
        if (navigator.url.includes(`about:blank`) ||
            navigator.url.includes(`https://www.facebook.com/plugins/like`) ||
            navigator.url.includes(`http://staticxx.facebook.com/connect`)) {
            return true;
        } else if (navigator.url.includes(`https://www.facebook.com/plugins/error`)) {
            Linking.openURL(navigator.url).catch(() => {
                Hf.log(`warn1`, `FBLikeWebViewComponent - Unable to open Facebook website at ${navigator.url}.`);
            });
            component.webView.stopLoading();
            return false;
        } else {
            return true;
        }
    }
    componentWillMount () {
        const component = this;
        NetInfo.isConnected.fetch().then((online) => {
            if (online) {
                component.setState(() => {
                    return {
                        disabled: false
                    };
                });
            } else {
                component.setState(() => {
                    return {
                        disabled: true
                    };
                });
            }
        });
    }
    componentDidMount () {
        const component = this;
        setTimeout(() => {
            component.setState(() => {
                return {
                    shouldShowWebView: true
                };
            });
        }, CONSTANT.GENERAL.LOADING_WEBVIEW_DELAY_MS);
    }
    render () {
        const component = this;
        const {
            disabled,
            shouldShowWebView
        } = component.state;

        if (disabled) {
            return (
                <LayoutView
                    style = {{
                        ...Ht.Theme.general.dropShadow.shallow,
                        width: 105,
                        height: 28,
                        margin: 6,
                        borderRadius: 3,
                        backgroundColor: Ht.Theme.general.color.light.disabled
                    }}
                    orientation = 'horizontal'
                    alignment = 'center'
                    selfAlignment = 'stretch'
                    overlay = 'opaque'
                >
                    <TitleText
                        size = 'small'
                        color = { Ht.Theme.palette.white }
                    > Like </TitleText>
                </LayoutView>
            );
        } else {
            return (
                <View
                    style = {{
                        ...Ht.Theme.general.dropShadow.shallow,
                        width: 105,
                        height: 44
                    }}
                >
                    {
                        shouldShowWebView ? <WebView
                            ref = {(webView) => {
                                component.webView = webView;
                            }}
                            style = {{
                                backgroundColor: `transparent`
                            }}
                            contentInset = {{
                                left: 0
                            }}
                            scalesPageToFit = { false }
                            automaticallyAdjustContentInsets = { true }
                            javaScriptEnabled = { true }
                            scrollEnabled = { false }
                            source = {{
                                html: `
                                <?xml version="1.0" encoding="utf-8"?>
                                <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
                                <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
                                    <head>
                                        <title>CNN</title>
                                        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
                                    </head>
                                    <body>
                                        <!-- load facebook SDK -->
                                        <div id="fb-root"></div>
                                        <script>(function(d, s, id) {
                                            var js, fjs = d.getElementsByTagName(s)[0];
                                            if (d.getElementById(id)) return;
                                            js = d.createElement(s); js.id = id;
                                            js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.12&appId=${CONSTANT.API_KEY.FB_APP_ID}';
                                            fjs.parentNode.insertBefore(js, fjs);
                                        }(document, 'script', 'facebook-jssdk'));</script>
                                        <!-- like button code -->
                                        <div
                                            class="fb-like"
                                            data-href="${CONSTANT.URL.YEA_FB2}"
                                            data-width="100"
                                            data-height="44"
                                            data-layout="button_count"
                                            data-action="like"
                                            data-size="large"
                                            data-show-faces="false"
                                            data-share="false">
                                        </div>
                                        <iframe
                                            src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2FEarth247%2F&width=50&layout=button_count&action=like&size=large&show_faces=false&share=false&height=21&appId=357208711456901"
                                            width="100"
                                            height="44"
                                            style="border:none;overflow:hidden"
                                            scrolling="no"
                                            frameborder="0"
                                            allowTransparency="true"
                                            allow="encrypted-media">
                                        </iframe>
                                    </body>
                                </html>
                                `
                            }}
                            onShouldStartLoadWithRequest = { component.onShouldStartLoadWithRequest }
                            onNavigationStateChange = { component.onShouldStartLoadWithRequest }
                        /> : null
                    }
                </View>
            );
        }
    }
}
