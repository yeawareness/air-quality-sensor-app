/**
 * Copyright (c) 2017-present, Virida YEAwareness, Org. All rights reserved.
 *
 *------------------------------------------------------------------------
 *
 * @module AQActionableTipModalComponent
 * @description - Virida client-native app air quality actionable tip modal component.
 *
 * @author Tuan Le (tuan.t.lei@gmail.com)
 *
 *------------------------------------------------------------------------
 * @flow
 */
'use strict'; // eslint-disable-line

import { Ht } from 'hypertoxin';

import React from 'react';

import ReactNative from 'react-native'; // eslint-disable-line

import PropTypes from 'prop-types';

import CollapsibleView from 'react-native-collapsible';

import CONSTANT from '../constant';

const {
    Component
} = React;

const {
    Modal,
    ScrollView
} = ReactNative;

const {
    HeaderView,
    BodyView,
    LayoutView,
    ItemView
} = Ht.View;

const {
    FlatButton
} = Ht.Button;

const {
    IconImage
} = Ht.Image;

const {
    HeadlineText,
    TitleText,
    InfoText
} = Ht.Text;

const {
    Divider
} = Ht.Misc;

export default class AQActionableTipModalComponent extends Component {
    static propTypes = {
        visible: PropTypes.bool,
        aqSample: PropTypes.shape({
            aqParam: PropTypes.string,
            aqAlertMessage: PropTypes.string,
            aqAlertIndex: PropTypes.number,
            aqi: PropTypes.number
        }),
        onClose: PropTypes.func
    }
    static defaultProps = {
        visible: false,
        aqSample: {
            aqParam: ``,
            aqAlertMessage: ``,
            aqAlertIndex: 0,
            aqi: 0
        },
        onClose: () => null
    }
    constructor (props) {
        super(props);
        this.state = {
            sectionVisibility: {
                healthImactSection: true,
                recommendedActionSection: false
            }
        };
    }
    onToggleHealthImpactSection = () => {
        const component = this;
        component.setState((prevState) => {
            return {
                sectionVisibility: {
                    healthImactSection: !prevState.sectionVisibility.healthImactSection
                }
            };
        });
    }
    onToggleRecommendedActionsSection = () => {
        const component = this;

        component.setState((prevState) => {
            return {
                sectionVisibility: {
                    recommendedActionSection: !prevState.sectionVisibility.recommendedActionSection
                }
            };
        });
    }
    renderSections (aqActionableTip) {
        const component = this;
        const {
            sectionVisibility
        } = component.state;

        return (
            <ScrollView
                style = {{
                    flexShrink: 1,
                    flexDirection: `column`,
                    width: CONSTANT.GENERAL.DEVICE_WIDTH,
                    marginHorizontal: 6,
                    paddingVertical: 6
                }}
                scrollEnabled = { true }
                directionalLockEnabled = { true }
                scrollEventThrottle = { 16 }
            >
                <ItemView
                    key = '1'
                    style = {{
                        width: CONSTANT.GENERAL.DEVICE_WIDTH
                    }}
                    onPress = { component.onToggleHealthImpactSection }
                >
                    <TitleText
                        room = 'content-left'
                        indentation = { 6 }
                    > Health Impact </TitleText>
                    <FlatButton
                        room = 'action-right'
                        overlay = 'transparent'
                        onPress = { component.onToggleHealthImpactSection }
                    >
                        <IconImage
                            room = 'content-center'
                            source = { sectionVisibility.healthImactSection ? `collapse` : `expand` }
                            size = 'large'
                        />
                    </FlatButton>
                </ItemView>
                <CollapsibleView
                    style = {{
                        alignItems: `center`,
                        width: CONSTANT.GENERAL.DEVICE_WIDTH,
                        height: 70,
                        paddingHorizontal: 6
                    }}
                    collapsed = { !sectionVisibility.healthImactSection }
                >
                    <InfoText>{ aqActionableTip.healthImpactMessage }</InfoText>
                </CollapsibleView>
                <Divider/>
                <ItemView
                    key = '2'
                    style = {{
                        width: CONSTANT.GENERAL.DEVICE_WIDTH
                    }}
                    onPress = { component.onToggleRecommendedActionsSection }
                >
                    <TitleText
                        room = 'content-left'
                        indentation = { 6 }
                    > Recommended Actions </TitleText>
                    <FlatButton
                        room = 'action-right'
                        overlay = 'transparent'
                        onPress = { component.onToggleRecommendedActionsSection }
                    >
                        <IconImage
                            room = 'content-center'
                            source = { sectionVisibility.recommendedActionSection ? `collapse` : `expand` }
                            size = 'large'
                        />
                    </FlatButton>
                </ItemView>
                <CollapsibleView
                    style = {{
                        alignItems: `center`,
                        width: CONSTANT.GENERAL.DEVICE_WIDTH,
                        height: 70,
                        paddingHorizontal: 6
                    }}
                    collapsed = { !sectionVisibility.recommendedActionSection }
                >
                    <InfoText>{ aqActionableTip.actionableMessage }</InfoText>
                </CollapsibleView>
            </ScrollView>
        );
    }
    render () {
        const component = this;
        const {
            visible,
            aqSample,
            onClose
        } = component.props;
        let aqActionableTip = null;

        switch (aqSample.aqParam) { // eslint-disable-line
        case `pm25`:
            aqActionableTip = CONSTANT.AQ_ACTIONABLE.PM25_TIPS[aqSample.aqAlertIndex];
            break;
        case `voc`:
            aqActionableTip = CONSTANT.AQ_ACTIONABLE.VOC_TIPS[aqSample.aqAlertIndex];
            break;
        }

        return aqActionableTip !== null ? (
            <Modal
                animationType = 'slide'
                transparent = { false }
                visible = { visible }
            >
                <HeaderView label = 'Actionable Tip' >
                    <FlatButton
                        room = 'action-left'
                        overlay = 'transparent'
                        onPress = { onClose }
                    >
                        <IconImage
                            room = 'content-center'
                            source = 'close'
                            size = 'large'
                        />
                    </FlatButton>
                </HeaderView>
                <BodyView>
                    <HeadlineText color = { Ht.Theme.general.aqAlertColors[aqSample.aqAlertIndex] }
                    >{ `Air Quality is ${aqSample.aqAlertMessage}` }</HeadlineText>
                    <LayoutView
                        style = {{
                            height: 70
                        }}
                        orientation = 'vertical'
                        alignment = 'stretch'
                        selfAlignment = 'center'
                    >
                        <HeadlineText> AQI  </HeadlineText>
                        <TitleText
                            size = 'large'
                            color = { Ht.Theme.general.aqAlertColors[aqSample.aqAlertIndex] }
                        >{ aqSample.aqi }</TitleText>
                    </LayoutView>
                    <InfoText size = 'large' >{ aqActionableTip.pollutantStatus }</InfoText>
                    <Divider/>
                    {
                        component.renderSections(aqActionableTip)
                    }
                </BodyView>
            </Modal>
        ) : null;
    }
}
