/**
 * Copyright (c) 2017-present, Virida YEAwareness, Org. All rights reserved.
 *
 *------------------------------------------------------------------------
 *
 * @module AQWhatisModalComponent
 * @description - Virida client-native app air quality whatis modal component.
 *
 * @author Tuan Le (tuan.t.lei@gmail.com)
 *
 *------------------------------------------------------------------------
 * @flow
 */
'use strict'; // eslint-disable-line

import { Ht } from 'hypertoxin';

import React from 'react';

import ReactNative from 'react-native'; // eslint-disable-line

import PropTypes from 'prop-types';

import CollapsibleView from 'react-native-collapsible';

import CONSTANT from '../constant';

const {
    Component
} = React;

const {
    Image,
    Modal,
    ScrollView
} = ReactNative;

const {
    HeaderView,
    BodyView,
    LayoutView,
    ItemView
} = Ht.View;

const {
    FlatButton
} = Ht.Button;

const {
    IconImage
} = Ht.Image;

const {
    HeadlineText,
    TitleText,
    InfoText
} = Ht.Text;

const {
    Divider
} = Ht.Misc;

export default class AQWhatisModalComponent extends Component {
    static propTypes = {
        visible: PropTypes.bool,
        aqSample: PropTypes.shape({
            aqParam: PropTypes.string,
            aqAlertIndex: PropTypes.number,
            aqi: PropTypes.number,
            aqConcentration: PropTypes.number,
            aqUnit: PropTypes.string
        }),
        onClose: PropTypes.func
    }
    static defaultProps = {
        visible: false,
        aqSample: {
            aqParam: ``,
            aqAlertIndex: 0,
            aqi: 0,
            aqConcentration: 0,
            aqUnit: ``
        },
        onClose: () => null
    }
    constructor (props) {
        super(props);
        this.state = {
            sectionVisibility: {
                whatisSection: true,
                healthImactSection: false,
                sourceOfEmissionSection: false,
                environmentalDamageSection: false
            }
        };
    }
    onToggleWhatisSection = () => {
        const component = this;
        component.setState((prevState) => {
            return {
                sectionVisibility: {
                    whatisSection: !prevState.sectionVisibility.whatisSection
                }
            };
        });
    }
    onToggleSourceOfEmissionSection = () => {
        const component = this;
        component.setState((prevState) => {
            return {
                sectionVisibility: {
                    sourceOfEmissionSection: !prevState.sectionVisibility.sourceOfEmissionSection
                }
            };
        });
    }
    onToggleHealthImpactSection = () => {
        const component = this;
        component.setState((prevState) => {
            return {
                sectionVisibility: {
                    healthImactSection: !prevState.sectionVisibility.healthImactSection
                }
            };
        });
    }
    onToggleEnvironmentalDamageSection = () => {
        const component = this;
        component.setState((prevState) => {
            return {
                sectionVisibility: {
                    environmentalDamageSection: !prevState.sectionVisibility.environmentalDamageSection
                }
            };
        });
    }
    renderPM25Sections (aqParamInfo) {
        const component = this;
        const {
            sectionVisibility
        } = component.state;

        return (
            <ScrollView
                style = {{
                    flexShrink: 1,
                    flexDirection: `column`,
                    width: CONSTANT.GENERAL.DEVICE_WIDTH,
                    marginHorizontal: 6,
                    paddingVertical: 6
                }}
                scrollEnabled = { true }
                directionalLockEnabled = { true }
                scrollEventThrottle = { 16 }
            >
                <ItemView
                    key = '0'
                    style = {{
                        width: CONSTANT.GENERAL.DEVICE_WIDTH
                    }}
                    onPress = { component.onToggleWhatisSection }
                >
                    <TitleText
                        room = 'content-left'
                        indentation = { 6 }
                    > What is PM2.5? </TitleText>
                    <FlatButton
                        room = 'action-right'
                        overlay = 'transparent'
                        onPress = { component.onToggleWhatisSection }
                    >
                        <IconImage
                            room = 'content-center'
                            source = { sectionVisibility.whatisSection ? `collapse` : `expand` }
                            size = 'large'
                        />
                    </FlatButton>
                </ItemView>
                <CollapsibleView
                    style = {{
                        alignItems: `center`,
                        width: CONSTANT.GENERAL.DEVICE_WIDTH,
                        height: 425,
                        paddingHorizontal: 6
                    }}
                    collapsed = { !sectionVisibility.whatisSection }
                >
                    <InfoText >{ aqParamInfo.whatis }</InfoText>
                    <Image
                        style = {{
                            width: CONSTANT.GENERAL.DEVICE_WIDTH - 25,
                            height: 250
                        }}
                        resizeMode = 'contain'
                        source = { Ht.Theme.imageSource.pm25ScaleDiagram }
                    />
                </CollapsibleView>
                <Divider/>
                <ItemView
                    key = '1'
                    style = {{
                        width: CONSTANT.GENERAL.DEVICE_WIDTH
                    }}
                    onPress = { component.onToggleSourceOfEmissionSection }
                >
                    <TitleText
                        room = 'content-left'
                        indentation = { 6 }
                    > Sources of PM2.5 Pollution </TitleText>
                    <FlatButton
                        room = 'action-right'
                        overlay = 'transparent'
                        onPress = { component.onToggleSourceOfEmissionSection }
                    >
                        <IconImage
                            room = 'content-center'
                            source = { sectionVisibility.sourceOfEmissionSection ? `collapse` : `expand` }
                            size = 'large'
                        />
                    </FlatButton>
                </ItemView>
                <CollapsibleView
                    style = {{
                        alignItems: `center`,
                        width: CONSTANT.GENERAL.DEVICE_WIDTH,
                        height: 200,
                        paddingHorizontal: 6
                    }}
                    collapsed = { !sectionVisibility.sourceOfEmissionSection }
                >
                    <InfoText>{ aqParamInfo.source }</InfoText>
                </CollapsibleView>
                <Divider/>
                <ItemView
                    key = '2'
                    style = {{
                        width: CONSTANT.GENERAL.DEVICE_WIDTH
                    }}
                    onPress = { component.onToggleHealthImpactSection }
                >
                    <TitleText
                        room = 'content-left'
                        indentation = { 6 }
                    > PM2.5 Impacts On Health </TitleText>
                    <FlatButton
                        room = 'action-right'
                        overlay = 'transparent'
                        onPress = { component.onToggleHealthImpactSection }
                    >
                        <IconImage
                            room = 'content-center'
                            source = { sectionVisibility.healthImactSection ? `collapse` : `expand` }
                            size = 'large'
                        />
                    </FlatButton>
                </ItemView>
                <CollapsibleView
                    style = {{
                        alignItems: `center`,
                        width: CONSTANT.GENERAL.DEVICE_WIDTH,
                        height: 600,
                        paddingHorizontal: 6
                    }}
                    collapsed = { !sectionVisibility.healthImactSection }
                >
                    <InfoText >{ aqParamInfo.healthImpact }</InfoText>
                    <Image
                        style = {{
                            width: CONSTANT.GENERAL.DEVICE_WIDTH - 25,
                            height: 350
                        }}
                        resizeMode = 'stretch'
                        source = { Ht.Theme.imageSource.pm25HealthImpact }
                    />
                </CollapsibleView>
                <Divider/>
                <ItemView
                    key = '3'
                    style = {{
                        width: CONSTANT.GENERAL.DEVICE_WIDTH
                    }}
                    onPress = { component.onToggleEnvironmentalDamageSection }
                >
                    <TitleText
                        room = 'content-left'
                        indentation = { 6 }
                    > PM2.5 Impacts On The Environment </TitleText>
                    <FlatButton
                        room = 'action-right'
                        overlay = 'transparent'
                        onPress = { component.onToggleEnvironmentalDamageSection }
                    >
                        <IconImage
                            room = 'content-center'
                            source = { sectionVisibility.environmentalDamageSection ? `collapse` : `expand` }
                            size = 'large'
                        />
                    </FlatButton>
                </ItemView>
                <CollapsibleView
                    style = {{
                        alignItems: `center`,
                        width: CONSTANT.GENERAL.DEVICE_WIDTH,
                        height: 200,
                        paddingHorizontal: 6
                    }}
                    collapsed = { !sectionVisibility.environmentalDamageSection }
                >
                    <InfoText >{ aqParamInfo.environmentalDamage }</InfoText>
                </CollapsibleView>
            </ScrollView>
        );
    }
    renderNO2Sections (aqParamInfo) {
        const component = this;
        const {
            sectionVisibility
        } = component.state;

        return (
            <ScrollView
                style = {{
                    flexShrink: 1,
                    flexDirection: `column`,
                    width: CONSTANT.GENERAL.DEVICE_WIDTH,
                    marginHorizontal: 6,
                    paddingVertical: 6
                }}
                scrollEnabled = { true }
                directionalLockEnabled = { true }
                scrollEventThrottle = { 16 }
            >
                <ItemView
                    key = '0'
                    style = {{
                        width: CONSTANT.GENERAL.DEVICE_WIDTH
                    }}
                    onPress = { component.onToggleWhatisSection }
                >
                    <TitleText
                        room = 'content-left'
                        indentation = { 6 }
                    > What is NO2? </TitleText>
                    <FlatButton
                        room = 'action-right'
                        overlay = 'transparent'
                        onPress = { component.onToggleWhatisSection }
                    >
                        <IconImage
                            room = 'content-center'
                            source = { sectionVisibility.whatisSection ? `collapse` : `expand` }
                            size = 'large'
                        />
                    </FlatButton>
                </ItemView>
                <CollapsibleView
                    style = {{
                        alignItems: `center`,
                        width: CONSTANT.GENERAL.DEVICE_WIDTH,
                        height: 100,
                        paddingHorizontal: 6
                    }}
                    collapsed = { !sectionVisibility.whatisSection }
                >
                    <InfoText >{ aqParamInfo.whatis }</InfoText>
                </CollapsibleView>
                <Divider/>
                <ItemView
                    key = '1'
                    style = {{
                        width: CONSTANT.GENERAL.DEVICE_WIDTH
                    }}
                    onPress = { component.onToggleSourceOfEmissionSection }
                >
                    <TitleText
                        room = 'content-left'
                        indentation = { 6 }
                    > Sources of NO2 Pollution </TitleText>
                    <FlatButton
                        room = 'action-right'
                        overlay = 'transparent'
                        onPress = { component.onToggleSourceOfEmissionSection }
                    >
                        <IconImage
                            room = 'content-center'
                            source = { sectionVisibility.sourceOfEmissionSection ? `collapse` : `expand` }
                            size = 'large'
                        />
                    </FlatButton>
                </ItemView>
                <CollapsibleView
                    style = {{
                        alignItems: `center`,
                        width: CONSTANT.GENERAL.DEVICE_WIDTH,
                        height: 100,
                        paddingHorizontal: 6
                    }}
                    collapsed = { !sectionVisibility.sourceOfEmissionSection }
                >
                    <InfoText>{ aqParamInfo.source }</InfoText>
                </CollapsibleView>
                <Divider/>
                <ItemView
                    key = '2'
                    style = {{
                        width: CONSTANT.GENERAL.DEVICE_WIDTH
                    }}
                    onPress = { component.onToggleHealthImpactSection }
                >
                    <TitleText
                        room = 'content-left'
                        indentation = { 6 }
                    > NO2 Impacts On Health </TitleText>
                    <FlatButton
                        room = 'action-right'
                        overlay = 'transparent'
                        onPress = { component.onToggleHealthImpactSection }
                    >
                        <IconImage
                            room = 'content-center'
                            source = { sectionVisibility.healthImactSection ? `collapse` : `expand` }
                            size = 'large'
                        />
                    </FlatButton>
                </ItemView>
                <CollapsibleView
                    style = {{
                        alignItems: `center`,
                        width: CONSTANT.GENERAL.DEVICE_WIDTH,
                        height: 300,
                        paddingHorizontal: 6
                    }}
                    collapsed = { !sectionVisibility.healthImactSection }
                >
                    <InfoText >{ aqParamInfo.healthImpact }</InfoText>
                </CollapsibleView>
                <Divider/>
                <ItemView
                    key = '3'
                    style = {{
                        width: CONSTANT.GENERAL.DEVICE_WIDTH
                    }}
                    onPress = { component.onToggleEnvironmentalDamageSection }
                >
                    <TitleText
                        room = 'content-left'
                        indentation = { 6 }
                    > NO2 Impacts On The Environment </TitleText>
                    <FlatButton
                        room = 'action-right'
                        overlay = 'transparent'
                        onPress = { component.onToggleEnvironmentalDamageSection }
                    >
                        <IconImage
                            room = 'content-center'
                            source = { sectionVisibility.environmentalDamageSection ? `collapse` : `expand` }
                            size = 'large'
                        />
                    </FlatButton>
                </ItemView>
                <CollapsibleView
                    style = {{
                        alignItems: `center`,
                        width: CONSTANT.GENERAL.DEVICE_WIDTH,
                        height: 200,
                        paddingHorizontal: 6
                    }}
                    collapsed = { !sectionVisibility.environmentalDamageSection }
                >
                    <InfoText >{ aqParamInfo.environmentalDamage }</InfoText>
                </CollapsibleView>
            </ScrollView>
        );
    }
    renderO3Sections (aqParamInfo) {
        const component = this;
        const {
            sectionVisibility
        } = component.state;

        return (
            <ScrollView
                style = {{
                    flexShrink: 1,
                    flexDirection: `column`,
                    width: CONSTANT.GENERAL.DEVICE_WIDTH,
                    marginHorizontal: 6,
                    paddingVertical: 6
                }}
                scrollEnabled = { true }
                directionalLockEnabled = { true }
                scrollEventThrottle = { 16 }
            >
                <ItemView
                    key = '0'
                    style = {{
                        width: CONSTANT.GENERAL.DEVICE_WIDTH
                    }}
                    onPress = { component.onToggleWhatisSection }
                >
                    <TitleText
                        room = 'content-left'
                        indentation = { 6 }
                    > What is O3? </TitleText>
                    <FlatButton
                        room = 'action-right'
                        overlay = 'transparent'
                        onPress = { component.onToggleWhatisSection }
                    >
                        <IconImage
                            room = 'content-center'
                            source = { sectionVisibility.whatisSection ? `collapse` : `expand` }
                            size = 'large'
                        />
                    </FlatButton>
                </ItemView>
                <CollapsibleView
                    style = {{
                        alignItems: `center`,
                        width: CONSTANT.GENERAL.DEVICE_WIDTH,
                        height: 300,
                        paddingHorizontal: 6
                    }}
                    collapsed = { !sectionVisibility.whatisSection }
                >
                    <InfoText >{ aqParamInfo.whatis }</InfoText>
                </CollapsibleView>
                <Divider/>
                <ItemView
                    key = '1'
                    style = {{
                        width: CONSTANT.GENERAL.DEVICE_WIDTH
                    }}
                    onPress = { component.onToggleSourceOfEmissionSection }
                >
                    <TitleText
                        room = 'content-left'
                        indentation = { 6 }
                    > Sources of O3 Pollution </TitleText>
                    <FlatButton
                        room = 'action-right'
                        overlay = 'transparent'
                        onPress = { component.onToggleSourceOfEmissionSection }
                    >
                        <IconImage
                            room = 'content-center'
                            source = { sectionVisibility.sourceOfEmissionSection ? `collapse` : `expand` }
                            size = 'large'
                        />
                    </FlatButton>
                </ItemView>
                <CollapsibleView
                    style = {{
                        alignItems: `center`,
                        width: CONSTANT.GENERAL.DEVICE_WIDTH,
                        height: 100,
                        paddingHorizontal: 6
                    }}
                    collapsed = { !sectionVisibility.sourceOfEmissionSection }
                >
                    <InfoText>{ aqParamInfo.source }</InfoText>
                </CollapsibleView>
                <Divider/>
                <ItemView
                    key = '2'
                    style = {{
                        width: CONSTANT.GENERAL.DEVICE_WIDTH
                    }}
                    onPress = { component.onToggleHealthImpactSection }
                >
                    <TitleText
                        room = 'content-left'
                        indentation = { 6 }
                    > O3 Impacts On Health </TitleText>
                    <FlatButton
                        room = 'action-right'
                        overlay = 'transparent'
                        onPress = { component.onToggleHealthImpactSection }
                    >
                        <IconImage
                            room = 'content-center'
                            source = { sectionVisibility.healthImactSection ? `collapse` : `expand` }
                            size = 'large'
                        />
                    </FlatButton>
                </ItemView>
                <CollapsibleView
                    style = {{
                        alignItems: `center`,
                        width: CONSTANT.GENERAL.DEVICE_WIDTH,
                        height: 300,
                        paddingHorizontal: 6
                    }}
                    collapsed = { !sectionVisibility.healthImactSection }
                >
                    <InfoText >{ aqParamInfo.healthImpact }</InfoText>
                </CollapsibleView>
                <Divider/>
                <ItemView
                    key = '3'
                    style = {{
                        width: CONSTANT.GENERAL.DEVICE_WIDTH
                    }}
                    onPress = { component.onToggleEnvironmentalDamageSection }
                >
                    <TitleText
                        room = 'content-left'
                        indentation = { 6 }
                    > O3 Impacts On The Environment </TitleText>
                    <FlatButton
                        room = 'action-right'
                        overlay = 'transparent'
                        onPress = { component.onToggleEnvironmentalDamageSection }
                    >
                        <IconImage
                            room = 'content-center'
                            source = { sectionVisibility.environmentalDamageSection ? `collapse` : `expand` }
                            size = 'large'
                        />
                    </FlatButton>
                </ItemView>
                <CollapsibleView
                    style = {{
                        alignItems: `center`,
                        width: CONSTANT.GENERAL.DEVICE_WIDTH,
                        height: 200,
                        paddingHorizontal: 6
                    }}
                    collapsed = { !sectionVisibility.environmentalDamageSection }
                >
                    <InfoText >{ aqParamInfo.environmentalDamage }</InfoText>
                </CollapsibleView>
            </ScrollView>
        );
    }
    renderCOSections (aqParamInfo) {
        const component = this;
        const {
            sectionVisibility
        } = component.state;

        return (
            <ScrollView
                style = {{
                    flexShrink: 1,
                    flexDirection: `column`,
                    width: CONSTANT.GENERAL.DEVICE_WIDTH,
                    marginHorizontal: 6,
                    paddingVertical: 6
                }}
                scrollEnabled = { true }
                directionalLockEnabled = { true }
                scrollEventThrottle = { 16 }
            >
                <ItemView
                    key = '0'
                    style = {{
                        width: CONSTANT.GENERAL.DEVICE_WIDTH
                    }}
                    onPress = { component.onToggleWhatisSection }
                >
                    <TitleText
                        room = 'content-left'
                        indentation = { 6 }
                    > What is CO? </TitleText>
                    <FlatButton
                        room = 'action-right'
                        overlay = 'transparent'
                        onPress = { component.onToggleWhatisSection }
                    >
                        <IconImage
                            room = 'content-center'
                            source = { sectionVisibility.whatisSection ? `collapse` : `expand` }
                            size = 'large'
                        />
                    </FlatButton>
                </ItemView>
                <CollapsibleView
                    style = {{
                        alignItems: `center`,
                        width: CONSTANT.GENERAL.DEVICE_WIDTH,
                        height: 200,
                        paddingHorizontal: 6
                    }}
                    collapsed = { !sectionVisibility.whatisSection }
                >
                    <InfoText >{ aqParamInfo.whatis }</InfoText>
                </CollapsibleView>
                <Divider/>
                <ItemView
                    key = '1'
                    style = {{
                        width: CONSTANT.GENERAL.DEVICE_WIDTH
                    }}
                    onPress = { component.onToggleSourceOfEmissionSection }
                >
                    <TitleText
                        room = 'content-left'
                        indentation = { 6 }
                    > Sources of CO Pollution </TitleText>
                    <FlatButton
                        room = 'action-right'
                        overlay = 'transparent'
                        onPress = { component.onToggleSourceOfEmissionSection }
                    >
                        <IconImage
                            room = 'content-center'
                            source = { sectionVisibility.sourceOfEmissionSection ? `collapse` : `expand` }
                            size = 'large'
                        />
                    </FlatButton>
                </ItemView>
                <CollapsibleView
                    style = {{
                        alignItems: `center`,
                        width: CONSTANT.GENERAL.DEVICE_WIDTH,
                        height: 200,
                        paddingHorizontal: 6
                    }}
                    collapsed = { !sectionVisibility.sourceOfEmissionSection }
                >
                    <InfoText>{ aqParamInfo.source }</InfoText>
                </CollapsibleView>
                <Divider/>
                <ItemView
                    key = '2'
                    style = {{
                        width: CONSTANT.GENERAL.DEVICE_WIDTH
                    }}
                    onPress = { component.onToggleHealthImpactSection }
                >
                    <TitleText
                        room = 'content-left'
                        indentation = { 6 }
                    > CO Impacts On Health </TitleText>
                    <FlatButton
                        room = 'action-right'
                        overlay = 'transparent'
                        onPress = { component.onToggleHealthImpactSection }
                    >
                        <IconImage
                            room = 'content-center'
                            source = { sectionVisibility.healthImactSection ? `collapse` : `expand` }
                            size = 'large'
                        />
                    </FlatButton>
                </ItemView>
                <CollapsibleView
                    style = {{
                        alignItems: `center`,
                        width: CONSTANT.GENERAL.DEVICE_WIDTH,
                        height: 300,
                        paddingHorizontal: 6
                    }}
                    collapsed = { !sectionVisibility.healthImactSection }
                >
                    <InfoText >{ aqParamInfo.healthImpact }</InfoText>
                </CollapsibleView>
                <Divider/>
                <ItemView
                    key = '3'
                    style = {{
                        width: CONSTANT.GENERAL.DEVICE_WIDTH
                    }}
                    onPress = { component.onToggleEnvironmentalDamageSection }
                >
                    <TitleText
                        room = 'content-left'
                        indentation = { 6 }
                    > CO Impacts On The Environment </TitleText>
                    <FlatButton
                        room = 'action-right'
                        overlay = 'transparent'
                        onPress = { component.onToggleEnvironmentalDamageSection }
                    >
                        <IconImage
                            room = 'content-center'
                            source = { sectionVisibility.environmentalDamageSection ? `collapse` : `expand` }
                            size = 'large'
                        />
                    </FlatButton>
                </ItemView>
                <CollapsibleView
                    style = {{
                        alignItems: `center`,
                        width: CONSTANT.GENERAL.DEVICE_WIDTH,
                        height: 300,
                        paddingHorizontal: 6
                    }}
                    collapsed = { !sectionVisibility.environmentalDamageSection }
                >
                    <InfoText >{ aqParamInfo.environmentalDamage }</InfoText>
                </CollapsibleView>
            </ScrollView>
        );
    }
    renderSO2Sections (aqParamInfo) {
        const component = this;
        const {
            sectionVisibility
        } = component.state;

        return (
            <ScrollView
                style = {{
                    flexShrink: 1,
                    flexDirection: `column`,
                    width: CONSTANT.GENERAL.DEVICE_WIDTH,
                    marginHorizontal: 6,
                    paddingVertical: 6
                }}
                scrollEnabled = { true }
                directionalLockEnabled = { true }
                scrollEventThrottle = { 16 }
            >
                <ItemView
                    key = '0'
                    style = {{
                        width: CONSTANT.GENERAL.DEVICE_WIDTH
                    }}
                    onPress = { component.onToggleWhatisSection }
                >
                    <TitleText
                        room = 'content-left'
                        indentation = { 6 }
                    > What is SO2? </TitleText>
                    <FlatButton
                        room = 'action-right'
                        overlay = 'transparent'
                        onPress = { component.onToggleWhatisSection }
                    >
                        <IconImage
                            room = 'content-center'
                            source = { sectionVisibility.whatisSection ? `collapse` : `expand` }
                            size = 'large'
                        />
                    </FlatButton>
                </ItemView>
                <CollapsibleView
                    style = {{
                        alignItems: `center`,
                        width: CONSTANT.GENERAL.DEVICE_WIDTH,
                        height: 300,
                        paddingHorizontal: 6
                    }}
                    collapsed = { !sectionVisibility.whatisSection }
                >
                    <InfoText >{ aqParamInfo.whatis }</InfoText>
                </CollapsibleView>
                <Divider/>
                <ItemView
                    key = '1'
                    style = {{
                        width: CONSTANT.GENERAL.DEVICE_WIDTH
                    }}
                    onPress = { component.onToggleSourceOfEmissionSection }
                >
                    <TitleText
                        room = 'content-left'
                        indentation = { 6 }
                    > Sources of SO2 Pollution </TitleText>
                    <FlatButton
                        room = 'action-right'
                        overlay = 'transparent'
                        onPress = { component.onToggleSourceOfEmissionSection }
                    >
                        <IconImage
                            room = 'content-center'
                            source = { sectionVisibility.sourceOfEmissionSection ? `collapse` : `expand` }
                            size = 'large'
                        />
                    </FlatButton>
                </ItemView>
                <CollapsibleView
                    style = {{
                        alignItems: `center`,
                        width: CONSTANT.GENERAL.DEVICE_WIDTH,
                        height: 200,
                        paddingHorizontal: 6
                    }}
                    collapsed = { !sectionVisibility.sourceOfEmissionSection }
                >
                    <InfoText>{ aqParamInfo.source }</InfoText>
                </CollapsibleView>
                <Divider/>
                <ItemView
                    key = '2'
                    style = {{
                        width: CONSTANT.GENERAL.DEVICE_WIDTH
                    }}
                    onPress = { component.onToggleHealthImpactSection }
                >
                    <TitleText
                        room = 'content-left'
                        indentation = { 6 }
                    > SO2 Impacts On Health </TitleText>
                    <FlatButton
                        room = 'action-right'
                        overlay = 'transparent'
                        onPress = { component.onToggleHealthImpactSection }
                    >
                        <IconImage
                            room = 'content-center'
                            source = { sectionVisibility.healthImactSection ? `collapse` : `expand` }
                            size = 'large'
                        />
                    </FlatButton>
                </ItemView>
                <CollapsibleView
                    style = {{
                        alignItems: `center`,
                        width: CONSTANT.GENERAL.DEVICE_WIDTH,
                        height: 200,
                        paddingHorizontal: 6
                    }}
                    collapsed = { !sectionVisibility.healthImactSection }
                >
                    <InfoText >{ aqParamInfo.healthImpact }</InfoText>
                </CollapsibleView>
                <Divider/>
                <ItemView
                    key = '3'
                    style = {{
                        width: CONSTANT.GENERAL.DEVICE_WIDTH
                    }}
                    onPress = { component.onToggleEnvironmentalDamageSection }
                >
                    <TitleText
                        room = 'content-left'
                        indentation = { 6 }
                    > SO2 Impacts On The Environment </TitleText>
                    <FlatButton
                        room = 'action-right'
                        overlay = 'transparent'
                        onPress = { component.onToggleEnvironmentalDamageSection }
                    >
                        <IconImage
                            room = 'content-center'
                            source = { sectionVisibility.environmentalDamageSection ? `collapse` : `expand` }
                            size = 'large'
                        />
                    </FlatButton>
                </ItemView>
                <CollapsibleView
                    style = {{
                        alignItems: `center`,
                        width: CONSTANT.GENERAL.DEVICE_WIDTH,
                        height: 200,
                        paddingHorizontal: 6
                    }}
                    collapsed = { !sectionVisibility.environmentalDamageSection }
                >
                    <InfoText >{ aqParamInfo.environmentalDamage }</InfoText>
                </CollapsibleView>
            </ScrollView>
        );
    }
    render () {
        const component = this;
        const {
            visible,
            aqSample,
            onClose
        } = component.props;
        let label = ``;
        let aqParamInfo = null;

        switch (aqSample.aqParam) { // eslint-disable-line
        case `pm25`:
            label = `Particulate Matter (PM2.5) Info`;
            aqParamInfo = CONSTANT.AQ_INFO.PM25;
            return aqParamInfo !== null ? (
                <Modal
                    animationType = 'slide'
                    transparent = { false }
                    visible = { visible }
                >
                    <HeaderView
                        ref = {(animatedHeader) => {
                            component.animatedHeader = animatedHeader;
                        }}
                        label = { label }
                    >
                        <FlatButton
                            room = 'action-left'
                            overlay = 'transparent'
                            onPress = { onClose }
                        >
                            <IconImage
                                room = 'content-center'
                                source = 'close'
                                size = 'large'
                            />
                        </FlatButton>
                    </HeaderView>
                    <BodyView>
                        <LayoutView
                            orientation = 'vertical'
                            alignment = 'center'
                        >
                            <LayoutView
                                style = {{
                                    height: 100
                                }}
                                orientation = 'horizontal'
                                alignment = 'start'
                            >
                                <HeadlineText> AQI   </HeadlineText>
                                <HeadlineText> PM25  </HeadlineText>
                            </LayoutView>
                            <LayoutView
                                style = {{
                                    height: 100
                                }}
                                orientation = 'horizontal'
                                alignment = 'start'
                            >
                                <TitleText
                                    size = 'large'
                                    color = { Ht.Theme.general.aqAlertColors[aqSample.aqAlertIndex] }>{ aqSample.aqi }
                                </TitleText>
                                <LayoutView
                                    orientation = 'vertical'
                                    alignment = 'center'
                                >
                                    <TitleText
                                        size = 'large'
                                        color = { Ht.Theme.general.aqAlertColors[aqSample.aqAlertIndex] }>{ aqSample.aqConcentration }
                                    </TitleText>
                                    <InfoText
                                        size = 'large'
                                        indentation = { 6 }
                                    >{ aqSample.aqUnit }</InfoText>
                                </LayoutView>
                            </LayoutView>
                        </LayoutView>
                        <Divider/>
                        {
                            component.renderPM25Sections(aqParamInfo)
                        }
                    </BodyView>
                </Modal>
            ) : null;
        case `no2`:
            label = `Nitrogen Dioxide (NO2) Info`;
            aqParamInfo = CONSTANT.AQ_INFO.NO2;
            return aqParamInfo !== null ? (
                <Modal
                    animationType = 'slide'
                    transparent = { false }
                    visible = { visible }
                >
                    <HeaderView
                        ref = {(animatedHeader) => {
                            component.animatedHeader = animatedHeader;
                        }}
                        label = { label }
                    >
                        <FlatButton
                            room = 'action-left'
                            overlay = 'transparent'
                            onPress = { onClose }
                        >
                            <IconImage
                                room = 'content-center'
                                source = 'close'
                                size = 'large'
                            />
                        </FlatButton>
                    </HeaderView>
                    <BodyView>
                        <LayoutView
                            orientation = 'vertical'
                            alignment = 'center'
                        >
                            <LayoutView
                                style = {{
                                    height: 100
                                }}
                                orientation = 'horizontal'
                                alignment = 'start'
                            >
                                <HeadlineText> AQI   </HeadlineText>
                                <HeadlineText> NO2  </HeadlineText>
                            </LayoutView>
                            <LayoutView
                                style = {{
                                    height: 100
                                }}
                                orientation = 'horizontal'
                                alignment = 'start'
                            >
                                <TitleText
                                    size = 'large'
                                    color = { Ht.Theme.general.aqAlertColors[aqSample.aqAlertIndex] }>{ aqSample.aqi }
                                </TitleText>
                                <LayoutView
                                    orientation = 'vertical'
                                    alignment = 'center'
                                >
                                    <TitleText
                                        size = 'large'
                                        color = { Ht.Theme.general.aqAlertColors[aqSample.aqAlertIndex] }>{ aqSample.aqConcentration }
                                    </TitleText>
                                    <InfoText
                                        size = 'large'
                                        indentation = { 6 }
                                    >{ aqSample.aqUnit }</InfoText>
                                </LayoutView>
                            </LayoutView>
                        </LayoutView>
                        <Divider/>
                        {
                            component.renderNO2Sections(aqParamInfo)
                        }
                    </BodyView>
                </Modal>
            ) : null;
        case `o3`:
            label = `Ozone (O3) Info`;
            aqParamInfo = CONSTANT.AQ_INFO.O3;
            return aqParamInfo !== null ? (
                <Modal
                    animationType = 'slide'
                    transparent = { false }
                    visible = { visible }
                >
                    <HeaderView
                        ref = {(animatedHeader) => {
                            component.animatedHeader = animatedHeader;
                        }}
                        label = { label }
                    >
                        <FlatButton
                            room = 'action-left'
                            overlay = 'transparent'
                            onPress = { onClose }
                        >
                            <IconImage
                                room = 'content-center'
                                source = 'close'
                                size = 'large'
                            />
                        </FlatButton>
                    </HeaderView>
                    <BodyView>
                        <LayoutView
                            orientation = 'vertical'
                            alignment = 'center'
                        >
                            <LayoutView
                                style = {{
                                    height: 100
                                }}
                                orientation = 'horizontal'
                                alignment = 'start'
                            >
                                <HeadlineText> AQI   </HeadlineText>
                                <HeadlineText> O3  </HeadlineText>
                            </LayoutView>
                            <LayoutView
                                style = {{
                                    height: 100
                                }}
                                orientation = 'horizontal'
                                alignment = 'start'
                            >
                                <TitleText
                                    size = 'large'
                                    color = { Ht.Theme.general.aqAlertColors[aqSample.aqAlertIndex] }>{ aqSample.aqi }
                                </TitleText>
                                <LayoutView
                                    orientation = 'vertical'
                                    alignment = 'center'
                                >
                                    <TitleText
                                        size = 'large'
                                        color = { Ht.Theme.general.aqAlertColors[aqSample.aqAlertIndex] }>{ aqSample.aqConcentration }
                                    </TitleText>
                                    <InfoText
                                        size = 'large'
                                        indentation = { 6 }
                                    >{ aqSample.aqUnit }</InfoText>
                                </LayoutView>
                            </LayoutView>
                        </LayoutView>
                        <Divider/>
                        {
                            component.renderO3Sections(aqParamInfo)
                        }
                    </BodyView>
                </Modal>
            ) : null;
        case `co`:
            label = `Carbon Monoxide (CO) Info`;
            aqParamInfo = CONSTANT.AQ_INFO.CO;
            return aqParamInfo !== null ? (
                <Modal
                    animationType = 'slide'
                    transparent = { false }
                    visible = { visible }
                >
                    <HeaderView
                        ref = {(animatedHeader) => {
                            component.animatedHeader = animatedHeader;
                        }}
                        label = { label }
                    >
                        <FlatButton
                            room = 'action-left'
                            overlay = 'transparent'
                            onPress = { onClose }
                        >
                            <IconImage
                                room = 'content-center'
                                source = 'close'
                                size = 'large'
                            />
                        </FlatButton>
                    </HeaderView>
                    <BodyView>
                        <LayoutView
                            orientation = 'vertical'
                            alignment = 'center'
                        >
                            <LayoutView
                                style = {{
                                    height: 100
                                }}
                                orientation = 'horizontal'
                                alignment = 'start'
                            >
                                <HeadlineText> AQI   </HeadlineText>
                                <HeadlineText> CO  </HeadlineText>
                            </LayoutView>
                            <LayoutView
                                style = {{
                                    height: 100
                                }}
                                orientation = 'horizontal'
                                alignment = 'start'
                            >
                                <TitleText
                                    size = 'large'
                                    color = { Ht.Theme.general.aqAlertColors[aqSample.aqAlertIndex] }>{ aqSample.aqi }
                                </TitleText>
                                <LayoutView
                                    orientation = 'vertical'
                                    alignment = 'center'
                                >
                                    <TitleText
                                        size = 'large'
                                        color = { Ht.Theme.general.aqAlertColors[aqSample.aqAlertIndex] }>{ aqSample.aqConcentration }
                                    </TitleText>
                                    <InfoText
                                        size = 'large'
                                        indentation = { 6 }
                                    >{ aqSample.aqUnit }</InfoText>
                                </LayoutView>
                            </LayoutView>
                        </LayoutView>
                        <Divider/>
                        {
                            component.renderCOSections(aqParamInfo)
                        }
                    </BodyView>
                </Modal>
            ) : null;
        case `so2`:
            label = `Sulfur Dioxide (SO2) Info`;
            aqParamInfo = CONSTANT.AQ_INFO.SO2;
            return aqParamInfo !== null ? (
                <Modal
                    animationType = 'slide'
                    transparent = { false }
                    visible = { visible }
                >
                    <HeaderView
                        ref = {(animatedHeader) => {
                            component.animatedHeader = animatedHeader;
                        }}
                        label = { label }
                    >
                        <FlatButton
                            room = 'action-left'
                            overlay = 'transparent'
                            onPress = { onClose }
                        >
                            <IconImage
                                room = 'content-center'
                                source = 'close'
                                size = 'large'
                            />
                        </FlatButton>
                    </HeaderView>
                    <BodyView>
                        <LayoutView
                            orientation = 'vertical'
                            alignment = 'center'
                        >
                            <LayoutView
                                style = {{
                                    height: 100
                                }}
                                orientation = 'horizontal'
                                alignment = 'start'
                            >
                                <HeadlineText> AQI   </HeadlineText>
                                <HeadlineText> SO2  </HeadlineText>
                            </LayoutView>
                            <LayoutView
                                style = {{
                                    height: 100
                                }}
                                orientation = 'horizontal'
                                alignment = 'start'
                            >
                                <TitleText
                                    size = 'large'
                                    color = { Ht.Theme.general.aqAlertColors[aqSample.aqAlertIndex] }>{ aqSample.aqi }
                                </TitleText>
                                <LayoutView
                                    orientation = 'vertical'
                                    alignment = 'center'
                                >
                                    <TitleText
                                        size = 'large'
                                        color = { Ht.Theme.general.aqAlertColors[aqSample.aqAlertIndex] }>{ aqSample.aqConcentration }
                                    </TitleText>
                                    <InfoText
                                        size = 'large'
                                        indentation = { 6 }
                                    >{ aqSample.aqUnit }</InfoText>
                                </LayoutView>
                            </LayoutView>
                        </LayoutView>
                        <Divider/>
                        {
                            component.renderSO2Sections(aqParamInfo)
                        }
                    </BodyView>
                </Modal>
            ) : null;
        case `voc`:
            label = `VOC Info`;
            aqParamInfo = CONSTANT.AQ_INFO.VOC;
            return null;
        default:
            return null;
        }
    }
}
