/**
  * Copyright (c) 2017-present, Virida YEAwareness, Org. All rights reserved.
  *
  *------------------------------------------------------------------------
  *
  * @description -  Virida client-native app ui theme.
  *
  * @author Tuan Le (tuan.t.lei@gmail.com)
  *
  *------------------------------------------------------------------------
  * @flow
  */
'use strict'; // eslint-disable-line

import colorTheme from './color-theme';

import googleMapTheme from './google-map-theme';

import iconPreset from './icon-preset';

import marker1Image from '../../assets/images/marker-1.png';
import marker2Image from '../../assets/images/marker-2.png';
import marker3Image from '../../assets/images/marker-3.png';
import marker4Image from '../../assets/images/marker-4.png';
import marker5Image from '../../assets/images/marker-5.png';
import marker6Image from '../../assets/images/marker-6.png';

import yeaLogoImage from '../../assets/logos/yea-logo.png';
import fbLogoImage from '../../assets/logos/fb-logo.png';
import instagramLogoImage from '../../assets/logos/instagram-logo.png';

import happyFaceImage from '../../assets/images/happy-face.png';
import okFaceImage from '../../assets/images/ok-face.png';
import sadFaceImage from '../../assets/images/sad-face.png';

import airPollutionSourcesImage from '../../assets/images/air-pollution-sources.png';
import pm25PollutionImage from '../../assets/images/pm25-pollution.png';
import pm25LungDiseaseImage from '../../assets/images/pm25-lung-disease.png';
import projectViridaImage from '../../assets/images/project-virida.png';
import appFeature1Image from '../../assets/images/app-feature1.png';
import appFeature2Image from '../../assets/images/app-feature2.png';
import appFeature3Image from '../../assets/images/app-feature3.png';

import pm25ScaleDiagramImage from '../../assets/images/pm25-scale-diagram.jpg';
import pm25HealthImpactImage from '../../assets/images/pm25-health-impact.jpg';

import aqGradientScaleImage from '../../assets/images/aq-gradient-scale.png';
import backgroundGradientImage from '../../assets/images/background-gradient.png';
import logoBackgroundGradientImage from '../../assets/images/logo-background-gradient.png';

export default {
    icon: iconPreset,
    imageSource: {
        markers: [
            marker1Image,
            marker2Image,
            marker3Image,
            marker4Image,
            marker5Image,
            marker6Image
        ],
        yeaLogo: yeaLogoImage,
        fbLogo: fbLogoImage,
        instagramLogo: instagramLogoImage,
        happyFace: happyFaceImage,
        okFace: okFaceImage,
        sadFace: sadFaceImage,
        airPollutionSources: airPollutionSourcesImage,
        pm25Pollution: pm25PollutionImage,
        pm25LungDisease: pm25LungDiseaseImage,
        projectVirida: projectViridaImage,
        appFeature1: appFeature1Image,
        appFeature2: appFeature2Image,
        appFeature3: appFeature3Image,
        pm25ScaleDiagram: pm25ScaleDiagramImage,
        pm25HealthImpact: pm25HealthImpactImage,
        aqGradientScale: aqGradientScaleImage,
        backgroundGradient: backgroundGradientImage,
        logoBackgroundGradient: logoBackgroundGradientImage
    },
    general: {
        frostLevel: 25,
        aqAlertColors: colorTheme.aqAlerts,
        color: colorTheme,
        googleMap: googleMapTheme
    },
    field: {
        search: {
            corner: `round50`,
            dropShadowed: true
        },
        color: {
            search: {
                input: colorTheme.text
            }
        }
    },
    button: {
        flat: {
            corner: `round50`
        },
        raised: {
            corner: `round50`
        },
        color: {
            flat: {
                accent: {
                    light: colorTheme.light.accent
                },
                primary: {
                    light: colorTheme.light.primary
                },
                secondary: {
                    light: colorTheme.light.secondary
                },
                disabled: {
                    light: colorTheme.light.disabled
                },
                label: {
                    light: colorTheme.text.dark,
                    dark: colorTheme.text.light
                },
                ripple: {
                    dark: colorTheme.light.accent,
                    light: colorTheme.light.accent
                }
            },
            raised: {
                accent: {
                    light: colorTheme.light.accent
                },
                primary: {
                    light: colorTheme.light.primary
                },
                secondary: {
                    light: colorTheme.light.secondary
                },
                disabled: {
                    light: colorTheme.light.disabled
                },
                label: {
                    light: colorTheme.text.dark,
                    dark: colorTheme.text.light
                },
                ripple: {
                    dark: colorTheme.light.accent,
                    light: colorTheme.light.accent
                }
            }
        }
    },
    image: {
        avatar: {
            dropShadowed: false
        },
        icon: {
            dropShadowed: false
        },
        color: {
            icon: {
                accent: {
                    dark: colorTheme.dark.accent,
                    light: colorTheme.light.accent
                },
                primary: colorTheme.text,
                secondary: {
                    dark: colorTheme.dark.primary,
                    light: colorTheme.light.primary
                },
                disabled: {
                    dark: colorTheme.dark.disabled,
                    light: colorTheme.light.disabled
                }
            }
        }
    },
    text: {
        color: colorTheme.text
    },
    view: {
        header: {
            dropShadowed: true
        },
        body: {
            overlay: `transparent`
        },
        layout: {
            overlay: `transparent`
        },
        item: {
            overlay: `transparent`
        },
        color: {
            header: {
                navigation: {
                    light: colorTheme.light.primary
                },
                status: {
                    light: colorTheme.light.primary
                },
                label: colorTheme.text
            },
            layout: {
                opacity: colorTheme.opacity
            },
            item: {
                ripple: {
                    light: colorTheme.light.accent
                }
            },
            size: {
                item: 48
            }
        }
    }
};
