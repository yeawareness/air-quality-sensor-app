/**
  * Copyright (c) 2017-present, Virida YEAwareness, Org. All rights reserved.
  *
  *------------------------------------------------------------------------
  *
  * @description -  Virida client-native app ui icon preset.
  *
  * @author Tuan Le (tuan.t.lei@gmail.com)
  *
  *------------------------------------------------------------------------
  * @flow
  */
'use strict'; // eslint-disable-line

import sensorIcon from '../../assets/icons/png/sensor-3x.png';
import battery100Icon from '../../assets/icons/png/battery-100-3x.png';
import battery75Icon from '../../assets/icons/png/battery-75-3x.png';
import battery50Icon from '../../assets/icons/png/battery-50-3x.png';
import battery25Icon from '../../assets/icons/png/battery-25-3x.png';
import battery0Icon from '../../assets/icons/png/battery-0-3x.png';
import batteryChargingIcon from '../../assets/icons/png/battery-charging-3x.png';

import arrowLeftIcon from '../../assets/icons/png/arrow-left-3x.png';
import arrowRightIcon from '../../assets/icons/png/arrow-right-3x.png';

import backIcon from '../../assets/icons/png/back-3x.png';
import forwardIcon from '../../assets/icons/png/forward-3x.png';
import expandIcon from '../../assets/icons/png/expand-3x.png';
import collapseIcon from '../../assets/icons/png/collapse-3x.png';

import searchIcon from '../../assets/icons/png/search-3x.png';
import currentLocationIcon from '../../assets/icons/png/current-location-3x.png';

import historyIcon from '../../assets/icons/png/history-3x.png';
import recallIcon from '../../assets/icons/png/recall-3x.png';
import tuneIcon from '../../assets/icons/png/tune-3x.png';
import infoIcon from '../../assets/icons/png/info-3x.png';
import forecastIcon from '../../assets/icons/png/forecast-3x.png';

import brightnessIcon from '../../assets/icons/png/brightness-3x.png';
import copyrightIcon from '../../assets/icons/png/copyright-3x.png';
import notificationIcon from '../../assets/icons/png/notification-3x.png';

import closeIcon from '../../assets/icons/png/close-3x.png';
import deleteIcon from '../../assets/icons/png/delete-3x.png';

import outdoorIcon from '../../assets/icons/png/outdoor-3x.png';
import indoorIcon from '../../assets/icons/png/indoor-3x.png';
import tempIcon from '../../assets/icons/png/temp-3x.png';
import humidityIcon from '../../assets/icons/png/humidity-3x.png';

import monitorIcon from '../../assets/icons/png/monitor-3x.png';
import mapIcon from '../../assets/icons/png/map-3x.png';
import timelineIcon from '../../assets/icons/png/timeline-3x.png';
import settingIcon from '../../assets/icons/png/setting-3x.png';

export default {
    sensor: sensorIcon,
    battery100: battery100Icon,
    battery75: battery75Icon,
    battery50: battery50Icon,
    battery25: battery25Icon,
    battery0: battery0Icon,
    batteryCharging: batteryChargingIcon,

    arrowLeft: arrowLeftIcon,
    arrowRight: arrowRightIcon,

    back: backIcon,
    forward: forwardIcon,
    expand: expandIcon,
    collapse: collapseIcon,

    search: searchIcon,
    currentLocation: currentLocationIcon,

    history: historyIcon,
    recall: recallIcon,
    tune: tuneIcon,
    info: infoIcon,
    forecast: forecastIcon,

    brightness: brightnessIcon,
    copyright: copyrightIcon,
    notification: notificationIcon,

    close: closeIcon,
    delete: deleteIcon,

    outdoor: outdoorIcon,
    indoor: indoorIcon,
    temp: tempIcon,
    humidity: humidityIcon,

    monitor: monitorIcon,
    map: mapIcon,
    timeline: timelineIcon,
    setting: settingIcon
};
