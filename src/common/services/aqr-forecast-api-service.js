/**
 * Copyright (c) 2017-present, Virida YEAwareness, Org. All rights reserved.
 *
 *------------------------------------------------------------------------
 *
 * @module AQRForecastAPIService
 * @description - Virida client-native app air quality regional forecast server api service.
 *
 * @author Tuan Le (tuan.t.lei@gmail.com)
 *
 *------------------------------------------------------------------------
 * @flow
 */
'use strict'; // eslint-disable-line

import { Hf } from 'hyperflow';

import moment from 'moment';

import ReactNative from 'react-native'; // eslint-disable-line

import AQAlertComposite from '../composites/aq-alert-composite';

import CONSTANT from '../constant';

import EVENT from '../events/aqr-api-event';

const {
    NetInfo
} = ReactNative;

const AQRForecastAPIService = Hf.Service.augment({
    composites: [
        AQAlertComposite
    ],
    setupAirNowGetAQRForecastDataAPI: function setupAirNowGetAQRForecastDataAPI (jsonContentHeaders) {
        const service = this;
        service.incoming(EVENT.REQUEST.AIR_NOW_AQR_FORECAST_DATA).handle((requestor) => {
            NetInfo.isConnected.fetch().then((online) => {
                if (online) {
                    if (Hf.isSchema({
                        aqParams: [ `string` ],
                        timestamp: `string`,
                        latitude: `number`,
                        longitude: `number`,
                        radius: `number`
                    }).of(requestor)) {
                        const {
                            aqParams,
                            timestamp,
                            latitude,
                            longitude,
                            radius
                        } = requestor;
                        const timeoutId = setTimeout(() => {
                            service.outgoing(EVENT.RESPONSE.TO.AIR_NOW_AQR_FORECAST_DATA.TIMED_OUT).emit();
                        }, CONSTANT.HTTP.API_REQUEST_TIMEOUT_MS);
                        let url;
                        url = `${CONSTANT.URL.AIR_NOW_GET_AQR_FORECAST_DATA_API}?format=application/json&latitude=`;
                        url = `${url}${latitude}&longitude=${longitude}&distance=${radius * 2}&date=${moment(timestamp).format(`YYYY-MM-DD`)}&API_KEY=${CONSTANT.API_KEY.AIR_NOW}`;
                        Hf.log(`info1`, url);
                        fetch(url, {
                            method: `get`,
                            headers: jsonContentHeaders
                        }).then((response) => {
                            clearTimeout(timeoutId);
                            if (response.status === CONSTANT.HTTP.CODE.OK) {
                                Hf.log(`info1`, `AirNow server responsed with status ${CONSTANT.HTTP.CODE.OK}.`);
                                return response.json();
                            } else if (response.status === CONSTANT.HTTP.CODE.ERROR) {
                                service.outgoing(EVENT.RESPONSE.TO.AIR_NOW_AQR_FORECAST_DATA.ERROR).emit();
                                Hf.log(`warn1`, `AQRForecastAPIService - Server responsed with error status ${CONSTANT.HTTP.CODE.ERROR}.`);
                            }
                        }).then((results) => {
                            if (Hf.isNonEmptyArray(results)) {
                                let reportingRegionName = `${results[0].ReportingArea}, ${results[0].StateCode}`;
                                results = results.filter((result) => aqParams.includes(result.ParameterName.toLowerCase().replace(/\./g, ``)));
                                service.outgoing(EVENT.RESPONSE.TO.AIR_NOW_AQR_FORECAST_DATA.OK).emit(() => {
                                    return {
                                        aqrInfo: {
                                            timestamp,
                                            reportingRegionName
                                        },
                                        aqrForecasts: Array(5).fill({
                                            aqParam: ``,
                                            aqi: 0,
                                            aqDiscussion: ``,
                                            day: ``
                                        }).map((aqForecast, index) => {
                                            if (index <= results.length - 1) {
                                                const result = results[index];
                                                const aqParam = result.ParameterName.toLowerCase().replace(/\./g, ``);
                                                const aqi = parseInt(result.AQI, 10) >= 0 ? parseInt(result.AQI, 10) : 0;
                                                const aqAlert = service.getAQIAlert(aqi);
                                                const aqDiscussion = Hf.isString(result.Discussion) ? result.Discussion : ``;
                                                const day = moment(result.DateForecast.replace(/\s+/, ``)).add(1, `days`).format(`ddd`);

                                                return {
                                                    aqParam,
                                                    aqi,
                                                    aqAlertIndex: aqAlert.index,
                                                    aqDiscussion,
                                                    day
                                                };
                                            } else {
                                                return {
                                                    ...aqForecast,
                                                    day: moment().add(index + 1, `days`).format(`ddd`)
                                                };
                                            }
                                        })
                                    };
                                });
                            } else {
                                service.outgoing(EVENT.RESPONSE.TO.AIR_NOW_AQR_FORECAST_DATA.NOT_FOUND).emit();
                            }
                        }).catch((error) => {
                            clearTimeout(timeoutId);
                            service.outgoing(EVENT.RESPONSE.TO.AIR_NOW_AQR_FORECAST_DATA.ERROR).emit();
                            Hf.log(`warn1`, `AQRForecastAPIService - Unable to do get request. ${error.message}`);
                        });
                    } else {
                        service.outgoing(EVENT.RESPONSE.TO.AIR_NOW_AQR_FORECAST_DATA.ERROR).emit();
                        Hf.log(`warn1`, `AQRForecastAPIService - Invalid AirNow api request params.`);
                    }
                } else {
                    service.outgoing(EVENT.RESPONSE.TO.AIR_NOW_AQR_FORECAST_DATA.ERROR).emit();
                    Hf.log(`warn1`, `AQRForecastAPIService - Device is offline.`);
                }
            });
        });
    },
    setup: function setup (done) {
        const service = this;
        const jsonContentHeaders = new Headers();
        jsonContentHeaders.append(`Accept`, `gzip`);
        jsonContentHeaders.append(`Accept`, `deflate`);
        jsonContentHeaders.append(`Accept`, `application/json`);
        jsonContentHeaders.append(`Content-Type`, `application/json`);
        service.setupAirNowGetAQRForecastDataAPI(jsonContentHeaders);
        done();
    },
    teardown: function teardown (done) {
        done();
    }
});
export default AQRForecastAPIService;
