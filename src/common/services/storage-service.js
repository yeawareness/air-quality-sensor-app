/**
 * Copyright (c) 2017-present, Virida YEAwareness, Org. All rights reserved.
 *
 *------------------------------------------------------------------------
 *
 * @module StorageService
 * @description - Virida client-native app local storage service.
 *
 * @author Tuan Le (tuan.t.lei@gmail.com)
 *
 *------------------------------------------------------------------------
 * @flow
 */
'use strict'; // eslint-disable-line

import { Hf } from 'hyperflow';

import { AsyncStorage } from 'react-native';

import EVENT from '../events/storage-event';

const DEFAULT_BUNDLE = {
    virida: {
        setting: {
            aqsSkipped: true,
            showIntro: true,
            // mapTheme: `flat`,
            notification: {
                unhealthyAQAlert: true,
                dailyAQAlert: true
            }
        },
        aqs: {
            info: {
                mId: ``,
                alias: `Virida Sensor`,
                rev: ``,
                skew: ``,
                firmwareVersion: ``,
                cDatestamp: ``
            },
            setting: {
                general: {
                    outdoor: false,
                    ledBrightnessLvl: 20,
                    aqSampleCount: 4,
                    mInterval: 28,
                    editable: {
                        ledBrightnessLvl: 20,
                        aqSampleCount: 4,
                        mInterval: 28
                    }
                },
                calibration: {
                    pm25: {
                        lowLvlMv: 600,
                        lowLvlTargetThreshold: 0.5
                    },
                    voc: {
                        ro: 400000,
                        refCOPPM: 0.2,
                        refCOPPMTargetWindow: 0.5
                    },
                    editable: {
                        pm25: {
                            lowLvlTargetThreshold: 0.5
                        },
                        voc: {
                            refCOPPM: 0.2,
                            refCOPPMTargetWindow: 0.5
                        }
                    }
                }
            }
        }
    }
};

const StorageService = Hf.Service.augment({
    composites: [
        Hf.Storage.ASyncStorageComposite
    ],
    getProvider: function getProvider () {
        return {
            storage: AsyncStorage
        };
    },
    setupStorageRead: function setupStorageRead () {
        const service = this;
        service.incoming(EVENT.REQUEST.READ_SETTING).handle(() => {
            service.fetch(
                `virida.setting`
            ).read().then((results) => {
                if (Hf.isNonEmptyArray(results) && Hf.isObject(results[0])) {
                    return results[0];
                } else {
                    service.outgoing(EVENT.RESPONSE.TO.READ_SETTING.NOT_FOUND).emit();
                    Hf.log(`warn1`, `StorageService - Setting data is not found in storage.`);
                }
            }).then((setting) => {
                service.outgoing(EVENT.RESPONSE.TO.READ_SETTING.OK).emit(() => setting);
                Hf.log(`info1`, `Setting was read from storage.`);
            }).catch((error) => {
                service.outgoing(EVENT.RESPONSE.TO.READ_SETTING.ERROR).emit();
                Hf.log(`warn1`, `StorageService - Unable to read from storage. ${error.message}`);
            });
        });
        service.incoming(EVENT.REQUEST.READ_AQS_INFO).handle(() => {
            service.fetch(
                `virida.aqs.info`
            ).read().then((results) => {
                if (Hf.isNonEmptyArray(results) && Hf.isObject(results[0])) {
                    return results[0];
                } else {
                    service.outgoing(EVENT.RESPONSE.TO.READ_AQS_INFO.NOT_FOUND).emit();
                    Hf.log(`warn1`, `StorageService - Air quality sensor info data is not found in storage.`);
                }
            }).then((aqsInfo) => {
                service.outgoing(EVENT.RESPONSE.TO.READ_AQS_INFO.OK).emit(() => aqsInfo);
                Hf.log(`info1`, `Air quality sensor info data was read from storage.`);
            }).catch((error) => {
                service.outgoing(EVENT.RESPONSE.TO.READ_AQS_INFO.ERROR).emit();
                Hf.log(`warn1`, `StorageService - Unable to read from storage. ${error.message}`);
            });
        });
        service.incoming(EVENT.REQUEST.READ_AQS_GENERAL_SETTING).handle(() => {
            service.fetch(
                `virida.aqs.setting.general`
            ).read().then((results) => {
                if (Hf.isNonEmptyArray(results) && Hf.isObject(results[0])) {
                    return results[0];
                } else {
                    service.outgoing(EVENT.RESPONSE.TO.READ_AQS_GENERAL_SETTING.NOT_FOUND).emit();
                    Hf.log(`warn1`, `StorageService - Air quality sensor general setting data is not found in storage.`);
                }
            }).then((aqsGeneralSetting) => {
                service.outgoing(EVENT.RESPONSE.TO.READ_AQS_GENERAL_SETTING.OK).emit(() => aqsGeneralSetting);
                Hf.log(`info1`, `Air quality sensor general setting data was read from storage.`);
            }).catch((error) => {
                service.outgoing(EVENT.RESPONSE.TO.READ_AQS_GENERAL_SETTING.ERROR).emit();
                Hf.log(`warn1`, `StorageService - Unable to read from storage. ${error.message}`);
            });
        });
        service.incoming(EVENT.REQUEST.READ_AQS_CALIBRATION_SETTING).handle(() => {
            service.fetch(
                `virida.aqs.setting.calibration`
            ).read().then((results) => {
                if (Hf.isNonEmptyArray(results) && Hf.isObject(results[0])) {
                    return results[0];
                } else {
                    service.outgoing(EVENT.RESPONSE.TO.READ_AQS_CALIBRATION_SETTING.NOT_FOUND).emit();
                    Hf.log(`warn1`, `StorageService - Air quality sensor calibration setting data is not found in storage.`);
                }
            }).then((aqsCalibrationSetting) => {
                service.outgoing(EVENT.RESPONSE.TO.READ_AQS_CALIBRATION_SETTING.OK).emit(() => aqsCalibrationSetting);
                Hf.log(`info1`, `Air quality sensor general calibration data was read from storage.`);
            }).catch((error) => {
                service.outgoing(EVENT.RESPONSE.TO.READ_AQS_CALIBRATION_SETTING.ERROR).emit();
                Hf.log(`warn1`, `StorageService - Unable to read from storage. ${error.message}`);
            });
        });
    },
    setupStorageWrite: function setupStorageWrite () {
        const service = this;
        service.incoming(EVENT.REQUEST.WRITE_SETTING).handle((setting) => {
            service.fetch(
                `virida.setting`
            ).write({
                bundle: {
                    setting
                }
            }).then((results) => {
                if (Hf.isNonEmptyArray(results) && Hf.isObject(results[0])) {
                    return results[0];
                } else {
                    service.outgoing(EVENT.RESPONSE.TO.WRITE_SETTING.ERROR).emit();
                }
            }).then((result) => {
                service.outgoing(EVENT.RESPONSE.TO.WRITE_SETTING.OK).emit(() => result.setting);
                Hf.log(`info1`, `Setting data was written to storage.`);
            }).catch((error) => {
                service.outgoing(EVENT.RESPONSE.TO.WRITE_SETTING.ERROR).emit();
                Hf.log(`warn1`, `StorageService - Unable to write to storage. ${error.message}`);
            });
        });
        service.incoming(EVENT.REQUEST.WRITE_AQS_INFO).handle((aqsInfo) => {
            service.fetch(
                `virida.aqs.info`
            ).write({
                bundle: {
                    info: aqsInfo
                }
            }).then((results) => {
                if (Hf.isNonEmptyArray(results) && Hf.isObject(results[0])) {
                    return results[0];
                } else {
                    service.outgoing(EVENT.RESPONSE.TO.WRITE_AQS_INFO.ERROR).emit();
                }
            }).then((result) => {
                service.outgoing(EVENT.RESPONSE.TO.WRITE_AQS_INFO.OK).emit(() => result.aqsInfo);
                Hf.log(`info1`, `Air quality sensor info data was written to storage.`);
            }).catch((error) => {
                service.outgoing(EVENT.RESPONSE.TO.WRITE_AQS_INFO.ERROR).emit();
                Hf.log(`warn1`, `StorageService - Unable to write to storage. ${error.message}`);
            });
        });
        service.incoming(EVENT.REQUEST.WRITE_AQS_GENERAL_SETTING).handle((aqsGeneralSetting) => {
            service.fetch(
                `virida.aqs.setting.general`
            ).write({
                bundle: {
                    general: aqsGeneralSetting
                }
            }).then((results) => {
                if (Hf.isNonEmptyArray(results) && Hf.isObject(results[0])) {
                    return results[0];
                } else {
                    service.outgoing(EVENT.RESPONSE.TO.WRITE_AQS_GENERAL_SETTING.ERROR).emit();
                }
            }).then((result) => {
                service.outgoing(EVENT.RESPONSE.TO.WRITE_AQS_GENERAL_SETTING.OK).emit(() => result.aqsGeneralSetting);
                Hf.log(`info1`, `Air quality sensor general setting data was written to storage.`);
            }).catch((error) => {
                service.outgoing(EVENT.RESPONSE.TO.WRITE_AQS_GENERAL_SETTING.ERROR).emit();
                Hf.log(`warn1`, `StorageService - Unable to write to storage. ${error.message}`);
            });
        });
        service.incoming(EVENT.REQUEST.WRITE_AQS_CALIBRATION_SETTING).handle((aqsCalibrationSetting) => {
            service.fetch(
                `virida.aqs.setting.calibration`
            ).write({
                bundle: {
                    calibration: aqsCalibrationSetting
                }
            }).then((results) => {
                if (Hf.isNonEmptyArray(results) && Hf.isObject(results[0])) {
                    return results[0];
                } else {
                    service.outgoing(EVENT.RESPONSE.TO.WRITE_AQS_CALIBRATION_SETTING.ERROR).emit();
                }
            }).then((result) => {
                service.outgoing(EVENT.RESPONSE.TO.WRITE_AQS_CALIBRATION_SETTING.OK).emit(() => result.aqsCalibrationSetting);
                Hf.log(`info1`, `Air quality sensor calibration setting data was written to storage.`);
            }).catch((error) => {
                service.outgoing(EVENT.RESPONSE.TO.WRITE_AQS_CALIBRATION_SETTING.ERROR).emit();
                Hf.log(`warn1`, `StorageService - Unable to write to storage. ${error.message}`);
            });
        });
    },
    setup: function setup (done) {
        const service = this;

        service.incoming(EVENT.DO.STORAGE_INITIALIZATION).handle(() => {
            service.fetch(
                `virida`
            ).write({
                bundle: DEFAULT_BUNDLE,
                touchRoot: true
            }).then(() => {
                service.outgoing(EVENT.AS.STORAGE_INITIALIZED).emit();
                Hf.log(`info1`, `Storage initialized.`);
            }).catch((error) => {
                Hf.log(`warn1`, `StorageService - Unable to write to storage. ${error.message}`);
            });
        });

        service.setupStorageRead();
        service.setupStorageWrite();

        done();
    },
    teardown: function teardown (done) {
        done();
    }
});
export default StorageService;
