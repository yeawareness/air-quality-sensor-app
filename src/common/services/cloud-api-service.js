/**
 * Copyright (c) 2017-present, Virida YEAwareness, Org. All rights reserved.
 *
 *------------------------------------------------------------------------
 *
 * @module CloudAPIService
 * @description - Virida client-native app cloud api service.
 *
 * @author Tuan Le (tuan.t.lei@gmail.com)
 *
 *------------------------------------------------------------------------
 * @flow
 */
'use strict'; // eslint-disable-line

import { Hf } from 'hyperflow';

import ReactNative from 'react-native'; // eslint-disable-line

import CONSTANT from '../../common/constant';

import EVENT from '../events/cloud-api-event';

const SELECT_AQ_RECORD_API = `http://virida.azurewebsites.net/api/aq`;
const INSERT_AQ_SAMPLE_API = `http://virida.azurewebsites.net/api/aq`;

const {
    NetInfo
} = ReactNative;

const CloudAPIService = Hf.Service.augment({
    composites: [
        Hf.State.MutationComposite
    ],
    state: {
        prevNetInfoType: {
            value: ``
        },
        accessPermit: {
            value: {
                credentialId: ``,
                accessToken: ``,
                accessTokenProvider: ``
            }
        },
        accessGranted: {
            computable: {
                contexts: [
                    `accessPermit`
                ],
                compute () {
                    return true;
                    // return !Hf.isEmpty(this.accessPermit.credentialId) &&
                    //        !Hf.isEmpty(this.accessPermit.accessToken) &&
                    //        !Hf.isEmpty(this.accessPermit.accessTokenProvider);
                }
            }
        }
    },
    setupCloudAPISelectionAPI: function setupCloudAPIInsertionAPI (jsonContentHeaders) {
        const service = this;

        service.incoming(EVENT.REQUEST.CLOUD_API_SELECTION).handle((requestor) => {
            NetInfo.getConnectionInfo().then((netInfo) => {
                if (netInfo.type === `wifi` || netInfo.type === `cell` || netInfo.type === `unknown`) {
                    if (service.accessGranted) {
                        // const {
                        //     credentialId,
                        //     accessToken,
                        //     accessTokenProvider
                        // } = service.accessPermit;
                        const timeout = setTimeout(() => {
                            clearTimeout(timeout);
                            service.outgoing(EVENT.RESPONSE.TO.CLOUD_API_SELECTION.TIMED_OUT).emit();
                            Hf.log(`warn1`, `CloudAPIService - Post request timeout.`);
                        }, CONSTANT.HTTP.API_REQUEST_TIMEOUT_MS);

                        // switch (accessTokenProvider) { // eslint-disable-line
                        // case `jwt`:
                        //     jsonContentHeaders.append(`Authorization`, `JWT ${accessToken}`);
                        //     break;
                        // case `facebook`:
                        //     jsonContentHeaders.append(`Authorization`, `Bearer ${accessToken}`);
                        //     break;
                        // }

                        fetch(SELECT_AQ_RECORD_API, {
                            method: `post`,
                            headers: jsonContentHeaders,
                            body: JSON.stringify({
                                ...requestor
                                // credentialId
                            })
                        }).then((response) => {
                            if (response.status === CONSTANT.HTTP.CODE.OK) {
                                clearTimeout(timeout);
                                Hf.log(`info1`, `Server responsed with status ${CONSTANT.HTTP.CODE.OK}.`);
                                service.outgoing(EVENT.RESPONSE.TO.CLOUD_API_SELECTION.OK).emit();
                            } else if (response.status === CONSTANT.HTTP.CODE.NOT_FOUND) {
                                clearTimeout(timeout);
                                service.outgoing(EVENT.RESPONSE.TO.CLOUD_API_SELECTION.NOT_FOUND).emit();
                                Hf.log(`warn1`, `CloudAPIService - Server responsed with error status ${CONSTANT.HTTP.CODE.NOT_FOUND}.`);
                            } else if (response.status === CONSTANT.HTTP.CODE.ERROR) {
                                clearTimeout(timeout);
                                service.outgoing(EVENT.RESPONSE.TO.CLOUD_API_SELECTION.ERROR).emit();
                                Hf.log(`warn1`, `CloudAPIService - Server responsed with error status ${CONSTANT.HTTP.CODE.ERROR}.`);
                            }
                        }).catch((error) => {
                            clearTimeout(timeout);
                            service.outgoing(EVENT.RESPONSE.TO.CLOUD_API_SELECTION.ERROR).emit();
                            Hf.log(`warn1`, `CloudAPIService - Unable to do post request. ${error.message}`);
                        });
                    } else {
                        service.outgoing(EVENT.RESPONSE.TO.CLOUD_API_SELECTION.UNAUTHORIZED).emit();
                        Hf.log(`warn1`, `CloudAPIService - Access not granted.`);
                    }
                } else {
                    if ((netInfo.type === `none` || netInfo.type === `unknown`) && (service.prevNetInfoType !== `none` && service.prevNetInfoType !== `unknown`)) {
                        service.outgoing(EVENT.RESPONSE.TO.CLOUD_API_SELECTION.NOT_AVAILABLE).emit();
                    }
                    Hf.log(`info1`, `Net connection is offline.`);
                }
                service.reduce({
                    prevNetInfoType: netInfo.type
                });
            });
        });
    },
    setupCloudAPIInsertionAPI: function setupCloudAPIInsertionAPI (jsonContentHeaders) {
        const service = this;
        service.incoming(EVENT.REQUEST.CLOUD_API_INSERTION).handle((requestor) => {
            NetInfo.getConnectionInfo().then((netInfo) => {
                if (netInfo.type === `wifi` || netInfo.type === `cell` || netInfo.type === `unknown`) {
                    if (service.accessGranted) {
                        // const {
                        //     credentialId,
                        //     accessToken,
                        //     accessTokenProvider
                        // } = service.accessPermit;
                        const timeout = setTimeout(() => {
                            clearTimeout(timeout);
                            service.outgoing(EVENT.RESPONSE.TO.CLOUD_API_INSERTION.TIMED_OUT).emit();
                            Hf.log(`warn1`, `CloudAPIService - Post request timeout.`);
                        }, CONSTANT.HTTP.API_REQUEST_TIMEOUT_MS);

                        // switch (accessTokenProvider) { // eslint-disable-line
                        // case `jwt`:
                        //     jsonContentHeaders.append(`Authorization`, `JWT ${accessToken}`);
                        //     break;
                        // case `facebook`:
                        //     jsonContentHeaders.append(`Authorization`, `Bearer ${accessToken}`);
                        //     break;
                        // }

                        fetch(INSERT_AQ_SAMPLE_API, {
                            method: `post`,
                            headers: jsonContentHeaders,
                            body: JSON.stringify({
                                ...requestor
                                // credentialId
                            })
                        }).then((response) => {
                            if (response.status === CONSTANT.HTTP.CODE.OK) {
                                clearTimeout(timeout);
                                Hf.log(`info1`, `Server responsed with status ${CONSTANT.HTTP.CODE.OK}.`);
                                service.outgoing(EVENT.RESPONSE.TO.CLOUD_API_INSERTION.OK).emit();
                            } else if (response.status === CONSTANT.HTTP.CODE.NOT_FOUND) {
                                clearTimeout(timeout);
                                // service.outgoing(EVENT.RESPONSE.TO.CLOUD_API_INSERTION.NOT_FOUND).emit();
                                Hf.log(`warn1`, `CloudAPIService - Server responsed with error status ${CONSTANT.HTTP.CODE.NOT_FOUND}.`);
                            } else if (response.status === CONSTANT.HTTP.CODE.ERROR) {
                                clearTimeout(timeout);
                                service.outgoing(EVENT.RESPONSE.TO.CLOUD_API_INSERTION.ERROR).emit();
                                Hf.log(`warn1`, `CloudAPIService - Server responsed with error status ${CONSTANT.HTTP.CODE.ERROR}.`);
                            }
                        }).catch((error) => {
                            clearTimeout(timeout);
                            service.outgoing(EVENT.RESPONSE.TO.CLOUD_API_INSERTION.ERROR).emit();
                            Hf.log(`warn1`, `CloudAPIService - Unable to do post request. ${error.message}`);
                        });
                    } else {
                        service.outgoing(EVENT.RESPONSE.TO.CLOUD_API_INSERTION.UNAUTHORIZED).emit();
                        Hf.log(`warn1`, `CloudAPIService - Access not granted.`);
                    }
                } else {
                    if ((netInfo.type === `none` || netInfo.type === `unknown`) && (service.prevNetInfoType !== `none` && service.prevNetInfoType !== `unknown`)) {
                        service.outgoing(EVENT.RESPONSE.TO.CLOUD_API_INSERTION.NOT_AVAILABLE).emit();
                    }
                    Hf.log(`info1`, `Net connection is offline.`);
                }
                service.reduce({
                    prevNetInfoType: netInfo.type
                });
            });
        });
    },
    setup: function setup (done) {
        const service = this;
        const jsonContentHeaders = new Headers();

        service.incoming(EVENT.DO.MUTATE_CLOUD_API_ACCESS_PERMIT).handle((accessPermit) => {
            if (service.reduce({
                accessPermit
            })) {
                service.outgoing(EVENT.AS.CLOUD_API_ACCESS_PERMIT_MUTATED).emit();
            }
        });

        jsonContentHeaders.append(`Accept`, `gzip`);
        jsonContentHeaders.append(`Accept`, `deflate`);
        jsonContentHeaders.append(`Accept`, `application/json`);
        jsonContentHeaders.append(`Content-Type`, `application/json`);
        jsonContentHeaders.append(`Cache-Control`, `no-cache, no-store, max-age=0, must-revalidate`);

        service.setupCloudAPISelectionAPI(jsonContentHeaders);
        service.setupCloudAPIInsertionAPI(jsonContentHeaders);

        done();
    },
    teardown: function teardown (done) {
        done();
    }
});
export default CloudAPIService;
