/**
 * Copyright (c) 2017-present, Virida YEAwareness, Org. All rights reserved.
 *
 *------------------------------------------------------------------------
 *
 * @module AQRFeedAPIService
 * @description - Virida client-native app air quality regional feed server api service.
 *
 * @author Tuan Le (tuan.t.lei@gmail.com)
 *
 *------------------------------------------------------------------------
 * @flow
 */
'use strict'; // eslint-disable-line

import { Hf } from 'hyperflow';

import ReactNative from 'react-native'; // eslint-disable-line

import AQAlertComposite from '../composites/aq-alert-composite';

import CONSTANT from '../constant';

import EVENT from '../events/aqr-api-event';

const {
    NetInfo
} = ReactNative;

const AQRFeedAPIService = Hf.Service.augment({
    composites: [
        AQAlertComposite
    ],
    setupAirNowGetAQRFeedDataAPI: function setupAirNowGetAQRFeedDataAPI (jsonContentHeaders) {
        const service = this;
        service.incoming(EVENT.REQUEST.AIR_NOW_AQR_FEED_DATA).handle((requestor) => {
            NetInfo.isConnected.fetch().then((online) => {
                if (online) {
                    if (Hf.isSchema({
                        aqParams: [ `string` ],
                        timestamp: `string`,
                        latitude: `number`,
                        longitude: `number`,
                        radius: `number`
                    }).of(requestor)) {
                        const {
                            aqParams,
                            timestamp,
                            latitude,
                            longitude,
                            radius
                        } = requestor;
                        const timeoutId = setTimeout(() => {
                            service.outgoing(EVENT.RESPONSE.TO.AIR_NOW_AQR_FEED_DATA.TIMED_OUT).emit();
                        }, CONSTANT.HTTP.API_REQUEST_TIMEOUT_MS);
                        let url;
                        url = `${CONSTANT.URL.AIR_NOW_GET_AQR_FEED_DATA_API}?format=application/json&latitude=`;
                        url = `${url}${latitude}&longitude=${longitude}&distance=${radius * 2}&API_KEY=${CONSTANT.API_KEY.AIR_NOW}`;
                        Hf.log(`info1`, url);
                        fetch(url, {
                            method: `get`,
                            headers: jsonContentHeaders
                        }).then((response) => {
                            clearTimeout(timeoutId);
                            if (response.status === CONSTANT.HTTP.CODE.OK) {
                                Hf.log(`info1`, `AirNow server responsed with status ${CONSTANT.HTTP.CODE.OK}.`);
                                return response.json();
                            } else if (response.status === CONSTANT.HTTP.CODE.ERROR) {
                                service.outgoing(EVENT.RESPONSE.TO.AIR_NOW_AQR_FEED_DATA.ERROR).emit();
                                Hf.log(`warn1`, `AQRFeedAPIService - Server responsed with error status ${CONSTANT.HTTP.CODE.ERROR}.`);
                            }
                        }).then((results) => {
                            if (Hf.isNonEmptyArray(results)) {
                                const reportingRegionName = `${results[0].ReportingArea}, ${results[0].StateCode}`;
                                service.outgoing(EVENT.RESPONSE.TO.AIR_NOW_AQR_FEED_DATA.OK).emit(() => {
                                    return {
                                        aqrInfo: {
                                            timestamp,
                                            reportingRegionName
                                        },
                                        aqrSamples: results.filter((result) => aqParams.includes(result.ParameterName.toLowerCase().replace(/\./g, ``))).map((result) => {
                                            const aqi = parseInt(result.AQI, 10) >= 0 ? parseInt(result.AQI, 10) : 0;
                                            const aqAlert = service.getAQIAlert(aqi);
                                            const aqParam = result.ParameterName.toLowerCase().replace(/\./g, ``);
                                            if (aqParam === `pm25`) {
                                                return {
                                                    ...service.getPM25LvlFromAQI(aqi),
                                                    aqParam,
                                                    aqi,
                                                    aqAlertIndex: aqAlert.index,
                                                    aqAlertMessage: aqAlert.message
                                                };
                                            } else if (aqParam === `pm10`) {
                                                return {
                                                    ...service.getPM10LvlFromAQI(aqi),
                                                    aqParam,
                                                    aqi,
                                                    aqAlertIndex: aqAlert.index,
                                                    aqAlertMessage: aqAlert.message
                                                };
                                            } else if (aqParam === `o3`) {
                                                return {
                                                    ...service.getO3LvlFromAQI(aqi),
                                                    aqParam,
                                                    aqi,
                                                    aqAlertIndex: aqAlert.index,
                                                    aqAlertMessage: aqAlert.message
                                                };
                                            } else if (aqParam === `no2`) {
                                                return {
                                                    ...service.getNO2LvlFromAQI(aqi),
                                                    aqParam,
                                                    aqi,
                                                    aqAlertIndex: aqAlert.index,
                                                    aqAlertMessage: aqAlert.message
                                                };
                                            } else if (aqParam === `so2`) {
                                                return {
                                                    ...service.getSO2LvlFromAQI(aqi),
                                                    aqParam,
                                                    aqi,
                                                    aqAlertIndex: aqAlert.index,
                                                    aqAlertMessage: aqAlert.message
                                                };
                                            } else if (aqParam === `co`) {
                                                return {
                                                    ...service.getCOLvlFromAQI(aqi),
                                                    aqParam,
                                                    aqi,
                                                    aqAlertIndex: aqAlert.index,
                                                    aqAlertMessage: aqAlert.message
                                                };
                                            }
                                        })
                                    };
                                });
                            } else {
                                service.outgoing(EVENT.RESPONSE.TO.AIR_NOW_AQR_FEED_DATA.NOT_FOUND).emit();
                            }
                        }).catch((error) => {
                            clearTimeout(timeoutId);
                            service.outgoing(EVENT.RESPONSE.TO.AIR_NOW_AQR_FEED_DATA.ERROR).emit();
                            Hf.log(`warn1`, `AQRFeedAPIService - Unable to do get request. ${error.message}`);
                        });
                    } else {
                        service.outgoing(EVENT.RESPONSE.TO.AIR_NOW_AQR_FEED_DATA.ERROR).emit();
                        Hf.log(`warn1`, `AQRFeedAPIService - Invalid AirNow api request params.`);
                    }
                } else {
                    service.outgoing(EVENT.RESPONSE.TO.AIR_NOW_AQR_FEED_DATA.ERROR).emit();
                    Hf.log(`warn1`, `AQRFeedAPIService - Device is offline.`);
                }
            });
        });
    },
    setupAQICNGetAQRFeedDataAPI: function setupAQICNGetAQRFeedDataAPI (jsonContentHeaders) {
        const service = this;
        service.incoming(EVENT.REQUEST.AQICN_AQR_FEED_DATA).delay(CONSTANT.HTTP.AQICN_API_REQUEST_DELAY_MS).handle((requestor) => {
            NetInfo.isConnected.fetch().then((online) => {
                if (online) {
                    if (Hf.isSchema({
                        aqParams: [ `string` ],
                        timestamp: `string`,
                        latitude: `number`,
                        longitude: `number`
                    }).of(requestor)) {
                        const {
                            aqParams,
                            timestamp,
                            latitude,
                            longitude
                        } = requestor;
                        const timeoutId = setTimeout(() => {
                            service.outgoing(EVENT.RESPONSE.TO.AQICN_AQR_FEED_DATA.TIMED_OUT).emit();
                        }, CONSTANT.HTTP.API_REQUEST_TIMEOUT_MS);
                        const url = `${CONSTANT.URL.AQICN_GET_AQR_FEED_DATA_API}geo:${latitude};${longitude}/?token=${CONSTANT.API_KEY.AQICN}`;
                        Hf.log(`info1`, url);
                        fetch(url, {
                            method: `get`,
                            headers: jsonContentHeaders
                        }).then((response) => {
                            clearTimeout(timeoutId);
                            if (response.status === CONSTANT.HTTP.CODE.OK) {
                                Hf.log(`info1`, `AQICN server responsed with status ${CONSTANT.HTTP.CODE.OK}.`);
                                return response.json();
                            } else if (response.status === CONSTANT.HTTP.CODE.ERROR) {
                                service.outgoing(EVENT.RESPONSE.TO.AQICN_AQR_FEED_DATA.ERROR).emit();
                                Hf.log(`warn1`, `AQICNAPIService - Server responsed with error status ${CONSTANT.HTTP.CODE.ERROR}.`);
                            }
                        }).then(({
                            status,
                            data
                        }) => {
                            if (status === `ok` && Hf.isObject(data)) {
                                let reportingRegionName = ``;
                                const nameSegments = Hf.stringToArray(data.city.name, `,`);
                                if (nameSegments.length <= 2) {
                                    reportingRegionName = data.city.name;
                                } else {
                                    reportingRegionName = Hf.arrayToString(nameSegments.slice(nameSegments.length - 2), `,`);
                                }
                                service.outgoing(EVENT.RESPONSE.TO.AQICN_AQR_FEED_DATA.OK).emit(() => {
                                    return {
                                        aqrInfo: {
                                            timestamp,
                                            reportingRegionName
                                        },
                                        aqrSamples: Object.keys(data.iaqi).filter((key) => aqParams.includes(key)).map((key) => {
                                            const aqParam = key;
                                            const aqi = parseInt(data.iaqi[key].v, 10);
                                            const aqAlert = service.getAQIAlert(aqi);
                                            if (aqParam === `pm25`) {
                                                return {
                                                    ...service.getPM25LvlFromAQI(parseFloat(data.iaqi[key].v)),
                                                    aqParam,
                                                    aqi,
                                                    aqAlertIndex: aqAlert.index,
                                                    aqAlertMessage: aqAlert.message
                                                };
                                            } else if (aqParam === `pm10`) {
                                                return {
                                                    ...service.getPM10LvlFromAQI(parseFloat(data.iaqi[key].v)),
                                                    aqParam,
                                                    aqi,
                                                    aqAlertIndex: aqAlert.index,
                                                    aqAlertMessage: aqAlert.message
                                                };
                                            } else if (aqParam === `o3`) {
                                                return {
                                                    ...service.getO3LvlFromAQI(parseFloat(data.iaqi[key].v)),
                                                    aqParam,
                                                    aqi,
                                                    aqAlertIndex: aqAlert.index,
                                                    aqAlertMessage: aqAlert.message
                                                };
                                            } else if (aqParam === `no2`) {
                                                return {
                                                    ...service.getNO2LvlFromAQI(parseFloat(data.iaqi[key].v)),
                                                    aqParam,
                                                    aqi,
                                                    aqAlertIndex: aqAlert.index,
                                                    aqAlertMessage: aqAlert.message
                                                };
                                            } else if (aqParam === `so2`) {
                                                return {
                                                    ...service.getSO2LvlFromAQI(parseFloat(data.iaqi[key].v)),
                                                    aqParam,
                                                    aqi,
                                                    aqAlertIndex: aqAlert.index,
                                                    aqAlertMessage: aqAlert.message
                                                };
                                            } else if (aqParam === `co`) {
                                                return {
                                                    ...service.getCOLvlFromAQI(aqi),
                                                    aqParam,
                                                    aqi,
                                                    aqAlertIndex: aqAlert.index,
                                                    aqAlertMessage: aqAlert.message
                                                };
                                            }
                                        })
                                    };
                                });
                            } else {
                                service.outgoing(EVENT.RESPONSE.TO.AQICN_AQR_FEED_DATA.NOT_FOUND).emit();
                            }
                        }).catch((error) => {
                            clearTimeout(timeoutId);
                            service.outgoing(EVENT.RESPONSE.TO.AQICN_AQR_FEED_DATA.ERROR).emit();
                            Hf.log(`warn1`, `AQICNAPIService - Unable to do get request. ${error.message}`);
                        });
                    } else {
                        service.outgoing(EVENT.RESPONSE.TO.AQICN_AQR_FEED_DATA.ERROR).emit();
                        Hf.log(`warn1`, `AQICNAPIService - Invalid AQICN api request params.`);
                    }
                } else {
                    service.outgoing(EVENT.RESPONSE.TO.AQICN_AQR_FEED_DATA.ERROR).emit();
                    Hf.log(`warn1`, `AQICNAPIService - Device is offline.`);
                }
            });
        });
    },
    setup: function setup (done) {
        const service = this;
        const jsonContentHeaders = new Headers();
        jsonContentHeaders.append(`Accept`, `gzip`);
        jsonContentHeaders.append(`Accept`, `deflate`);
        jsonContentHeaders.append(`Accept`, `application/json`);
        jsonContentHeaders.append(`Content-Type`, `application/json`);
        service.setupAirNowGetAQRFeedDataAPI(jsonContentHeaders);
        service.setupAQICNGetAQRFeedDataAPI(jsonContentHeaders);
        done();
    },
    teardown: function teardown (done) {
        done();
    }
});
export default AQRFeedAPIService;
