/**
 * Copyright (c) 2017-present, Virida YEAwareness, Org. All rights reserved.
 *
 *------------------------------------------------------------------------
 *
 * @module AQRNotificationAPIService
 * @description - Virida client-native app air quality regional notification server api service.
 *
 * @author Tuan Le (tuan.t.lei@gmail.com)
 *
 *------------------------------------------------------------------------
 * @flow
 */
'use strict'; // eslint-disable-line

import { Hf } from 'hyperflow';

import moment from 'moment';

import ReactNative from 'react-native'; // eslint-disable-line

import BackgroundGeolocation from 'react-native-background-geolocation';

import BackgroundFetch from 'react-native-background-fetch';

import PushNotification from 'react-native-push-notification';

import AQAlertComposite from '../composites/aq-alert-composite';

import CONSTANT from '../constant';

import EVENT from '../events/aqr-api-event';

const {
    NetInfo,
    PushNotificationIOS
} = ReactNative;

let debounceTimeoutId = null;

const AQRNotificationAPIService = Hf.Service.augment({
    composites: [
        Hf.State.MutationComposite,
        AQAlertComposite
    ],
    state: {
        notification: {
            value: {
                unhealthyAQAlert: true,
                dailyAQAlert: true,
                scheduledTime: {
                    dailyAQAlert: moment(CONSTANT.NOTIFICATION.DAILY_AQ_ALERT_TIME, `h:mm:ssa`).add(24, `hours`).format()
                }
            }
        }
    },
    $init: function $init () {
        const service = this;
        PushNotification.configure({
            onNotification: service.onNotification,
            popInitialNotification: true,
            requestPermissions: true,
            'content-available': 1
        });
    },
    onNotification: function onNotification ({
        userInteraction,
        foreground,
        finish
    }) {
        if (!userInteraction) {
            PushNotification.setApplicationIconBadgeNumber(0);
        }
        if (foreground) {
            PushNotification.setApplicationIconBadgeNumber(0);
        }
        finish(PushNotificationIOS.FetchResult.NoData);
        Hf.log(`info1`, `AQ alert notification pushed.`);
    },
    setupAirNowAQRAlertNotification: function setupAirNowAQRAlertNotification (jsonContentHeaders) {
        const service = this;
        // BackgroundGeolocation.on(`heartbeat`, ({
        //     location
        // }) => {
        //     if (debounceTimeoutId === null) {
        //         debounceTimeoutId = setTimeout(() => {
        //             if (service.notification.unhealthyAQAlert || service.notification.dailyAQAlert) {
        //                 NetInfo.isConnected.fetch().then((online) => {
        //                     if (online) {
        //                         const {
        //                             activity,
        //                             coords: coordinate
        //                         } = location;
        //                         const duration = moment.duration(moment().diff(moment(service.notification.scheduledTime.dailyAQAlert)));
        //                         let url = `${CONSTANT.URL.AIR_NOW_GET_AQR_FEED_DATA_API}?format=application/json&latitude=`;
        //                         url = `${url}${coordinate.latitude}&longitude=${coordinate.longitude}&distance=${CONSTANT.AQR.REGION_SEARCH_RADIUS_MILE * 2}&API_KEY=${CONSTANT.API_KEY.AIR_NOW}`;
        //
        //                         fetch(url, {
        //                             method: `get`,
        //                             headers: jsonContentHeaders
        //                         }).then((response) => {
        //                             if (response.status === CONSTANT.HTTP.CODE.OK) {
        //                                 Hf.log(`info1`, `AirNow server responsed with status ${CONSTANT.HTTP.CODE.OK}.`);
        //                                 return response.json();
        //                             } else if (response.status === CONSTANT.HTTP.CODE.ERROR) {
        //                                 Hf.log(`warn1`, `AQRNotificationAPIService - Server responsed with error status ${CONSTANT.HTTP.CODE.ERROR}.`);
        //                             }
        //                         }).then((results) => {
        //                             if (Hf.isNonEmptyArray(results)) {
        //                                 results.filter((result) => result.ParameterName === `PM2.5`).forEach((result) => {
        //                                     const aqAlert = service.getAQIAlert(parseInt(result.AQI, 10) >= 0 ? parseInt(result.AQI, 10) : 0);
        //                                     if (service.notification.unhealthyAQAlert &&
        //                                         aqAlert.aqi >= 200 ||
        //                                        ((activity.type === `still` && aqAlert.aqi >= 150) ||
        //                                        ((activity.type === `on_foot` || activity.type === `running` || activity.type === `on_bicycle`) && aqAlert.aqi >= 100))) {
        //                                         PushNotification.localNotification({
        //                                             title: `Unhealthy Air Quality Alert!!!`,
        //                                             message: `Current Air Quality Is ${aqAlert.message} With AQI = ${aqAlert.aqi}`,
        //                                             playSound: true,
        //                                             soundName: `default`
        //                                         });
        //                                     }
        //
        //                                     if (service.notification.dailyAQAlert && duration.asMilliseconds() >= 0) {
        //                                         PushNotification.localNotification({
        //                                             title: `Daily Air Quality Alert`,
        //                                             message: `Current Air Quality Is ${aqAlert.message} With AQI = ${aqAlert.aqi}`,
        //                                             playSound: true,
        //                                             soundName: `default`
        //                                         });
        //                                         service.reduce({
        //                                             notification: {
        //                                                 scheduledTime: {
        //                                                     dailyAQAlert: moment(CONSTANT.NOTIFICATION.DAILY_AQ_ALERT_TIME, `h:mm:ssa`).add(24, `hours`).format()
        //                                                 }
        //                                             }
        //                                         });
        //                                         Hf.log(`info1`, `Next daily air quality alert notification scheduled.`);
        //                                     } else {
        //                                         Hf.log(`info1`, `Time until next daily air quality alert notification ${duration.asMilliseconds()}.`);
        //                                     }
        //                                 });
        //                             }
        //                         }).catch((error) => {
        //                             Hf.log(`warn1`, `AQRNotificationAPIService - Unable to do get request. ${error.message}`);
        //                         });
        //                     }
        //                 });
        //             }
        //             clearTimeout(debounceTimeoutId);
        //             debounceTimeoutId = null;
        //         }, 1000);
        //         Hf.log(`info1`, `Air quality notification check refreshing.`);
        //     }
        //     Hf.log(`info1`, `Background Heatbeat.`);
        // });
        BackgroundFetch.configure({
            minimumFetchInterval: CONSTANT.GENERAL.BACKGROUND_HEARTBEAT_INTERVAL_M,
            stopOnTerminate: false,
            startOnBoot: true
        }, () => {
            if (debounceTimeoutId === null) {
                debounceTimeoutId = setTimeout(() => {
                    if (service.notification.unhealthyAQAlert || service.notification.dailyAQAlert) {
                        BackgroundGeolocation.getCurrentPosition({
                            samples: 1,
                            persist: false
                        }).then((location) => {
                            const {
                                activity,
                                coords: coordinate
                            } = location;
                            NetInfo.isConnected.fetch().then((online) => {
                                if (online) {
                                    const duration = moment.duration(moment().diff(moment(service.notification.scheduledTime.dailyAQAlert)));
                                    let url = `${CONSTANT.URL.AIR_NOW_GET_AQR_FEED_DATA_API}?format=application/json&latitude=`;
                                    url = `${url}${coordinate.latitude}&longitude=${coordinate.longitude}&distance=${CONSTANT.AQR.REGION_SEARCH_RADIUS_MILE * 2}&API_KEY=${CONSTANT.API_KEY.AIR_NOW}`;
                                    fetch(url, {
                                        method: `get`,
                                        headers: jsonContentHeaders
                                    }).then((response) => {
                                        if (response.status === CONSTANT.HTTP.CODE.OK) {
                                            Hf.log(`info1`, `AirNow server responsed with status ${CONSTANT.HTTP.CODE.OK}.`);
                                            return response.json();
                                        } else if (response.status === CONSTANT.HTTP.CODE.ERROR) {
                                            Hf.log(`warn1`, `AQRNotificationAPIService - Server responsed with error status ${CONSTANT.HTTP.CODE.ERROR}.`);
                                        }
                                    }).then((results) => {
                                        if (Hf.isNonEmptyArray(results)) {
                                            results.filter((result) => result.ParameterName === `PM2.5`).forEach((result) => {
                                                const aqAlert = service.getAQIAlert(parseInt(result.AQI, 10) >= 0 ? parseInt(result.AQI, 10) : 0);
                                                // PushNotification.localNotification({
                                                //     title: `TEST Alert!!!`,
                                                //     message: `Current Air Quality Is ${aqAlert.message} With AQI = ${aqAlert.aqi} - Activity is ${activity.type}.`,
                                                //     playSound: true,
                                                //     soundName: `default`
                                                // });
                                                if (service.notification.unhealthyAQAlert &&
                                                    aqAlert.aqi >= 200 ||
                                                   ((activity.type === `still` && aqAlert.aqi >= 150) ||
                                                   ((activity.type === `on_foot` || activity.type === `running` || activity.type === `on_bicycle`) && aqAlert.aqi >= 100))) {
                                                    PushNotification.localNotification({
                                                        title: `Unhealthy Air Quality Alert!!!`,
                                                        message: `Current Air Quality Is ${aqAlert.message} With AQI = ${aqAlert.aqi}`,
                                                        playSound: true,
                                                        soundName: `default`
                                                    });
                                                }
                                                if (service.notification.dailyAQAlert && duration.asMilliseconds() >= 0) {
                                                    PushNotification.localNotification({
                                                        title: `Daily Air Quality Alert`,
                                                        message: `Current Air Quality Is ${aqAlert.message} With AQI = ${aqAlert.aqi}`,
                                                        playSound: true,
                                                        soundName: `default`
                                                    });
                                                    service.reduce({
                                                        notification: {
                                                            scheduledTime: {
                                                                dailyAQAlert: moment(CONSTANT.NOTIFICATION.DAILY_AQ_ALERT_TIME, `h:mm:ssa`).add(24, `hours`).format()
                                                            }
                                                        }
                                                    });
                                                    Hf.log(`info1`, `Next daily air quality alert notification scheduled.`);
                                                } else {
                                                    Hf.log(`info1`, `Time until next daily air quality alert notification ${duration.asMilliseconds()}.`);
                                                }
                                            });
                                        }
                                        BackgroundFetch.finish(BackgroundFetch.FETCH_RESULT_NEW_DATA);
                                    }).catch((error) => {
                                        BackgroundFetch.finish(BackgroundFetch.FETCH_RESULT_NEW_DATA);
                                        Hf.log(`warn1`, `AQRNotificationAPIService - Unable to do get request. ${error.message}`);
                                    });
                                }
                            });
                        }).catch(error => {
                            BackgroundFetch.finish(BackgroundFetch.FETCH_RESULT_NEW_DATA);
                            Hf.log(`warn1`, `AQRNotificationAPIService - Unable to get device's geolocation. ${error.message}`);
                        });
                    } else {
                        BackgroundFetch.finish(BackgroundFetch.FETCH_RESULT_NEW_DATA);
                    }
                    clearTimeout(debounceTimeoutId);
                    debounceTimeoutId = null;
                }, 1000);
                Hf.log(`info1`, `Air quality notification check refreshing.`);
            } else {
                BackgroundFetch.finish(BackgroundFetch.FETCH_RESULT_NEW_DATA);
            }
            Hf.log(`info1`, `Background fetching.`);
        }, (error) => {
            Hf.log(`warn1`, `AQRNotificationAPIService - Unable to start background fetching. ${error.message}`);
        });
        BackgroundFetch.status((status) => {
            switch(status) { // eslint-disable-line
            case BackgroundFetch.STATUS_RESTRICTED:
                Hf.log(`warn1`, `AQRNotificationAPIService - Background fetching restricted.`);
                break;
            case BackgroundFetch.STATUS_DENIED:
                Hf.log(`warn1`, `AQRNotificationAPIService - Background fetching denied.`);
                break;
            case BackgroundFetch.STATUS_AVAILABLE:
                Hf.log(`info1`, `Starting background fetching.`);
                break;
            }
        });
    },
    setup: function setup (done) {
        const service = this;
        const jsonContentHeaders = new Headers();
        jsonContentHeaders.append(`Accept`, `gzip`);
        jsonContentHeaders.append(`Accept`, `deflate`);
        jsonContentHeaders.append(`Accept`, `application/json`);
        jsonContentHeaders.append(`Content-Type`, `application/json`);
        service.setupAirNowAQRAlertNotification(jsonContentHeaders);
        service.incoming(EVENT.DO.MUTATE_NOTIFICATION_SETTING).handle((notification) => {
            if (service.reduce({
                notification
            })) {
                Hf.log(`info1`, `Air quality notification setting updated.`);
            }
        });
        done();
    },
    teardown: function teardown (done) {
        BackgroundFetch.stop(() => {
            Hf.log(`info1`, `Stopping background fetching.`);
        }, (error) => {
            Hf.log(`warn1`, `AQRNotificationAPIService - Unable to stop background fetching. ${error.message}`);
        });
        done();
    }
});
export default AQRNotificationAPIService;
