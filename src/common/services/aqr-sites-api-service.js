/**
 * Copyright (c) 2017-present, Virida YEAwareness, Org. All rights reserved.
 *
 *------------------------------------------------------------------------
 *
 * @module AQRSitesAPIService
 * @description - Virida client-native app air quality regional sites server api service.
 *
 * @author Tuan Le (tuan.t.lei@gmail.com)
 *
 *------------------------------------------------------------------------
 * @flow
 */
'use strict'; // eslint-disable-line

import { Hf } from 'hyperflow';

import moment from 'moment';

import ReactNative from 'react-native'; // eslint-disable-line

import AQAlertComposite from '../composites/aq-alert-composite';

import CONSTANT from '../../common/constant';

import EVENT from '../events/aqr-api-event';

const {
    NetInfo
} = ReactNative;

const AQRSitesAPIService = Hf.Service.augment({
    composites: [
        AQAlertComposite
    ],
    setupAirNowGetAQRSiteDataAPI: function setupAirNowGetAQRSiteDataAPI (jsonContentHeaders) {
        const service = this;
        service.incoming(EVENT.REQUEST.AIR_NOW_AQR_SITE_DATA).handle((requestor) => {
            NetInfo.isConnected.fetch().then((online) => {
                if (online) {
                    if (Hf.isSchema({
                        aqParams: [ `string` ],
                        timestamp: `string`,
                        regionBBox: {
                            northeast: {
                                latitude: `number`,
                                longitude: `number`
                            },
                            southwest: {
                                latitude: `number`,
                                longitude: `number`
                            }
                        }
                    }).of(requestor)) {
                        const {
                            aqParams,
                            timestamp,
                            regionBBox
                        } = requestor;
                        const timeoutId = setTimeout(() => {
                            service.outgoing(EVENT.RESPONSE.TO.AIR_NOW_AQR_SITE_DATA.TIMED_OUT).emit();
                        }, CONSTANT.HTTP.API_REQUEST_TIMEOUT_MS);
                        const bBoxPts = `${regionBBox.southwest.longitude},${regionBBox.southwest.latitude},${regionBBox.northeast.longitude},${regionBBox.northeast.latitude}`;
                        let url;
                        url = `${CONSTANT.URL.AIR_NOW_GET_AQR_SITE_DATA_API}?format=application/json&verbose=1&startDate=${moment(timestamp).utc().subtract(1, `minutes`).format(`YYYY-MM-DDThh`)}&endDate=${moment(timestamp).utc().format(`YYYY-MM-DDThh`)}&parameters=${aqParams}&BBOX=${bBoxPts}&dataType=B&API_KEY=${CONSTANT.API_KEY.AIR_NOW}`;
                        Hf.log(`info1`, url);
                        fetch(url, {
                            method: `get`,
                            headers: jsonContentHeaders
                        }).then((response) => {
                            clearTimeout(timeoutId);
                            if (response.status === CONSTANT.HTTP.CODE.OK) {
                                Hf.log(`info1`, `AirNow server responsed with status ${CONSTANT.HTTP.CODE.OK}.`);
                                return response.json();
                            } else if (response.status === CONSTANT.HTTP.CODE.ERROR) {
                                service.outgoing(EVENT.RESPONSE.TO.AIR_NOW_AQR_SITE_DATA.ERROR).emit();
                                Hf.log(`warn1`, `AirNowAPIService - Server responsed with error status ${CONSTANT.HTTP.CODE.ERROR}.`);
                            }
                        }).then((results) => {
                            if (Hf.isNonEmptyArray(results)) {
                                const aqrSites = results.filter((result) => aqParams.includes(result.Parameter.toLowerCase().replace(/\./g, ``))).map((result) => {
                                    const aqi = parseInt(result.AQI, 10) >= 0 ? parseInt(result.AQI, 10) : 0;
                                    const aqAlert = service.getAQIAlert(aqi);
                                    return {
                                        info: {
                                            code: result.IntlAQSCode,
                                            name: result.SiteName,
                                            latitude: result.Latitude,
                                            longitude: result.Longitude
                                        },
                                        aqSample: {
                                            aqParam: result.Parameter.toLowerCase().replace(/\./g, ``),
                                            aqi: aqi,
                                            aqAlertIndex: aqAlert.index,
                                            aqAlertMessage: aqAlert.message,
                                            aqUnit: `µg / m³`,
                                            aqConcentration: parseFloat(result.Value)
                                        }
                                    };
                                }).sort((resultA, resultB) => {
                                    return resultB.aqi - resultA.aqi;
                                }).filter((result, index) => index <= CONSTANT.AQR.SITE_COUNT_LIMIT);
                                if (!Hf.isEmpty(aqrSites)) {
                                    service.outgoing(EVENT.RESPONSE.TO.AIR_NOW_AQR_SITE_DATA.OK).emit(() => {
                                        return {
                                            aqrInfo: {
                                                timestamp,
                                                regionBBox
                                            },
                                            aqrSites
                                        };
                                    });
                                } else {
                                    service.outgoing(EVENT.RESPONSE.TO.AIR_NOW_AQR_SITE_DATA.NOT_FOUND).emit();
                                }
                            } else {
                                service.outgoing(EVENT.RESPONSE.TO.AIR_NOW_AQR_SITE_DATA.NOT_FOUND).emit();
                            }
                        }).catch((error) => {
                            clearTimeout(timeoutId);
                            service.outgoing(EVENT.RESPONSE.TO.AIR_NOW_AQR_SITE_DATA.ERROR).emit();
                            Hf.log(`warn1`, `AirNowAPIService - Unable to do get request. ${error.message}`);
                        });
                    } else {
                        service.outgoing(EVENT.RESPONSE.TO.AIR_NOW_AQR_SITE_DATA.ERROR).emit();
                        Hf.log(`warn1`, `AirNowAPIService - Invalid AirNow api request params.`);
                    }
                } else {
                    service.outgoing(EVENT.RESPONSE.TO.AIR_NOW_AQR_SITE_DATA.ERROR).emit();
                    Hf.log(`warn1`, `AirNowAPIService - Device is offline.`);
                }
            });
        });
    },
    setupAQICNGetAQRSiteDataAPI: function setupAQICNGetAQRSiteDataAPI (jsonContentHeaders) {
        const service = this;
        service.incoming(EVENT.REQUEST.AQICN_AQR_SITE_DATA).delay(CONSTANT.HTTP.AQICN_API_REQUEST_DELAY_MS).handle((requestor) => {
            NetInfo.isConnected.fetch().then((online) => {
                if (online) {
                    if (Hf.isSchema({
                        aqParams: [ `string` ],
                        timestamp: `string`,
                        regionBBox: {
                            northeast: {
                                latitude: `number`,
                                longitude: `number`
                            },
                            southwest: {
                                latitude: `number`,
                                longitude: `number`
                            }
                        }
                    }).of(requestor)) {
                        const {
                            aqParams,
                            timestamp,
                            regionBBox
                        } = requestor;
                        const timeoutId = setTimeout(() => {
                            service.outgoing(EVENT.RESPONSE.TO.AQICN_AQR_SITE_DATA.TIMED_OUT).emit();
                        }, CONSTANT.HTTP.API_REQUEST_TIMEOUT_MS);
                        const bBoxPts = `${regionBBox.southwest.latitude},${regionBBox.southwest.longitude},${regionBBox.northeast.latitude},${regionBBox.northeast.longitude}`;
                        let url;
                        url = `${CONSTANT.URL.AQICN_GET_AQR_SITE_DATA_API}?latlng=${bBoxPts}&token=${CONSTANT.API_KEY.AQICN}`;
                        Hf.log(`info1`, url);
                        fetch(url, {
                            method: `get`,
                            headers: jsonContentHeaders
                        }).then((response) => {
                            clearTimeout(timeoutId);
                            if (response.status === CONSTANT.HTTP.CODE.OK) {
                                Hf.log(`info1`, `AQICN server responsed with status ${CONSTANT.HTTP.CODE.OK}.`);
                                return response.json();
                            } else if (response.status === CONSTANT.HTTP.CODE.ERROR) {
                                service.outgoing(EVENT.RESPONSE.TO.AQICN_AQR_SITE_DATA.ERROR).emit();
                                Hf.log(`warn1`, `AQRSitesAPIService - Server responsed with error status ${CONSTANT.HTTP.CODE.ERROR}.`);
                            }
                        }).then(({
                            status,
                            data
                        }) => {
                            if (status === `ok` && Hf.isNonEmptyArray(data)) {
                                const aqrSites = data.filter((result) => result.aqi !== `-`).map((result) => {
                                    const aqi = parseInt(result.aqi, 10) >= 0 ? parseInt(result.aqi, 10) : 0;
                                    const aqAlert = service.getAQIAlert(aqi);
                                    return {
                                        info: {
                                            code: `${result.uid}`,
                                            name: ``,
                                            latitude: result.lat,
                                            longitude: result.lon
                                        },
                                        aqSample: {
                                            ...service.getPM25LvlFromAQI(aqi),
                                            aqParam: `pm25`,
                                            aqi,
                                            aqAlertIndex: aqAlert.index,
                                            aqAlertMessage: aqAlert.message
                                        }
                                    };
                                }).sort((resultA, resultB) => {
                                    return resultB.aqi - resultA.aqi;
                                }).filter((result, index) => index <= CONSTANT.AQR.SITE_COUNT_LIMIT);
                                if (!Hf.isEmpty(aqrSites)) {
                                    service.outgoing(EVENT.RESPONSE.TO.AQICN_AQR_SITE_DATA.OK).emit(() => {
                                        return {
                                            aqrInfo: {
                                                timestamp,
                                                regionBBox
                                            },
                                            aqrSites
                                        };
                                    });
                                } else {
                                    service.outgoing(EVENT.RESPONSE.TO.AQICN_AQR_SITE_DATA.NOT_FOUND).emit();
                                }
                            } else {
                                service.outgoing(EVENT.RESPONSE.TO.AQICN_AQR_SITE_DATA.NOT_FOUND).emit();
                            }
                        }).catch((error) => {
                            clearTimeout(timeoutId);
                            service.outgoing(EVENT.RESPONSE.TO.AQICN_AQR_SITE_DATA.ERROR).emit();
                            Hf.log(`warn1`, `AQRSitesAPIService - Unable to do get request. ${error.message}`);
                        });
                    } else {
                        service.outgoing(EVENT.RESPONSE.TO.AQICN_AQR_SITE_DATA.ERROR).emit();
                        Hf.log(`warn1`, `AQRSitesAPIService - Invalid AQICN api request params.`);
                    }
                } else {
                    service.outgoing(EVENT.RESPONSE.TO.AQICN_AQR_SITE_DATA.ERROR).emit();
                    Hf.log(`warn1`, `AQRSitesAPIService - Device is offline.`);
                }
            });
        });
    },
    setup: function setup (done) {
        const service = this;
        const jsonContentHeaders = new Headers();
        jsonContentHeaders.append(`Accept`, `gzip`);
        jsonContentHeaders.append(`Accept`, `deflate`);
        jsonContentHeaders.append(`Accept`, `application/json`);
        jsonContentHeaders.append(`Content-Type`, `application/json`);
        service.setupAirNowGetAQRSiteDataAPI(jsonContentHeaders);
        service.setupAQICNGetAQRSiteDataAPI(jsonContentHeaders);
        done();
    },
    teardown: function teardown (done) {
        done();
    }
});
export default AQRSitesAPIService;
