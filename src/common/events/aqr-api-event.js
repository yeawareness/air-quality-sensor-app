/**
 * Copyright (c) 2017-present, Virida YEAwareness, Org. All rights reserved.
 *
 *------------------------------------------------------------------------
 *
 * @description - Boki client native app air quality regional api event ids.
 *
 * @author Tuan Le (tuan.t.lei@gmail.com)
 *
 *------------------------------------------------------------------------
 * @flow
 */
'use strict'; // eslint-disable-line

import { Hf } from 'hyperflow';

const aqrAPIEventMap = {
    doEvents: [
        `mutate-notification-setting`
    ],
    requestEvents: [
        `air-now-aqr-feed-data`,
        `air-now-aqr-site-data`,
        `air-now-aqr-forecast-data`,
        `aqicn-aqr-feed-data`,
        `aqicn-aqr-site-data`
    ]
};

export default Hf.Event.create(aqrAPIEventMap);
