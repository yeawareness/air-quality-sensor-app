/**
 * Copyright (c) 2017-present, Virida YEAwareness, Org. All rights reserved.
 *
 *------------------------------------------------------------------------
 *
 * @description - Boki client native app AQICN api event ids.
 *
 * @author Tuan Le (tuan.t.lei@gmail.com)
 *
 *------------------------------------------------------------------------
 * @flow
 */
'use strict'; // eslint-disable-line

import { Hf } from 'hyperflow';

const aqicnAPIEventMap = {
    requestEvents: [
        `aqicn-aqr-feed-data`,
        `aqicn-aqr-site-data`
    ]
};

export default Hf.Event.create(aqicnAPIEventMap);
