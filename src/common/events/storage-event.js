/**
 * Copyright (c) 2017-present, Virida YEAwareness, Org. All rights reserved.
 *
 *------------------------------------------------------------------------
 *
 * @description - Virida client native app storage event ids.
 *
 * @author Tuan Le (tuan.t.lei@gmail.com)
 *
 *------------------------------------------------------------------------
 * @flow
 */
'use strict'; // eslint-disable-line

import { Hf } from 'hyperflow';

export default Hf.Event.create({
    asEvents: [
        `storage-initialized`
    ],
    doEvents: [
        `storage-initialization`
    ],
    requestEvents: [
        `read-setting`,
        `write-setting`,
        `read-aqs-info`,
        `write-aqs-info`,
        `read-aqs-general-setting`,
        `write-aqs-general-setting`,
        `read-aqs-calibration-setting`,
        `write-aqs-calibration-setting`
    ]
});
