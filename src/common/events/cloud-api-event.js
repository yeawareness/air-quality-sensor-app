/**
 * Copyright (c) 2017-present, Virida YEAwareness, Org. All rights reserved.
 *
 *------------------------------------------------------------------------
 *
 * @description - Virida client native app cloud api server event ids.
 *
 * @author Tuan Le (tuan.t.lei@gmail.com)
 *
 *------------------------------------------------------------------------
 * @flow
 */
'use strict'; // eslint-disable-line

import { Hf } from 'hyperflow';

export default Hf.Event.create({
    onEvents: [
        `close-alert-modal`
    ],
    doEvents: [
        `mutate-alert`,
        `mutate-cloud-api-access-permit`
    ],
    broadcastEvents: [
        `access-permit`
    ],
    requestEvents: [
        `cloud-api-selection`,
        `cloud-api-insertion`
    ]
});
