/**
 * Copyright (c) 2017-present, Virida YEAwareness, Org. All rights reserved.
 *
 *------------------------------------------------------------------------
 *
 * @description - Boki client native app AirNow api event ids.
 *
 * @author Tuan Le (tuan.t.lei@gmail.com)
 *
 *------------------------------------------------------------------------
 * @flow
 */
'use strict'; // eslint-disable-line

import { Hf } from 'hyperflow';

const airnowAPIEventMap = {
    doEvents: [
        `mutate-notification-setting`
    ],
    requestEvents: [
        `air-now-aqr-feed-data`,
        `air-now-aqr-site-data`,
        `air-now-aqr-forecast-data`
    ]
};

export default Hf.Event.create(airnowAPIEventMap);
