/**
 * Copyright (c) 2017-present, Virida YEAwareness, Org. All rights reserved.
 *
 *------------------------------------------------------------------------
 *
 * @module BLEDomain
 * @description - Virida client-native air quality sensor ble domain.
 *
 * @author Tuan Le (tuan.t.lei@gmail.com)
 *
 *------------------------------------------------------------------------
 * @flow
 */
'use strict'; // eslint-disable-line

import { Hf } from 'hyperflow';

import moment from 'moment';

import BLEStore from '../stores/ble-store';

import BLEDiscoveryModalInterface from '../interfaces/ble-discovery-modal-interface';

import BLEService from '../services/ble-service';

import CONSTANT from '../../common/constant';

import EVENT from '../events/ble-event';

const BLEDomain = Hf.Domain.augment({
    $init: function $init () {
        const domain = this;
        domain.register({
            store: BLEStore({
                name: `ble-store`
            }),
            intf: BLEDiscoveryModalInterface({
                name: `ble-discovery-modal`
            }),
            services: [
                BLEService({
                    name: `ble-service`
                })
            ]
        });
    },
    setup: function setup (done) {
        const domain = this;
        domain.incoming(EVENT.DO.BLE_RESET).repeat();
        // domain.incoming(EVENT.REQUEST.AQS_DISCOVER_AND_CONNECT).handle((mId) => {
        //     domain.outgoing(EVENT.DO.MUTATE_BLE_CONNECTIVITY_STATE).emit(() => CONSTANT.BLE.DISCOVERING_STATE);
        //     domain.outgoing(EVENT.DO.MUTATE_BLE_CONNECTIVITY_STATE).emit(() => CONSTANT.BLE.CONNECTING_STATE);
        // });
        // domain.incoming(EVENT.ON.AQS_WHOAMI).handle((mId) => {
        //
        // });
        domain.incoming(EVENT.ON.AQS_SKIPPING).handle(() => {
            domain.outgoing(EVENT.DO.MUTATE_BLE_CONNECTIVITY_STATE).emit(() => CONSTANT.BLE.DISCONNECTED_STATE);
            domain.outgoing(EVENT.BROADCAST.AQS_SKIPPED).emit();
        });
        domain.incoming(EVENT.ON.AQS_DISCOVERY).handle(() => {
            domain.outgoing(EVENT.DO.MUTATE_DISCOVERED_AQS_INFOS).emit(() => []);
            domain.outgoing(EVENT.DO.MUTATE_BLE_CONNECTIVITY_STATE).emit(() => CONSTANT.BLE.DISCOVERING_STATE);
            domain.outgoing(EVENT.REQUEST.BLE_DISCOVERY).emit();
        });
        domain.incoming(EVENT.RESPONSE.TO.BLE_DISCOVERY.OK).handle((discoveredAQSInfos) => {
            domain.outgoing(EVENT.DO.MUTATE_BLE_CONNECTIVITY_STATE).emit(() => CONSTANT.BLE.DISCOVERED_STATE);
            return discoveredAQSInfos;
        }).relay(EVENT.DO.MUTATE_DISCOVERED_AQS_INFOS);
        domain.incoming(EVENT.RESPONSE.TO.BLE_DISCOVERY.NOT_FOUND).handle(() => {
            domain.outgoing(EVENT.DO.MUTATE_DISCOVERED_AQS_INFOS).emit(() => []);
            domain.outgoing(EVENT.DO.MUTATE_BLE_CONNECTIVITY_STATE).emit(() => CONSTANT.BLE.DISCONNECTED_STATE);
            return {
                visible: true,
                title: `Ble Alert`,
                message: `No Virida Air Quality Sensor Was Found.`
            };
        }).relay(EVENT.BROADCAST.BLE_ALERT);
        domain.incoming(EVENT.RESPONSE.TO.BLE_DISCOVERY.NOT_AVAILABLE).handle(() => {
            domain.outgoing(EVENT.DO.MUTATE_DISCOVERED_AQS_INFOS).emit(() => []);
            domain.outgoing(EVENT.DO.MUTATE_BLE_CONNECTIVITY_STATE).emit(() => CONSTANT.BLE.DISCONNECTED_STATE);
            return {
                visible: true,
                title: `Ble Alert`,
                message: `Unable To Discover Virida Air Quality Sensor. Check That Bluetooth Is On.`
            };
        }).relay(EVENT.BROADCAST.BLE_ALERT);
        domain.incoming(EVENT.RESPONSE.TO.BLE_DISCOVERY.ERROR).handle(() => {
            domain.outgoing(EVENT.DO.MUTATE_DISCOVERED_AQS_INFOS).emit(() => []);
            domain.outgoing(EVENT.DO.MUTATE_BLE_CONNECTIVITY_STATE).emit(() => CONSTANT.BLE.DISCONNECTED_STATE);
            return {
                visible: true,
                title: `Ble Alert`,
                message: `Unable To Discover Virida Air Quality Sensor Via Bluetooth. An Error Occured.`
            };
        }).relay(EVENT.BROADCAST.BLE_ALERT);
        domain.incoming(EVENT.ON.AQS_CONNECTION).handle((mId) => {
            domain.outgoing(EVENT.DO.MUTATE_BLE_CONNECTIVITY_STATE).emit(() => CONSTANT.BLE.CONNECTING_STATE);
            return mId;
        }).relay(EVENT.REQUEST.BLE_CONNECTION);
        domain.incoming(EVENT.RESPONSE.TO.BLE_CONNECTION.OK).handle(() => {
            domain.outgoing(EVENT.DO.MUTATE_BLE_CONNECTIVITY_STATE).emit(() => CONSTANT.BLE.CONNECTED_STATE);
            domain.outgoing(
                EVENT.BROADCAST.BLE_CONNECTION_ESTABLISHED,
                EVENT.BROADCAST.AQS_LINK_ESTABLISHED
            ).emit();
            domain.outgoing(EVENT.REQUEST.BLE_PAYLOAD_TRANSMIT).emit(() => {
                return {
                    hdr: CONSTANT.AQS.BLE.PAYLOAD.HDR.START_SUBSCRIPTION,
                    contentBuff: 0x00
                };
            });
            domain.incoming(EVENT.REQUEST.AQS_INFO).handle(() => {
                return {
                    hdr: CONSTANT.AQS.BLE.PAYLOAD.HDR.INFO,
                    contentBuff: 0x00
                };
            }).relay(EVENT.REQUEST.BLE_PAYLOAD_TRANSMIT);
            domain.incoming(EVENT.REQUEST.AQS_STATUS).handle(() => {
                return {
                    hdr: CONSTANT.AQS.BLE.PAYLOAD.HDR.STATUS,
                    contentBuff: 0x00
                };
            }).relay(EVENT.REQUEST.BLE_PAYLOAD_TRANSMIT);
            domain.incoming(EVENT.REQUEST.AQS_GENERAL_SETTING).handle(() => {
                return {
                    hdr: CONSTANT.AQS.BLE.PAYLOAD.HDR.GENERAL_SETTING,
                    contentBuff: 0x00
                };
            }).relay(EVENT.REQUEST.BLE_PAYLOAD_TRANSMIT);
            domain.incoming(EVENT.REQUEST.AQS_CALIBRATION_SETTING).handle(() => {
                return {
                    hdr: CONSTANT.AQS.BLE.PAYLOAD.HDR.CALIBRATION_SETTING,
                    contentBuff: 0x00
                };
            }).relay(EVENT.REQUEST.BLE_PAYLOAD_TRANSMIT);
        });
        domain.incoming(EVENT.RESPONSE.TO.BLE_PAYLOAD_TRANSMIT.OK).handle(() => {
            Hf.log(`info1`, `Transimition to air quality sensor via bluetooth was successful.`);
        });
        domain.incoming(EVENT.RESPONSE.TO.BLE_PAYLOAD_TRANSMIT.ERROR).handle(() => {
            return {
                visible: true,
                title: `Ble Alert`,
                message: `Unable To Transimit Payload to Virida Air Quality Sensor Via Bluetooth.`
            };
        }).relay(EVENT.BROADCAST.BLE_ALERT);
        domain.incoming(EVENT.RESPONSE.TO.BLE_PAYLOAD_TRANSMIT.NOT_AVAILABLE).handle(() => {
            return {
                visible: true,
                title: `Ble Alert`,
                message: `Transimition to Virida Air Quality Sensor Via Bluetooth Is Not Available.`
            };
        }).relay(EVENT.BROADCAST.BLE_ALERT);
        domain.incoming(
            EVENT.RESPONSE.TO.BLE_CONNECTION.TIMED_OUT,
            EVENT.RESPONSE.TO.BLE_CONNECTION.ERROR
        ).handle(() => {
            domain.outgoing(EVENT.DO.MUTATE_DISCOVERED_AQS_INFOS).emit(() => []);
            domain.outgoing(EVENT.DO.MUTATE_BLE_CONNECTIVITY_STATE).emit(() => CONSTANT.BLE.DISCONNECTED_STATE);
            return {
                visible: true,
                title: `Ble Alert`,
                message: `Unable To Connect to Virida Air Quality Sensor Via Bluetooth.`
            };
        }).relay(EVENT.BROADCAST.BLE_ALERT);
        domain.incoming(EVENT.BROADCAST.BLE_CONNECTION_LOST).handle(() => {
            domain.outgoing(EVENT.DO.MUTATE_DISCOVERED_AQS_INFOS).emit(() => []);
            domain.outgoing(EVENT.DO.MUTATE_BLE_CONNECTIVITY_STATE).emit(() => CONSTANT.BLE.DISCONNECTED_STATE);
            domain.outgoing(EVENT.BROADCAST.AQS_LINK_LOST).emit();
            return {
                visible: true,
                title: `Ble Alert`,
                message: `Lost Connection To Virida Air Quality Sensor.`
            };
        }).relay(EVENT.BROADCAST.BLE_ALERT);
        domain.incoming(EVENT.BROADCAST.BLE_PAYLOAD_RECEIVED).handle(({
            mId,
            hdr,
            contentBuff
        }) => {
            switch (hdr) {
            case CONSTANT.AQS.BLE.PAYLOAD.HDR.INFO:
                const cDatestamp = contentBuff.readUInt16BE(1).toString();
                const [
                    rev,
                    skew,
                    firmwareVersion
                ] = contentBuff.toString(`ascii`, 3).split(`;`);
                const aqsInfo = {
                    mId,
                    rev,
                    skew,
                    firmwareVersion,
                    cDatestamp
                };
                domain.outgoing(EVENT.RESPONSE.WITH.AQS_INFO).emit(() => aqsInfo);
                Hf.log(`info1`, `Reading air quality sensor info ${JSON.stringify(aqsInfo, null, `\t`)}.`);
                break;
            case CONSTANT.AQS.BLE.PAYLOAD.HDR.STATUS:
                const docked = contentBuff.readUInt8(0) === 1;
                const aqsStatus = {
                    docked
                };
                domain.outgoing(EVENT.RESPONSE.WITH.AQS_STATUS).emit(() => aqsStatus);
                Hf.log(`info1`, `Reading air quality sensor status ${JSON.stringify(aqsStatus, null, `\t`)}.`);
                break;
            case CONSTANT.AQS.BLE.PAYLOAD.HDR.GENERAL_SETTING:
                const aqsGeneralSetting = {
                    ledBrightnessLvl: contentBuff.readUInt8(0),
                    aqSampleCount: contentBuff.readUInt8(1),
                    mInterval: contentBuff.readUInt8(2)
                };
                domain.outgoing(EVENT.RESPONSE.WITH.AQS_GENERAL_SETTING).emit(() => aqsGeneralSetting);
                Hf.log(`info1`, `Reading air quality sensor general setting ${JSON.stringify(aqsGeneralSetting, null, `\t`)}`);
                break;
            case CONSTANT.AQS.BLE.PAYLOAD.HDR.CALIBRATION_SETTING:
                const aqsCalibrationSetting = {
                    pm25: {
                        lowLvlMv: contentBuff.readUInt16BE(0),
                        lowLvlTargetThreshold: contentBuff.readUInt16BE(2) / 100
                    },
                    voc: {
                        ro: contentBuff.readUInt32BE(4),
                        refCOPPM: contentBuff.readUInt16BE(8) / 100,
                        refCOPPMTargetWindow: contentBuff.readUInt16BE(10) / 100
                    }
                };

                domain.outgoing(EVENT.RESPONSE.WITH.AQS_CALIBRATION_SETTING).emit(() => aqsCalibrationSetting);
                Hf.log(`info1`, `Reading air quality sensor calibration setting ${JSON.stringify(aqsCalibrationSetting, null, `\t`)}`);
                break;
            case CONSTANT.AQS.BLE.PAYLOAD.HDR.ENTERING_SLEEP_STATE:
                domain.outgoing(EVENT.BROADCAST.AQS_ENTERING_SLEEP_STATE).emit();
                Hf.log(`info1`, `Connected air quality sensor is entering sleep mode.`);
                break;
            case CONSTANT.AQS.BLE.PAYLOAD.HDR.EXITING_SLEEP_STATE:
                domain.outgoing(EVENT.BROADCAST.AQS_EXITING_SLEEP_STATE).emit();
                Hf.log(`info1`, `Connected air quality sensor is exiting sleep mode.`);
                break;
            case CONSTANT.AQS.BLE.PAYLOAD.HDR.SUBSCRIPTION_STARTED:
                domain.outgoing(EVENT.BROADCAST.AQS_SUBSCRIBED).emit();
                Hf.log(`info1`, `Air quality sensor subscription started.`);
                break;
            case CONSTANT.AQS.BLE.PAYLOAD.HDR.SUBSCRIPTION_ENDED:
                domain.outgoing(EVENT.BROADCAST.AQS_UNSUBSCRIBED).emit();
                Hf.log(`info1`, `Air quality sensor subscription ended.`);
                break;
            case CONSTANT.AQS.BLE.PAYLOAD.HDR.SAMPLING_STARTED:
                domain.outgoing(EVENT.BROADCAST.AQS_SAMPLING_STARTED).emit();
                Hf.log(`info1`, `Air quality sensor sampling started.`);
                break;
            case CONSTANT.AQS.BLE.PAYLOAD.HDR.SAMPLING_ENDED:
                const timestamp = moment().toISOString();
                domain.outgoing(EVENT.BROADCAST.AQS_DATA).emit(() => {
                    if (contentBuff.length === 7) {
                        return {
                            timestamp,
                            aqsCondition: {
                                battLvl: {
                                    unit: `%`,
                                    value: contentBuff.readUInt8(0)
                                },
                                temp: {
                                    unit: `°C`,
                                    value: contentBuff.readUInt16BE(1) / 100
                                },
                                humidity: {
                                    unit: `%`,
                                    value: contentBuff.readUInt16BE(3) / 100
                                }
                            },
                            aqsSamples: [{
                                aqParam: `pm25`,
                                aqUnit: `µg / m³`,
                                aqConcentration: contentBuff.readUInt16BE(5) / 100
                            }]
                        };
                    } else if (contentBuff.length === 9) {
                        return {
                            timestamp,
                            aqsCondition: {
                                battLvl: {
                                    unit: `%`,
                                    value: contentBuff.readUInt8(0)
                                },
                                temp: {
                                    unit: `°C`,
                                    value: contentBuff.readUInt16BE(1) / 100
                                },
                                humidity: {
                                    unit: `%`,
                                    value: contentBuff.readUInt16BE(3) / 100
                                }
                            },
                            aqsSamples: [{
                                aqParam: `pm25`,
                                aqUnit: `µg / m³`,
                                aqConcentration: contentBuff.readUInt16BE(5) / 100
                            }, {
                                aqParam: `voc`,
                                aqUnit: `ro / rs`,
                                aqConcentration: contentBuff.readUInt16BE(7) / 100
                            }]
                        };
                    } else {
                        Hf.log(`warn1`, `BLEDomain - Unrecognized BLE RX payload buffer size ${contentBuff.length}.`);
                    }
                });
                domain.outgoing(EVENT.BROADCAST.AQS_SAMPLING_ENDED).emit();
                Hf.log(`info1`, `Air quality sensor sampling ended.`);
                break;
            default:
                if ((hdr & CONSTANT.AQS.BLE.PAYLOAD.HDR.MERROR_CODE) === CONSTANT.AQS.BLE.PAYLOAD.HDR.MERROR_CODE) {
                    const merrorCode = hdr & (CONSTANT.AQS.BLE.PAYLOAD.MERROR_CODE >> 4);
                    switch (merrorCode) {
                    case CONSTANT.AQS.BLE.PAYLOAD.MERROR.DHT_SENSOR_READING:
                        Hf.log(`warn1`, `BLEDomain - Air quality sensor DHT sensor reading error.`);
                        break;
                    case CONSTANT.AQS.BLE.PAYLOAD.MERROR.VOC_SENSOR_READING:
                        Hf.log(`warn1`, `BLEDomain - Air quality sensor VOC sensor reading error.`);
                        break;
                    case CONSTANT.AQS.BLE.PAYLOAD.MERROR.PM25_SENSOR_READING:
                        Hf.log(`warn1`, `BLEDomain - Air quality sensor PM25 sensor reading error.`);
                        break;
                    case CONSTANT.AQS.BLE.PAYLOAD.MERROR.VOC_SENSOR_CALIBRATION:
                        Hf.log(`warn1`, `BLEDomain -  Air quality sensor VOC sensor calibration error.`);
                        break;
                    case CONSTANT.AQS.BLE.PAYLOAD.MERROR.PM25_SENSOR_CALIBRATION:
                        Hf.log(`warn1`, `BLEDomain - Air quality sensor PM25 sensor calibration error.`);
                        break;
                    case CONSTANT.AQS.BLE.PAYLOAD.MERROR.INCORRECT_PAYLOAD_SIZE:
                        Hf.log(`warn1`, `BLEDomain - Air quality sensor payload size error.`);
                        break;
                    case CONSTANT.AQS.BLE.PAYLOAD.MERROR.SAMPLE_COUNT:
                        Hf.log(`warn1`, `BLEDomain - Air quality sensor sample count error.`);
                        break;
                    case CONSTANT.AQS.BLE.PAYLOAD.MERROR.MONITOR_INTERVAL:
                        Hf.log(`warn1`, `BLEDomain - Air quality sensor monitor interval error.`);
                        break;
                    case CONSTANT.AQS.BLE.PAYLOAD.MERROR.TX_PAYLOAD_BUFF_OVERFLOW:
                        Hf.log(`warn1`, `BLEDomain - Air quality sensor tx buffer overflow error.`);
                        break;
                    case CONSTANT.AQS.BLE.PAYLOAD.MERROR.RX_PAYLOAD_BUFF_OVERFLOW:
                        Hf.log(`warn1`, `BLEDomain - Air quality sensor rx buffer overflow error.`);
                        break;
                    case CONSTANT.AQS.BLE.PAYLOAD.MERROR.UNKNOW_PAYLOAD_HDR:
                        Hf.log(`warn1`, `BLEDomain - Air quality sensor unknow playload error.`);
                        break;
                    default:
                        break;
                    }
                }
                break;
            }
        });
        done();
    }
});
export default BLEDomain;
