/**
 * Copyright (c) 2017-present, Virida YEAwareness, Org. All rights reserved.
 *
 *------------------------------------------------------------------------
 *
 * @module AppDomain
 * @description - Virida client-native app entry domain.
 *
 * @author Tuan Le (tuan.t.lei@gmail.com)
 *
 *------------------------------------------------------------------------
 * @flow
 */
'use strict'; // eslint-disable-line

import { Hf } from 'hyperflow';

import ReactNative from 'react-native'; // eslint-disable-line

import CodePush from 'react-native-code-push';

import BackgroundGeolocation from 'react-native-background-geolocation';

import AppStore from '../stores/app-store';

import AppViewInterface from '../interfaces/app-view-interface';

import StorageService from '../../common/services/storage-service';

// import BLEDomain from './ble-domain';

import MonitorDomain from '../../monitor/domains/monitor-domain';

import MapDomain from '../../map/domains/map-domain';

import TimelineDomain from '../../timeline/domains/timeline-domain';

import SettingDomain from '../../setting/domains/setting-domain';

import CONSTANT from '../../common/constant';

import EVENT from '../events/app-event';

const {
    AppState,
    NetInfo
} = ReactNative;

let periodicRefreshIntervalId = null;

const AppDomain = Hf.Domain.augment({
    $init: function $init () {
        const domain = this;
        domain.register({
            store: AppStore({
                name: `app-store`
            }),
            intf: AppViewInterface({
                name: `app-view`
            }),
            services: [
                StorageService({
                    name: `storage-service`
                })
            ],
            childDomains: [
                // BLEDomain({
                //     name: `ble`
                // }),
                MonitorDomain({
                    name: `monitor`
                }),
                MapDomain({
                    name: `map`
                }),
                TimelineDomain({
                    name: `timeline`
                }),
                SettingDomain({
                    name: `setting`
                })
            ]
        });
    },
    setup: function setup (done) {
        const domain = this;
        domain.outgoing(EVENT.DO.MUTATE_SEQUENCE).emit(() => () => {
            return {
                seq: CONSTANT.SEQ.STARTUP
            };
        });
        domain.outgoing(EVENT.DO.STORAGE_INITIALIZATION).emit();
        domain.incoming(EVENT.AS.STORAGE_INITIALIZED).forward(EVENT.REQUEST.READ_SETTING);
        domain.incoming(EVENT.RESPONSE.TO.READ_SETTING.OK).handle(({
            aqsSkipped,
            showIntro
        }) => {
            if (showIntro) {
                domain.outgoing(EVENT.DO.MUTATE_SEQUENCE).emit(() => () => {
                    return {
                        seq: CONSTANT.SEQ.INTRO
                    };
                });
            } else {
                if (aqsSkipped) {
                    domain.outgoing(EVENT.DO.MONITOR_ACTIVATION).emit(() => true);
                    domain.outgoing(EVENT.DO.MUTATE_SEQUENCE).emit(() => () => {
                        return {
                            seq: CONSTANT.SEQ.AQS_SKIPPED
                        };
                    });
                } else {
                    domain.outgoing(EVENT.REQUEST.READ_AQS_INFO).emit();
                }
            }
            domain.incoming(
                EVENT.ON.INTRODUCTION_FINISHED,
                EVENT.ON.INTRODUCTION_SKIPPED
            ).handle(() => {
                if (aqsSkipped) {
                    domain.outgoing(EVENT.DO.MONITOR_ACTIVATION).emit(() => true);
                    domain.outgoing(EVENT.DO.MUTATE_SEQUENCE).emit(() => () => {
                        return {
                            seq: CONSTANT.SEQ.AQS_SKIPPED
                        };
                    });
                } else {
                    domain.outgoing(EVENT.REQUEST.READ_AQS_INFO).emit();
                }
            });
        });
        domain.incoming(EVENT.RESPONSE.TO.READ_AQS_INFO.OK).handle(({
            mId
        }) => {
            if (Hf.isNonEmptyString(mId)) {
                // domain.outgoing(EVENT.REQUEST.AQS_DISCOVER_AND_CONNECT).emit(() => mId);
            } else {
                domain.outgoing(EVENT.DO.MUTATE_SEQUENCE).emit(() => () => {
                    return {
                        seq: CONSTANT.SEQ.AQS_LINKING
                    };
                });
            }
            domain.outgoing(EVENT.DO.MUTATE_SEQUENCE).emit(() => () => {
                return {
                    seq: CONSTANT.SEQ.AQS_LINKING
                };
            });
        });
        domain.incoming(EVENT.DO.REDISCOVER_AQS).forward(EVENT.DO.RESET);
        domain.incoming(EVENT.AS.RESET).handle(() => {
            domain.outgoing(
                EVENT.DO.MONITOR_ACTIVATION,
                EVENT.DO.MAP_ACTIVATION,
                EVENT.DO.TIMELINE_ACTIVATION,
                EVENT.DO.SETTING_ACTIVATION
            ).emit(() => false);
            domain.outgoing(
                EVENT.DO.MONITOR_RESET,
                EVENT.DO.MAP_RESET,
                EVENT.DO.TIMELINE_RESET,
                EVENT.DO.SETTING_RESET
            ).emit();
            domain.outgoing(EVENT.DO.MUTATE_SEQUENCE).emit(() => () => {
                return {
                    seq: CONSTANT.SEQ.AQS_LINKING
                };
            });
            domain.outgoing(EVENT.REQUEST.WRITE_SETTING).emit(() => {
                return {
                    aqsSkipped: false
                };
            });
        });
        domain.incoming(EVENT.BROADCAST.AQS_SKIPPED).handle(() => {
            domain.outgoing(EVENT.DO.MONITOR_ACTIVATION).emit(() => true);
            domain.outgoing(EVENT.REQUEST.WRITE_SETTING).emit(() => {
                return {
                    aqsSkipped: true
                };
            });
            domain.outgoing(EVENT.DO.MUTATE_SEQUENCE).emit(() => () => {
                return {
                    seq: CONSTANT.SEQ.AQS_SKIPPED
                };
            });
        });
        domain.incoming(EVENT.BROADCAST.AQS_LINK_ESTABLISHED).handle(() => {
            domain.outgoing(EVENT.DO.MONITOR_ACTIVATION).emit(() => true);
            domain.outgoing(EVENT.REQUEST.AQS_INFO).emit();
            domain.outgoing(EVENT.REQUEST.AQS_STATUS).delay(CONSTANT.AQS.STATUS_REQUEST_DELAY_MS).emit();
            domain.outgoing(EVENT.REQUEST.AQS_GENERAL_SETTING).delay(CONSTANT.AQS.GENERAL_SETTING_REQUEST_DELAY_MS).emit();
            domain.outgoing(EVENT.REQUEST.AQS_CALIBRATION_SETTING).delay(CONSTANT.AQS.CALIBRATION_SETTING_REQUEST_DELAY_MS).emit();
            domain.outgoing(EVENT.DO.MUTATE_SEQUENCE).emit(() => () => {
                return {
                    seq: CONSTANT.SEQ.AQS_LINKED
                };
            });
        });
        domain.incoming(EVENT.BROADCAST.AQS_LINK_LOST).handle(() => {
            domain.outgoing(
                EVENT.BROADCAST.MONITOR_RESET,
                EVENT.BROADCAST.MAP_RESET,
                EVENT.BROADCAST.TIMELINE_RESET,
                EVENT.BROADCAST.SETTING_RESET
            ).emit();
            domain.outgoing(EVENT.DO.MUTATE_SEQUENCE).emit(() => () => {
                return {
                    seq: CONSTANT.SEQ.AQS_LINKING
                };
            });
        });
        domain.incoming(EVENT.BROADCAST.AQS_SUBSCRIBED).handle(() => {
            domain.outgoing(EVENT.DO.MUTATE_SEQUENCE).emit(() => ({
                seq
            }) => {
                return {
                    seq: seq | CONSTANT.SEQ.AQS_MONITORING
                };
            });
        });
        domain.incoming(EVENT.BROADCAST.AQS_UNSUBSCRIBED).handle(() => {
            domain.outgoing(EVENT.DO.MUTATE_SEQUENCE).emit(() => ({
                seq
            }) => {
                return {
                    seq: seq ^ CONSTANT.SEQ.AQS_MONITORING
                };
            });
        });
        domain.incoming(EVENT.BROADCAST.AQS_ENTERING_SLEEP_STATE).handle(() => {
            domain.outgoing(EVENT.DO.MUTATE_SEQUENCE).emit(() => ({
                seq
            }) => {
                return {
                    seq: seq | CONSTANT.SEQ.AQS_ASLEEP
                };
            });
        });
        domain.incoming(EVENT.BROADCAST.AQS_EXITING_SLEEP_STATE).handle(() => {
            domain.outgoing(EVENT.DO.MUTATE_SEQUENCE).emit(() => ({
                seq
            }) => {
                return {
                    seq: seq ^ CONSTANT.SEQ.AQS_ASLEEP
                };
            });
        });
        domain.incoming(EVENT.AS.SEQUENCE_MUTATED).handle((seq) => {
            if ((seq & CONSTANT.SEQ.STARTUP) === CONSTANT.SEQ.STARTUP) {
                CodePush.sync({
                    updateDialog: false,
                    // updateDialog: {
                    //     updateTitle: `Virida Has An Update`,
                    //     optionalUpdateMessage: `New Update Available. Install?`,
                    //     optionalIgnoreButtonLabel: `Yes`,
                    //     optionalInstallButtonLabel: `No`
                    // },
                    checkFrequency: CodePush.CheckFrequency.ON_APP_RESUME,
                    installMode: CodePush.InstallMode.ON_NEXT_RESUME // codePush.InstallMode.ON_NEXT_SUSPEND
                }, (syncStatus) => {
                    switch (syncStatus) { // eslint-disable-line
                    case CodePush.SyncStatus.CHECKING_FOR_UPDATE:
                        Hf.log(`info1`, `Code push is checking for new update.`);
                        break;
                    case CodePush.SyncStatus.AWAITING_USER_ACTION:
                        Hf.log(`info1`, `Code push is waiting for user interaction.`);
                        break;
                    case CodePush.SyncStatus.DOWNLOADING_PACKAGE:
                        Hf.log(`info1`, `Code push is downloading new update.`);
                        break;
                    case CodePush.SyncStatus.INSTALLING_UPDATE:
                        Hf.log(`info1`, `Code push is installing new update.`);
                        break;
                    case CodePush.SyncStatus.UP_TO_DATE:
                        Hf.log(`info1`, `Code push shows app is uptodate.`);
                        break;
                    case CodePush.SyncStatus.UPDATE_IGNORED:
                        Hf.log(`info1`, `Code push update cancelled by user.`);
                        break;
                    case CodePush.SyncStatus.UPDATE_INSTALLED:
                        Hf.log(`info1`, `Code push update installed and will be applied on restart.`);
                        break;
                    case CodePush.SyncStatus.UNKNOWN_ERROR:
                        Hf.log(`info1`, `Code push received an unknown error status.`);
                        break;
                    }
                }, (downloadProgress) => {
                    if (Hf.isObject(downloadProgress)) {
                        Hf.log(`info1`, `Code push sync downloading ${downloadProgress.receivedBytes} of ${downloadProgress.totalBytes}.`);
                    }
                });
                BackgroundGeolocation.ready({
                    foregroundService: false,
                    forceReloadOnHeartbeat: false,
                    stopOnTerminate: true,
                    startOnBoot: false,
                    preventSuspend: false,
                    useSignificantChangesOnly: true,
                    disableMotionActivityUpdates: false,
                    debug: false, // Hf.DEVELOPMENT,
                    maxRecordsToPersist: 1,
                    distanceFilter: 10,
                    stopTimeout: 1,
                    // heartbeatInterval: CONSTANT.GENERAL.BACKGROUND_HEARTBEAT_INTERVAL_M * 60,
                    desiredAccuracy: BackgroundGeolocation.DESIRED_ACCURACY_VERY_LOW,
                    // notificationPriority: BackgroundGeolocation.NOTIFICATION_PRIORITY_MIN,
                    logLevel: BackgroundGeolocation.LOG_LEVEL_OFF // Hf.DEVELOPMENT ? BackgroundGeolocation.LOG_LEVEL_VERBOSE : BackgroundGeolocation.LOG_LEVEL_OFF
                }).then(({
                    enabled
                }) => {
                    if (enabled) {
                        BackgroundGeolocation.stop(() => {
                            Hf.log(`info1`, `Initially stop background geolocation tracking.`);
                        }, (error) => {
                            Hf.log(`warn1`, `ViridaApp - Unable to stop background geolocation tracking. ${error.message}`);
                        });
                    }
                    Hf.log(`info1`, `Background geolocation tracking ready.`);
                }).catch((error) => {
                    Hf.log(`warn1`, `ViridaApp - Unable to create background geolocation tracking. ${error.message}`);
                });
            }
            if ((seq & CONSTANT.SEQ.AQS_LINKING) === CONSTANT.SEQ.AQS_LINKING) {
                clearInterval(periodicRefreshIntervalId);
                periodicRefreshIntervalId = null;
                Hf.log(`info1`, `Stopping periodic refresh.`);
                BackgroundGeolocation.stop(() => {
                    Hf.log(`info1`, `Stopping background geolocation tracking.`);
                }, (error) => {
                    Hf.log(`warn1`, `AppDomain - Unable to stop background geolocation tracking. ${error.message}`);
                });
                AppState.removeEventListener(`change`);
                NetInfo.removeEventListener(`connectionChange`);
            }
            if ((seq & CONSTANT.SEQ.AQS_LINKED) === CONSTANT.SEQ.AQS_LINKED ||
                (seq & CONSTANT.SEQ.AQS_SKIPPED) === CONSTANT.SEQ.AQS_SKIPPED) {
                if ((seq & CONSTANT.SEQ.AQS_LINKED) === CONSTANT.SEQ.AQS_LINKED) {
                    if ((seq & CONSTANT.SEQ.AQS_MONITORING) === CONSTANT.SEQ.AQS_MONITORING && (seq & CONSTANT.SEQ.AQS_ASLEEP) !== CONSTANT.SEQ.AQS_ASLEEP) {
                        domain.outgoing(EVENT.BROADCAST.AQS_MONITORING_STARTED).emit();
                    } else {
                        domain.outgoing(EVENT.BROADCAST.AQS_MONITORING_ENDED).emit();
                    }
                }
                if (periodicRefreshIntervalId === null) {
                    periodicRefreshIntervalId = setInterval(() => {
                        domain.outgoing(EVENT.BROADCAST.PERIODIC_REFRESH).emit();
                    }, CONSTANT.GENERAL.PERIODIC_REFRESH_INTERVAL_MS);
                    Hf.log(`info1`, `Starting periodic refresh.`);
                } else {
                    clearInterval(periodicRefreshIntervalId);
                    periodicRefreshIntervalId = setInterval(() => {
                        domain.outgoing(EVENT.BROADCAST.PERIODIC_REFRESH).emit();
                    }, CONSTANT.GENERAL.PERIODIC_REFRESH_INTERVAL_MS);
                    Hf.log(`info1`, `Starting periodic refresh.`);
                }
                BackgroundGeolocation.start(() => {
                    Hf.log(`info1`, `Starting background geolocation tracking.`);
                }, (error) => {
                    Hf.log(`warn1`, `AppDomain - Unable to start background geolocation tracking. ${error.message}`);
                });
                AppState.addEventListener(`change`, (nextAppState) => {
                    let nextRunMode;
                    domain.outgoing(EVENT.DO.MUTATE_RUN_MODE).emit(() => ({
                        runMode
                    }) => {
                        if (runMode === `background-running` && nextAppState === `active`) {
                            domain.outgoing(EVENT.BROADCAST.RUN_MODE).emit(() => `foreground-running`);
                            nextRunMode = `foreground-running`;
                            Hf.log(`info1`, `Entering foreground run mode.`);
                        } else if (runMode === `foreground-running` && (nextAppState === `background` || nextAppState === `inactive `)) {
                            domain.outgoing(EVENT.BROADCAST.RUN_MODE).emit(() => `background-running`);
                            nextRunMode = `background-running`;
                            Hf.log(`info1`, `Entering background run mode.`);
                        } else {
                            nextRunMode = runMode;
                        }
                        return {
                            runMode: nextRunMode
                        };
                    });
                });
                NetInfo.addEventListener(`connectionChange`, (connectionInfo) => {
                    if (connectionInfo.type !== `none`) {
                        Hf.log(`info1`, `Device is online.`);
                    } else {
                        Hf.log(`info1`, `Device is offline.`);
                    }
                });
            }
        });
        domain.incoming(EVENT.RESPONSE.WITH.AQS_STATUS).forward(EVENT.BROADCAST.AQS_STATUS);
        domain.incoming(EVENT.RESPONSE.WITH.AQS_INFO).forward(EVENT.REQUEST.WRITE_AQS_INFO);
        domain.incoming(EVENT.RESPONSE.WITH.AQS_GENERAL_SETTING).forward(EVENT.REQUEST.WRITE_AQS_GENERAL_SETTING);
        domain.incoming(EVENT.RESPONSE.WITH.AQS_CALIBRATION_SETTING).forward(EVENT.REQUEST.WRITE_AQS_CALIBRATION_SETTING);

        domain.incoming(EVENT.RESPONSE.TO.WRITE_SETTING.OK).forward(EVENT.BROADCAST.SETTING_WRITTEN);
        domain.incoming(EVENT.RESPONSE.TO.WRITE_AQS_INFO.OK).forward(EVENT.BROADCAST.AQS_INFO_WRITTEN);
        domain.incoming(EVENT.RESPONSE.TO.WRITE_AQS_GENERAL_SETTING.OK).forward(EVENT.BROADCAST.AQS_GENERAL_SETTING_WRITTEN);
        domain.incoming(EVENT.RESPONSE.TO.WRITE_AQS_CALIBRATION_SETTING.OK).forward(EVENT.BROADCAST.AQS_CALIBRATION_SETTING_WRITTEN);
        domain.incoming(EVENT.BROADCAST.SETTING_WRITTEN).repeat();
        domain.incoming(EVENT.BROADCAST.AQS_INFO_WRITTEN).repeat();
        domain.incoming(EVENT.BROADCAST.AQS_GENERAL_SETTING_WRITTEN).repeat();
        domain.incoming(EVENT.BROADCAST.AQS_CALIBRATION_SETTING_WRITTEN).repeat();

        domain.incoming(
            EVENT.RESPONSE.TO.READ_SETTING.NOT_FOUND,
            EVENT.RESPONSE.TO.READ_SETTING.ERROR,
            EVENT.RESPONSE.TO.READ_AQS_INFO.NOT_FOUND,
            EVENT.RESPONSE.TO.READ_AQS_INFO.ERROR
        ).delay(CONSTANT.GENERAL.ALERT_MODAL_POPUP_DELAY_MS).handle(() => ({
            alert
        }) => {
            return !alert.visible ? {
                alert: {
                    visible: true,
                    title: `App Storage`,
                    message: `Unable To Read Data From Storage.`
                }
            } : alert;
        }).relay(EVENT.DO.MUTATE_ALERT);
        domain.incoming(
            EVENT.RESPONSE.TO.WRITE_SETTING.ERROR,
            EVENT.RESPONSE.TO.WRITE_AQS_INFO.ERROR,
            EVENT.RESPONSE.TO.WRITE_AQS_GENERAL_SETTING.ERROR,
            EVENT.RESPONSE.TO.WRITE_AQS_CALIBRATION_SETTING.ERROR
        ).delay(CONSTANT.GENERAL.ALERT_MODAL_POPUP_DELAY_MS).handle(() => ({
            alert
        }) => {
            return !alert.visible ? {
                alert: {
                    visible: true,
                    title: `App Storage`,
                    message: `Unable To Write Data To Storage.`
                }
            } : alert;
        }).relay(EVENT.DO.MUTATE_ALERT);
        domain.incoming(
            EVENT.BROADCAST.BLE_ALERT,
            EVENT.BROADCAST.MONITOR_ALERT,
            EVENT.BROADCAST.MAP_ALERT,
            EVENT.BROADCAST.TIMELINE_ALERT,
            EVENT.BROADCAST.SETTING_ALERT
        ).delay(CONSTANT.GENERAL.ALERT_MODAL_POPUP_DELAY_MS).handle((_alert) => ({
            alert
        }) => {
            return !alert.visible ? {
                alert: _alert
            } : alert;
        }).relay(EVENT.DO.MUTATE_ALERT);
        domain.incoming(EVENT.ON.CLOSE_ALERT_MODAL).handle(() => ({
            alert
        }) => {
            return alert.visible ? {
                visible: false,
                title: ``,
                message: ``
            } : alert;
        }).relay(EVENT.DO.MUTATE_ALERT);
        domain.incoming(
            EVENT.BROADCAST.AQS_SAMPLING_STARTED,
            EVENT.BROADCAST.AQS_SAMPLING_ENDED
        ).repeat();
        domain.incoming(EVENT.BROADCAST.AQS_DATA).handle(({
            timestamp,
            aqsCondition,
            aqsSamples
        }) => {
            domain.outgoing(EVENT.BROADCAST.AQS_CONDITION).emit(() => {
                return {
                    timestamp,
                    condition: aqsCondition
                };
            });
            domain.outgoing(EVENT.BROADCAST.AQS_SAMPLES).emit(() => {
                return {
                    timestamp,
                    aqSamples: aqsSamples
                };
            });
        });
        domain.incoming(EVENT.ON.GO_TO_MONITOR).handle(() => {
            domain.outgoing(
                EVENT.DO.MONITOR_ACTIVATION
            ).emit(() => true);
            domain.outgoing(
                EVENT.DO.MAP_ACTIVATION,
                EVENT.DO.TIMELINE_ACTIVATION,
                EVENT.DO.SETTING_ACTIVATION
            ).emit(() => false);
        });
        domain.incoming(EVENT.ON.GO_TO_MAP).handle(() => {
            domain.outgoing(EVENT.DO.MAP_ACTIVATION).emit(() => true);
            domain.outgoing(
                EVENT.DO.MONITOR_ACTIVATION,
                EVENT.DO.TIMELINE_ACTIVATION,
                EVENT.DO.SETTING_ACTIVATION
            ).emit(() => false);
        });
        domain.incoming(EVENT.ON.GO_TO_TIMELINE).handle(() => {
            domain.outgoing(EVENT.DO.TIMELINE_ACTIVATION).emit(() => true);
            domain.outgoing(
                EVENT.DO.MONITOR_ACTIVATION,
                EVENT.DO.MAP_ACTIVATION,
                EVENT.DO.SETTING_ACTIVATION
            ).emit(() => false);
        });
        domain.incoming(EVENT.ON.GO_TO_SETTING).handle(() => {
            domain.outgoing(EVENT.DO.SETTING_ACTIVATION).emit(() => true);
            domain.outgoing(
                EVENT.DO.MONITOR_ACTIVATION,
                EVENT.DO.MAP_ACTIVATION,
                EVENT.DO.TIMELINE_ACTIVATION
            ).emit(() => false);
        });
        done();
    },
    teardown: function teardown (done) {
        clearInterval(periodicRefreshIntervalId);
        periodicRefreshIntervalId = null;
        Hf.log(`info1`, `Stopping periodic refresh.`);
        BackgroundGeolocation.stop(() => {
            Hf.log(`info1`, `Stopping background geolocation tracking.`);
        }, (error) => {
            Hf.log(`warn1`, `AppDomain - Unable to stop background geolocation tracking. ${error.message}`);
        });
        BackgroundGeolocation.removeListeners();
        AppState.removeEventListener(`change`);
        NetInfo.removeEventListener(`connectionChange`);
        done();
    }
});
export default AppDomain;
