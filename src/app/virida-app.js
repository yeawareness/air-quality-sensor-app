/**
 * Copyright (c) 2017-present, Virida YEAwareness, Org. All rights reserved.
 *
 *------------------------------------------------------------------------
 *
 * @module Virida
 * @description -  Virida client-native app.
 *
 * @author Tuan Le (tuan.t.lei@gmail.com)
 *
 *------------------------------------------------------------------------
 * @flow
 */
'use strict'; // eslint-disable-line

import { Hf } from 'hyperflow';

import React from 'react';

import ReactNative from 'react-native'; // eslint-disable-line

import PropTypes from 'prop-types';

import CreateReactClass from 'create-react-class';

import CodePush from 'react-native-code-push';

import AppDomain from './domains/app-domain';

const ViridaApp = Hf.App.augment({
    composites: [
        Hf.React.AppComponentComposite
    ],
    $init: function $init () {
        const app = this;
        app.register({
            domain: AppDomain({
                name: app.name
            }),
            component: {
                lib: {
                    React,
                    ReactNative,
                    PropTypes,
                    CreateReactClass
                },
                renderer: ReactNative
            }
        });
    },
    renderToTarget: function renderToTarget () {
        const app = this;
        const AppComponent = CodePush()(app.getTopComponent());
        ReactNative.AppRegistry.registerComponent(app.name, () => AppComponent);
    }
})({
    name: `Virida`
});

export default ViridaApp;
