/**
 * Copyright (c) 2017-present, Virida YEAwareness, Org. All rights reserved.
 *
 *------------------------------------------------------------------------
 *
 * @module BLEDiscoveryModalInterface
 * @description - Virida client-native air quality sensor ble discovery modal interface.
 *
 * @author Tuan Le (tuan.t.lei@gmail.com)
 *
 *------------------------------------------------------------------------
 * @flow
 */
'use strict'; // eslint-disable-line

import { Hf } from 'hyperflow';

import { Ht } from 'hypertoxin';

import React from 'react';

import ReactNative from 'react-native'; // eslint-disable-line

import * as Animatable from 'react-native-animatable';

import CONSTANT from '../../common/constant';

import EVENT from '../events/ble-event';

const {
    Modal,
    FlatList
} = ReactNative;

const {
    BodyView,
    LayoutView,
    ItemView
} = Ht.View;

const {
    FlatButton,
    RaisedButton
} = Ht.Button;

const {
    IconImage
} = Ht.Image;

const {
    HeadlineText,
    TitleText
} = Ht.Text;

const AnimatedView = Animatable.View;

Animatable.initializeRegistryWithDefinitions({
    radarOn: {
        from: {
            opacity: 0,
            scale: 0.3
        },
        to: {
            opacity: 1,
            scale: 1
        }
    },
    radarOff: {
        from: {
            opacity: 1,
            scale: 1,
            width: 200,
            height: 200
        },
        to: {
            opacity: 0,
            scale: 0,
            width: 0,
            height: 0
        }
    },
    fadeIn: {
        from: {
            opacity: 0
        },
        to: {
            opacity: 1
        }
    },
    fadeOut: {
        from: {
            opacity: 1
        },
        to: {
            opacity: 0
        }
    }
});

const BLEDiscoveryModalInterface = Hf.Interface.augment({
    composites: [
        Hf.React.ComponentComposite
    ],
    state: {
        visible: {
            value: false
        }
    },
    setup: function setup (done) {
        done();
    },
    teardown: function teardown (done) {
        done();
    },
    onPressDiscoverAQSButton: function onPressDiscoverAQSButton () {
        const component = this;

        component.outgoing(EVENT.ON.AQS_DISCOVERY).emit();
    },
    onPressSkipAQSButton: function onPressSkipAQSButton () {
        const component = this;

        component.outgoing(EVENT.ON.AQS_SKIPPING).emit();
    },
    renderAQSDiscovering: function renderAQSDiscovering () {
        const component = this;
        const {
            bleConnectivityState
        } = component.state;
        return (
            <AnimatedView
                animation = { bleConnectivityState === CONSTANT.BLE.DISCONNECTED_STATE || bleConnectivityState === CONSTANT.BLE.DISCOVERING_STATE ? `fadeIn` : `fadeOut` }
                duration = { 300 }
                style = {{
                    justifyContent: `center`,
                    alignItems: `stretch`,
                    alignSelf: `stretch`,
                    marginTop: CONSTANT.GENERAL.DEVICE_HEIGHT * 0.75,
                    marginHorizontal: 6,
                    backgroundColor: `transparent`
                }}
                useNativeDriver = { false }
            >
                <RaisedButton
                    label = 'DISCOVER AIR QUALITY SENSOR'
                    busy = { bleConnectivityState === CONSTANT.BLE.DISCOVERING_STATE }
                    onPress = { component.onPressDiscoverAQSButton }
                />
                <RaisedButton
                    label = 'DONT HAVE AIR QUALITY SENSOR'
                    disabled = { bleConnectivityState === CONSTANT.BLE.DISCOVERING_STATE }
                    onPress = { component.onPressSkipAQSButton }
                />
            </AnimatedView>
        );
    },
    renderAQSDiscovered: function renderAQSDiscovered () {
        const component = this;
        const {
            bleConnectivityState,
            discoveredAQSInfos
        } = component.state;
        if (discoveredAQSInfos.length > 0) {
            return (
                <AnimatedView
                    animation = { bleConnectivityState === CONSTANT.BLE.DISCOVERED_STATE ? `fadeIn` : `fadeOut` }
                    duration = { 300 }
                    style = {{
                        justifyContent: `center`,
                        alignItems: `stretch`,
                        alignSelf: `stretch`,
                        marginTop: CONSTANT.GENERAL.DEVICE_HEIGHT * 0.75,
                        marginBottom: 72,
                        marginHorizontal: 6,
                        backgroundColor: `transparent`
                    }}
                    useNativeDriver = { false }
                >
                    <HeadlineText size = 'small' >{ `Discovered ${discoveredAQSInfos.length} Air Quality Sensors` }</HeadlineText>
                    <FlatList
                        data = { discoveredAQSInfos.map((discoveredAQSInfo, index) => {
                            return {
                                key: `${index}`,
                                ...discoveredAQSInfo
                            };
                        }) }
                        renderItem = {(listData) => {
                            const discoveredAQSInfo = listData.item;
                            return (
                                <ItemView
                                    style = {{
                                        width: CONSTANT.GENERAL.DEVICE_WIDTH
                                    }}
                                    onPress = {() => {
                                        component.outgoing(EVENT.ON.AQS_CONNECTION).emit(() => discoveredAQSInfo.mId);
                                    }}
                                >
                                    <LayoutView
                                        room = 'content-left'
                                        orientation = 'vertical'
                                        alignment = 'center'
                                    >
                                        <IconImage
                                            source = 'sensor'
                                            color = 'primary'
                                        />
                                        <TitleText size = 'small' >{ discoveredAQSInfo.alias }</TitleText>
                                    </LayoutView>
                                    {
                                        discoveredAQSInfos.length > 1 ? <FlatButton
                                            room = 'action-right'
                                            overlay = 'transparent'
                                            label = 'WHO AM I'
                                            onPress = {() => {
                                                component.outgoing(EVENT.ON.AQS_WHOAMI).emit(() => discoveredAQSInfo.mId);
                                            }}
                                        /> : <FlatButton
                                            room = 'action-right'
                                            overlay = 'transparent'
                                            label = 'REFRESH'
                                            onPress = { component.onPressDiscoverAQSButton }
                                        />
                                    }
                                </ItemView>
                            );
                        }}
                    />
                    {
                        discoveredAQSInfos.length > 1 ? <RaisedButton
                            label = 'REFRESH'
                            onPress = { component.onPressDiscoverAQSButton }
                        /> : null
                    }
                </AnimatedView>
            );
        } else {
            return null;
        }
    },
    render: function render () {
        const component = this;
        const {
            visible
        } = component.props;
        return (
            <Modal
                animationType = 'fade'
                // animationType = 'none'
                transparent = { true }
                visible = { visible }
            >
                <BodyView>
                    {
                        component.renderAQSDiscovering()
                    }
                    {
                        component.renderAQSDiscovered()
                    }
                </BodyView>
            </Modal>
        );
    }
});
export default BLEDiscoveryModalInterface;
