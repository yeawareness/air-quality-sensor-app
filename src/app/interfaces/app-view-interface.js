/**
 * Copyright (c) 2017-present, Virida YEAwareness, Org. All rights reserved.
 *
 *------------------------------------------------------------------------
 *
 * @module AppViewInterface
 * @description - Virida client-native app view interface.
 *
 * @author Tuan Le (tuan.t.lei@gmail.com)
 *
 *------------------------------------------------------------------------
 * @flow
 */
'use strict'; // eslint-disable-line

import { Hf } from 'hyperflow';

import { Ht } from 'hypertoxin';

import React from 'react';

import ReactNative from 'react-native'; // eslint-disable-line

import { createBottomTabNavigator } from 'react-navigation';

import AppIntroViewInterface from './app-intro-view-interface';

// import MovingWave from '../../common/components/moving-wave-component';

import CONSTANT from '../../common/constant';

import EVENT from '../events/app-event';

const {
    Alert,
    // Image,
    StatusBar
    // Text
} = ReactNative;

const {
    ScreenView
    // LayoutView
} = Ht.View;

const {
    IconImage
    // WallpaperImage
} = Ht.Image;

const AppTabNavigator = createBottomTabNavigator({
    monitor: {
        screen: (props) => {
            const {
                screenProps
            } = props;
            const {
                component
            } = screenProps;
            const [ MonitorView ] = component.getComponentComposites(`monitor-view`);
            return (
                <MonitorView { ...props }/>
            );
        },
        navigationOptions: () => {
            return {
                tabBarVisible: true,
                tabBarLabel: `MONITOR`,
                tabBarIcon: (style) => (
                    <IconImage
                        source = 'monitor'
                        size = 'large'
                        color = { style.tintColor }
                    />
                )
            };
        }
    },
    map: {
        screen: (props) => {
            const {
                screenProps
            } = props;
            const {
                component
            } = screenProps;
            const [ MapView ] = component.getComponentComposites(`map-view`);
            return (
                <MapView { ...props }/>
            );
        },
        navigationOptions: () => {
            return {
                tabBarVisible: true,
                tabBarLabel: `MAPS`,
                tabBarIcon: (style) => (
                    <IconImage
                        source = 'map'
                        size = 'large'
                        color = { style.tintColor }
                    />
                )
            };
        }
    },
    timeline: {
        screen: (props) => {
            const {
                screenProps
            } = props;
            const {
                component
            } = screenProps;
            const [ TimelineView ] = component.getComponentComposites(`timeline-view`);
            return (
                <TimelineView { ...props }/>
            );
        },
        navigationOptions: () => {
            return {
                tabBarVisible: true,
                tabBarLabel: `TIMELINE`,
                tabBarIcon: (style) => (
                    <IconImage
                        source = 'timeline'
                        size = 'large'
                        color = { style.tintColor }
                    />
                )
            };
        }
    },
    setting: {
        screen: (props) => {
            const {
                screenProps
            } = props;
            const {
                component
            } = screenProps;
            const [ SettingView ] = component.getComponentComposites(`setting-view`);
            return (
                <SettingView { ...props }/>
            );
        },
        navigationOptions: (props) => {
            const {
                navigation
            } = props;
            const routeParams = navigation.state.params;

            return {
                tabBarVisible: Hf.isObject(routeParams) ? routeParams.tabBarVisible : true,
                tabBarLabel: `SETTINGS`,
                tabBarIcon: (style) => (
                    <IconImage
                        source = 'setting'
                        size = 'large'
                        color = { style.tintColor }
                    />
                )
            };
        }
    }
}, {
    swipeEnabled: false,
    animationEnabled: false,
    lazy: true,
    removeClippedSubviews: true,
    initialRouteName: `monitor`,
    transitionConfig: () => ({
        transitionSpec: {
            duration: 300
        }
    }),
    tabBarOptions: {
        scrollEnabled: false,
        showIcon: true,
        activeTintColor: Ht.Theme.general.color.light.secondary,
        inactiveTintColor: Ht.Theme.general.color.light.primary,
        activeBackgroundColor: `transparent`,
        inactiveBackgroundColor: `transparent`,
        labelStyle: {
            ...Ht.Theme.font.boldSmaller
        },
        tabStyle: {
            justifyContent: `center`,
            alignItems: `center`
        },
        style: {
            zIndex: 10,
            position: `absolute`,
            left: 0,
            right: 0,
            bottom: 0,
            backgroundColor: Ht.Theme.palette.white
        }
    }
});

const AppAQSSkippedTabNavigator = createBottomTabNavigator({
    monitor: {
        screen: (props) => {
            const {
                screenProps
            } = props;
            const {
                component
            } = screenProps;
            const [ MonitorView ] = component.getComponentComposites(`monitor-view`);
            return (
                <MonitorView { ...props }/>
            );
        },
        navigationOptions: () => {
            return {
                tabBarVisible: true,
                tabBarLabel: `MONITOR`,
                tabBarIcon: (style) => (
                    <IconImage
                        source = 'monitor'
                        size = 'large'
                        color = { style.tintColor }
                    />
                )
            };
        }
    },
    map: {
        screen: (props) => {
            const {
                screenProps
            } = props;
            const {
                component
            } = screenProps;
            const [ MapView ] = component.getComponentComposites(`map-view`);
            return (
                <MapView { ...props }/>
            );
        },
        navigationOptions: () => {
            return {
                tabBarVisible: true,
                tabBarLabel: `MAPS`,
                tabBarIcon: (style) => (
                    <IconImage
                        source = 'map'
                        size = 'large'
                        color = { style.tintColor }
                    />
                )
            };
        }
    },
    setting: {
        screen: (props) => {
            const {
                screenProps
            } = props;
            const {
                component
            } = screenProps;
            const [ SettingView ] = component.getComponentComposites(`setting-view`);
            return (
                <SettingView { ...props }/>
            );
        },
        navigationOptions: (props) => {
            const {
                navigation
            } = props;
            const routeParams = navigation.state.params;
            return {
                tabBarVisible: Hf.isObject(routeParams) ? routeParams.tabBarVisible : true,
                tabBarLabel: `SETTINGS`,
                tabBarIcon: (style) => (
                    <IconImage
                        source = 'setting'
                        size = 'large'
                        color = { style.tintColor }
                    />
                )
            };
        }
    }
}, {
    swipeEnabled: false,
    animationEnabled: false,
    lazy: true,
    removeClippedSubviews: true,
    initialRouteName: `monitor`,
    transitionConfig: () => ({
        transitionSpec: {
            duration: 300
        }
    }),
    tabBarOptions: {
        scrollEnabled: false,
        showIcon: true,
        activeTintColor: Ht.Theme.general.color.light.secondary,
        inactiveTintColor: Ht.Theme.general.color.light.primary,
        activeBackgroundColor: `transparent`,
        inactiveBackgroundColor: `transparent`,
        labelStyle: {
            ...Ht.Theme.font.boldSmaller
        },
        tabStyle: {
            justifyContent: `center`,
            alignItems: `center`
        },
        style: {
            zIndex: 10,
            position: `absolute`,
            left: 0,
            right: 0,
            bottom: 0,
            backgroundColor: Ht.Theme.palette.white
        }
    }
});

const AppViewInterface = Hf.Interface.augment({
    composites: [
        Hf.React.ComponentComposite
    ],
    $init: function $init () {
        const intf = this;
        intf.composedOf(
            AppIntroViewInterface({
                name: `app-intro-view`
            })
        );
    },
    setup: function setup (done) {
        const intf = this;
        intf.incoming(
            EVENT.ON.INTRODUCTION_SKIPPED,
            EVENT.ON.INTRODUCTION_FINISHED
        ).repeat();
        done();
    },
    teardown: function teardown (done) {
        done();
    },
    renderAlertModal: function renderAlertModal (title, message) {
        const component = this;
        return Alert.alert(
            title,
            message,
            [{
                text: `CLOSE`,
                onPress: () => component.outgoing(EVENT.ON.CLOSE_ALERT_MODAL).emit()
            }], {
                cancelable: false
            }
        );
    },
    render: function render () {
        const component = this;
        const [
            AppIntroView
            // BLEDiscoveryModal
        ] = component.getComponentComposites(
            `app-intro-view`
            // `ble-discovery-modal`
        );
        const {
            alert,
            seq
        } = component.state;
        if ((seq & CONSTANT.SEQ.INTRO) === CONSTANT.SEQ.INTRO) {
            return (
                <AppIntroView/>
            );
        } else if ((seq & CONSTANT.SEQ.AQS_LINKING) === CONSTANT.SEQ.AQS_LINKING) {
            // return (
            //     <ScreenView style = {{
            //         backgroundColor: Ht.Theme.general.color.light.primary
            //     }}>
            //         <StatusBar
            //             barStyle = 'dark-content'
            //             networkActivityIndicatorVisible = { false }
            //         />
            //         <WallpaperImage source = { Ht.Theme.imageSource.backgroundGradient }>
            //             <LayoutView
            //                 style = {{
            //                     marginTop: CONSTANT.GENERAL.DEVICE_HEIGHT * 0.18,
            //                     marginLeft: 21
            //                 }}
            //                 orientation = 'vertical'
            //                 alignment = 'center'
            //                 selfAlignment = 'start'
            //             >
            //                 <Text
            //                     style = {{
            //                         flexWrap: `wrap`,
            //                         fontFamily: `HelveticaNeue-Thin`,
            //                         fontSize: 55,
            //                         fontWeight: `200`,
            //                         color: Ht.Theme.palette.lightBlue,
            //                         backgroundColor: `transparent`
            //                     }}
            //                 > Virida </Text>
            //                 <Image
            //                     style = {{
            //                         width: 48,
            //                         height: 48,
            //                         marginLeft: 21
            //                     }}
            //                     resizeMode = 'contain'
            //                     source = { Ht.Theme.imageSource.yeaLogo }
            //                 />
            //             </LayoutView>
            //             <MovingWave
            //                 waves = {[{
            //                     color: Ht.Theme.palette.cyan,
            //                     lineThickness: 3,
            //                     amplitude: 40,
            //                     phase: 45,
            //                     verticalOffset: 5
            //                 }, {
            //                     color: Ht.Theme.palette.teal,
            //                     lineThickness: 1,
            //                     amplitude: 60,
            //                     phase: 90,
            //                     verticalOffset: 15
            //                 }, {
            //                     color: Ht.Theme.palette.deepBlue,
            //                     lineThickness: 9,
            //                     amplitude: 80,
            //                     phase: 120,
            //                     verticalOffset: 20
            //                 }]}
            //             />
            //             <BLEDiscoveryModal visible = { (seq & CONSTANT.SEQ.AQS_LINKING) === CONSTANT.SEQ.AQS_LINKING }/>
            //             {
            //                 !alert.visible ? null : component.renderAlertModal(alert.title, alert.message)
            //             }
            //         </WallpaperImage>
            //     </ScreenView>
            // );
            return null;
        } else if ((seq & CONSTANT.SEQ.AQS_SKIPPED) === CONSTANT.SEQ.AQS_SKIPPED) {
            return (
                <ScreenView style = {{
                    backgroundColor: Ht.Theme.general.color.light.primary
                }}>
                    <StatusBar
                        barStyle = 'dark-content'
                        networkActivityIndicatorVisible = { false }
                    />
                    <AppAQSSkippedTabNavigator
                        screenProps = {{
                            component
                        }}
                        onNavigationStateChange = {(prevState, currentState) => {
                            if (Hf.isObject(currentState)) {
                                const route = currentState.routes[currentState.index];
                                switch (route.routeName) {
                                case `monitor`:
                                    component.outgoing(EVENT.ON.GO_TO_MONITOR).emit();
                                    Hf.log(`info1`, `Go to monitor tab.`);
                                    break;
                                case `map`:
                                    component.outgoing(EVENT.ON.GO_TO_MAP).emit();
                                    Hf.log(`info1`, `Go to map tab.`);
                                    break;
                                case `setting`:
                                    component.outgoing(EVENT.ON.GO_TO_SETTING).emit();
                                    Hf.log(`info1`, `Go to setting tab.`);
                                    break;
                                default:
                                    break;
                                }
                            }
                            return null;
                        }}
                    />
                    {
                        !alert.visible ? null : component.renderAlertModal(alert.title, alert.message)
                    }
                </ScreenView>
            );
        } else if ((seq & CONSTANT.SEQ.AQS_LINKED) === CONSTANT.SEQ.AQS_LINKED) {
            return (
                <ScreenView style = {{
                    backgroundColor: Ht.Theme.general.color.light.primary
                }}>
                    <StatusBar
                        barStyle = 'dark-content'
                        networkActivityIndicatorVisible = { false }
                    />
                    <AppTabNavigator
                        screenProps = {{
                            component
                        }}
                        onNavigationStateChange = {(prevState, currentState) => {
                            if (Hf.isObject(currentState)) {
                                const route = currentState.routes[currentState.index];
                                switch (route.routeName) {
                                case `monitor`:
                                    component.outgoing(EVENT.ON.GO_TO_MONITOR).emit();
                                    Hf.log(`info1`, `Go to monitor tab.`);
                                    break;
                                case `map`:
                                    component.outgoing(EVENT.ON.GO_TO_MAP).emit();
                                    Hf.log(`info1`, `Go to map tab.`);
                                    break;
                                case `timeline`:
                                    component.outgoing(EVENT.ON.GO_TO_TIMELINE).emit();
                                    Hf.log(`info1`, `Go to timeline tab.`);
                                    break;
                                case `setting`:
                                    component.outgoing(EVENT.ON.GO_TO_SETTING).emit();
                                    Hf.log(`info1`, `Go to setting tab.`);
                                    break;
                                default:
                                    break;
                                }
                            }
                            return null;
                        }}
                    />
                    {
                        !alert.visible ? null : component.renderAlertModal(alert.title, alert.message)
                    }
                </ScreenView>
            );
        } else {
            return (
                <ScreenView style = {{
                    backgroundColor: Ht.Theme.general.color.light.primary
                }}>
                    <StatusBar
                        barStyle = 'dark-content'
                        networkActivityIndicatorVisible = { false }
                    />
                </ScreenView>
            );
        }
    }
});
export default AppViewInterface;
