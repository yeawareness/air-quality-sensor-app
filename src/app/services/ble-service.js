/**
 * Copyright (c) 2017-present, Virida YEAwareness, Org. All rights reserved.
 *
 *------------------------------------------------------------------------
 *
 * @module BLEService
 * @description - Virida client-native app ble service.
 *
 * @author Tuan Le (tuan.t.lei@gmail.com)
 *
 *------------------------------------------------------------------------
 * @flow
 */
'use strict'; // eslint-disable-line

import { Hf } from 'hyperflow';

import { Buffer } from 'buffer';

import { BleManager } from 'react-native-ble-plx';

import CONSTANT from '../../common/constant';

import EVENT from '../events/ble-event';

let bleManager = null;

const BLEService = Hf.Service.augment({
    composites: [
        Hf.State.MutationComposite
    ],
    state: {
        connectedMId: {
            value: ``
        }
    },
    $init: function $init () {
        bleManager = new BleManager({
            // restoreStateIdentifier: ``,
            // restoreStateFunction: null
        });
    },
    setup: function setup (done) {
        const service = this;
        if (bleManager !== null) {
            service.incoming(EVENT.REQUEST.BLE_DISCOVERY).handle(() => {
                const subscription = bleManager.onStateChange((state) => {
                    switch (state) {
                    case `PoweredOn`:
                        let discoveredAQSInfos = [];
                        const timeout = setTimeout(() => {
                            clearTimeout(timeout);
                            if (!Hf.isEmpty(discoveredAQSInfos)) {
                                service.outgoing(EVENT.RESPONSE.TO.BLE_DISCOVERY.OK).emit(() => discoveredAQSInfos);
                                Hf.log(`info1`, `Air quality sensors discovered.`);
                            } else {
                                service.outgoing(EVENT.RESPONSE.TO.BLE_DISCOVERY.NOT_FOUND).emit();
                                Hf.log(`info1`, `Air quality sensor not found.`);
                            }
                            bleManager.stopDeviceScan();
                        }, CONSTANT.BLE.DISCOVERY_TIMEOUT_MS);
                        service.outgoing(EVENT.BROADCAST.BLE_ENABLED).emit(() => true);
                        bleManager.startDeviceScan(null, {
                            allowDuplicates: true
                        }, (error, discoverdDevice) => {
                            if (Hf.isObject(error)) {
                                service.outgoing(EVENT.RESPONSE.TO.BLE_DISCOVERY.ERROR).emit();
                                Hf.log(`warn1`, `BLEService - Air quality sensors discovery error. ${error.message}`);
                            } else if (Hf.isString(discoverdDevice.name) &&
                                       discoverdDevice.name.substring(0, CONSTANT.AQS.BLE.ALIAS.length) === CONSTANT.AQS.BLE.ALIAS &&
                                       !discoveredAQSInfos.some((discoveredAQS) => discoveredAQS.mId === discoverdDevice.id)) {
                                discoveredAQSInfos.push({
                                    mId: discoverdDevice.id,
                                    alias: discoverdDevice.name
                                });
                            }
                        });
                        subscription.remove();
                        break;
                    case `PoweredOff`:
                        service.outgoing(EVENT.BROADCAST.BLE_ENABLED).emit(() => false);
                        service.outgoing(EVENT.RESPONSE.TO.BLE_DISCOVERY.NOT_AVAILABLE).emit();
                        subscription.remove();
                        Hf.log(`warn1`, `BLEService - Air quality sensors discovery error. Bluetooth is not on.`);
                        break;
                    case `Resetting`:
                        service.outgoing(EVENT.BROADCAST.BLE_CONNECTION_RESET).emit();
                        subscription.remove();
                        Hf.log(`warn1`, `BLEService - Air quality sensors discovery reset. Bluetooth is resetting.`);
                        break;
                    default:
                        service.outgoing(EVENT.RESPONSE.TO.BLE_DISCOVERY.ERROR).emit();
                        Hf.log(`warn1`, `BLEService - Air quality sensors discovery error. Service is unsupported.`);
                        break;
                    }
                }, true);
            });
            // service.incoming(EVENT.REQUEST.BLE_DISCOVERY_AND_CONNECTION).handle((mId) => {
            // });
            service.incoming(EVENT.REQUEST.BLE_CONNECTION).handle((mId) => {
                const timeout = setTimeout(() => {
                    clearTimeout(timeout);
                    service.outgoing(EVENT.RESPONSE.TO.BLE_CONNECTION.TIMED_OUT).emit();
                    Hf.log(`warn1`, `BLEService - Air quality sensors connection timeout.`);
                }, CONSTANT.BLE.CONNECTION_TIMEOUT_MS);
                bleManager.isDeviceConnected(mId).then((connected) => {
                    if (connected) {
                        clearTimeout(timeout);
                        service.outgoing(EVENT.RESPONSE.TO.BLE_CONNECTION.OK).emit();
                        Hf.log(`info1`, `Connection re-established to air quality sensor.`);
                    } else {
                        bleManager.connectToDevice(mId).then((connectingAQS) => {
                            const subscription = connectingAQS.onDisconnected(() => {
                                service.outgoing(EVENT.BROADCAST.BLE_CONNECTION_LOST).emit();
                                subscription.remove();
                                service.reduce({
                                    connectedMId: ``
                                });
                                Hf.log(`warn1`, `BLEService - Lost connection to air quality sensor.`);
                            });
                            return connectingAQS.discoverAllServicesAndCharacteristics();
                        }).then((connectedAQS) => {
                            return connectedAQS.services();
                        }).then((aqsServices) => {
                            const [
                                aqsService
                            ] = aqsServices.filter((_aqsService) => _aqsService.uuid === CONSTANT.AQS.BLE.SERVICE_ID);
                            if (Hf.isDefined(aqsService)) {
                                return aqsService.characteristics();
                            }
                        }).then((aqsCharacteristics) => {
                            if (Hf.isNonEmptyArray(aqsCharacteristics)) {
                                if (aqsCharacteristics.map((aqsCharacteristic) => {
                                    return aqsCharacteristic.uuid;
                                }).includes(CONSTANT.AQS.BLE.TX_CHARACTERISTIC_ID, CONSTANT.AQS.BLE.RX_CHARACTERISTIC_ID)) {
                                    clearTimeout(timeout);
                                    service.reduce({
                                        connectedMId: mId
                                    });
                                    service.outgoing(EVENT.RESPONSE.TO.BLE_CONNECTION.OK).emit();
                                    Hf.log(`info1`, `Connection established to air quality sensor.`);
                                }
                            } else {
                                clearTimeout(timeout);
                                service.outgoing(EVENT.RESPONSE.TO.BLE_CONNECTION.ERROR).emit();
                                Hf.log(`warn1`, `BLEService - Air quality sensor connection error.`);
                            }
                        }).catch(() => {
                            clearTimeout(timeout);
                            service.outgoing(EVENT.RESPONSE.TO.BLE_CONNECTION.ERROR).emit();
                            Hf.log(`warn1`, `BLEService - Air quality sensor connection error.`);
                        });
                    }
                });
            });

            service.incoming(EVENT.BROADCAST.BLE_CONNECTION_ESTABLISHED).handle(() => {
                let expectedRxPayloadContentByteCount = 0;
                const subscription = bleManager.monitorCharacteristicForDevice(
                    service.connectedMId,
                    CONSTANT.AQS.BLE.SERVICE_ID,
                    CONSTANT.AQS.BLE.RX_CHARACTERISTIC_ID,
                    (error, characteristic) => {
                        if (Hf.isObject(error)) {
                            service.outgoing(EVENT.BROADCAST.BLE_CONNECTION_LOST).emit();
                            service.reduce({
                                connectedMId: ``
                            });
                            Hf.log(`warn1`, `BLEService - Lost connection to air quality sensor. ${error.message}`);
                        } else {
                            const rxPayloadBuff = Buffer.from(characteristic.value, `base64`);

                            if (rxPayloadBuff.byteLength === 2) {
                                expectedRxPayloadContentByteCount = rxPayloadBuff.readUInt8(1);
                                if (expectedRxPayloadContentByteCount === 0) {
                                    service.outgoing(EVENT.BROADCAST.BLE_PAYLOAD_RECEIVED).emit(() => {
                                        return {
                                            mId: service.connectedMId,
                                            hdr: rxPayloadBuff.readUInt8(0),
                                            contentBuff: null
                                        };
                                    });
                                }
                            }
                            if (rxPayloadBuff.byteLength === expectedRxPayloadContentByteCount) {
                                service.outgoing(EVENT.BROADCAST.BLE_PAYLOAD_RECEIVED).emit(() => {
                                    return {
                                        mId: service.connectedMId,
                                        hdr: rxPayloadBuff.readUInt8(0),
                                        contentBuff: rxPayloadBuff
                                    };
                                });
                                expectedRxPayloadContentByteCount = 0;
                            }
                            Hf.log(`info1`, `Received air quality sensor payload:0x${rxPayloadBuff.toString(`hex`).match(/.{2}/g).join(` 0x`)}.`);
                        }
                    }
                );
                service.incoming(EVENT.REQUEST.BLE_PAYLOAD_TRANSMIT).handle(({
                    hdr,
                    contentBuff
                }) => {
                    const txPayloadBuff = new Buffer([ hdr, contentBuff ]);
                    if (subscription) {
                        bleManager.writeCharacteristicWithoutResponseForDevice(
                            service.connectedMId,
                            CONSTANT.AQS.BLE.SERVICE_ID,
                            CONSTANT.AQS.BLE.TX_CHARACTERISTIC_ID,
                            txPayloadBuff.toString(`base64`)
                        ).then(() => {
                            service.outgoing(EVENT.RESPONSE.TO.BLE_PAYLOAD_TRANSMIT.OK).emit();
                            Hf.log(`info1`, `Transmitted payload to air quality sensor for payload with hdr ${hdr}.`);
                        }).catch(() => {
                            service.outgoing(EVENT.RESPONSE.TO.BLE_PAYLOAD_TRANSMIT.ERROR).emit();
                            Hf.log(`warn1`, `BLEService - Air quality sensor transiting error for payload with hdr ${hdr}`);
                        });
                    } else {
                        service.outgoing(EVENT.RESPONSE.TO.BLE_PAYLOAD_TRANSMIT.NOT_AVAILABLE).emit();
                        Hf.log(`warn1`, `BLEService - Air quality sensor payload transition not available for payload with hdr ${hdr}.`);
                    }
                });
                service.incoming(EVENT.REQUEST.BLE_DISCONNECTION).handle(() => {
                    bleManager.cancelDeviceConnection(service.connectedMId).then(() => {
                        service.outgoing(EVENT.RESPONSE.TO.BLE_DISCONNECTION.OK).emit();
                        service.reduce({
                            connectedMId: ``
                        });
                        subscription.remove();
                        Hf.log(`info1`, `Disconnecting from  air quality sensor.`);
                    }).catch(() => {
                        service.outgoing(EVENT.RESPONSE.TO.BLE_DISCONNECTION.ERROR).emit();
                        Hf.log(`warn1`, `BLEService - Air quality sensor disconnection error.`);
                    });
                });
            });
        } else {
            Hf.log(`error`, `BLEService - Unable to create BLE manager instance.`);
        }

        done();
    },
    teardown: function teardown (done) {
        const service = this;
        if (bleManager !== null) {
            bleManager.isDeviceConnected(service.connectedMId).then((connected) => {
                if (connected) {
                    bleManager.cancelDeviceConnection(service.connectedMId).then(() => {
                        bleManager = null;
                        done();
                    });
                } else {
                    bleManager = null;
                    done();
                }
            });
        }
    }
});
export default BLEService;
