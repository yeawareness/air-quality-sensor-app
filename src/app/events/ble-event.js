/**
 * Copyright (c) 2017-present, Virida YEAwareness, Org. All rights reserved.
 *
 *------------------------------------------------------------------------
 *
 * @description - Virida client native air quality sensor ble event ids.
 *
 * @author Tuan Le (tuan.t.lei@gmail.com)
 *
 *------------------------------------------------------------------------
 * @flow
 */
'use strict'; // eslint-disable-line

import { Hf } from 'hyperflow';

export default Hf.Event.create({
    onEvents: [
        `aqs-discovery`,
        `aqs-skipping`,
        `aqs-whoami`,
        `aqs-connection`
    ],
    doEvents: [
        `ble-reset`,
        `mutate-ble-connectivity-state`,
        `mutate-discovered-aqs-infos`
    ],
    broadcastEvents: [
        `ble-alert`,
        `ble-enabled`,
        `ble-error`,
        `ble-connection-lost`,
        `ble-connection-reset`,
        `ble-connection-established`,
        `ble-payload-received`,

        `aqs-skipped`,
        `aqs-link-lost`,
        `aqs-link-established`,
        `aqs-subscribed`,
        `aqs-unsubscribed`,
        `aqs-entering-sleep-state`,
        `aqs-exiting-sleep-state`,

        `aqs-sampling-started`,
        `aqs-sampling-ended`,

        `aqs-data`
    ],
    requestEvents: [
        `ble-discovery`,
        `ble-connection`,
        `ble-discovery-and-connection`,
        `ble-disconnection`,
        `ble-payload-transmit`,

        `aqs-discover-and-connect`,

        `aqs-info`,
        `aqs-status`,
        `aqs-general-setting`,
        `aqs-calibration-setting`
    ]
});
