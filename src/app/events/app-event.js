/**
 * Copyright (c) 2017-present, Virida YEAwareness, Org. All rights reserved.
 *
 *------------------------------------------------------------------------
 *
 * @description - Virida client native app event ids.
 *
 * @author Tuan Le (tuan.t.lei@gmail.com)
 *
 *------------------------------------------------------------------------
 * @flow
 */
'use strict'; // eslint-disable-line

import { Hf } from 'hyperflow';

export default Hf.Event.create({
    onEvents: [
        `go-to-monitor`,
        `go-to-map`,
        `go-to-timeline`,
        `go-to-setting`,
        `close-alert-modal`,
        `introduction-finished`,
        `introduction-skipped`
    ],
    asEvents: [
        `reset`,
        `storage-initialized`,
        `sequence-mutated`
    ],
    doEvents: [
        `reset`,
        `ble-reset`,
        `monitor-reset`,
        `map-reset`,
        `timeline-reset`,
        `setting-reset`,
        `storage-initialization`,
        `monitor-activation`,
        `map-activation`,
        `timeline-activation`,
        `setting-activation`,
        `mutate-alert`,
        `mutate-sequence`,
        `mutate-run-mode`,
        `mutate-tab-bar-visibility`,
        `rediscover-aqs`
    ],
    broadcastEvents: [
        `aqs-skipped`,
        `aqs-link-lost`,
        `aqs-link-established`,
        `aqs-entering-sleep-state`,
        `aqs-exiting-sleep-state`,
        `aqs-subscribed`,
        `aqs-unsubscribed`,
        `aqs-monitoring-started`,
        `aqs-monitoring-ended`,
        `aqs-sampling-started`,
        `aqs-sampling-ended`,
        `aqs-status`,
        `aqs-data`,
        `aqs-samples`,

        `run-mode`,
        `periodic-refresh`,

        `setting-written`,
        `aqs-info-written`,
        `aqs-general-setting-written`,
        `aqs-calibration-setting-written`,

        `ble-alert`,
        `monitor-alert`,
        `map-alert`,
        `timeline-alert`,
        `setting-alert`
    ],
    requestEvents: [
        `aqs-discover-and-connect`,
        `aqs-info`,
        `aqs-status`,
        `aqs-general-setting`,
        `aqs-calibration-setting`,

        `read-setting`,
        `write-setting`,
        `read-aqs-info`,
        `write-aqs-info`,
        `read-aqs-general-setting`,
        `write-aqs-general-setting`,
        `read-aqs-calibration-setting`,
        `write-aqs-calibration-setting`
    ]
});
