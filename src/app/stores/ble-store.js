/**
 * Copyright (c) 2017-present, Virida YEAwareness, Org. All rights reserved.
 *
 *------------------------------------------------------------------------
 *
 * @module BLEStore
 * @description - Virida client-native air quality sensor ble store.
 *
 * @author Tuan Le (tuan.t.lei@gmail.com)
 *
 *------------------------------------------------------------------------
 * @flow
 */
'use strict'; // eslint-disable-line

import { Hf } from 'hyperflow';

import CONSTANT from '../../common/constant';

import EVENT from '../events/ble-event';

const BLEStore = Hf.Store.augment({
    state: {
        bleConnectivityState: {
            value: CONSTANT.BLE.DISCONNECTED_STATE,
            oneOf: [
                CONSTANT.BLE.DISCONNECTING_STATE,
                CONSTANT.BLE.DISCONNECTED_STATE,
                CONSTANT.BLE.DISCOVERING_STATE,
                CONSTANT.BLE.DISCOVERED_STATE,
                CONSTANT.BLE.CONNECTING_STATE,
                CONSTANT.BLE.CONNECTED_STATE
            ]
        },
        discoveredAQSInfos: {
            value: []
        }
    },
    setup: function setup (done) {
        const store = this;
        store.incoming(EVENT.DO.BLE_RESET).handle(() => {
            store.reconfig({
                bleConnectivityState: CONSTANT.BLE.DISCONNECTED_STATE,
                discoveredAQSInfos: []
            }, {
                suppressMutationEvent: true
            });
        });
        store.incoming(EVENT.DO.MUTATE_BLE_CONNECTIVITY_STATE).handle((bleConnectivityState) => {
            store.reduce({
                bleConnectivityState
            }, {
                forceMutationEvent: true
                // TODO: fix update of bleConnectivityState, only update if forceMutationEvent = true
            });
        });
        store.incoming(EVENT.DO.MUTATE_DISCOVERED_AQS_INFOS).handle((discoveredAQSInfos) => {
            store.reconfig({
                discoveredAQSInfos
            });
        });
        done();
    },
    teardown: function teardown (done) {
        done();
    }
});
export default BLEStore;
