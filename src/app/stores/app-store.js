/**
 * Copyright (c) 2017-present, Virida YEAwareness, Org. All rights reserved.
 *
 *------------------------------------------------------------------------
 *
 * @module AppStore
 * @description - Virida client-native app store.
 *
 * @author Tuan Le (tuan.t.lei@gmail.com)
 *
 *------------------------------------------------------------------------
 * @flow
 */
'use strict'; // eslint-disable-line

import { Hf } from 'hyperflow';

import CONSTANT from '../../common/constant';

import EVENT from '../events/app-event';

const AppStore = Hf.Store.augment({
    state: {
        alert: {
            value: {
                visible: false,
                title: ``,
                message: ``
            }
        },
        runMode: {
            value: `foreground-running`,
            oneOf: [
                `foreground-running`,
                `background-running`
            ]
        },
        seq: {
            value: CONSTANT.SEQ.STARTUP,
            oneOf: [
                CONSTANT.SEQ.STARTUP,
                CONSTANT.SEQ.INTRO,
                CONSTANT.SEQ.AQS_SKIPPED,
                CONSTANT.SEQ.AQS_LINKING,
                CONSTANT.SEQ.AQS_LINKED,
                CONSTANT.SEQ.AQS_LINKED | CONSTANT.SEQ.AQS_ASLEEP,
                CONSTANT.SEQ.AQS_LINKED | CONSTANT.SEQ.AQS_MONITORING
            ]
        }
    },
    setup: function setup (done) {
        const store = this;
        store.incoming(EVENT.DO.RESET).handle(() => {
            store.reconfig({
                alert: {
                    visible: false,
                    title: ``,
                    message: ``
                },
                runMode: `foreground-running`,
                seq: CONSTANT.SEQ.STARTUP
            }, {
                suppressMutationEvent: true
            });
            store.outgoing(EVENT.AS.RESET).emit();
        });
        store.incoming(EVENT.DO.MUTATE_SEQUENCE).handle((mutateSeq) => {
            if (store.reduce(mutateSeq)) {
                store.outgoing(EVENT.AS.SEQUENCE_MUTATED).emit(() => store.seq);
            }
        });
        store.incoming(EVENT.DO.MUTATE_RUN_MODE).handle((mutateRunMode) => {
            store.reduce(mutateRunMode);
        });
        store.incoming(EVENT.DO.MUTATE_ALERT).handle((mutateAlert) => {
            store.reduce(mutateAlert);
        });
        done();
    },
    teardown: function teardown (done) {
        done();
    }
});
export default AppStore;
