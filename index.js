/**
 * Copyright (c) 2016-present, Virida, Inc. All rights reserved.
 *
 * Licensed TBA
 *
 *------------------------------------------------------------------------
 *
 * @description - Virida app entry point for ios and android.
 *
 * @author Tuan Le (tuan.t.lei@gmail.com)
 *
 *------------------------------------------------------------------------
 * @flow
 */
'use strict'; //eslint-disable-line

const DEVELOPMENT = false;

/*
 * Currently JavaScriptCore does not provide a `self` reference
 * to the global object, which is utilized by browser libraries (i.e bluebird)
 * to have a reliably reference to the global object which works in browsers
 * and web-workers alike.
 */
if (!global.self) {
    global.self = global;
}

if (DEVELOPMENT) {
    require(`react-devtools-core`).connectToDevTools({
        host: `172.31.98.229`, // `10.0.1.9`,
        port: `8097`
    });
}

/* load and initialize hyperflow */
require(`hyperflow`).init({
    target: `client-native`,
    development: DEVELOPMENT
});

/* load and initialize hypertoxin */
require(`hypertoxin`).init({
    customTheme: require(`./src/common/theme`).default
});

const ViridaApp = require(`./src/app/virida-app`).default;

/* start the app!!! */
ViridaApp.start();
