# [Virida Client Native]
## Virida Air Quality Sensor (AQS) app for Android and iOS - receive particulate (PM2.5) and VOC gasses level in the air from AQS module and also regional AQI data from AirNow.

----
**iOS Development 0.1.0.2 (06/12/2018)**
```
    - Features implemented:
        Added more pollution types (co, no2, so2, and o3) to monitor feed.
    - Changes:
        Switched monitor feed from AirNow to AQICN
        Fixed last updated time display.
        Fixed collapsible view issue with some iPhone.
```
**iOS Release 0.1.0.1 (05/08/2018)**
```
    Released version 0.1.0.1 to app store!!!
    - Features implemented:
        Implemented code push feature.
    - Changes:
        UI cleanups.
```
**iOS Development 0.1.0-beta10 & 0.1.0-beta11 (05/01/2018)**
```
    - Features implemented:
        Added proper periodic refresh events and background heartbeat.
        Added AQICN API. MapView now show AQICN site data.
        Improved MapView performance.
    - Changes:
        UI cleanups. Add donate and social media buttons in MonitorView.
    - Bug fixes:
        Mics performance and stability improvement for MapView.
```
**iOS Development 0.1.0-beta9 (04/10/2018)**
```
    - Features implemented:
    - Changes:
        Improved MonitorView visual and UIX.
        Added tab view for MonitorView forecasts, pollutants, and sensor.
        Improved MapView.
        Removed tab navigation animation.
    - Bug fixes:
```
**iOS Development 0.1.0-beta8 (04/03/2018)**
```
    - Features implemented:
    - Changes:
    - Bug fixes:
        Oops accidentally removed alert event in AppDomain.
        Fixed map constant stop and loading from AirNow.
        Minor styling fixes.
```
**iOS Development 0.1.0-beta7 (04/03/2018)**
```
    - Features implemented:
    - Changes:
        Internal, added background running mode event broadcasting.
        Added more info for AQI scale chart in MonitorView.
        Switched to google map.
        Improved map performance.
        Code cleanups.
    - Bug fixes:
        Turn off map marker clustering for now.
        Fixed notification - finally working!
```
**iOS Development 0.1.0-beta6 (03/26/2018)**
```
    - Features implemented:
    - Changes:
        Styling cleanup.
        Finished Policy.
        Update Setting UIs.
        Finished App intro.
        Added stacked bar graph for aq forecast.
        Added notification feature.
    - Bug fixes:
        Improved map UIx.
        Improved monitor UIx.
        Fixed notification timing.
        Fixed fast pressed navigation issue.
```
**iOS Development 0.1.0-beta5 (03/20/2018)**
```
    - Features implemented:
        Updated to latest react native servion 0.54.2
        Updated to latest hyperflow version 0.2.0
        Updated to latest hypertoxin version 0.1.1
    - Changes:
        Changed setting view stylings
    - Bug fixes:
```
**iOS Development 0.1.0-beta4 (12/19/2017)**
```
    - Features implemented:
    - Changes:
        Added ios launch image asset for iPhone X
    - Bug fixes:
        Fixed timestamp param for AirNow API call.
        Fixed stuck loading modal view in MapView and MonitorView.
        Fixed font scaling for iPhone X and iPhone * Plus
```
**iOS Development 0.1.0-beta3 (12/16/2017)**
```
    - Initial Commit Version!!!
    - Features implemented:
        AirNow regional air quality monitoring feature.
        PM2.5 and VOC air quality data from AQS module via BLE monitoring feature.
        AirNow air quality map feature.
    - Changes:
    - Bug fixes:
```
